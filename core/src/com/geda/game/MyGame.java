package com.geda.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import Controller.Game.AudioManager;
import Controller.GameController;
import GameGDX.GDX;
import GameGDX.Language;
import GameGDX.Loader;
import GameGDX.Scene;
import ScreenGame.LoadingScreen;
import zen.IZen;

public class MyGame extends ApplicationAdapter {
	public static int V_WIDTH = 720;
	public static int V_HEIGHT = 1280;
	public static int PLAYER_WIDTH=80;
	public static int PLAYER_HEIGHT=80;
	public static final float PPM = 100;



	public static final short NOTHING_BIT = 0;
	public static final short PLAYER_BIT = 0x0001;
	public static final short ENEMY_BIT = 0x0002;
	public static final short GROUND_BIT =0x0004;
	public static final short FRIEND_BIT =0x0008;
	/* WARNING Using AssetManager in a static way can cause issues, especially on Android.
	Instead you may want to pass around Assetmanager to those the classes that need it.
	We will use it in the static context to save time for now. */
	public static  MyGame instance;
	public static IZen sdk;
//	PlayScreen playScreen;
//	PlayScreenExtra playScreenExtra;
	boolean stopPlay=false;
	long startTime=0;
	long eslapseTime;
	private SpriteBatch batch;
	private Texture texture;
	private Sprite sprite;

	public LoadingScreen getLoadingScreen() {
		return loadingScreen;
	}

	LoadingScreen loadingScreen;



	public MyGame(IZen zen){
		instance = this;
		sdk = zen;
	}

	@Override
	public void create () {
		V_HEIGHT= 720;
		V_WIDTH=1280;
		loadingScreen=new LoadingScreen(this);
		new AudioManager();
		new GDX();
		new Loader();
		sdk.OnShow();
		new Scene();
		new Language();
		if (!GDX.IsHTML())
		{
			Loader.instance.LoadAssets();
		}

	}

	@Override
	public void resize(int width, int height) {
		loadingScreen.resize(width,height);
		super.resize(width, height);
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		if (Gdx.graphics.getDeltaTime() < (1f / 60f)) {

			float sleepTime = (1f / 60f) - Gdx.graphics.getDeltaTime();
			try {
				Thread.sleep((long) sleepTime);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
		loadingScreen.render(Gdx.graphics.getDeltaTime());
		if (GameController.instance != null) {
			GameController.instance.Update(Gdx.graphics.getDeltaTime());
			GameController.instance.Render(Gdx.graphics.getDeltaTime());
		}
		super.render();
		// Do all your rendering here
		// ...

		// Check the profiler data.
		// You can view in debugger, log it, etc...
	}
	public void setScreen(Screen screen){
		this.setScreen(screen);
	}
	public double getEslapseTime(){
		long end = System.nanoTime();

		long elapsedtime = end - startTime;

		// 1 second = 1_000_000_000 nano seconds
		double seconds= (double) elapsedtime / 1_000_000_000;
		return seconds;
	}
}
