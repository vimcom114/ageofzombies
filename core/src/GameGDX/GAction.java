package GameGDX;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by BaDuy on 5/2/2018.
 */

public class GAction {
    public static SequenceAction ColorToColor(Color cl1, float t1 , Color cl2, float t2, Interpolation interpolation)
    {
        ColorAction color1Action = Actions.color(cl1,t1,interpolation);
        ColorAction color2Action = Actions.color(cl2,t2,interpolation);

        return new SequenceAction(color1Action,color2Action);
    }
    public static void ColorTo(Actor actor, Color color, float duration, Interpolation interpolation, Action done)
    {
        ColorAction action = Actions.color(color, duration, interpolation);
        if (done==null) actor.addAction(action);
        else {
            SequenceAction sequen = new SequenceAction(action, done);
            actor.addAction(sequen);
        }
    }
    public static void MoveTo(Actor actor, float x, float y, int algin, float duration, Interpolation interpolation, Action done)
    {
        MoveToAction action = Actions.moveToAligned(x, y,algin, duration,interpolation);
        if (done==null) actor.addAction(action);
        else {
            SequenceAction sequen = new SequenceAction(action, done);
            actor.addAction(sequen);
        }
    }

    public static void ScaleTo(Actor actor, float scaleX, float scaleY, float duration, Interpolation interpolation, Action done)
    {
        ScaleToAction action = Actions.scaleTo(scaleX,scaleY,duration,interpolation);
        if (done==null) actor.addAction(action);
        else {
            SequenceAction sequen = new SequenceAction(action, done);
            actor.addAction(sequen);
        }
    }
    public static void RotateTo(Actor actor, float rotation, float duration, Interpolation interpolation, Action done)
    {
        RotateToAction action = Actions.rotateTo(rotation,duration,interpolation);
        if (done==null) actor.addAction(action);
        else {
            SequenceAction sequen = new SequenceAction(action, done);
            actor.addAction(sequen);
        }
    }

    public static  void FadeIn(Actor actor, float duration){
        actor.addAction(Actions.fadeIn(duration));

    }
    public static  void FadeOut(Actor actor, float duration){
         actor.addAction(Actions.fadeOut(duration));

    }

    public static void LabelCount(final Label label, final String content , final int start, final int end, final int del , final float duration)
    {
        DelayAction acDelay = Actions.delay(duration, new Action() {
            @Override
            public boolean act(float delta) {
                label.setText(content+String.valueOf(start));
                if (start>=end)
                {
                    label.setText(content+String.valueOf(end));
                    return true;
                }
                LabelCount(label, content, start+del, end,del, duration);
                return true;
            }
        });
        label.addAction(acDelay);
    }
}
