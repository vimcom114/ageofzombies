package GameGDX;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.ArrayList;
import java.util.List;

public class Animation {
    public int getIndexFrame() {
        return indexFrame;
    }

    public void setIndexFrame(int indexFrame) {
        this.indexFrame = indexFrame;
    }

    private int indexFrame = 0;

    public float getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(float timeFrame) {
        this.timeFrame = timeFrame;
    }

    private float timeFrame = 0;
    private float timeDelay = 0;
    private Image image;
    private List<Drawable> listDraw;

    private float timeUpdate = 0;
    private float timeDelayUpdate = 0;

    public Animation(Image image, String name , int start, int end, float timeDelay)
    {
        this.timeDelay = timeDelay;
        this.image = image;
        listDraw = new ArrayList<Drawable>();
        LoadTexute(name, start, end);
    }
    public Animation(Image image, String name , float timeDelay)
    {
        this.timeDelay = timeDelay;
        this.image = image;
        listDraw = new ArrayList<Drawable>();
    }
    public Animation(Image image, String name , int start, int end)
    {
        this.image = image;
        listDraw = new ArrayList<Drawable>();
        LoadTexute(name, start, end);
    }
    public void addTextureRegion(TextureRegion textureRegion){
        TextureRegionDrawable drawable = new TextureRegionDrawable(textureRegion);
        listDraw.add(drawable);
    }
    private void LoadTexute(String name , int start,int end)
    {
        for(int i=start;i<=end;i++)
        {
            String st = name+i;
            TextureRegionDrawable drawable = new TextureRegionDrawable(new TextureRegion(Loader.GetTexture(st)));
            listDraw.add(drawable);
        }
    }

    public void Start(float timeFrame)
    {
        indexFrame = 0;
        timeUpdate = 0;
        timeDelayUpdate = 0;
        image.setDrawable(listDraw.get(indexFrame));
        this.timeFrame = timeFrame;
    }
    private void NextFrame()
    {
        indexFrame++;
        if (indexFrame>=listDraw.size())
        {
            indexFrame=0;
            timeDelayUpdate = 0;
        }
        image.setDrawable(listDraw.get(indexFrame));
        timeUpdate=0;
    }
    public void Update()
    {
        if (timeDelay>0) timeDelayUpdate+= GDX.DeltaTime();
        if (timeDelayUpdate<timeDelay) return;

        if (listDraw.size()<=1) return;
        timeUpdate+= GDX.DeltaTime();
        if (timeUpdate>=timeFrame) NextFrame();
    }
}
