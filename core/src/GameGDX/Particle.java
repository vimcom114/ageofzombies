package GameGDX;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.geda.game.MyGame;


public class Particle extends Group {
    private ParticleEffectPool.PooledEffect pe;
    private ParticleEffectPool pool;
    private boolean isLoop = false;

    public boolean isRun() {
        return isRun;
    }

    public void setRun(boolean run) {
        isRun = run;
    }

    private boolean isRun = false;

    public boolean isStopWhenComp() {
        return stopWhenComp;
    }

    public void setStopWhenComp(boolean stopWhenComp) {
        this.stopWhenComp = stopWhenComp;
    }

    private boolean stopWhenComp=false;

    private float x, y,z=90;
    boolean isStop=false;
   // MyWorld myWorld;
    String Name;

    public Particle(String name, float x, float y, float scale, boolean isLoop, boolean isRun, Group parent, int init, int max) {
        try {
            Name=name;
            this.x = x;
            this.y = y;
            this.isLoop = isLoop;
            this.isRun = isRun;
            if (parent != null) parent.addActor(this);
            pool = GParPool.getPool(name, init, max);
            pe = pool.obtain();
            pe.scaleEffect(scale);
            pe.setPosition(x, y);
        } catch (Exception ex) {
//            MyGame.sdk.log("Error Particle " + ex.getMessage());
//            GDX.log("Error Particle " + ex.getMessage());
        }
    }

    public Particle(String name, float x, float y, float scale, boolean isLoop, boolean isRun, Group parent) {
        this(name, x, y, scale, isLoop, isRun, parent, 2, 5);
    }

    public Particle(String name, Group parent) {

        try {
            Name=name;
            this.x = x;
            this.y = y;
            this.isLoop = isLoop;
            this.isRun = isRun;
            pool = GParPool.getPool(name, 2, 7);
            pe = pool.obtain();
            pe.reset(true);
            pe.scaleEffect(1);
            pe.setPosition(x, y);
            this.setRotation(z);
            if (parent != null) parent.addActor(this);
        } catch (Exception ex) {
            GDX.Show("Error Particle " + ex.getMessage()+"  "+name);
        }
    }

    public void Start(boolean isRun) {

        this.isRun = isRun;
    }
    public void Loop(boolean isLoop) {
        this.isLoop = isLoop;
    }

    public void setX(float x) {
        pe.setPosition(x, y);
    }
    public void setY(float y) {
        pe.setPosition(x, y);
    }
    public void setPOS(float x, float y,float z) {
        try {
            for (ParticleEmitter pa : pe.getEmitters())
                for (Sprite sp : pa.getSprites())
                    sp.setRotation(z);
            pe.setPosition(x, y);

        }catch (Exception e){

            this.remove();
        }
    }
    public void setSizeWorld() {
        try {
            for (ParticleEmitter pa : pe.getEmitters())
                pa.scaleSize(1/ MyGame.PPM);
        } catch (Exception e) {
            this.remove();
        }
    }
    public void setSizeSprite(float w,float h) {
        try {
            for (ParticleEmitter pa : pe.getEmitters())
                for (Sprite sp : pa.getSprites())
                    sp.setSize(w, h);
        } catch (Exception e) {
            this.remove();
        }
    }
    public void setScaleE(float scale) {
        if(pe!=null) {
            pe.reset(true);
            pe.scaleEffect(scale / MyGame.PPM);
        }
    }

    @Override
    public void act(float delta) {
        if(isStop||pe==null) return;
        if (isRun==false) return;
        if (pe == null || isRun==false) return;
        if (!isLoop && pe.isComplete()) {
            pool.free(pe);
            this.remove();
            return;
        }
        if (isLoop && pe.isComplete()&&isRun){
            if(isStopWhenComp()==false)pe.start();
            else Start(false);
        }

        pe.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(isStop) return;
        if (pe == null || !isRun) return;
        pe.draw(batch);
        super.draw(batch, parentAlpha);
    }
    public void Draw(Batch batch){
        pe.draw(batch);
    }
    public void setStop(boolean bool){
        isStop=bool;
    }

}