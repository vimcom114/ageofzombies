package GameGDX.encrypt;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import GameGDX.GDX;

public class EncryptUtil {
    private static boolean encrypt;
    private final static String zenText = "zen";
    private final static int keyCrypt = 5;

    public static void ProtectAssets(boolean encrypt, AssetManager assets, String... folder) {
        assets.setLoader(Texture.class, new TextureByteLoader(new InternalFileHandleResolver()));
        assets.setLoader(TextureAtlas.class, new AtlasByteLoader(new InternalFileHandleResolver()));
        assets.setLoader(BitmapFont.class, new FontByteLoader(new InternalFileHandleResolver()));
        if (!GDX.isDesktop()) return;
        EncryptUtil.encrypt = encrypt;
        for (String path : folder) {
            FileHandle file = Gdx.files.local(path);
            if (!file.isDirectory()) ProceedFile(file);
            else for (FileHandle file_child : file.list()) ProceedFolder(file_child);
        }
    }

    private final static void ProceedFolder(FileHandle file) {
        if (!file.isDirectory()) ProceedFile(file);
        else for (FileHandle file_child : file.list()) ProceedFolder(file_child);
    }

    private final static void ProceedFile(FileHandle file) {
        if (!(file.extension().equals("png") || file.extension().equals("jpg") || file.extension().equals("txt"))) return;
        if (encrypt) encrypt(file);
        else decrypt(file);
    }

    public static final byte[] getProceededByte(byte[] bytes) {
        return !isEncyptedFile(bytes) ? bytes : getDecryptBytes(bytes);
    }

    public final static boolean isEncyptedFile(byte[] bytes) {
        String s = "";
        for (int i = 0; i < zenText.length(); i++) s += (char) bytes[bytes.length - zenText.length() + i];
        return s.equals(zenText);
    }

    private final static void encrypt(FileHandle fileHandle) {
        byte[] bytes = fileHandle.readBytes();
        if (isEncyptedFile(bytes)) return;
        fileHandle.writeBytes(getEncryptBytes(bytes), false);
        fileHandle.writeBytes(zenText.getBytes(), true);
    }
    private final static void decrypt(FileHandle fileHandle) {
        byte[] bytes = fileHandle.readBytes();
        if (!isEncyptedFile(bytes)) return;
        fileHandle.writeBytes(getDecryptBytes(bytes), false);
    }

    private static byte encryptByte(byte normal) {
        return (byte) (normal + keyCrypt);
    }
    private static byte decryptByte(byte normal) {
        return (byte) (normal - keyCrypt);
    }

    private final static byte[] getEncryptBytes(byte[] bytes) {
        for (int i = 0; i < bytes.length; i++) bytes[i] = encryptByte(bytes[i]);
        return bytes;
    }
    public final static byte[] getDecryptBytes(byte[] bytes) {
        byte[] newBytes = new byte[bytes.length - zenText.length()];
        for (int i = 0; i < newBytes.length; i++) newBytes[i] = decryptByte(bytes[i]);
        return newBytes;
    }
}