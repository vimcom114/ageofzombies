package GameGDX.encrypt;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.glutils.FileTextureData;
import com.badlogic.gdx.utils.Array;

public class TextureByteLoader extends AsynchronousAssetLoader<Texture, TextureByteLoader.TextureParameter> {

    static public class TextureLoaderInfo {
        String filename;
        TextureData data;
        Texture texture;
        byte[] bytes;
    }

    TextureLoaderInfo info = new TextureLoaderInfo();

    public TextureByteLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, TextureParameter parameter) {
        info.filename = fileName;

        byte[] bytes = Gdx.files.internal(fileName).readBytes();
        bytes = EncryptUtil.getProceededByte(bytes);

        if (parameter == null || parameter.textureData == null) {
            Pixmap.Format format = null;
            boolean genMipMaps = false;
            info.texture = null;

            if (parameter != null) {
                format = parameter.format;
                genMipMaps = parameter.genMipMaps;
                info.texture = parameter.texture;
            }

            info.data = new FileTextureData(file, new Pixmap(bytes, 0, bytes.length), format, true);
        } else {
            info.data = parameter.textureData;
            info.texture = parameter.texture;
        }
        info.bytes = bytes;
        if (!info.data.isPrepared()) info.data.prepare();
    }

    @Override
    public Texture loadSync(AssetManager manager, String fileName, FileHandle file, TextureParameter parameter) {
        if (info == null) return null;
        Texture texture = info.texture;
        if (texture != null) {
            texture.load(info.data);
        } else {
            texture = new Texture(new Pixmap(info.bytes, 0, info.bytes.length));
        }
        if (parameter != null) {
            texture.setFilter(parameter.minFilter, parameter.magFilter);
            texture.setWrap(parameter.wrapU, parameter.wrapV);
        }
        return texture;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, TextureParameter parameter) {
        return null;
    }

    static public class TextureParameter extends AssetLoaderParameters<Texture> {
        /**
         * the format of the final Texture. Uses the source images format if null
         **/
        public Pixmap.Format format = null;
        /**
         * whether to generate mipmaps
         **/
        public boolean genMipMaps = false;
        /**
         * The texture to put the {@link TextureData} in, optional.
         **/
        public Texture texture = null;
        /**
         * TextureData for textures created on the fly, optional. When set, all format and genMipMaps are ignored
         */
        public TextureData textureData = null;
        public Texture.TextureFilter minFilter = Texture.TextureFilter.Nearest;
        public Texture.TextureFilter magFilter = Texture.TextureFilter.Nearest;
        public Texture.TextureWrap wrapU = Texture.TextureWrap.ClampToEdge;
        public Texture.TextureWrap wrapV = Texture.TextureWrap.ClampToEdge;
    }
}