package GameGDX;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import Controller.Game.AudioManager;



public class ClickEvent extends ClickListener {

    protected  boolean canClick = false;
    public ClickEvent()
    {
        super();
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        AudioManager.instance.PlayAudio("btClick");
        if (pointer!=0) return false;
        return super.touchDown(event, x, y, pointer, button);
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (pointer!=0) return;
        super.touchUp(event, x, y, pointer, button);
    }

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        if (pointer!=0) return;
        canClick = true;
        super.enter(event, x, y, pointer, fromActor);
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        if (pointer!=0) return;
        canClick = false;
        super.exit(event, x, y, pointer, toActor);
    }

    @Override
    public void clicked(InputEvent event, float x, float y) {
        //Music.Play("pop");
    }
    public static void ScaleSmooth(final Actor im, final  float Scale, final float ScaleEnd, final float time){

        GAction.ScaleTo(im, Scale-0.1f, Scale-0.1f, 0.05f, Interpolation.linear, new Action() {
            @Override
            public boolean act(float delta) {
                actor.clearActions();
                GAction.ScaleTo(im, Scale+0.1f, Scale+0.1f, 0.05f, Interpolation.linear, new Action() {
                    @Override
                    public boolean act(float delta) {
                        actor.clearActions();
                        GAction.ScaleTo(im, ScaleEnd, ScaleEnd, time, Interpolation.linear,null);
                        return false;
                    }
                });
                return false;
            }
        });

    }
}
