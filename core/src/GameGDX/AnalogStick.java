package GameGDX;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import Controller.GameController;

public class AnalogStick extends JoyTick {

    private static MyTouchpadStyle touchpadStyle;
    private static Skin touchpadSkin;
    private static Drawable touchBackground;
    private static Drawable touchKnob;
    private float myWidth;
    private boolean Touch=false;
    private boolean isMoveRight;
    private boolean isMoveLeft=false;
    private boolean isMoveUp;
    private boolean isMoveDown;
    private float prevPercenX=0,prevPercenY=0;

    public AnalogStick(float x, float y,float pW,float pH,String txttouchBG,String txttouchknob) {
        super(0, getTouchpadStyle(txttouchBG,txttouchknob));
        setBounds(0, 0, pW, pH);
        setPosition(x,y);
        this.setColor(1,1,1,0.5f);

    }
    private static MyTouchpadStyle getTouchpadStyle(String txttouchBG,String txtKnob) {

//        touchpadSkin = new Skin();
//        touchpadSkin.add("touchBackground", Loader.GetTexture("touchBackground"));
//
//        touchpadSkin.add("touchKnob", Loader.GetTexture("touchKnob"));

        touchpadStyle = new MyTouchpadStyle();

        touchBackground = new TextureRegionDrawable(Loader.GetTexture(txttouchBG));
        touchKnob =  new TextureRegionDrawable(Loader.GetTexture(txtKnob));
        touchpadStyle.background = touchBackground;
        touchpadStyle.knob = touchKnob;
        return touchpadStyle;
    }
    public void changeTxtTouchBG(String txttouchBG){
        touchBackground = new TextureRegionDrawable(Loader.GetTexture(txttouchBG));
        touchpadStyle.background = touchBackground;
    }
    public void changeTxtTouchKnob(String txtKnob){
        touchKnob =  new TextureRegionDrawable(Loader.GetTexture(txtKnob));
        touchpadStyle.knob = touchKnob;
    }
    public boolean isMoveLeft() {
        return isMoveLeft;
    }

    public void setMoveLeft(boolean moveLeft) {
        isMoveLeft = moveLeft;
    }

    public boolean isMoveUp() {
        return isMoveUp;
    }

    public void setMoveUp(boolean moveUp) {
        isMoveUp = moveUp;
    }

    public boolean isMoveDown() {
        return isMoveDown;
    }

    public void setMoveDown(boolean moveDown) {
        isMoveDown = moveDown;
    }

    public float getPrevPercenX() {
        return prevPercenX;
    }

    public void setPrevPercenX(float prevPercenX) {
        this.prevPercenX = prevPercenX;
    }

    public float getPrevPercenY() {
        return prevPercenY;
    }

    public void setPrevPercenY(float prevPercenY) {
        this.prevPercenY = prevPercenY;
    }
    public boolean isMoveRight() {
        return isMoveRight;
    }

    public void setMoveRight(boolean moveRight) {
        isMoveRight = moveRight;
    }
    public boolean isTouch() {
        return Touch;
    }

    public void setTouch(boolean touch) {
        Touch = touch;
    }
//    @Override
//    public void act(float delta) {
//        super.act(delta);
//        if(isTouched()){
//            this.setColor(1,1,1,1f);
//            Touch=true;
//            checkDirMove(getKnobPercentX(),getKnobPercentY());
//            if(checkTrueDir){
//                if(timetoCheckTrueDir>0){
//                    timetoCheckTrueDir-=delta;
//                }else{
//                    if(isMoveLeft&&getKnobPercentX()>0.0f){
//                        isMoveLeft=false;
//                        isMoveRight=true;
//                    }
//
//                    if(isMoveRight&&getKnobPercentX()<0.0f){
//                        isMoveRight=false;
//                        isMoveLeft=true;
//                    }
//
//                    if(isMoveDown&&getKnobPercentY()>0.0f){
//                        isMoveDown=false;
//                        isMoveUp=true;
//                    }
//
//                    if(isMoveUp&&getKnobPercentY()<0.0f){
//                        isMoveUp=false;
//                        isMoveDown=true;
//                    }
//                }
//            }
//            GameController.instance.getMyWorld().getPlayer().setRotBody(getAngleDegree());
//        }else{
//            if(Touch) {
//                Touch=false;
//                this.setColor(1,1,1,0.5f);
//                prevPercenX=0.0f;
//                prevPercenY=0.0f;
//                GameController.instance.getMyWorld().HandleInPut("RIGHT", false);
//                GameController.instance.getMyWorld().HandleInPut("LEFT", false);
//                GameController.instance.getMyWorld().HandleInPut("UP", false);
//                GameController.instance.getMyWorld().HandleInPut("DOWN", false);
//            }
//        }
//
//    }



}
