package GameGDX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import GameGDX.GSpine.MySpine;
import GameGDX.GSpine.spine.utils.TwoColorPolygonBatch;


/**
 * Created by BaDuy on 5/2/2018.
 */

public class Scene {
    public static Scene instance;

    public static int width = Gdx.graphics.getWidth();
    public static int height = Gdx.graphics.getHeight();

    private Group background;
    private Group game;
    private Group ui;

    private Stage stage;

    public Scene()
    {
        instance = this;

        Init();
    }
    private void Init()
    {
        background = new Group();
        game = new Group();
        game.addActor(new Group());
        game.addActor(new Group());
        ui = new Group();
        MySpine.setRenderer();
        stage = new Stage(new StretchViewport(width,height), new TwoColorPolygonBatch());
        stage.addActor(background);
        stage.addActor(game);
        stage.addActor(ui);
        
        Gdx.input.setInputProcessor(stage);
        Gdx.input.setCatchBackKey(true);
    }
    public void AddInput(InputListener inputListener)
    {
        stage.addListener(inputListener);
    }

    public void AddToGame(int layer, Actor actor)
    {
        Group gr = (Group)(game.getChildren().get(layer));
        gr.addActor(actor);
    }
    public void AddToBackground(Actor actor)
    {
        background.addActor(actor);
    }
    public void AddToUI(Actor actor)
    {
        ui.removeActor(actor);
        ui.addActor(actor);
    }

    public void Act()
    {
        stage.act(GDX.DeltaTime());
    }
    public void Render()
    {
        stage.draw();
    }
    public void Resize(int width,int height)
    {
        stage.getViewport().update(width,height);
    }
    public Camera GetCamera()
    {
        return stage.getCamera();
    }
    public static void ScaleSmooth(final Actor im, final float MaxScale, final float time){

        GAction.ScaleTo(im, MaxScale, MaxScale, 0.1f, Interpolation.linear, new Action() {
            @Override
            public boolean act(float delta) {
                actor.clearActions();
                GAction.ScaleTo(im, MaxScale-0.3f, MaxScale-0.3f, time, Interpolation.linear, new Action() {
                    @Override
                    public boolean act(float delta) {
                        actor.clearActions();
                        GAction.ScaleTo(im, MaxScale-0.15f, MaxScale-0.15f, time, Interpolation.linear, new Action() {
                            @Override
                            public boolean act(float delta) {
                                actor.clearActions();
                                GAction.ScaleTo(im, 1f, 1f, time, Interpolation.linear, new Action() {
                                    @Override
                                    public boolean act(float delta) {
                                        actor.clearActions();
                                        return false;
                                    }
                                });
                                return false;
                            }
                        });
                        return false;
                    }
                });
                return false;
            }
        });

    }
}
