package GameGDX;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

/**
 * Created by BaDuy on 5/2/2018.
 */

public class UI {
    public static TextField newTextField(String ct, Color cl, float x, float y, int ali, int maxLenght, Group parent) {
        Drawable cursor = new TextureRegionDrawable(Loader.GetTexture("cursor"));
        Drawable tfSelect = new TextureRegionDrawable(Loader.GetTexture("tfSelect"));
        Drawable tfbg = new TextureRegionDrawable(Loader.GetTexture("SelectB_BG"));
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle(Loader.instance.font, cl, cursor, tfSelect, tfbg);
        final TextField textField = new TextField(ct, textFieldStyle);
        textField.setSize(Loader.GetTexture("SelectB_BG").getRegionWidth(), Loader.GetTexture("SelectB_BG").getRegionHeight());
        textField.setPosition(x, y, ali);
        textField.setAlignment(Align.center);
        textFieldStyle.font.getData().setScale(0.5f);
        textField.setMaxLength(maxLenght);
        if (parent != null) parent.addActor(textField);
        return textField;
    }
    public static ScrollPane newScroll(Actor ac, float x, float y, int ali, float w, float h, Group parent) {
        ScrollPane scroll = new ScrollPane(ac);
        scroll.setSize(w, h);
        scroll.setPosition(x, y, ali);
        if (parent != null) parent.addActor(scroll);
        return scroll;
    }
    public static SelectBox newSelectBox(String[] listname){
        // Primative Data

        // Font
        // List Style
        List.ListStyle lstStyle = new List.ListStyle();
        // lstStyle.background = new NinePatchDrawable(new NinePatch(new Texture(Gdx.files.internal("ButtonGreyUp.png")), 10, 10, 10, 10));
        lstStyle.selection = new NinePatchDrawable(new NinePatch(Loader.GetTexture("SelectB")));
        lstStyle.background= new NinePatchDrawable(new NinePatch(Loader.GetTexture("SelectB_BG")));

        lstStyle.font = Loader.instance.font_sport;
        lstStyle.fontColorSelected = Color.YELLOW;
        lstStyle.fontColorUnselected = Color.BLACK;


        // Select Box Style
        SelectBox.SelectBoxStyle sbStyle = new SelectBox.SelectBoxStyle();
        sbStyle.listStyle = lstStyle;
        sbStyle.scrollStyle = new ScrollPane.ScrollPaneStyle();
        sbStyle.background = new NinePatchDrawable(new NinePatch(Loader.GetTexture("popup_language")));
        sbStyle.backgroundOpen = new NinePatchDrawable(new NinePatch(Loader.GetTexture("popup_language")));

        sbStyle.font = Loader.instance.font_sport;
        sbStyle.font.getData().setScale(0.5f);
        sbStyle.font.getData().padLeft=-10;
        // Select Box
        SelectBox<String> monthsSB = new SelectBox<String>(sbStyle);
        monthsSB.setItems(listname);

        return monthsSB;
    }
    private static void EffectPressedButton(final Actor actor)
    {
        actor.addListener(new ClickListener(){
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (pointer!=0) return;
                actor.setColor(Color.LIGHT_GRAY);
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (pointer!=0) return;
                actor.setColor(Color.WHITE);
                super.exit(event, x, y, pointer, toActor);
            }

        });
    }

    public static TextureRegion NewTexture(int width, int height)
    {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fillRectangle(0, 0, width, height);
        Texture texture = new Texture(pixmap);
        pixmap.dispose();

        return new TextureRegion(texture);
    }
    //image
    public static Image NewImage(TextureRegion texture, float x, float y, int align , float width, float height, Group parent)
    {
        Image img = new Image(texture);
        img.setSize(width, height);
        img.setPosition(x, y,align);
        parent.addActor(img);

        return img;
    }

    public static Image NewImage(TextureRegion texture, float x, float y, int align, Group parent)
    {
        return NewImage(texture,x,y, align ,texture.getRegionWidth(),texture.getRegionHeight(),parent);
    }
    public static Image NewImage(Color color, float x, float y, int align , int width, int height, Group parent)
    {

        Image image = new Image(NewTexture(width,height));
        image.setColor(color);
        image.setPosition(x,y,align);

        parent.addActor(image);
        return image;
    }
    public static Image NewImage(TextureRegion texture, Group parent)
    {
        return NewImage(texture,parent.getWidth()/2,parent.getHeight()/2, Align.center,texture.getRegionWidth(),texture.getRegionHeight(),parent);
    }
    public static Image NewImage(TextureRegion texture , float width, float height, Group parent)
    {
        return NewImage(texture,parent.getWidth()/2,parent.getHeight()/2, Align.center,width,height,parent);
    }

    //NewLabel
    private static float fontScale = 1f;

    public static void SetText(Label label, String content, float maxScale)
    {
        for(int i=10;i>0;i--)
        {
            float scale = 0.1f*i;
            //label.setFontScale(maxScale*scale*fontScale);
            label.setText(content);
            if(label.getPrefHeight()<label.getHeight()) return;
        }
    }
    public static void SetTextInLine(Label label, String content, float maxScale)
    {
        label.setWrap(false);
        for(int i=10;i>0;i--)
        {
            float scale = 0.1f*i;
            label.setFontScale(maxScale*scale*fontScale);
            label.setText(content);
            if(label.getPrefWidth()<label.getWidth()) return;
        }
    }
    public static void fitLabel(Label lb, String content, boolean wrap) {
        lb.setText(content);
        lb.setWrap(wrap);
        if (wrap) {
            if (lb.getPrefHeight() <= lb.getHeight()) return;
            for (int i = 10; i > 0; i--) {
                lb.setFontScale(0.1f * i * lb.getFontScaleX());
                if (lb.getPrefHeight() <= lb.getHeight()) return;
            }
        } else {
            if (lb.getPrefWidth() <= lb.getWidth()) return;
            lb.setFontScale(lb.getWidth() / lb.getPrefWidth());
        }
    }
    public static Label NewLabel(String content, int alignment, Color color , float scale, float x, float y, int align, float width, float height , Group parent)
    {

        Label.LabelStyle labelStyle = new Label.LabelStyle(Loader.instance.font, Color.WHITE);
        Label label = new Label(content, labelStyle);
        label.setSize(width, height);
        label.setPosition(x,y,align);
        label.setAlignment(alignment);
        label.setTouchable(Touchable.disabled);
        label.setWrap(true);
        label.setColor(color);
        label.setFontScale(scale*fontScale);
        SetText(label,content,scale);

        parent.addActor(label);
        return label;
    }
    public static Label NewLabel(final String Content, boolean useRunnable, int alignment, Color color , float scale, float x, float y, int align, float width, float height , Group parent)
    {
        String content= Language.GetLang(Content);
        Label.LabelStyle labelStyle = new Label.LabelStyle(Loader.instance.font, Color.WHITE);
        final Label label = new Label(content, labelStyle);
        label.setSize(width, height);
        label.setPosition(x,y,align);
        label.setAlignment(alignment);
        label.setTouchable(Touchable.disabled);
        label.setWrap(true);
        label.setColor(color);
        label.setFontScale(scale*fontScale);
        parent.addActor(label);
        SetText(label,content,scale);

        if(useRunnable) {
            Language.AddLang(Content, new Runnable() {
                @Override
                public void run() {
                    label.setText("" + Language.GetLang(Content));
                }
            });
        }
        return label;
    }
    public static Label NewLabel(String content, Color color , float scale, float x, float y, int align, float width, float height , Group parent)
    {
        return NewLabel(content, Align.center, color, scale, x, y, align,width,height, parent);
    }
    public static Label NewLabel(String content, boolean useRunnable, Color color , float scale, float x, float y, int align, float width, float height , Group parent)
    {
        return NewLabel(content,useRunnable, Align.center, color, scale, x, y, align,width,height, parent);
    }
    public static Label NewLabel(final String Content, boolean useRunnable, Color color , float scale, float x, float y, int align, Group parent)
    {
        String content= Language.GetLang(Content);
        Label.LabelStyle labelStyle = new Label.LabelStyle(Loader.instance.font, color);
        final Label label = new Label(content, labelStyle);
        label.setFontScale(scale*fontScale);
        label.setPosition(x,y,align);
        label.setAlignment(align);
        label.setTouchable(Touchable.disabled);
        parent.addActor(label);
        SetText(label,content,scale);
        if(useRunnable) {
            Language.AddLang(Content, new Runnable() {
                @Override
                public void run() {
                    label.setText("" + Language.GetLang(Content));
                }
            });
        }
        return label;
    }
    public static Label NewLabel(final String content, Color color , float scale, float x, float y, int align, Group parent)
    {
        Label.LabelStyle labelStyle = new Label.LabelStyle(Loader.instance.font, color);
        final Label label = new Label(content, labelStyle);
        label.setFontScale(scale*fontScale);
        label.setPosition(x,y,align);
        label.setAlignment(align);
        label.setTouchable(Touchable.disabled);
        parent.addActor(label);
        SetText(label,content,scale);
        return label;
    }
    //button
    public static Image NewButton(TextureRegion texture, float x, float y, int align , float width, float height, Group parent)
    {
        Image img = NewImage(texture, x, y, align , width, height,parent);
        EffectPressedButton(img);
        return img;
    }
    public static Image NewButton(TextureRegion texture, float x, float y, int align , Group parent)
    {
        Image img = NewImage(texture, x, y, align , texture.getRegionWidth(), texture.getRegionHeight(),parent);
        EffectPressedButton(img);
        return img;
    }
    //buttonText
    public static Group NewTextButton(String content, float scale, Color cl, TextureRegion texture , float x, float y, int align , float width, float height, Group parent)
    {
        Group group = new Group();
        group.setSize(width, height);
        group.setPosition(x,y,align);

        Image button = NewImage(texture, 0, 0, Align.bottomLeft , width, height,group);
        NewLabel(content, cl,scale,0,0, Align.bottomLeft,width-10,height-10,group);
        EffectPressedButton(button);

        parent.addActor(group);
        return group;
    }
    public static Group NewTextButton(String content, float scale, Color cl, float x, float y, int align , float width, float height, Group parent)
    {
        return NewTextButton(content, scale, cl, Loader.instance.GetTexture("button"), x, y, align, width, height, parent);
    }
    public static Group NewTextButton(String content, float scale, Color cl, float x, float y, int align, Group parent)
    {
        TextureRegion texture = Loader.instance.GetTexture("button");
        return NewTextButton(content, scale, cl, Loader.instance.GetTexture("button"), x, y, align, texture.getRegionWidth(), texture.getRegionHeight(), parent);
    }
    public static Group NewTextButton(String content, float scale, Color cl, TextureRegion texture , float x, float y, int align , Group parent)
    {
        return NewTextButton(content, scale, cl, texture, x, y, align,texture.getRegionWidth(),texture.getRegionHeight(), parent);
    }
    public static Group NewTextButton(String content, int alignment, float scale, Color color, float xLabel, float yLabel, int alignLb , float widthLabel, float heightLabel , TextureRegion texture , float x, float y, int align, float width, float height , Group parent)
    {
        Group group = new Group();
        group.setSize(width, height);
        group.setPosition(x,y,align);

        Image button = NewImage(texture, 0, 0, Align.bottomLeft , width, height,group);
        EffectPressedButton(button);
        NewLabel(content,alignment, color,scale,xLabel,yLabel,alignLb,widthLabel,heightLabel,group);

        parent.addActor(group);
        return group;
    }



}
