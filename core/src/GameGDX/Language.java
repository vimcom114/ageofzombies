package GameGDX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class Language {
    public static Language instance;

    public static String[] values = {"en", "vi"};
    public static String locale = "locale";

    private static ArrayList<String> list = new ArrayList<>();
    public static String s_lang;

    private static HashMap<String, String> mapInfo = new HashMap<>();
    private static HashMap<String, Runnable> cbChange = new HashMap<>();

    public Language() {
        instance = this;
        Init();
        s_lang = Locale.getDefault().getLanguage();
    }

    private void Init() {
        try {
            String path = "language.txt";
            FileHandle file = Gdx.files.internal(path);
            JsonValue json = new JsonReader().parse(file.reader("UTF-8"));
            String[] list = json.getString("list").split("-");
            for (String l : list) instance.list.add(l);
            JsonValue data = json.get("data");
            for (int i = 0; i < data.size; i++) {
                JsonValue js = data.get(i);
                String key = js.getString("key");
                for (String s : list) {
                    String ct = js.getString(s);
                    mapInfo.put(key + "-" + s, ct);
                    js.addChild(s, new JsonValue(ct));
                }
            }
        } catch (Exception e) {
            GDX.Show("EX :"+e);
        }
    }

    public static void SetLang(String nlang) {
        if (!list.contains(nlang)) s_lang = list.get(0);
        else s_lang = nlang;
        for (Runnable run : cbChange.values()) {
            if (run != null) {
                run.run();
            }
        }
    }

    public static String GetLang(String key) {
        return mapInfo.get(key + "-" + s_lang);
    }

    public static void AddLang(String key, Runnable run) {
        cbChange.put(key, run);
    }
}