package GameGDX.AssetLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.List;

import GameGDX.GDX;

public class DataGame {
    private String path = "data.txt";

    public List<NodeAsset> assetsPre;
    public List<NodeAsset> assets;
    public List<NodeAsset> assetsLate;

    public DataGame() {
        assets = new ArrayList<NodeAsset>();
        assetsPre = new ArrayList<NodeAsset>();
        assetsLate = new ArrayList<NodeAsset>();
        CreateAssetPreload();
        //CreateGameAsset();
        LoadGameAsset();
    }
    private void CreateAssetPreload()
    {
    }
    private void CreateGameAsset()
    {
        LoadFile();
    }
    private void LoadGameAsset()
    {
        String data = Gdx.files.internal(path).readString();
        ReadData(data);
    }

    //Read data
    private void ReadData(String data)
    {

        String[] arr = data.split("#2");
        for (String s : arr)
        {
            String[] l = s.split("#1",2);
            if (l[0].equals("assetsPre") && l[1].length()>0) AddToList(l[1],assetsPre);
            if (l[0].equals("assets") && l[1].length()>0) AddToList(l[1],assets);
            if (l[0].equals("assetsLate") && l[1].length()>0) AddToList(l[1],assetsLate);
        }
    }
    private void AddToList(String data,List<NodeAsset> list)
    {
        String[] st = data.split("#0");

        for (String s : st)
            list.add(new NodeAsset(s));

    }

    //load file and save
    private void LoadFile()
    {
        LoadFile(assets,NodeAsset.Kind.Spine, Gdx.files.internal("Spine/"),false);
        LoadFile(assets,NodeAsset.Kind.Texture, Gdx.files.internal("DataTexture/"),true);
        LoadFileParticle(assets,NodeAsset.Kind.Particle, Gdx.files.internal("Particle/"));
        LoadFile(assets,NodeAsset.Kind.Texture, Gdx.files.internal("Texture/"),false);
        LoadFile(assets,NodeAsset.Kind.Sound, Gdx.files.internal("Sounds/"),false);
        LoadFile(assets,NodeAsset.Kind.Music, Gdx.files.internal("Musics/"),false);

        //save data
        Gdx.files.local(path).writeString(DataToString(),false);
    }

    private void LoadFile(List<NodeAsset> list, NodeAsset.Kind kind, FileHandle fh, boolean atlas)
    {
        if (fh.isDirectory()==false)
        {
           // if (fh.extension().equals("DS_Store")||fh.extension().equals("meta")) return;
            list.add(new NodeAsset(kind,fh.nameWithoutExtension(),fh.path(),fh.length(),atlas));
        }
        else
        {
            FileHandle[] listFile = fh.list();
            for(FileHandle child:listFile) LoadFile(list,kind,child,atlas);
        }
    }
    private void LoadFileParticle(List<NodeAsset> list, NodeAsset.Kind kind, FileHandle fh)
    {
        if (fh.isDirectory()==false)
        {
            // if (fh.extension().equals("DS_Store")||fh.extension().equals("meta")) return;
            list.add(new NodeAsset(kind,fh.nameWithoutExtension(),fh.path(),fh.length(),false));
        }
        else
        {
            FileHandle[] listFile = fh.list();
            for(FileHandle child:listFile){
                if( child.toString().toLowerCase().endsWith(".png")==false&&child.toString().toLowerCase().endsWith(".jpg")==false){
                    LoadFile(list,kind,child,false);
                }
            }
        }
    }
    //write
    private String AssetsToString(String name,List<NodeAsset> list)
    {
        String s ="";
        for(int i=0;i<list.size()-1;i++)
            s+=list.get(i).ToString()+"#0";
        if (list.size()>0)
        s = s+list.get(list.size()-1).ToString();
        return name+"#1"+s;
    }
    private String DataToString()
    {
        return AssetsToString("assetsPre",assetsPre)+"#2"+AssetsToString("assets",assets)+"#2"+AssetsToString("assetsLate",assetsLate);
    }
}
