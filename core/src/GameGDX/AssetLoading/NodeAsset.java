package GameGDX.AssetLoading;

import Controller.Game.AudioManager;

public class NodeAsset {
    public enum Kind {
        Particle,
        Spine,
        Texture,
        Sound,
        Music
    }

    public Kind kind;
    public String name;
    public String url;
    public long size;
    public boolean atlas;
    public NodeAsset(Kind kind,String name, String url,long size, boolean atlas)
    {
        this.kind = kind;
        this.name = name;
        this.url = url;
        this.size = size;
        this.atlas = atlas;
    }
    public NodeAsset(String data)
    {
        String[] kq = data.split(":");
        kind = Kind.valueOf(kq[0]);
        name = kq[1];
        if(kind.toString().equals("Sound")){
            AudioManager.instance.addNameSound(name);
        }else if(kind.toString().equals("Music")){
            AudioManager.instance.addNameMusic(name);
        }
        url = kq[2];
        size = Long.parseLong(kq[3]);
        atlas = Boolean.parseBoolean(kq[4]);
    }
    public String ToString()
    {
        return kind+":"+name+":"+url+":"+size+":"+atlas;
    }
}
