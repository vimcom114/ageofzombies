package GameGDX;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;

import java.util.Random;


public class GDX {
    public static GDX instance;

    private Preferences prefs;
    private Random random;
    public GDX()
    {
        instance = this;
        random = new Random();
        prefs = Gdx.app.getPreferences("Save");
       prefs.clear();
    }
    //App
    public static boolean isDesktop()
    {
        return Gdx.app.getType()== Application.ApplicationType.Desktop;
    }
    public static boolean IsHTML()
    {
        return Gdx.app.getType()== Application.ApplicationType.WebGL;
    }
    public static void Show(boolean value)
    {
        Show(String.valueOf(value));
    }
    public static void Show(String content)
    {
        Gdx.app.log("log",content);
    }
    public static void Show(int value)
    {
        Show(String.valueOf(value));
    }
    public static void Show(float value)
    {
        Show(String.valueOf(value));
    }
    public static float DeltaTime()
    {
        return Gdx.graphics.getDeltaTime();
    }
    public static void Quit()
    {
        Gdx.app.exit();
    }
    public static boolean IsPressedBack()
    {
        return Gdx.input.isKeyJustPressed(Input.Keys.BACK);
    }

    //Prefs
    public static int GetPrefInteger(String key, int defaul)
    {
        return instance.prefs.getInteger(key,defaul);
    }
    public static float GetPrefFloat(String key, float defaul)
    {
        return instance.prefs.getFloat(key,defaul);
    }
    public static long GetPrefLong(String key, long defaul)
    {
        return instance.prefs.getLong(key,defaul);
    }
    public static void SetPrefLong(String key, long value)
    {
        instance.prefs.putLong(key,value);
        instance.prefs.flush();
    }
    public static void SetPrefInteger(String key, int value)
    {
        instance.prefs.putInteger(key,value);
        instance.prefs.flush();
    }
    public static void SetPrefFloat(String key, float value)
    {
        instance.prefs.putFloat(key,value);
        instance.prefs.flush();
    }
    public static String GetPrefString(String key, String defaul)
    {
        return instance.prefs.getString(key,defaul);
    }
    public static void SetPrefString(String key, String value)
    {
        instance.prefs.putString(key,value);
        instance.prefs.flush();
    }
    public static boolean GetPrefBoolean(String key, boolean defaul)
    {
        return instance.prefs.getBoolean(key,defaul);
    }
    public static void SetPrefBoolean(String key, boolean value)
    {
        instance.prefs.putBoolean(key,value);
        instance.prefs.flush();
    }

    //random
    public static int RandomInt(int start, int end)
    {
        int del = end-start+1;
        return start+instance.random.nextInt(del);
    }

    public static float RandomFloat(float start, float end)
    {
        float del = end - start;
        return start+instance.random.nextFloat()*del;
    }
    public static void PostRunable(final Runnable runnable)
    {
        Gdx.app.postRunnable(runnable);
    }

    //pixmap
//    public static Pixmap GetPixmapScreen()
//    {
//        return MyGame.sdk.getFrameBufferPixmap(0,0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight());
//    }
}
