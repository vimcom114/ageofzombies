package GameGDX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;

import java.util.HashMap;

public class GParPool {
//    private static TextureAtlas atlasPar = GDX.newAtlas("particle");
    private static HashMap<String, ParticleEffectPool> mapPool = new HashMap<>();

    public static ParticleEffectPool getPool(String name, int init, int max) {
        if (!mapPool.containsKey(name)) {
            ParticleEffect pe = new ParticleEffect();
//            pe.load(GDX.internal("particles/"+name), atlasPar);
            pe.load(Loader.GetFileParticle(name), Gdx.files.internal("Particle"));

            mapPool.put(name, new ParticleEffectPool(pe, init, max));

        }

        return mapPool.get(name);
    }


}