package GameGDX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.geda.game.MyGame;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import GameGDX.AssetLoading.DataGame;
import GameGDX.AssetLoading.NodeAsset;
import GameGDX.encrypt.EncryptUtil;


public class Loader {
    public static Loader instance;

    public BitmapFont font,font_sport,font_Nuti;

    private Map<String,NodeAsset> mapSpine_;
    private Map<String,NodeAsset> mapTexture;
    private Map<String,NodeAsset> mapSound;
    private Map<String,NodeAsset> mapMusic;

    private Map<String,NodeAsset> mapAssetLater;

    private Map<String, NodeAsset> mapFileParticle;
    public FileHandle rootParticle;

    private TextureAtlas atlas,atlasLoading;
    private AssetManager assets;

    private DataGame dataGame;

    public Loader()
    {
        instance = this;
        mapSpine_ = new HashMap<String, NodeAsset>();
        mapTexture = new HashMap<String, NodeAsset>();
        mapFileParticle = new HashMap<String, NodeAsset>();
        mapSound = new HashMap<String, NodeAsset>();
        mapMusic = new HashMap<String, NodeAsset>();

        mapAssetLater = new HashMap<String, NodeAsset>();

        assets = new AssetManager();
        Config.ProtectFile();
       // EncryptUtil.ProtectAssets(true, assets, new String[]{"Atlas","Spine","Fonts",Config.folder});

        dataGame = new DataGame();
        InitMapAssetLater();
        Preload();
        MyGame.instance.getLoadingScreen().startLoadingScreen(assets);
    }
    private void InitMapAssetLater()
    {
        for(NodeAsset node : dataGame.assetsLate)
            if (GDX.IsHTML()) mapAssetLater.put(node.name,node);
            else LoadNodeAsset(node,false);
    }
    //PreLoad
    private void Preload()
    {


        LoadFont();

        LoadDataAsset(dataGame.assetsPre);

        //init Particle Asset
        rootParticle = Gdx.files.internal("Particle/");
       // mapFileParticle = new HashMap<String, FileHandle>();
      //  mapFileParticle.put("loading", Gdx.files.internal("Particle/loading"));

    }
    //LoadAsset
    public void LoadAssets()
    {

        LoadDataAsset(dataGame.assets);
    }

    public HashMap<String, TextureAtlas> mapSpine = new HashMap<>();
    public HashMap<String, NodeAsset> mapSpineNode = new HashMap<>();
//    public String[] nameAtlasSpine = {"animation_fire", "chinese_unicorn", "figure"};
    private void LoadDataAsset(List<NodeAsset> list)
    {
        assets.load("Atlas/DataTexture.atlas", TextureAtlas.class);
        assets.finishLoading();
        atlas = assets.get("Atlas/DataTexture.atlas", TextureAtlas.class);

//        for (int i = 0; i < nameAtlasSpine.length; i++) {
//            mapSpine.put(nameAtlasSpine[i], assets.get("Spine/"+nameAtlasSpine[i]+".atlas", TextureAtlas.class));
//        }

        for (NodeAsset as : list) LoadNodeAsset(as,false);
        assets.finishLoading();
//        for (String name) {
//            mapSpine.put(nameAtlasSpine[i], assets.get("Spine/"+nameAtlasSpine[i]+".atlas", TextureAtlas.class));
//        }
    }
    public void LoadNodeAsset(NodeAsset as,boolean waiting)
    {
        if (as.kind==NodeAsset.Kind.Spine && AddMapname(as,mapSpineNode)) LoadSpine(as);
        if (as.kind==NodeAsset.Kind.Texture && AddMapname(as,mapTexture)) LoadTexture(as);
        if (as.kind==NodeAsset.Kind.Particle && AddMapname(as,mapFileParticle)) LoadParticle(as);
        if (as.kind==NodeAsset.Kind.Sound && AddMapname(as,mapSound)) LoadSound(as);
        if (as.kind==NodeAsset.Kind.Music && AddMapname(as,mapMusic)) LoadMusic(as);
        if (waiting) assets.finishLoading();
    }

    private void LoadFont()
    {
        String root = "Fonts/";
//        TextureRegion textureRegion = new TextureRegion(new Texture(Gdx.files.internal(root+"Anton-Regular.png")));
//        font = new BitmapFont(Gdx.files.internal(root+"Anton-Regular.fnt"),textureRegion,false);
        TextureRegion textureRegion =getTextureFromBytes(root+"font.png");
        font = new BitmapFont(Gdx.files.internal(root+"font.fnt"),textureRegion,false);
        TextureRegion textureRegion_1 = getTextureFromBytes(root+"font.png");
        font_sport = new BitmapFont(Gdx.files.internal(root+"font.fnt"),textureRegion_1,false);
    }
    public static TextureRegion getTextureFromBytes(String path) {
        byte[] bytes = Gdx.files.internal(path).readBytes();
        if (EncryptUtil.isEncyptedFile(bytes)) bytes = EncryptUtil.getDecryptBytes(bytes);
        Pixmap pixmap = new Pixmap(bytes, 0, bytes.length);
        return new TextureRegion(new Texture(pixmap));
    }
    private boolean AddMapname(NodeAsset node, Map<String,NodeAsset> map)
    {
        if (map.containsKey(node.name))
        {
            //GDX.Show("Erro trung ten: "+node.url+"// "+map.get(node.name).url);
            return false;
        }
        map.put(node.name,node);
        return true;
    }
    private void LoadParticle(NodeAsset node)
    {
        if(node.atlas) return;
        assets.load(node.url, ParticleEffect.class);
    }
    private void LoadTexture(NodeAsset node)
    {
        if(node.atlas) return;
        assets.load(node.url, Texture.class);
    }
    private void LoadSpine(NodeAsset node)
    {
        assets.load(node.url, TextureAtlas.class);
    }
    private void LoadSound(NodeAsset node)
    {
        if (GDX.IsHTML()) LoadSound_H5(node.url);
        else assets.load(node.url, Sound.class);
    }
    private void LoadMusic(NodeAsset node)
    {
        if (GDX.IsHTML()) LoadSound_H5(node.url);
        else assets.load(node.url, Music.class);
    }

    //get value
    public static NodeAsset GetNodeAssetLate(String name)
    {
        return instance.mapAssetLater.get(name);
    }
    public static TextureRegion GetTexture(String name)
    {
        NodeAsset as = instance.mapTexture.get(name);
        String path0 = as.url.split("\\.")[0];
        if (instance.atlas!=null) return instance.atlas.findRegion(path0);
        return new TextureRegion(instance.assets.get(as.url, Texture.class));
    }
    public static TextureAtlas GetTextureSpine(String name)
    {
        NodeAsset as = instance.mapSpineNode.get(name);

        return instance.assets.get(as.url , TextureAtlas.class);
    }
    public static TextureRegion GetTextureNoAtlas(String name)
    {
        NodeAsset as = instance.mapTexture.get(name);
        return new TextureRegion(instance.assets.get(as.url, Texture.class));
    }
    public static FileHandle GetFileParticle(String name)
    {
        FileHandle fileHandle=Gdx.files.internal(instance.mapFileParticle.get(name).url);
        return fileHandle;
    }
    public  String geturlSound(String name)
    {
        String url = instance.mapSound.get(name).url;
        return url;
    }
    public  String geturlMusic(String name)
    {
        String url = instance.mapMusic.get(name).url;
        return url;
    }
    public static Sound getSound(String url)
    {
        Sound sound = instance.assets.get(url, Sound.class);
        return sound;
    }
    public static Music getMusic(String url)
    {
        Music music = instance.assets.get(url, Music.class);
        return music;
    }
    public static void PlayMusic(String name)
    {
        String url = instance.mapMusic.get(name).url;
        if (GDX.IsHTML()) PlayMusic_H5(name);
        else
        {
            Music music = instance.assets.get(url, Music.class);
            music.setLooping(true);
            music.play();

        }
    }
    public static void StopAllSound()
    {
        if (GDX.IsHTML()) StopAllSound_H5();
        else
        {
            for (NodeAsset node : instance.mapMusic.values())
                instance.assets.get(node.url, Music.class).pause();
        }
    }

    public void StopMusic(String name){
        String url = instance.mapMusic.get(name).url;
        if (GDX.IsHTML()) PlayMusic_H5(name);
        else
        {
            Music music = instance.assets.get(url, Music.class);
            music.stop();
        }
    }

    //html
    private static native void LoadSound_H5(String url) /*-{
            $wnd.loadSound(url);
        }-*/;

    private static native void PlaySound_H5(String url) /*-{
            $wnd.playSound(url);
        }-*/;

    private static native void PlayMusic_H5(String url) /*-{
            $wnd.playLoopSound(url);
        }-*/;
    private static native void StopAllSound_H5() /*-{
            $wnd.stopAllSound();
        }-*/;
}
