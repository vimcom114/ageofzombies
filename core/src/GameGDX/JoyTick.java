package GameGDX;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Null;
import com.badlogic.gdx.utils.Pools;

public class JoyTick extends Widget {

    private MyTouchpadStyle style;

    public static class MyTouchpadStyle {
        /** Stretched in both directions. */
        public @Null
        Drawable background;
        public @Null Drawable knob;

        public MyTouchpadStyle () {
        }

        public MyTouchpadStyle (@Null Drawable background, @Null Drawable knob) {
            this.background = background;
            this.knob = knob;
        }

        public MyTouchpadStyle (MyTouchpadStyle style) {
            background = style.background;
            knob = style.knob;
        }
    }
    boolean touched;
    boolean resetOnTouchUp = true;
    private float deadzoneRadius;
    private final Circle knobBounds = new Circle(0, 0, 0);
    private final Circle touchBounds = new Circle(0, 0, 0);
    private final Circle deadzoneBounds = new Circle(0, 0, 0);
    private final Vector2 knobPosition = new Vector2();
    private final Vector2 knobPercent = new Vector2();

    private  float angleDegree=0;

    /** @param deadzoneRadius The distance in pixels from the center of the touchpad required for the knob to be moved. */
    public JoyTick (float deadzoneRadius, Skin skin) {
        this(deadzoneRadius, skin.get(MyTouchpadStyle.class));
    }

    /** @param deadzoneRadius The distance in pixels from the center of the touchpad required for the knob to be moved. */
    public JoyTick (float deadzoneRadius, Skin skin, String styleName) {
        this(deadzoneRadius, skin.get(styleName,MyTouchpadStyle.class));
    }

    /** @param deadzoneRadius The distance in pixels from the center of the touchpad required for the knob to be moved. */
    public JoyTick (float deadzoneRadius, MyTouchpadStyle style) {
        if (deadzoneRadius < 0) throw new IllegalArgumentException("deadzoneRadius must be > 0");
        this.deadzoneRadius = deadzoneRadius;

        knobPosition.set(getWidth() / 2f, getHeight() / 2f);

        setStyle(style);
        setSize(getPrefWidth(), getPrefHeight());

        addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
               touchDownJoy(x,y,false);
                return true;
            }

            public void touchDragged (InputEvent event, float x, float y, int pointer) {
                touchDragJoy(x,y,false);
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                touchUpJoy(x,y,false);
            }
        });
    }
    public void touchDownJoy(float x,float y,boolean Outside){
        if (touched) return;
        touched = true;
        calculatePositionAndValue(x, y, false,Outside);
    }
    public void touchDragJoy(float x,float y,boolean Outside){
        calculatePositionAndValue(x, y, false,Outside);
    }
    public void touchUpJoy(float x,float y,boolean Outside){
        touched = false;
        calculatePositionAndValue(x, y, resetOnTouchUp,Outside);
    }
    void calculatePositionAndValue (float x, float y, boolean isTouchUp,boolean isOutSide) {

//        if(isOutSide) {
//             x = x * this.getWidth();
//             y = y * this.getHeight();
//        }
        float oldPositionX = knobPosition.x;
        float oldPositionY = knobPosition.y;
        float oldPercentX = knobPercent.x;
        float oldPercentY = knobPercent.y;
        float centerX = knobBounds.x;
        float centerY = knobBounds.y;
        knobPosition.set(centerX, centerY);
        knobPercent.set(0f, 0f);
        if (!isTouchUp) {
            if (!deadzoneBounds.contains(x, y)) {
                knobPercent.set((x - centerX) / knobBounds.radius, (y - centerY) / knobBounds.radius);
                float length = knobPercent.len();
                if (length > 1) knobPercent.scl(1 / length);
                if (knobBounds.contains(x, y)) {
                    knobPosition.set(x, y);
                } else {
                    knobPosition.set(knobPercent).nor().scl(knobBounds.radius).add(knobBounds.x, knobBounds.y);
                }
            }
        }
        if (oldPercentX != knobPercent.x || oldPercentY != knobPercent.y) {
            ChangeListener.ChangeEvent changeEvent = Pools.obtain(ChangeListener.ChangeEvent.class);
            if (fire(changeEvent)) {
                knobPercent.set(oldPercentX, oldPercentY);
                knobPosition.set(oldPositionX, oldPositionY);
            }
            Pools.free(changeEvent);
        }
        calculatedegree();
    }

    public void setStyle (MyTouchpadStyle style) {
        if (style == null) throw new IllegalArgumentException("style cannot be null");
        this.style = style;
        invalidateHierarchy();
    }


    public MyTouchpadStyle getStyle () {
        return style;
    }

    public Actor hit (float x, float y, boolean touchable) {
        if (touchable && this.getTouchable() != Touchable.enabled) return null;
        if (!isVisible()) return null;
        return touchBounds.contains(x, y) ? this : null;
    }

    public void layout () {
        // Recalc pad and deadzone bounds
        float halfWidth = getWidth() / 2;
        float halfHeight = getHeight() / 2;
        float radius = Math.min(halfWidth, halfHeight);

        touchBounds.set(halfWidth, halfHeight, radius);
        if (style.knob != null) radius -= Math.max(style.knob.getMinWidth(), style.knob.getMinHeight()) / 2;
        knobBounds.set(halfWidth, halfHeight, radius);
        deadzoneBounds.set(halfWidth, halfHeight, deadzoneRadius);
        // Recalc pad values and knob position
        knobPosition.set(halfWidth, halfHeight);
        knobPercent.set(0, 0);
    }

    public void draw (Batch batch, float parentAlpha) {
        validate();

        Color c = getColor();
        batch.setColor(c.r, c.g, c.b, c.a * parentAlpha);

        float x = getX();
        float y = getY();
        float w = getWidth();
        float h = getHeight();

        final Drawable bg = style.background;
        if (bg != null) bg.draw(batch, x, y, w, h);

        final Drawable knob = style.knob;
        if (knob != null) {
            x += knobPosition.x - knob.getMinWidth() / 2f;
            y += knobPosition.y - knob.getMinHeight() / 2f;
            knob.draw(batch, x, y, knob.getMinWidth(), knob.getMinHeight());
        }
    }

    public float getPrefWidth () {
        return style.background != null ? style.background.getMinWidth() : 0;
    }

    public float getPrefHeight () {
        return style.background != null ? style.background.getMinHeight() : 0;
    }
    public void setTouched(boolean touch){
        touched=touch;
    }
    public boolean isTouched () {
        return touched;
    }

    public boolean getResetOnTouchUp () {
        return resetOnTouchUp;
    }

    /** @param reset Whether to reset the knob to the center on touch up. */
    public void setResetOnTouchUp (boolean reset) {
        this.resetOnTouchUp = reset;
    }

    /** @param deadzoneRadius The distance in pixels from the center of the touchpad required for the knob to be moved. */
    public void setDeadzone (float deadzoneRadius) {
        if (deadzoneRadius < 0) throw new IllegalArgumentException("deadzoneRadius must be > 0");
        this.deadzoneRadius = deadzoneRadius;
        invalidate();
    }

    /** Returns the x-position of the knob relative to the center of the widget. The positive direction is right. */
    public float getKnobX () {
        return knobPosition.x;
    }

    /** Returns the y-position of the knob relative to the center of the widget. The positive direction is up. */
    public float getKnobY () {
        return knobPosition.y;
    }

    /** Returns the x-position of the knob as a percentage from the center of the touchpad to the edge of the circular movement
     * area. The positive direction is right. */
    public float getKnobPercentX () {
        return knobPercent.x;
    }

    /** Returns the y-position of the knob as a percentage from the center of the touchpad to the edge of the circular movement
     * area. The positive direction is up. */
    public float getKnobPercentY () {
        return knobPercent.y;
    }

    private void calculatedegree(){
        Vector2 v = new Vector2(getKnobPercentX(),getKnobPercentY());
        float x=v.x<0.0f?-1*v.x:v.x;
        float y=v.y<0.0f?-1*v.y:v.y;
        float angle = v.angle();
        if(x>0.1f&&y<0.1f||x<0.1f&&y>0.1f||x>0.1f&&y>0.1f)angleDegree=angle;

    }
    public float getAngleDegree() {
        return angleDegree;
    }
    /** The style for a {@link Touchpad}.
     * @author Josh Street */

}
