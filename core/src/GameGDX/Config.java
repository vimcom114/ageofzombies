package GameGDX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;
import com.geda.game.MyGame;


import GameGDX.encrypt.EncryptUtil;

public class Config {
    public static String folder = "data/config/";
    private static JsonValue json;

    private static JsonReader jsonReader = new JsonReader();

    public static void saveFileJson(String fileName, JsonValue data) {
        try {
            Gdx.files.local(fileName).writeString(data.toJson(JsonWriter.OutputType.json), false);
        } catch (Exception e) {}
    }
    public static JsonValue readJson_Path(String path) { return readJson_File(Gdx.files.internal(path)); }
    public static JsonValue readJson_File(FileHandle file) { try { return jsonReader.parse(file); } catch (Exception e) { return null; } }
    public static JsonValue readJson_Value(String value) { try { return jsonReader.parse(value); } catch (Exception e) { return null; } }


    private static FileHandle getFile(boolean local, String path) { return local ? Gdx.files.local(path) : Gdx.files.internal(path); }
    public static void ProtectFile() {
        FileHandle file = getFile(GDX.isDesktop(), folder+"config.txt");
        if (GDX.isDesktop()) {
            JsonValue jsonMain = new JsonValue(JsonValue.ValueType.array);
            for (int i = 0; i < Parameter.values().length; i++) {
                JsonValue js = new JsonValue(JsonValue.ValueType.array);
                js.addChild(new JsonValue(Parameter.values()[i].name()));
                js.addChild(new JsonValue(Value[i]));
                jsonMain.addChild(js);
            }
            file.writeString(jsonMain.toJson(JsonWriter.OutputType.json), false);
        }
        byte[] bytes = file.readBytes();
        if (EncryptUtil.isEncyptedFile(bytes)) bytes = EncryptUtil.getDecryptBytes(bytes);
        json = readJson_Value(new String(bytes));
        ConvertParameter();
    }

    private static JsonValue getValueFromJson(Parameter par) { try { return json.get(par.ordinal()).get(1); } catch (Exception e) { return null; } }

    private static int getInt(int success, int fail) { try { return success; } catch (Exception e) { return fail; } }

    private static int getValue(Parameter par) { try { return getValueFromJson(par).asInt(); } catch (Exception e) { return parseInt(par); } }
    private static int getValue(Parameter par, String keyRemote) { try { return MyGame.sdk.GetConfigIntValue(keyRemote, getValueFromJson(par).asInt()); } catch (Exception e) { return parseInt(par); } }
    private static int[] getValues(Parameter par) { try { return parseInts(getValueFromJson(par).asString()); } catch (Exception e) { return parseInts(par); } }
    private static int[] getValues(Parameter par, String keyRemote) { try { return parseInts(MyGame.sdk.GetConfigString(keyRemote, getValueFromJson(par).asString())); } catch (Exception e) { return parseInts(par); } }

    private static int parseInt(Parameter par) { return getInt(Integer.parseInt(Value[par.ordinal()]), 0); }
    private static int parseInt(String s) { return getInt(Integer.parseInt(s), 0); }
    private static int[] parseInts(Parameter par) { return parseInts(Value[par.ordinal()]); }
    private static int[] parseInts(String s) {
        String[] arrS = s.split(",");
        int[] arr = new int[arrS.length];
        for (int i = 0; i < arr.length; i++) arr[i] = parseInt(arrS[i]);
        return arr;
    }

    private static String[] Value = {
            "5",
            "10",
            "20",
    };
    private enum Parameter {
        MONEY_5,
        MONEY_10,
        MONEY_20,
    }

    public static int MONEY_5;
    public static int MONEY_10;
    public static int MONEY_20;

    private static void ConvertParameter() {
        MONEY_5 = getValue(Parameter.MONEY_5);
        MONEY_10 = getValue(Parameter.MONEY_10);
        MONEY_20 = getValue(Parameter.MONEY_20);
    }
}