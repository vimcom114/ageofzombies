package GameGDX;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

/**
 * Created by BaDuy on 5/2/2018.
 */

public abstract class ScreenView extends Group {
    private boolean isShow=false;
    protected Image overlay;
    protected Group main;
    private Action done;



    private float WidthBG=0;
    private float HeightBG=0;

    public boolean isStop() {
        return Stop;
    }

    public void setStop(boolean stop) {
        Stop = stop;
    }

    private boolean Stop=false;
    public ScreenView(Stage stage)
    {
        this.setSize(stage.getWidth(),stage.getHeight());
        WidthBG=stage.getWidth()/1280;
        InitScreen(this.getWidth() , this.getHeight());
    }

    public ScreenView(TextureRegion background)
    {
        InitScreen(background.getRegionWidth(), background.getRegionHeight());
        UI.NewImage(background,main);


    }
    public ScreenView(float width, float height)
    {
        InitScreen(width, height);
    }
    private void InitScreen(float width, float height)
    {
        overlay = UI.NewImage(new Color(0,0,0,0f),0,0, Align.bottomLeft, (int)this.getWidth() ,(int) this.getHeight(),this);
        main = new Group();
        main.setSize(width, height);
        main.setOrigin(Align.center);
        main.setScale(0);
        main.setPosition(this.getWidth()/2 , this.getHeight()/2, Align.center);
        this.addActor(main);

        final Group screen = this;
        done = new Action() {
                @Override
                public boolean act(float delta) {
                    screen.setVisible(false);
                    return true;
                }
        };

        this.setVisible(false);

    }
    public void setOverlay(float fade){
        overlay.getColor().a=fade;
    }
    public void setOverlayColor(Color color){
        overlay.setColor(color);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(Stop)return;
        super.draw(batch, parentAlpha);
    }

    public void Show()
    {
        setShow(true);
      // BlockPur.sdk.ShowFullscreen();
        main.clearActions();
        this.setVisible(true);
        GAction.ScaleTo(main,1,1,0.4f, Interpolation.bounceOut,null);
    }
    public void Hide()
    {
        setShow(false);
        main.clearActions();
        GAction.ScaleTo(main,0,0,0.05f, Interpolation.fade,done);
    }
    public void settouchAbleOverlay(boolean able){
        if(able) {
            overlay.setTouchable(Touchable.enabled);

        }else{
            overlay.setTouchable(Touchable.disabled);

        }
    }
    public int getSizeindex(){
       return  this.getChildren().size;
    }
    public void setZindexOverlay(int index){
        overlay.setZIndex(index);
    }
    public void FadeInColor(float duration){
        GAction.FadeIn(overlay,duration);

    }
    public void FadeOutColor(float duration){
        GAction.FadeOut(overlay,duration);

    }
    public void setTextureOverlya(TextureRegion textureRegion){
        overlay.setDrawable(new TextureRegionDrawable(textureRegion));
    }
    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }
    public abstract void ShowMyScene();

    public abstract void HideMyScene();
    public float getWidthBG() {
        return WidthBG;
    }

    public void setWidthBG(float widthBG) {
        WidthBG = widthBG;
    }

    public float getHeightBG() {
        return HeightBG;
    }

    public void setHeightBG(float heightBG) {
        HeightBG = heightBG;
    }
}
