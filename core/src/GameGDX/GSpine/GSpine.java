package GameGDX.GSpine;

import com.badlogic.gdx.scenes.scene2d.Group;

public class GSpine extends Group {

    public  MySpine mySpine;
    private String NameAnim="";
    public GSpine(String name, float x, float y, int algin, Group parent, String nameanm)
    {
        mySpine = new MySpine(MySpine.getData2D(name));
        mySpine.SetAnimation(0,nameanm,true);
        setSize(mySpine.getWidth(),mySpine.getHeight());
        setPosition(x,y,algin);
        parent.addActor(this);
        this.addActor(mySpine);

    }
    public void setAnim(String nameAnim,boolean loop){
       try {
           NameAnim=nameAnim;
           mySpine.SetAnimation(0,nameAnim,loop);
       }catch (Exception ex){

       }
    }
    public void setStop(boolean stop){
        setVisible(!stop);
        mySpine.setStop(stop);
    }
    public boolean checkValidName(String state){
        boolean check=false;
        check=NameAnim.contains(state);
        return check;
    }
}
