package GameGDX.GSpine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

import GameGDX.GSpine.spine.AnimationState;
import GameGDX.GSpine.spine.AnimationStateData;
import GameGDX.GSpine.spine.Skeleton;
import GameGDX.GSpine.spine.SkeletonData;
import GameGDX.GSpine.spine.SkeletonJson;
import GameGDX.GSpine.spine.SkeletonRenderer;
import GameGDX.GSpine.spine.Slot;
import GameGDX.GSpine.spine.attachments.Attachment;
import GameGDX.GSpine.spine.attachments.MeshAttachment;
import GameGDX.Loader;


public class MySpine extends Actor {
    private static SkeletonRenderer skeletonRenderer;
    public Skeleton skeleton;
    public AnimationState state;
    boolean is_mesh;
    public String cur_animation="";

    public boolean isStop() {
        return Stop;
    }

    public void setStop(boolean stop) {
        Stop = stop;
    }

    private boolean Stop=false;
    public static void setRenderer()
    {
        skeletonRenderer=new SkeletonRenderer();
        skeletonRenderer.setPremultipliedAlpha(true);
    }
    public MySpine(SkeletonData data2D) {
        skeleton = new Skeleton(data2D);
        AnimationStateData stateData2D = new AnimationStateData(data2D);
        state = new AnimationState(stateData2D);
        is_mesh=checkIsMesh();
        setSize(data2D.getWidth(),data2D.getHeight());
    }

    public static SkeletonData getData2D(String name){
        TextureAtlas atlas2D = Loader.GetTextureSpine(name);
        SkeletonJson json2D = new SkeletonJson(atlas2D);
        SkeletonData data2D = json2D.readSkeletonData(Gdx.files.internal("Spine/"+name+".json"));
        return data2D;
    }

    public void SetAnimation(int trackIndex, String nameAnima, boolean loop) {
        if (nameAnima.equals(cur_animation))
            return;
        AnimationState.TrackEntry entry = state.setAnimation(trackIndex, nameAnima, loop);
        cur_animation=nameAnima;
    }

    @Override
    public void setPosition(float x, float y, int alignment) {
        super.setPosition(x, y, alignment);
        skeleton.getRootBone().setPosition(getX(), getY());
    }

    @Override
    public void setScale(float scaleXY) {
        super.setScale(scaleXY);
        //skeleton.setScale(getScaleX(),getScaleY());
        skeleton.getRootBone().setScale(scaleXY);
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        if(Stop)return;
        drawNormal(batch);
    }

    void drawNormal(Batch batch){
        skeletonRenderer.draw(batch,skeleton);
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    @Override
    public void act(float delta) {
        if(Stop)return;
        state.update(delta);
        state.apply(skeleton);
        skeleton.updateWorldTransform();
        super.act(delta);
    }

    boolean checkIsMesh() {
        Array<Slot> drawOrder = skeleton.getDrawOrder();
        for (int i = 0, n = drawOrder.size; i < n; i++) {
            Slot slot = drawOrder.get(i);
            Attachment attachment = slot.getAttachment();
            if (attachment instanceof MeshAttachment) {
                return true;
            }
        }
        return false;
    }
    public float FadeColor(float dt,float target){
        float cA=1;
        for (Slot slot : skeleton.getSlots()) {
            slot.getColor().set(1, 1, 1, slot.getColor().a + dt);
            cA = slot.getColor().a;
        }
        return cA;
    }
    public void setColorr(float a){
        for (Slot slot : skeleton.getSlots()) {
            slot.getColor().set(1,1,1,a);
        }
    }
    public void setRBGColorr(Color color){
        for (Slot slot : skeleton.getSlots()) {
            slot.getColor().set(color);
        }
    }
    public Color getDarColor(){
        Color myColor = null;
        for (Slot slot : skeleton.getSlots()) {
            myColor=slot.getDarkColor();
        }
        return myColor;
    }
    public void dardColorr(Color color){
        for (Slot slot : skeleton.getSlots()) {
            slot.getDarkColor().set(color);
        }
    }
    public void setDarkColorTime(Color color, float dt){
        for (Slot slot : skeleton.getSlots()) {
            slot.getDarkColor().lerp(color, 0.5f);
        }
    }
}