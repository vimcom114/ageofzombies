package ScreenGame;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;

import Controller.GameController;
import Controller.LevelDataControll;
import Controller.LevelSpawnControll;
import Controller.SpawnWave;
import GameGDX.ClickEvent;
import GameGDX.GAction;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.ScreenView;
import GameGDX.UI;

import static com.badlogic.gdx.Input.Keys.BACKSPACE;

public class LevelTools extends ScreenView {
    public class RowLVInfor extends Group{
        int ID=0;
        String[] listNameData;
        private SelectBox editDataTexture;
        private TextField editPositonSpawnX,editPositonSpawnY,editCountEnemy,editTimeToSpawn,editSpeedMove,editTimeToNextWave;
        public Label lbID;
        int whatFruit=0;
        public RowLVInfor(RowLVInfor rowLVInfor,int id,String[] listdata,Group parent){
            this.setSize(parent.getWidth(),63);
            ID=id;
            listNameData = listdata;

            Group gID=new Group();
            this.addActor(gID);
            gID.setSize(parent.getWidth()*0.1f,63);
            Image imBGsl=UI.NewImage(Loader.GetTexture("SelectB_BG"), 0, 0, Align.bottomLeft,gID.getWidth(),gID.getHeight(), gID);
            imBGsl.setSize(parent.getWidth()*0.1f,63);
            lbID=UI.NewLabel("" + ID, Color.RED, 0.5f, gID.getWidth() / 2, 0, Align.bottomLeft, gID);

            editDataTexture = UI.newSelectBox(listNameData);
            editDataTexture.setSize(parent.getWidth()*0.2f, 63);
            editDataTexture.setPosition(parent.getWidth()*0.1f,0);
            editDataTexture.setSelected(rowLVInfor.getNameDataTexture());
            this.addActor(editDataTexture);

            editCountEnemy = UI.newTextField(""+rowLVInfor.getCountEnemy(), Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editCountEnemy.setSize(parent.getWidth()*0.1f, 63);
            editCountEnemy.setPosition(parent.getWidth()*0.3f,0);
            this.addActor(editCountEnemy);



            editSpeedMove = UI.newTextField(""+rowLVInfor.getSpeedMove(), Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editSpeedMove.setSize(parent.getWidth()*0.1f, 63);
            editSpeedMove.setPosition(parent.getWidth()*0.4f,0);
            this.addActor(editSpeedMove);



            editTimeToSpawn = UI.newTextField(""+rowLVInfor.getTimeToSpawn(), Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToSpawn.setSize(parent.getWidth()*0.1f, 63);
            editTimeToSpawn.setPosition(parent.getWidth()*0.5f,0);
            this.addActor(editTimeToSpawn);



            editTimeToNextWave = UI.newTextField(""+rowLVInfor.getTimeToNextWave(), Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToNextWave.setSize(parent.getWidth()*0.1f, 63);
            editTimeToNextWave.setPosition(parent.getWidth()*0.6f,0);
            this.addActor( editTimeToNextWave);


            editPositonSpawnX = UI.newTextField(""+rowLVInfor.getPositionSpawn().x, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnX.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnX.setPosition(parent.getWidth()*0.7f,0);
            this.addActor(editPositonSpawnX);

            editPositonSpawnX.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosX();
                    return super.keyUp(event, keycode);
                }
            });

            editPositonSpawnY = UI.newTextField(""+rowLVInfor.getPositionSpawn().y, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnY.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnY.setPosition(parent.getWidth()*0.8f,0);
            this.addActor(editPositonSpawnY);


            editPositonSpawnY.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosY();
                    return super.keyUp(event, keycode);
                }
            });

            final Group gCopy=new Group();
            this.addActor( gCopy);
            gCopy.setSize(parent.getWidth()*0.1f, 63);
            UI.NewImage(Loader.GetTexture("btCoppy"), 0,0, Align.bottom,parent.getWidth()*0.1f, 63, gCopy);
            gCopy.setOrigin(Align.center);
            gCopy.setPosition(parent.getWidth()*1f,this.getHeight()/2,Align.center);
            gCopy.addListener(new ClickEvent(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    ClickEvent.ScaleSmooth(gCopy,1,1,0.05f);
                    return super.touchDown(event, x, y, pointer, button);
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    LevelTools.instance.Coppy(ID);
                    super.touchUp(event, x, y, pointer, button);

                }
            });      this.setSize(1100,100);


        }

        public RowLVInfor(int id,Group parent,String[] listdata){
            this.setSize(parent.getWidth(),63);
            ID=id-1;
            listNameData = listdata;
            Group gID=new Group();
            this.addActor(gID);
            gID.setSize(parent.getWidth()*0.1f,63);
            Image imBGsl=UI.NewImage(Loader.GetTexture("SelectB_BG"), 0, 0, Align.bottomLeft,gID.getWidth(),gID.getHeight(), gID);
            imBGsl.setSize(parent.getWidth()*0.1f,63);
            lbID=UI.NewLabel("" + ID, Color.RED, 0.5f, gID.getWidth() / 2, 0, Align.bottomLeft, gID);

            editDataTexture = UI.newSelectBox(listNameData);
            editDataTexture.setSize(parent.getWidth()*0.2f, 63);
            editDataTexture.setPosition(parent.getWidth()*0.1f,0);
            this.addActor(editDataTexture);

            editCountEnemy = UI.newTextField("2", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editCountEnemy.setSize(parent.getWidth()*0.1f, 63);
            editCountEnemy.setPosition(parent.getWidth()*0.3f,0);
            this.addActor(editCountEnemy);



            editSpeedMove = UI.newTextField("2", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editSpeedMove.setSize(parent.getWidth()*0.1f, 63);
            editSpeedMove.setPosition(parent.getWidth()*0.4f,0);
            this.addActor(editSpeedMove);



            editTimeToSpawn = UI.newTextField("2", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToSpawn.setSize(parent.getWidth()*0.1f, 63);
            editTimeToSpawn.setPosition(parent.getWidth()*0.5f,0);
            this.addActor(editTimeToSpawn);



            editTimeToNextWave = UI.newTextField("2", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToNextWave.setSize(parent.getWidth()*0.1f, 63);
            editTimeToNextWave.setPosition(parent.getWidth()*0.6f,0);
            this.addActor( editTimeToNextWave);

            editPositonSpawnX = UI.newTextField("1", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnX.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnX.setPosition(parent.getWidth()*0.7f,0);
            this.addActor(editPositonSpawnX);

            editPositonSpawnX.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosX();
                    return super.keyUp(event, keycode);
                }
            });

            editPositonSpawnY = UI.newTextField("2", Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnY.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnY.setPosition(parent.getWidth()*0.8f,0);
            this.addActor(editPositonSpawnY);

            editPositonSpawnY.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosY();
                    return super.keyUp(event, keycode);
                }
            });

            final Group gCopy=new Group();
            this.addActor( gCopy);
            gCopy.setSize(parent.getWidth()*0.1f, 63);
            UI.NewImage(Loader.GetTexture("btCoppy"), 0,0, Align.bottom,parent.getWidth()*0.1f, 63, gCopy);
            gCopy.setOrigin(Align.center);
            gCopy.setPosition(parent.getWidth()*1f,this.getHeight()/2,Align.center);
            gCopy.addListener(new ClickEvent(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    ClickEvent.ScaleSmooth(gCopy,1,1,0.05f);
                    return super.touchDown(event, x, y, pointer, button);
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    LevelTools.instance.Coppy(ID);
                    super.touchUp(event, x, y, pointer, button);

                }
            });
        }
        public RowLVInfor(int id,Group parent,String[] listdata,Vector2 positionSpaw, String dataTexture, int countEnemy, float timeToSpawn, float speedMove, float timeToNextWave){
            this.setSize(parent.getWidth(),63);
            ID=id;
            listNameData = listdata;

            Group gID=new Group();
            this.addActor(gID);
            gID.setSize(parent.getWidth()*0.1f,63);
            Image imBGsl=UI.NewImage(Loader.GetTexture("SelectB_BG"), 0, 0, Align.bottomLeft,gID.getWidth(),gID.getHeight(), gID);
            imBGsl.setSize(parent.getWidth()*0.1f,63);
            lbID=UI.NewLabel("" + ID, Color.RED, 0.5f, gID.getWidth() / 2, 0, Align.bottomLeft, gID);

            editDataTexture = UI.newSelectBox(listNameData);
            editDataTexture.setSize(parent.getWidth()*0.2f, 63);
            editDataTexture.setPosition(parent.getWidth()*0.1f,0);
            editDataTexture.setSelected(dataTexture);
            this.addActor(editDataTexture);

            editCountEnemy = UI.newTextField(""+countEnemy, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editCountEnemy.setSize(parent.getWidth()*0.1f, 63);
            editCountEnemy.setPosition(parent.getWidth()*0.3f,0);
            this.addActor(editCountEnemy);



            editSpeedMove = UI.newTextField(""+speedMove, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editSpeedMove.setSize(parent.getWidth()*0.1f, 63);
            editSpeedMove.setPosition(parent.getWidth()*0.4f,0);
            this.addActor(editSpeedMove);



            editTimeToSpawn = UI.newTextField(""+timeToSpawn, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToSpawn.setSize(parent.getWidth()*0.1f, 63);
            editTimeToSpawn.setPosition(parent.getWidth()*0.5f,0);
            this.addActor(editTimeToSpawn);



            editTimeToNextWave = UI.newTextField(""+timeToNextWave, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editTimeToNextWave.setSize(parent.getWidth()*0.1f, 63);
            editTimeToNextWave.setPosition(parent.getWidth()*0.6f,0);
            this.addActor( editTimeToNextWave);



            editPositonSpawnX = UI.newTextField(""+positionSpaw.x, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnX.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnX.setPosition(parent.getWidth()*0.7f,0);
            this.addActor(editPositonSpawnX);


            editPositonSpawnX.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosX();
                    return super.keyUp(event, keycode);
                }
            });

            editPositonSpawnY = UI.newTextField(""+positionSpaw.y, Color.WHITE, parent.getWidth()*0.1f, 0, Align.bottomLeft, 10, this);
            editPositonSpawnY.setSize(parent.getWidth()*0.1f, 63);
            editPositonSpawnY.setPosition(parent.getWidth()*0.8f,0);
            this.addActor(editPositonSpawnY);


            editPositonSpawnY.addListener(new InputListener(){

                @Override
                public boolean keyUp(InputEvent event, int keycode) {
                    if(keycode==Input.Keys.ENTER)TypePosY();
                    return super.keyUp(event, keycode);
                }
            });
            final Group gCopy=new Group();
            this.addActor( gCopy);
            gCopy.setSize(parent.getWidth()*0.1f, 63);
            UI.NewImage(Loader.GetTexture("btCoppy"), 0,0, Align.bottom,parent.getWidth()*0.1f, 63, gCopy);
            gCopy.setOrigin(Align.center);
            gCopy.setPosition(parent.getWidth()*1f,this.getHeight()/2,Align.center);
            gCopy.addListener(new ClickEvent(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    ClickEvent.ScaleSmooth(gCopy,1,1,0.05f);
                    return super.touchDown(event, x, y, pointer, button);
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    LevelTools.instance.Coppy(ID);
                    super.touchUp(event, x, y, pointer, button);

                }
            });
        }
       private void TypePosX(){
           float x=GameController.instance.getMyWorld().getCreator().convertTileMapXY(new Vector2(getPositionSpawn())).x;
           editPositonSpawnX.setText(""+(String.valueOf(x)));
       }
        private void TypePosY(){
            float y=GameController.instance.getMyWorld().getCreator().convertTileMapXY(new Vector2(getPositionSpawn())).y;
            editPositonSpawnY.setText(""+(String.valueOf(y)));
        }
        public int getID(){
            return Integer.parseInt(lbID.getText().toString());
        }

        public String getNameDataTexture(){
            return editDataTexture.getSelected().toString();
        }
        public Vector2 getPositionSpawn(){
            String TextX=editPositonSpawnX.getText();
            String TextY=editPositonSpawnY.getText();
            if(TextX.equals(""))TextX="0";
            if(TextY.equals(""))TextY="0";

            float x=Float.parseFloat(TextX);
            x= MathUtils.clamp(x,0.0f,10000000);
            float y=Float.parseFloat(TextY);
            y= MathUtils.clamp(y,0.0f,10000000);

            return new Vector2(x,y);
        }
        public int getCountEnemy(){
            int countEnemy=Integer.parseInt(editCountEnemy.getText());
            return countEnemy;
        }
        public float getTimeToSpawn(){
            float timetoSpawn=Float.parseFloat(editTimeToSpawn.getText());
            return timetoSpawn;
        }
        public float getSpeedMove(){
            float speedMove=Float.parseFloat(editSpeedMove.getText());
            return speedMove;
        }
        public float getTimeToNextWave(){
            float timetoNextWave=Float.parseFloat(editTimeToNextWave.getText());
            return timetoNextWave;
        }


    }
    public static LevelTools instance;
    GameController gameController;
    String[] DataNameShow;
    boolean isShow=true;
    private Group gBtHide;
    private Label lbHide;
    private Group gScrollSpawnWave;
    TextField editNameSpawnWave;
    CheckBox cbHaveBoss;
    float totalTimeAllWave=0;
    int countWave=0;
    int countEnemyAllWave=0;
    boolean HaveBoss=false;
    Label lbTotalTimeAllWave,lbCountWave,lbWhatLevel,lbCountEnemtAllWave;
    Table table;
    public ArrayList<RowLVInfor> listWaveOfLevel;
    int Level=1;
    int indexSave=0,indexRow=0;
    int indexInfoEdit=0;
    int indexSaveWave=0,indexRowWave=0;
    RowLVInfor IFRowsCoppy;
    int indexRowsCoppy=0,lastindexRowsCoppy=-1;
    public LevelTools(GameController gameController1, Stage stage, SpriteBatch sbatch) {
        super(stage);
        instance = this;
        gameController=gameController1;
        DataNameShow=gameController.getDataNameEnemy();
        listWaveOfLevel=new ArrayList<>();
        setOverlay(1);
        setOverlayColor(Color.WHITE);
        creatBtHideOrShow();
        creatUIInfors();
        creatTable();
        LevelDataControll.instance.setIndexLevel(Level);
        LoadShowData();
        Show();
        HideOrShow();

    }
    private void creatBtHideOrShow(){
        gBtHide=new Group();
        gBtHide.setSize(this.getWidth()*0.1f,this.getHeight());
        this.addActor(gBtHide);
        UI.NewImage(new Color(Color.BLUE),0,0, Align.bottomLeft, (int)gBtHide.getWidth() ,(int) gBtHide.getHeight(),gBtHide);
        Group gLbHide=new Group();
        lbHide=UI.NewLabel("Hide", Color.WHITE, 1.5f, 0,0, Align.bottomLeft,gLbHide);
        lbHide.setAlignment(Align.center);
        gLbHide.setSize(gBtHide.getWidth(),lbHide.getHeight());
        gLbHide.setPosition(gBtHide.getWidth()*0.4f, gBtHide.getHeight()/ 2,Align.center);
        gLbHide.setOrigin(Align.center);
        gLbHide.setRotation(-90);
        gBtHide.addActor(gLbHide);
        gBtHide.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                HideOrShow();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);

            }
        });
    }
    private void creatUIInfors(){
        lbWhatLevel=UI.NewLabel("LEVEL EDIT :", Color.RED, 0.5f, this.getWidth()/2 , this.getHeight()*1.02f, Align.top, this);

        editNameSpawnWave = UI.newTextField("2", Color.WHITE, 0, 0, Align.bottomLeft, 10, this);
        editNameSpawnWave.setSize(250, 45);
        editNameSpawnWave.setPosition(this.getWidth()*0.7f , this.getHeight()*1f, Align.top);
        this.addActor(editNameSpawnWave);


        lbTotalTimeAllWave=UI.NewLabel("TOTAL TIME AllWAVE :"+0 , Color.YELLOW, 0.4f,   this.getWidth() *0.2f, this.getHeight()-35, Align.top, this);
        lbCountWave=UI.NewLabel("COUNT WAVE :"+0 , Color.BLUE, 0.4f,   this.getWidth() *0.4f, this.getHeight()-35, Align.top, this);
        lbCountEnemtAllWave=UI.NewLabel("COUNT ENEMY All WAVE :"+0 , Color.GREEN, 0.4f,   this.getWidth() *0.6f, this.getHeight()-35, Align.top, this);

        Label lbPnale = UI.NewLabel("|        ID        | DataTexture       | Count Enemy  | Speed Move |TimeToSpawn | imeToNextWave|  Position X | Position Y", Color.RED, 0.3f, this.getWidth()*0.1f, this.getHeight() - 150, Align.bottomLeft, this);
        lbPnale.setSize(this.getWidth() - 250, 5);
        lbPnale.setAlignment(Align.bottomLeft);
        //creatTable();
        creatCheckBox();

        Image btRows = UI.NewButton(Loader.GetTexture("btRows"),  this.getWidth()*0.15f, this.getHeight() - 20, Align.top, this);
        btRows.setOrigin(Align.center);
        btRows.setScale(0.3f);
        btRows.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                indexRow+=1;
                RowLVInfor rowLVInfor = new RowLVInfor(indexRow, gScrollSpawnWave,DataNameShow);
                creatRows(rowLVInfor);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        Image btReload = UI.NewButton(Loader.GetTexture("btReload"), this.getWidth()*0.3f, this.getHeight() - 20, Align.top, this);
        btReload.setOrigin(Align.center);
        btReload.setScale(0.3f);
        btReload.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Reload();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        Image btDel = UI.NewButton(Loader.GetTexture("btDel"), this.getWidth()*0.4f, this.getHeight() - 20, Align.top, this);
        btDel.setOrigin(Align.center);
        btDel.setScale(0.3f);
        btDel.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                DelRows();
                return super.touchDown(event, x, y, pointer, button);
            }
        });

        Image btSave = UI.NewButton(Loader.GetTexture("btSave"), this.getWidth()*0.5f, this.getHeight() - 20, Align.top, this);
        btSave.setOrigin(Align.center);
        btSave.setScale(0.3f);
        btSave.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                SaveData();
                return super.touchDown(event, x, y, pointer, button);
            }
        });

        Image btNext = UI.NewButton(Loader.GetTexture("btNext"), this.getWidth()*0.6f, this.getHeight() - 20, Align.top, this);
        btNext.setOrigin(Align.center);
        btNext.setScale(0.3f);
        btNext.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                 Next();
                return super.touchDown(event, x, y, pointer, button);
            }
        });

        Image btPrev = UI.NewButton(Loader.GetTexture("btPrev"), this.getWidth()*0.7f, this.getHeight() - 20, Align.top, this);
        btPrev.setOrigin(Align.center);
        btPrev.setScale(0.3f);
        btPrev.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                 Prev();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
        Image btPaste = UI.NewButton(Loader.GetTexture("btPaste"), this.getWidth()*0.8f, this.getHeight() - 20, Align.top, this);
        btPaste.setOrigin(Align.center);
        btPaste.setScale(0.3f);
        btPaste.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                LevelTools.instance.Paste();
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }
    private void creatCheckBox(){
        Skin skin = new Skin();
        skin.add("Box", Loader.GetTexture("Box"));
        skin.add("Check",Loader.GetTexture("Check"));
        CheckBox.CheckBoxStyle cbStyle = new CheckBox.CheckBoxStyle();
        Drawable drawBox = new TextureRegionDrawable(Loader.GetTexture("Box"));// skin.newDrawable("Box"); //skin.getDrawable("Box");
        drawBox.setMinWidth(50);
        drawBox.setMinHeight(50);
        Drawable drawCheck = new TextureRegionDrawable(Loader.GetTexture("Check"));
        drawCheck.setMinWidth(50);
        drawCheck.setMinHeight(50);
        cbStyle.checkboxOn=drawCheck;
        cbStyle.checkboxOff=drawBox;
        cbStyle.font = Loader.instance.font;
        cbStyle.fontColor = Color.BLUE;

        final Group gCbHaveBoss=new Group();
        this.addActor(gCbHaveBoss);
        gCbHaveBoss.setSize(this.getWidth()*0.1f,50);
        gCbHaveBoss.setPosition(this.getWidth()*0.8f,this.getHeight()*0.9f);

        Label lbHaveBoss = UI.NewLabel("HaveBoss",Color.BLACK, 0.35f, gCbHaveBoss.getWidth() *0.8f, gCbHaveBoss.getHeight()/2, Align.center,300,50, gCbHaveBoss);

        cbHaveBoss = new CheckBox("", cbStyle);
        gCbHaveBoss.addActor( cbHaveBoss);
        cbHaveBoss.setY(gCbHaveBoss.getHeight()/2,Align.center);
        cbHaveBoss.setChecked(false);

        cbHaveBoss.addListener(new ChangeListener() {
            @Override
            public void changed (ChangeEvent event, Actor actor) {
                setHaveBoss(cbHaveBoss.isChecked());
            }
        });
    }
    void creatTable() {
        gScrollSpawnWave=new Group();
        gScrollSpawnWave.setSize(this.getWidth()*0.9f,this.getHeight()*0.8f);
        this.addActor(gScrollSpawnWave);
        gScrollSpawnWave.setPosition(this.getWidth()*0.1f,0);
        table = new Table();
        table.setSize(this.getWidth()*0.9f,this.getHeight()*0.8f);
        table.defaults().pad(5f);
        // table.setFillParent(true);
        table.align(Align.topLeft);
        ScrollPane scroll = UI.newScroll(table, 0, 0, Align.bottomLeft, this.getWidth()*0.9f,this.getHeight()*0.8f, gScrollSpawnWave);
        scroll.setScrollingDisabled(true, false);



    }
    void creatRows( RowLVInfor rowLVInfor){
        listWaveOfLevel.add(rowLVInfor);
        table.add(rowLVInfor).width(gScrollSpawnWave.getWidth()).height(gScrollSpawnWave.getHeight()*0.1f);
        table.row();
        if(indexSave!=indexRow) {
            for(int i=indexSave;i<listWaveOfLevel.size();i++) {
                SpawnWave spawnWave= LevelDataControll.instance.getSpawnWave(getNameSpawnWave(),isHaveBoss(),getCountWave(),Level);
                spawnWave.addNewWave();
            }
            indexSave=indexRow;
        }
    }
    void SaveData(){
        SpawnWave spawnWave= LevelDataControll.instance.getSpawnWave(getNameSpawnWave(),isHaveBoss(),getCountWave(),Level);
        for(int i=0;i<listWaveOfLevel.size();i++) {
            spawnWave.changeDataWave(i,getWavePositionSpawn(i),getWaveNameTexture(i),getWaveCountEnemy(i),getWaveTimeToSpawn(i),getWaveSpeedMove(i),getWaveTimeToNext(i));
        }

        listWaveOfLevel.clear();
        table.clearChildren();
        LoadShowData();
        LevelDataControll.instance.WriteData();
    }
    public void LoadShowData(){
        indexSave=0;
        int checkSize= LevelDataControll.instance.checkSizeSpawnWave();
        if(checkSize<=0)return;
        int Lengh= LevelDataControll.instance.checkLenghSpawnWave(Level);
        indexRow=Lengh;
        String spawnNameLevel="";
        boolean haveBoss=false;
        if(Lengh>0){
            indexSave=indexRow;
            String Data= LevelDataControll.instance.getDataSpawnWave(Level);
            String[] splitData=Data.split("/");
             spawnNameLevel=splitData[0];
            countWave=Integer.parseInt(splitData[1]);
            haveBoss=Boolean.parseBoolean(splitData[2]);
            totalTimeAllWave=Float.parseFloat(splitData[3]);
            countEnemyAllWave=Integer.parseInt(splitData[4]);
            for(int i=0;i<Lengh;i++){
                String DataWave= LevelDataControll.instance.getDataWave(Level,i);
                 splitData=DataWave.split("/");
                 Vector2 wavePosSpawn=new Vector2(Float.parseFloat(splitData[0]),Float.parseFloat(splitData[1]));
                 String nameDataTexture=splitData[2];
                 int countEnemy=Integer.parseInt(splitData[3]);
                 float timetoSpawn=Float.parseFloat(splitData[4]);
                 float speedMove=Float.parseFloat(splitData[5]);
                float timetoNext=Float.parseFloat(splitData[6]);
                RowLVInfor rowLVInfor=new RowLVInfor(i,gScrollSpawnWave,DataNameShow,wavePosSpawn,nameDataTexture,countEnemy,timetoSpawn,speedMove,timetoNext);
                creatRows(rowLVInfor);
            }


        }
        editNameSpawnWave.setText(spawnNameLevel);
        cbHaveBoss.setChecked(haveBoss);
        lbTotalTimeAllWave.setText("TOTAL TIME ALLWAVE :"+totalTimeAllWave);
        lbCountWave.setText("COUNT POINT :"+indexRow);
        lbCountEnemtAllWave.setText("COUNT ENEMY ALL WAVE :"+countEnemyAllWave);


    }
    void DelRows(){
        if(listWaveOfLevel.size()>0) {

            if (indexSave == indexRow) {
                indexSave -= 1;
            }
            indexRow-=1;
            listWaveOfLevel.remove(indexRow);
            SpawnWave spawnWave= LevelDataControll.instance.getSpawnWave(getNameSpawnWave(),isHaveBoss(),getCountWave(),Level);

            spawnWave.removeWave(indexRow );
            SaveData();

            // LevelCT.instance.WriteData();
        }

    }
    public void Coppy(int indexRows){
        indexRowsCoppy=indexRows;
        if(indexRowsCoppy!=lastindexRowsCoppy) {
            IFRowsCoppy = listWaveOfLevel.get(indexRows);
            RowLVInfor rowLVInforCoppy = new RowLVInfor(IFRowsCoppy, listWaveOfLevel.size(),DataNameShow,gScrollSpawnWave);
            IFRowsCoppy = rowLVInforCoppy;
            LevelDataControll.instance.Coppy(Level - 1, indexRows);
            lastindexRowsCoppy=indexRowsCoppy;
        }
    }

    public void Paste(){
        if(IFRowsCoppy==null) return;
        if(indexRowsCoppy==lastindexRowsCoppy){
            RowLVInfor rowLVInforCoppy = new RowLVInfor(IFRowsCoppy,listWaveOfLevel.size(),DataNameShow,gScrollSpawnWave);
            IFRowsCoppy = rowLVInforCoppy;
            LevelDataControll.instance.Coppy(Level - 1, indexRowsCoppy);
        }
        creatRows(IFRowsCoppy);
        LevelDataControll.instance.Paste();
    }
    void Next(){
        Level+=1;
        LevelDataControll.instance.getSpawnWave(getNameSpawnWave(),isHaveBoss(),getCountWave(),Level);
        LevelDataControll.instance.setIndexLevel(Level);

        listWaveOfLevel.clear();
        table.clearChildren();
        LoadShowData();
        //LevelCT.instance.ReLoad();
    }
    void Prev(){
        if(Level>1) {
            Level -= 1;
            if (Level <= 0) {
                Level = 0;
            }
            LevelDataControll.instance.setIndexLevel(Level);
            listWaveOfLevel.clear();
            table.clearChildren();
            LoadShowData();
           // LevelCT.instance.ReLoad();
        }
    }
    void Reload(){
        LevelSpawnControll.instance.ReLoad();
        LevelSpawnControll.instance.RestartSpawnWave();
    }
    @Override
    public void ShowMyScene() {

    }

    @Override
    public void HideMyScene() {

    }

    public void HideOrShow(){
       if(isShow){
           GAction.MoveTo(this,this.getWidth()*1f,0,Align.bottomLeft,0.3f, Interpolation.linear,null);
           lbHide.setText("Is Hiding");
       }else{
           GAction.MoveTo(this,0,0,Align.bottomLeft,0.3f, Interpolation.linear,null);
           lbHide.setText("Is Showing");
       }
       isShow=!isShow;
    }
    private boolean isHaveBoss() {
        return HaveBoss;
    }

    private void setHaveBoss(boolean haveBoss) {
        HaveBoss = haveBoss;
    }

    private String getNameSpawnWave(){
        return editNameSpawnWave.getText().toString();
    }
    private void setNameSpawnWave(String nameSpawnWave){
        if(editNameSpawnWave!=null)editNameSpawnWave.setText(nameSpawnWave);
    }
    private int getCountWave(){
        return listWaveOfLevel.size();
    }
    private void setCountWave(int countWave){
        lbCountWave.setText("COUNT WAVE :"+countWave);
    }

    private String getWaveNameTexture(int indexChild){
        return listWaveOfLevel.get(indexChild).getNameDataTexture();
    }

    private  int getWaveCountEnemy(int indexChild){
        return listWaveOfLevel.get(indexChild).getCountEnemy();
    }

    private float getWaveTimeToSpawn(int indexChild){
        return listWaveOfLevel.get(indexChild).getTimeToSpawn();

    }
    private float getWaveSpeedMove(int indexChild){
        return listWaveOfLevel.get(indexChild).getSpeedMove();
    }
    private float getWaveTimeToNext(int indexChild){
        return listWaveOfLevel.get(indexChild).getTimeToNextWave();
    }
    private Vector2 getWavePositionSpawn(int indexChild){
        return listWaveOfLevel.get(indexChild).getPositionSpawn();
    }
}
