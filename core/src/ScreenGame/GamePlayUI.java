package ScreenGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import Controller.GameController;
import Controller.ItemSpawnControll;
import Controller.LevelSpawnControll;
import GameGDX.AnalogStick;
import GameGDX.ClickEvent;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.ScreenView;
import GameGDX.UI;
import Model.JoystickDirection;

public class GamePlayUI extends ScreenView {

    public  static GamePlayUI instance;
    GameController gameController;

    private boolean startGame=false;

    private Group gYoyStick_1,gYoyStick_2,gTouchSpaceLeft,gTouchSpaceRight;
    private AnalogStick movementJoytick,attackJoytick;
    private JoystickDirection joystickDirection;
    private float timetoCheckTrueDir=0.05f;
    private boolean checkTrueDir=false;

    Group gIconWPDetail;
    Image imIconWPDetail;
    Label lbIconWPDetail;
    String strTxtIcon="IconGun_1";
    Label lbFPS;
    public GamePlayUI(GameController gameController1, Stage stage, SpriteBatch sbatch) {
        super(stage);
        instance=this;
        gameController=gameController1;

        Table table = new Table();
        //Top-Align table
        table.top();
        table.setSize(this.getWidth()*0.8f,this.getHeight()*0.05f);
        table.setPosition(this.getWidth()/2,this.getHeight(), Align.top);
        creatBtGamePlay();
        creatIconWeaponDetail();
        Show();
        lbFPS=UI.NewLabel("",Color.RED,0.4f,50,this.getHeight(),Align.top,this);
        //make the table fill the entire stage
        //define our labels using the String, and a Label style consisting of a font and color

    }
    void creatBtGamePlay(){
        gTouchSpaceLeft=new Group();
        this.addActor(gTouchSpaceLeft);
        gTouchSpaceLeft.setSize(this.getWidth()/2,this.getHeight()*0.8f);
        gTouchSpaceLeft.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

                movementJoytick.touchDownJoy(MovementJoyTickOutSide(x,y,1).x,MovementJoyTickOutSide(x,y,1).y,true);
                return super.touchDown(event, x, y, pointer, button);
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                movementJoytick.touchUpJoy(MovementJoyTickOutSide(x,y,1).x,MovementJoyTickOutSide(x,y,1).y,true);
            }
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                movementJoytick.touchDragJoy(MovementJoyTickOutSide(x,y,1).x,MovementJoyTickOutSide(x,y,1).y,true);
            }
        });

        gTouchSpaceRight=new Group();
        this.addActor(gTouchSpaceRight);
        gTouchSpaceRight.setSize(this.getWidth()/2,this.getHeight()*0.8f);
        gTouchSpaceRight.setPosition(this.getWidth()/2,0);
        gTouchSpaceRight.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                attackJoytick.touchDownJoy(MovementJoyTickOutSide(x,y,2).x,MovementJoyTickOutSide(x,y,2).y,true);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                attackJoytick.touchUpJoy(MovementJoyTickOutSide(x,y,2).x,MovementJoyTickOutSide(x,y,2).y,true);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                attackJoytick.touchDragJoy(MovementJoyTickOutSide(x,y,2).x,MovementJoyTickOutSide(x,y,2).y,true);
            }
        });

        gYoyStick_1=new Group();
        this.addActor(gYoyStick_1);
        movementJoytick=new AnalogStick(0,0,this.getWidth()*0.18f,this.getWidth()*0.18f,"touchBackground","touchKnob");
        gYoyStick_1.addActor(movementJoytick);
        gYoyStick_1.setSize(this.getWidth()*0.18f, this.getWidth()*0.18f);
        gYoyStick_1.setPosition(this.getWidth()*0.06f,this.getHeight()*0.07f);


        gYoyStick_2=new Group();
        this.addActor(gYoyStick_2);
        attackJoytick=new AnalogStick(0,0,this.getWidth()*0.18f,this.getWidth()*0.18f,"touchBackground","touchKnob");
        gYoyStick_2.addActor(attackJoytick);
        gYoyStick_2.setSize(this.getWidth()*0.18f, this.getWidth()*0.18f);
        gYoyStick_2.setPosition(this.getWidth()*0.8f,this.getHeight()*0.07f);

        joystickDirection=new JoystickDirection(this,new Vector2(this.getWidth()*0.75f,this.getHeight()*0.3f),"Item_2","touchBackground","touchKnob");
        ShowJoyDirection(false,"","");
    }
    void creatIconWeaponDetail(){
        gIconWPDetail=new Group();
        this.addActor(gIconWPDetail);
        Image imbgIconWP =UI.NewImage(Loader.GetTexture("IconBG"),0,0, Align.bottomLeft,gIconWPDetail);
        gIconWPDetail.setSize(imbgIconWP.getWidth(),imbgIconWP.getHeight());
        gIconWPDetail.setPosition(this.getWidth()*0.1f,this.getHeight()*0.9f,Align.center);
        imIconWPDetail =UI.NewImage(Loader.GetTexture(strTxtIcon),0,gIconWPDetail.getHeight()/2, Align.left,gIconWPDetail);
        lbIconWPDetail=UI.NewLabel("0",Color.WHITE,0.45f,gIconWPDetail.getWidth()*0.7f,gIconWPDetail.getHeight()*0.2f,Align.center,gIconWPDetail);
    }
    public void setIconWPDetail(String count,String txtIcon){
        if(strTxtIcon.equals(txtIcon)==false){
            imIconWPDetail.setDrawable(new TextureRegionDrawable(Loader.GetTexture(txtIcon)));
            strTxtIcon=txtIcon;
        }
        setCountIconWP(count);
    }
    public void setCountIconWP(String count){
        lbIconWPDetail.setText(""+count);
    }
    public void ShowJoyDirection(boolean show,String Count,String strTxtIcon){
        if(show==false){
            joystickDirection.Hide();
        }else{
            joystickDirection.Show(Count,strTxtIcon);
        }
    }
    public void setJoyDirectionCount(String Count){
        joystickDirection.setCount(Count);
    }
    @Override
    public void act(float delta) {
        super.act(delta);
        lbFPS.setText("FPS :"+Gdx.graphics.getFramesPerSecond());
        if(Gdx.input.isKeyJustPressed(Input.Keys.B))LevelTools.instance.HideOrShow();
        UpdateMovementJoyTick(delta);
        UpdateAttackJoyTick(delta);
        joystickDirection.UpdateAttackJoyTick(delta);

    }

    @Override
    public void Show() {
        super.Show();
    }

    @Override
    public void ShowMyScene() {

    }

    @Override
    public void HideMyScene() {

    }
    public void PauseGame(){

    }
    public boolean isStartGame() {
        return startGame;
    }

    public void setStartGame(boolean startGame) {
        this.startGame = startGame;
    }

    private void UpdateMovementJoyTick(float delta){
        boolean Touch=movementJoytick.isTouched();
        if(Touch){
            movementJoytick.setTouch(true);
            checkDirMovementJoy(movementJoytick.getKnobPercentX(),movementJoytick.getKnobPercentY());
            if(isCheckTrueDir()){
                if(timetoCheckTrueDir>0){
                    timetoCheckTrueDir-=delta;
                }else{
                    if(movementJoytick.isMoveLeft()&&movementJoytick.getKnobPercentX()>0.0f){
                        movementJoytick.setMoveLeft(false);
                        movementJoytick.setMoveRight(true);
                    }

                    if(movementJoytick.isMoveRight()&&movementJoytick.getKnobPercentX()<0.0f){
                        movementJoytick.setMoveLeft(true);
                        movementJoytick.setMoveRight(false);
                    }

                    if(movementJoytick.isMoveDown()&&movementJoytick.getKnobPercentY()>0.0f){
                        movementJoytick.setMoveDown(false);
                        movementJoytick.setMoveUp(true);
                    }

                    if( movementJoytick.isMoveUp()&& movementJoytick.getKnobPercentY()<0.0f){
                        movementJoytick.setMoveDown(true);
                        movementJoytick.setMoveUp(false);
                    }
                }
            }
            GameController.instance.getMyWorld().getPlayer().setRotBody(movementJoytick.getAngleDegree());
        }else{
            boolean isTouch=movementJoytick.isTouch();
            if(isTouch) {
                movementJoytick.setTouch(false);
                movementJoytick.setPrevPercenX(0.0f);
                movementJoytick.setPrevPercenY(0.0f);
                GameController.instance.getMyWorld().HandleInPut("RIGHT", false);
                GameController.instance.getMyWorld().HandleInPut("LEFT", false);
                GameController.instance.getMyWorld().HandleInPut("UP", false);
                GameController.instance.getMyWorld().HandleInPut("DOWN", false);
            }
        }
    }
    private void checkDirMovementJoy(float pX,float pY) {

        if (pX== 0.0f) {
            movementJoytick.setMoveLeft(false);
            movementJoytick.setMoveRight(false);
            checkTrueDir=false;
        } else if (pX>0.0f) {
            movementJoytick.setMoveLeft(false);
            movementJoytick.setMoveRight(true);
            timetoCheckTrueDir=0.05f;
            checkTrueDir=true;
        } else if (pX<0.0f) {
            movementJoytick.setMoveLeft(true);
            movementJoytick.setMoveRight(false);
            timetoCheckTrueDir=0.05f;
            checkTrueDir=true;
        }
        if (pY== 0.0f) {
            movementJoytick.setMoveDown(false);
            movementJoytick.setMoveUp(false);
            checkTrueDir=false;
        }if (pY>0.0f) {
            movementJoytick.setMoveDown(false);
            movementJoytick.setMoveUp(true);
            timetoCheckTrueDir=0.05f;
            checkTrueDir=true;
        }
        else if (pY<0.0f) {
            movementJoytick.setMoveDown(true);
            movementJoytick.setMoveUp(false);
            timetoCheckTrueDir=0.05f;
            checkTrueDir=true;
        }

        GameController.instance.getMyWorld().HandleInPut("RIGHT",movementJoytick.isMoveRight());
        GameController.instance.getMyWorld().HandleInPut("LEFT", movementJoytick.isMoveLeft());
        GameController.instance.getMyWorld().HandleInPut("UP", movementJoytick.isMoveUp());
        GameController.instance.getMyWorld().HandleInPut("DOWN", movementJoytick.isMoveDown());
        movementJoytick.setPrevPercenX( pX);
        movementJoytick.setPrevPercenY(pY);
    }
    public Vector2 MovementJoyTickOutSide(float x, float y,int caseControll){
         if(caseControll==1){
             x=x-gYoyStick_1.getX();
             y=y-gYoyStick_1.getY();
         }else if(caseControll==2){
             x=x-this.getWidth()*0.3f;
             y=y-gYoyStick_2.getY();
         }
          Vector2 percentXY=calculateJoyTickPercent(x,y);
          return percentXY;
    }
    private Vector2 calculateJoyTickPercent(float x, float y){
       Vector2 percentXY;
        percentXY=new Vector2(x,y);
//      // float pX=(x-this.getWidth()*0.25f)/(this.getWidth()*0.25f);
//        float pX=x/(this.getWidth()/2);
//        pX= MathUtils.clamp(pX, 0, 1);
//       // float pY=(y-this.getHeight()*0.4f)/(this.getHeight()*0.4f);
//        float pY=y/(this.getHeight()*0.8f);
//        pY= MathUtils.clamp(pY, 0, 1);
//        percentXY=new Vector2(pX,pY);
        return percentXY;
    }
    private void UpdateAttackJoyTick(float delta){
        boolean Touch=attackJoytick.isTouched();
        if(Touch){
            attackJoytick.setTouch(true);
            GameController.instance.getMyWorld().getPlayer().setRotBody(attackJoytick.getAngleDegree());
            GameController.instance.getMyWorld().HandleInPut("SPACE", true);
        }else{
            boolean isTouch=attackJoytick.isTouch();
            if(isTouch) {
                attackJoytick.setTouched(false);
                attackJoytick.setTouch(false);
                attackJoytick.setPrevPercenX(0.0f);
                attackJoytick.setPrevPercenY(0.0f);
                GameController.instance.getMyWorld().HandleInPut("SPACE", false);

            }
        }
    }
    public boolean isCheckTrueDir() {
        return checkTrueDir;
    }
    public void setCheckTrueDir(boolean checkTrueDir) {
        this.checkTrueDir = checkTrueDir;
    }
}
