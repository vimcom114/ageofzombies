package ScreenGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.geda.game.MyGame;

import Controller.GameController;
import GameGDX.GSpine.spine.utils.TwoColorPolygonBatch;
import Model.LoadingBar;


/**
 * @author Mats Svensson
 */
public class LoadingScreen implements Screen {

    private Stage stage;

    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;

    private float startX, endX;
    private float percent;

    private Actor loadingBar;
    AssetManager assetManager;
    MyGame mygame;
    private boolean Done;
    public LoadingScreen(MyGame myGame) {
        mygame = myGame;
        show();
    }

    @Override
    public void show() {


        stage = new Stage();
        stage = new Stage(new StretchViewport(1280,720), new TwoColorPolygonBatch());
        // Get our textureatlas from the manager
        // Grab the regions from the atlas and create some images
        loadingFrame = new Image(new Texture( Gdx.files.internal("loading-frame.png")));
        loadingBarHidden = new Image(new Texture( Gdx.files.internal("loading-bar-hidden.png")));
        screenBg =  new Image(new Texture( Gdx.files.internal("screen-bg.png")));
        loadingBg =  new Image(new Texture( Gdx.files.internal("loading-frame-bg.png")));

        // Add the loading bar animation
       Array<TextureRegion> frames = new Array<TextureRegion>();
        for (int i = 0; i < 5; i++) {
            TextureRegion texture =new TextureRegion( new Texture( Gdx.files.internal("loading-bar-anim_"+(i+1))+".png"));
            frames.add(texture);
        }
        Animation anim = new Animation(0.05f,frames);
        anim.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        loadingBar = new LoadingBar(anim);

        // Or if you only need a static bar, you can do
        // loadingBar = new Image(atlas.findRegion("loading-bar1"));

        // Add all the actors to the stage
        stage.addActor(screenBg);
        stage.addActor(loadingBar);
        stage.addActor(loadingBg);
        stage.addActor(loadingBarHidden);
        stage.addActor(loadingFrame);
        // Add everything to be loaded, for instance:
        // game.manager.load("data/assets1.pack", TextureAtlas.class);
        // game.manager.load("data/assets2.pack", TextureAtlas.class);
        // game.manager.load("data/assets3.pack", TextureAtlas.class);
    }



    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height);

        // Make the background fill the screen
        screenBg.setSize(width, height);

        // Place the logo in the middle of the screen and 100 px up


        // Place the loading frame in the middle of the screen
        loadingFrame.setX((stage.getWidth() - loadingFrame.getWidth()) / 2);
        loadingFrame.setY((stage.getHeight() - loadingFrame.getHeight()) / 2);

        // Place the loading bar at the same spot as the frame, adjusted a few px
        loadingBar.setX(loadingFrame.getX() + 15);
        loadingBar.setY(loadingFrame.getY() + 5);

        // Place the image that will hide the bar on top of the bar, adjusted a few px
        loadingBarHidden.setX(loadingBar.getX() + 35);
        loadingBarHidden.setY(loadingBar.getY() - 3);
        // The start position and how far to move the hidden loading bar
        startX = loadingBarHidden.getX();
        endX = 440;

        // The rest of the hidden bar
        loadingBg.setSize(450, 50);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setY(loadingBarHidden.getY() + 3);
    }

    @Override
    public void render(float delta) {
       if(Done)return;
       if(assetManager!=null) {

           // Interpolate the percentage to make it more smooth
           percent = Interpolation.linear.apply(percent, assetManager.getProgress(), 0.1f);
           if (percent>=0.99f) { // Load some, will return true if done loading
               hide();// If the screen is touched after the game is done loading, go to the main menu screen
           }
       }
        // Update positions (and size) to match the percentage
        loadingBarHidden.setX(startX + endX * percent);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setWidth(450 - 450 * percent);
        loadingBg.invalidate();

        // Show the loading screen
        stage.act();
        stage.draw();
    }
    public void startLoadingScreen(AssetManager assetManager1){
        assetManager=assetManager1;
    }
    @Override
    public void hide() {
        new GameController(mygame);
        Done=true;
    }

    @Override
    public void dispose() {

    }

}
