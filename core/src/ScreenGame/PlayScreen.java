package ScreenGame;

import com.geda.game.MyGame;


import Controller.GameController;


public class PlayScreen extends WorldScreen {
    public static PlayScreen instance;
    public PlayScreen(MyGame game, String name, float w, float h) {
        super(game,name,w,h);
        instance=this;
        GameController.instance.setMyWorldScreen(this);
       // Changetarget();
        //allows for debug lines of our box2d world.
    }

    @Override
    public void update(float dt) {
        super.update(dt);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void setStop(boolean stop) {
        super.setStop(stop);
    }

    @Override
    public void StopAndChangeMap(String NameMap, boolean change,boolean stop,boolean isReturnPoin) {

        if(stop==true&&change==false){
            setStop(true);
          //  PlayScreenExtra.instance.StopAndChangeMap(NameMap,true,false,isReturnPoin);
        }
        super.StopAndChangeMap(NameMap, change,stop,isReturnPoin);
    }
}
