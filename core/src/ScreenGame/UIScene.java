package ScreenGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.profiling.GLProfiler;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;

import Controller.Game.AudioManager;
import Controller.GameController;
import GameGDX.ClickEvent;
import GameGDX.GAction;
import GameGDX.GSpine.spine.utils.TwoColorPolygonBatch;
import GameGDX.Language;
import GameGDX.Loader;
import GameGDX.ScreenView;
import GameGDX.UI;


public class UIScene implements Screen {
    public  static UIScene instance;
    SpriteBatch batch;
   // MyWorld myWorld;
    public Stage stage;
    private Viewport viewport;

    //Mario score/time Tracking Variables

    GameController gameController;
    OrthographicCamera gamecam;

    private Group background;
    private Group game;
    private Group ui;

    GamePlayUI gamePlayUI;
    LevelMap levelMap;
   // Shopping shopping;
    StartMenu startMenu;
    //WinLose gameOver;
    //Pause Pause;
   // Setting settingPause;
    //LuckySpin luckySpin;
    //DailyGift dailyGift;
   // ClaimItem claimItem;
   // WinLose winLose;

    Group gBigAsk,gAsk,gBtBackHome,gBtRestart,gBtExit,gCancle,gBtRate;
    Label lbAsk,lbBackHome,lbRestart,lbExit,lbCancle,lbRate;
    Image imgTransistion;
    boolean FadeIn,FadeOut;
    int openScreenCase=0;

    Array<Vector2> input;
    Group gNotify;
    Label lbNotify;
    boolean startNotify=false;
    float posMoveNotify=0;
    private int whatShowPanelAsk;
    Label lbFPS;
    public float getScaleWidth() {
        return scaleWidth;
    }

    public void setScaleWidth(float scaleWidth) {
        this.scaleWidth = scaleWidth;
    }

    private float scaleWidth=0;

    public float getScaleHeight() {
        return scaleHeight;
    }

    public void setScaleHeight(float scaleHeight) {
        this.scaleHeight = scaleHeight;
    }

    private float scaleHeight=0;
    GLProfiler profiler;


    private ArrayList<ScreenView> SceneShow=new ArrayList<>();

    private boolean stopBackHandle=false;
    private boolean startMenuIsShow=false;
    private boolean levelSceneIsShow=false;
    private boolean backHomeIsShow=false;
    private ScreenView backHomeScene=null;

    private boolean startCountToPlay=false;
    private Group gCountToPlay;
    private Label lbCoutToPLay;
    private float TimeToPlay=0f;
    int CheckTime;
    float plusTimeFade=0;
    private LevelTools levelTools;//

    public UIScene(GameController gamecontroller){
        //define our tracking variables
        instance=this;
        profiler = new GLProfiler(Gdx.graphics);
        profiler.enable();
        gameController=gamecontroller;
        batch=new SpriteBatch();
       // myWorld=gameController.getMyWorld();
        gamecam = new OrthographicCamera();
        viewport = new ExtendViewport(1280,720, gamecam);
        stage = new Stage(viewport, new TwoColorPolygonBatch());
        setScaleWidth(stage.getWidth()/1280);
//        gamecam.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);
        background = new Group();
        game = new Group();
        ui = new Group();
        stage.addActor(background);
        stage.addActor(game);
        stage.addActor(ui);
        gamePlayUI=new GamePlayUI(gameController,stage,batch);
        levelTools=new LevelTools(gameController,stage,batch);
        game.addActor(gamePlayUI);
        levelMap=new LevelMap(stage);
//        shopping=new Shopping(stage);
        startMenu=new StartMenu(stage);
//        Pause=new Pause(stage);
//        settingPause=new Setting(stage);
//        luckySpin=new LuckySpin(stage);
//        dailyGift=new DailyGift(stage);
//        claimItem=new ClaimItem(stage);
//        winLose=new WinLose(stage);
//        gameOver=new WinLose(stage);
        ui.addActor(levelTools);

        ui.addActor(levelMap);
        ui.addActor(startMenu);
//        ui.addActor(Pause);
//        ui.addActor(shopping);
//        ui.addActor(settingPause);
//        ui.addActor(luckySpin);
//        ui.addActor(dailyGift);
//        ui.addActor(claimItem);
//        ui.addActor(gameOver);
        creatPanelAsk(ui);
        creatNotifyUI(ui);
        imgTransistion= UI.NewImage(Loader.GetTexture("Transistion"),+viewport.getWorldWidth()/2,viewport.getWorldHeight()/2, Align.center,stage.getWidth(),stage.getHeight(),ui);
        imgTransistion.setOrigin(Align.center);
        imgTransistion.setColor(1,1,1,0f);
        imgTransistion.setTouchable(Touchable.disabled);
        imgTransistion.setVisible(false);
        GameController.instance.addInputManager(stage);
        gCountToPlay=new Group();
        ui.addActor(gCountToPlay);
        gCountToPlay.setPosition(stage.getWidth()/2,stage.getHeight()/2, Align.center);
        lbCoutToPLay= UI.NewLabel("0", Color.WHITE,3,0,0, Align.center,gCountToPlay);
        gCountToPlay.setOrigin(Align.center);
        gCountToPlay.setVisible(false);
        input=new Array<>();
        input.add(new Vector2(50,50));
        input.add(new Vector2(stage.getWidth()/2,stage.getHeight()/2));
        input.add(new Vector2(stage.getWidth()/2+100,stage.getHeight()/2+100));

        lbFPS= UI.NewLabel("", Color.RED,0.5f,60,stage.getHeight()-100, Align.center,ui);
    }
    void creatNotifyUI(Group group){
//        gNotify = new Group();
//        group.addActor(gNotify);
//        Image imShowdowText = UI.NewImage(Loader.GetTexture("ShadowNoti"), gNotify);
//        imShowdowText.setSize(stage.getWidth()*0.7f,imShowdowText.getHeight());
//        gNotify.setSize(imShowdowText.getWidth(), imShowdowText.getHeight());
//        gNotify.setPosition(stage.getWidth()/2, stage.getHeight() / 2, Align.center);
//        imShowdowText.setPosition(0, 0);
//        lbNotify = UI.NewLabel("11", Color.GREEN, 1, gNotify.getWidth() / 2, gNotify.getHeight() / 2, Align.center, gNotify);
//        gNotify.setOrigin(Align.center);
//        gNotify.setVisible(false);
    }
    void creatPanelAsk(Group parent) {
//        gBigAsk=new Group();
//        parent.addActor(gBigAsk);
//        UI.NewImage(new Color(0,0,0,0.6f),0,0, Align.bottomLeft, (int)stage.getWidth() ,(int)stage.getHeight(),gBigAsk);
//        gBigAsk.setSize(stage.getWidth(),stage.getHeight());
//        gBigAsk.setOrigin(Align.center);
//        gAsk = new Group();
//        gBigAsk.addActor(gAsk);
//        Image imPanel = UI.NewImage(Loader.GetTexture("popup_ask"), gAsk);
//        imPanel.setSize(imPanel.getWidth(),imPanel.getHeight()*0.8f);
//        gAsk.setSize(imPanel.getWidth(), imPanel.getHeight());
//        gAsk.setPosition(stage.getWidth() / 2, stage.getHeight() / 2, Align.center);
//        imPanel.setPosition(0, 0);
//        lbAsk= UI.NewLabel("", Color.WHITE,1,gAsk.getWidth()/2,gAsk.getHeight()/2+70, Align.center,gAsk.getWidth()*0.8f,gAsk.getHeight()-150,gAsk);
//        gAsk.setOrigin(Align.center);
//        lbAsk.setWrap(true);
//
//        gBtBackHome =new Group();
//        gAsk.addActor(gBtBackHome);
//        Image imBtBack = UI.NewImage(Loader.GetTexture("BtRed"), gBtBackHome);
//        gBtBackHome.setSize(imBtBack.getWidth(),imBtBack.getHeight());
//        gBtBackHome.setPosition(gAsk.getWidth()-10,25, Align.bottomRight);
//        imBtBack.setPosition(0,0);
//        Image imiconBtBack = UI.NewImage(Loader.GetTexture("Pause_UI_3"), gBtBackHome);
//        lbBackHome= UI.NewLabel("backhome",true, Color.WHITE,1,gBtBackHome.getWidth(),gBtBackHome.getHeight()/2+5, Align.right,gBtBackHome.getWidth()*0.75f,gBtBackHome.getHeight()-15,gBtBackHome);
//        imiconBtBack.setPosition(10,gBtBackHome.getHeight()/2+5, Align.left);
//
//        gBtBackHome.setOrigin(Align.center);
//        gBtBackHome.setScale(0.6f);
//        gBtBackHome.addListener( new ClickEvent(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                ClickEvent.ScaleSmooth(gBtBackHome,0.6f,0.6f,0.05f);
//                return true;
//            }
//
//            @Override
//            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                HidePanelAsk();
//                GameController.instance.BackHome();
//                super.touchUp(event, x, y, pointer, button);
//            }
//        });
//
//        gBtRestart=new Group();
//        gAsk.addActor(gBtRestart);
//        Image imBtRestart = UI.NewImage(Loader.GetTexture("BtGreen"), gBtRestart);
//        gBtRestart.setSize(imBtRestart.getWidth(),imBtRestart.getHeight());
//        gBtRestart.setPosition(gAsk.getWidth()-10,25, Align.bottomRight);
//        imBtRestart.setPosition(0,0);
//        Image imiconBtRestart = UI.NewImage(Loader.GetTexture("IconRestart"), gBtRestart);
//        lbRestart= UI.NewLabel("restart",true, Color.WHITE,1,gBtRestart.getWidth(),gBtRestart.getHeight()/2+2, Align.right,gBtBackHome.getWidth()*0.79f,gBtBackHome.getHeight()-15,gBtRestart);
//        imiconBtRestart.setPosition(10,gBtRestart.getHeight()/2+5, Align.left);
//        gBtRestart.setOrigin(Align.center);
//        gBtRestart.setScale(0.6f);
//        gBtRestart.addListener( new ClickEvent(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                ClickEvent.ScaleSmooth(gBtRestart,0.6f,0.6f,0.05f);
//                return true;
//            }
//
//            @Override
//            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                MyGame.sdk.TrackLevelStart(LevelMap.instance.getIndexChoseLevel());
//                HidePanelAsk();
//                AudioManager.instance.StopAudio("BossBattle");
//                GameController.instance.RestartGame();
//                super.touchUp(event, x, y, pointer, button);
//            }
//        });
//
//        gBtExit=new Group();
//        gAsk.addActor(gBtExit);
//        Image imBtExit = UI.NewImage(Loader.GetTexture("BtRed"),gBtExit);
//        gBtExit.setSize(imBtExit.getWidth(),imBtExit.getHeight());
//        gBtExit.setPosition(gAsk.getWidth()-10,25, Align.bottomRight);
//        imBtExit.setPosition(0,0);
//        Image imiconimBtExit = UI.NewImage(Loader.GetTexture("Pause_UI_3"), gBtExit);
//        lbExit= UI.NewLabel("exit",true, Color.WHITE,1,gBtExit.getWidth(),gBtExit.getHeight()/2+5, Align.right,gBtBackHome.getWidth()*0.75f,gBtBackHome.getHeight()-15,gBtExit);
//        imiconimBtExit.setPosition(10,gBtRestart.getHeight()/2+5, Align.left);
//        gBtExit.setOrigin(Align.center);
//        gBtExit.setScale(0.6f);
//        gBtExit.addListener( new ClickEvent(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                ClickEvent.ScaleSmooth(gBtExit,0.6f,0.6f,0.05f);
//                return true;
//            }
//
//            @Override
//            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                System.exit(0);
//                HidePanelAsk();
//                super.touchUp(event, x, y, pointer, button);
//            }
//        });
//
//        gCancle=new Group();
//        gAsk.addActor( gCancle);
//        Image imBtCancle = UI.NewImage(Loader.GetTexture("BtOrgange"), gCancle);
//        gCancle.setSize(imBtCancle.getWidth(),imBtCancle.getHeight());
//        gCancle.setPosition(10,25);
//        imBtCancle.setPosition(0,0);
//        Image imiconimBtCancle = UI.NewImage(Loader.GetTexture("IconCancle"), gCancle);
//        lbCancle= UI.NewLabel("cancel",true, Color.WHITE,1, gCancle.getWidth(), gCancle.getHeight()/2+5, Align.right, gBtBackHome.getWidth()*0.75f,gBtBackHome.getHeight()-15, gCancle);
//        imiconimBtCancle.setPosition(10, gCancle.getHeight()/2+5, Align.left);
//        gCancle.setOrigin(Align.center);
//        gCancle.setScale(0.6f);
//        gCancle.addListener( new ClickEvent(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                ClickEvent.ScaleSmooth(gCancle,0.6f,0.6f,0.05f);
//                return true;
//            }
//
//            @Override
//            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                HidePanelAsk();
//                if(whatShowPanelAsk==3) GameController.instance.getMyWorldScreen().Pause(false,false);
//                super.touchUp(event, x, y, pointer, button);
//            }
//        });
//
//        gBtRate = new Group();
//
//        gAsk.addActor(gBtRate);
//        Image imrate = UI.NewImage(Loader.GetTexture("Botton_starcam"), 0, 0, Align.bottomLeft, gBtRate);
//        gBtRate.setSize(imBtCancle.getWidth(), imBtCancle.getHeight());
//        gBtRate.setPosition(10,25);
//        gBtRate.setScale(0.65f);
//        lbRate = UI.NewLabel("lbRate",true, Color.WHITE, 0.7f, gBtRate.getWidth() / 2+35  , gBtRate.getHeight() / 2+5, Align.center,gBtRate.getWidth()*0.72f,gBtRate.getHeight(),  gBtRate);
//
//
//        gBtRate.setOrigin(Align.center);
//        gBtRate.addListener(new ClickEvent() {
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                ClickEvent.ScaleSmooth(gBtRate, 0.65f, 0.65f, 0.05f);
//                return super.touchDown(event, x, y, pointer, button);
//            }
//
//            @Override
//            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//                super.touchUp(event, x, y, pointer, button);
//                MyGame.sdk.Rate();
//            }
//        });
//        HidePanelAsk();

    }
    public void ShowPanelAsk(int what){
        gBigAsk.setVisible(true);
        gBtBackHome.setVisible(false);
        gBtRestart.setVisible(false);
        gBtExit.setVisible(false);
        gBtRate.setVisible(false);
        gCancle.setPosition(10,25);
        whatShowPanelAsk=what;
        lbAsk.setFontScale(1f);
        ClickEvent.ScaleSmooth(gAsk,1,1,0.05f);
        switch (what){
            case 1://backhome
                lbAsk.setText(Language.GetLang("DoYouWantToBackHome"));
                gBtBackHome.setVisible(true);
                break;
            case 2://Restart
                lbAsk.setText(Language.GetLang("DoYouWantToRestartTheGame"));
                gBtRestart.setVisible(true);
                break;
            case 3://Exit
                //GameController.instance.getMyWorldScreen().Pause(true,false);
                lbAsk.setText(Language.GetLang("DoYouWantToExitTheGame"));
                gBtExit.setVisible(true);
                break;
            case 4://Rate
               // MyGame.sdk.ShowFullscreen();
                lbAsk.setFontScale(0.75f);
                gCancle.setPosition(gAsk.getWidth()-10,25, Align.bottomRight);
                lbAsk.setText(Language.GetLang("CompleteAllMap"));
                gCancle.setVisible(true);
                gBtRate.setVisible(true);
                break;
        }
    }
    public void HidePanelAsk(){
        ClickEvent.ScaleSmooth(gAsk,1,0,0.05f);
        gBtBackHome.setVisible(false);
        gBtRestart.setVisible(false);
        gBtExit.setVisible(false);
        gBigAsk.setVisible(false);
    }

    @Override
    public void show() {

    }

    public void Update(float delta) {
        stage.act(delta);
        if(Gdx.input.isKeyJustPressed(Input.Keys.BACK))CheckBackHandle();
        if (FadeIn) {

            if (imgTransistion.getColor().a >= 1) {
                FadeIn = false;
                imgTransistion.setColor(1, 1, 1, 1);
                OpenWhatScreen();
                Transistion(false, true);
                imgTransistion.setColor(1, 1, 1, 1);
            } else {
                float a = imgTransistion.getColor().a + ((1f+plusTimeFade) * delta);
                imgTransistion.setColor(1, 1, 1, a);
            }
        }
        if (FadeOut) {
            if (imgTransistion.getColor().a <= 0) {
                FadeOut = false;
                imgTransistion.setColor(1, 1, 1, 0);
                imgTransistion.setTouchable(Touchable.disabled);
                imgTransistion.setVisible(false);
                setStopBackHandle(false);
                plusTimeFade=0;
            } else {
                float a = imgTransistion.getColor().a - ((1f+plusTimeFade) * delta);
                imgTransistion.setColor(1, 1, 1, a);
            }
        }
        if (startNotify) {
            if (gNotify.getY(Align.center) >= posMoveNotify) {
                startNotify = false;
                gNotify.setVisible(false);
                gNotify.setScale(0);
            } else {
                gNotify.setY(gNotify.getY() + (250 * delta));
            }
        }

//        lbFPS.setText("FPS :" + Gdx.graphics.getFramesPerSecond());
    }

    @Override
    public void render(float delta) {
       stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() { stage.dispose(); }

    public void ShowNotify(String textNoti, Color color) {
        if(lbNotify==null)return;
        lbNotify.getStyle().fontColor=color;
        lbNotify.setText("" + textNoti);
        posMoveNotify=stage.getHeight()/2+500;
        gNotify.setPosition(stage.getWidth()/2, stage.getHeight()*0.4f, Align.center);
        gNotify.setVisible(true);
        gNotify.setScale(1);
        startNotify=true;

    }

    public void AddToGame(int layer, Actor actor)
    {
        Group gr = (Group)(game.getChildren().get(layer));
        gr.addActor(actor);
    }
    public void AddToBackground(Actor actor)
    {
        background.addActor(actor);
    }
    public void AddToUI(Actor actor)
    {
        ui.removeActor(actor);
        ui.addActor(actor);
    }
    public void FinishOpenScreen(int whatScreen){
        plusTimeFade=0;
        setStopBackHandle(true);
        openScreenCase=whatScreen;
        if(openScreenCase==3)plusTimeFade=0.5f;
        imgTransistion.setTouchable(Touchable.enabled);
        imgTransistion.setVisible(true);
        Transistion(true,false);
        AudioManager.instance.StopAudio("gameover");
        AudioManager.instance.StopAudio("gamewin");
    }
    void Transistion(boolean fadeIn,boolean fadeOut)
    {
        FadeIn=fadeIn;
        FadeOut=fadeOut;
        if(FadeIn){
            imgTransistion.setColor(1,1,1,0);
            imgTransistion.setVisible(true);
            imgTransistion.setTouchable(Touchable.enabled);
        }
    }
    void OpenWhatScreen(){
        LevelMap.instance.HideScene();
        switch (openScreenCase){
            case 1://
                levelMap.Show();
                levelMap.UnlockNextLevel();
                //GamePlayUI.instance.Restart();
               // GameController.instance.RestartAllScreen();
               // WinLose.instance.HideScene();
               // GameController.instance.setCoinTakeInOneGame(0);
                break;
            case 2://changeMap
                GameController.instance.StartChangeMap();
                break;

        }
    }

    public void GameOver(boolean gameover){

//        if(gameover) {
//            gameOver.Show();
//        }else{
//            gameOver.Hide();;
//        }
    }

    public static void ScaleSmooth(final Actor im){

        GAction.ScaleTo(im, 1.2f, 1.2f, 0.05f, Interpolation.linear, new Action() {
            @Override
            public boolean act(float delta) {
                actor.clearActions();
                GAction.ScaleTo(im, 0.9f, 0.9f, 0.05f, Interpolation.linear, new Action() {
                    @Override
                    public boolean act(float delta) {
                        actor.clearActions();
                        GAction.ScaleTo(im, 1.1f, 1.1f, 0.05f, Interpolation.linear, new Action() {
                            @Override
                            public boolean act(float delta) {
                                actor.clearActions();
                                GAction.ScaleTo(im, 1f, 1f, 0.05f, Interpolation.linear, new Action() {
                                    @Override
                                    public boolean act(float delta) {
                                        actor.clearActions();
                                        return false;
                                    }
                                });
                                return false;
                            }
                        });
                        return false;
                    }
                });
                return false;
            }
        });

    }
    private void CheckBackHandle(){
        if(isStopBackHandle())return;
        if(SceneShow.size()>0) {
           SceneShow.get(SceneShow.size()-1).HideMyScene();
        }else{
            if(isBackHomeIsShow()){
                if(backHomeScene!=null){
                    backHomeScene.HideMyScene();
                    //GameController.instance.BackHome();
                }
            }else if(isLevelSceneIsShow()){
                //LevelMap.instance.BackMenu();
            }else if(isStartMenuIsShow()){
                ShowPanelAsk(3);
            }else if(GamePlayUI.instance.isStartGame()&&FadeIn==false&&FadeOut==false){
                GamePlayUI.instance.PauseGame();
            }
        }
    }
    public void addScreenShow(ScreenView show){
        if(SceneShow.contains(show)==false)SceneShow.add(show);
    }
    public void hideScreenShow(ScreenView show){
      if(SceneShow.size()>0&&SceneShow.contains(show))SceneShow.remove(show);
    }
    public boolean isStartMenuIsShow() {
        return startMenuIsShow;
    }

    public void setStartMenuIsShow(boolean startMenuIsShow) {
        this.startMenuIsShow = startMenuIsShow;
    }
    public boolean isLevelSceneIsShow() {
        return levelSceneIsShow;
    }

    public void setLevelSceneIsShow(boolean levelSceneIsShow) {
        this.levelSceneIsShow = levelSceneIsShow;
    }
    public boolean isBackHomeIsShow() {
        return backHomeIsShow;
    }

    public void setBackHomeIsShow(boolean backHomeIsShow, ScreenView backhomescreen) {
        backHomeScene=backhomescreen;
        this.backHomeIsShow = backHomeIsShow;
    }
    public boolean isStopBackHandle() {
        return stopBackHandle;
    }

    public void setStopBackHandle(boolean stopBackHandle) {
        this.stopBackHandle = stopBackHandle;
    }
    public boolean isStartCountToPlay() {
        return startCountToPlay;
    }

    public void setStartCountToPlay(boolean startCountToPlay) {
        this.startCountToPlay = startCountToPlay;
    }

}
