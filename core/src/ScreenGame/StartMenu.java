package ScreenGame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import Controller.Game.AudioManager;
import Controller.GameData;
import GameGDX.ClickEvent;
import GameGDX.GDX;
import GameGDX.GSpine.GSpine;
import GameGDX.GSpine.spine.AnimationState;
import GameGDX.Language;
import GameGDX.Loader;
import GameGDX.Particle;
import GameGDX.ScreenView;
import GameGDX.UI;

public class StartMenu extends ScreenView {
    public  static StartMenu instance;
    Group gTouchPlay;
    Image imTouchPlay;
    Label lbMyMoney;
    int MyMoney=0;
    GSpine menuSpine;
    Image imageBg,imCloud;
    Group gBtIcon;
    boolean startAnimtion;
    int caseAnim;
    AnimationState.TrackEntry tracks;

    Label lbTimeSpin,lbTimeGift;
    Image btGift,spinShi;
    GSpine spineGift;
    Particle giftready;
    boolean startPlusPoint;
    float countPlusPoint;
    boolean isShow=false;
    Group gSpin,gGift;
    boolean startScaleBt;
    boolean scaleIn,scaleOut;
    public StartMenu(Stage stage) {
        super(stage);
        instance = this;
        imageBg = UI.NewImage(Loader.GetTexture("Start_BG"), this);
        imageBg.setOrigin(Align.center);
        imageBg.setScale(getWidthBG());
//        imCloud = UI.NewImage(Loader.GetTexture("Start_Cloud"), this);
//        imCloud.setPosition(this.getWidth() + 5, this.getHeight() / 2, Align.bottomLeft);
//        menuSpine = new GSpine("skeleton", this.getWidth() / 2, this.getHeight() / 2, Align.bottomLeft, this, "animation1");
//        menuSpine.setScale(1);
//        menuSpine.setVisible(false);

        creatUI();
        ShowScene();
        scaleIn=true;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                startAnimtion = true;
                startScaleBt=true;
            }
        }, 0.3f);
        //UI.NewImage(Loader.GetTexture("SpiderLine"),this.getWidth()/2,this.getHeight()/2,Align.center,this);
        //new GSpine("ExplodeKittenScene1",this.getWidth()/2,this.getHeight()/2,Align.center,this,"explode_scene1");
    }

    void creatUI(){
        gBtIcon=new Group();
        this.addActor(gBtIcon);
        gBtIcon.setSize(this.getWidth(),this.getHeight());

        gTouchPlay=new Group();
        gBtIcon.addActor(gTouchPlay);
        gTouchPlay.setSize(this.getWidth()*0.3f,this.getHeight()*0.1f);
        gTouchPlay.setPosition(this.getWidth()/2,this.getHeight()*0.3f, Align.center);
        imTouchPlay= UI.NewImage(Loader.GetTexture(Language.GetLang("TouchPlay")),gTouchPlay.getWidth()/2,gTouchPlay.getHeight()/2, Align.center,gTouchPlay);
        imTouchPlay.setOrigin(Align.center);
        Language.AddLang("TouchPlay", new Runnable() {
            @Override
            public void run() {
                imTouchPlay.setDrawable(new TextureRegionDrawable(Loader.GetTexture( Language.GetLang("TouchPlay"))));
            }
        });
        gTouchPlay.setOrigin(Align.center);
        gTouchPlay.addListener( new ClickEvent(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ClickEvent.ScaleSmooth(gTouchPlay,1,1,0.05f);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                HideScene();
                LevelMap.instance.Show();
                super.touchUp(event, x, y, pointer, button);
            }
        });


    }
    @Override
    public void act(float delta) {
        super.act(delta);
        if(isStop())return;

    }
    public void HideScene(){
       UIScene.instance.setStartMenuIsShow(false);
//        spineGift.mySpine.setStop(true);
        //menuSpine.mySpine.setStop(true);
     //   giftready.Start(false);
        startAnimtion=false;
       // if(caseAnim==3)menuSpine.setAnim("animation2",false);
        Hide();
        setStop(true);
//        if(LuckySpin.instance==null)return;
//        LuckySpin.instance.setStop(true);
//        DailyGift.instance.setStop(true);

    }
    public void ShowScene(){
//        MyGame.sdk.ShowBanner(true);
//        MyGame.sdk.TrackCustomEvent("MenuScreen");
        UIScene.instance.setStartMenuIsShow(true);
//        spineGift.mySpine.setStop(false);
//        menuSpine.mySpine.setStop(false);
//        imTouchPlay.setDrawable(new TextureRegionDrawable(Loader.GetTexture(Language.GetLang("TouchPlay"))));
      setMyMoney();
        if(caseAnim>=2)startAnimtion=true;
        if(caseAnim==3)menuSpine.setAnim("animation2",true);
        Show();
        AudioManager.instance.PlayAudio("Menu");
        setStop(false);
//        if(LuckySpin.instance==null)return;
//        LuckySpin.instance.setStop(false);
//        LuckySpin.instance.CheckTime();
//        DailyGift.instance.setStop(false);
//        DailyGift.instance.CheckTime();

    }
    public void setMyMoney(){
        MyMoney= GameData.Instance.GetMyMoney();

    }
    public void PlusMoney(int money){
        countPlusPoint=MyMoney+money;
        GameData.Instance.SetMyMoney(MyMoney+money);
        startPlusPoint=true;
    }
    public void setUnLockGift(boolean set){
        if(set==false){
            giftready.setVisible(false);
            giftready.Start(false);
            btGift.setVisible(false);
            spineGift.setAnim("animation",true);
            spineGift.setVisible(true);
        }else{
            giftready.setVisible(true);
            giftready.Start(true);
            btGift.setVisible(true);
            spineGift.setAnim("animation",false);
            spineGift.setVisible(false);
        }
    }
    @Override
    public void ShowMyScene() {

    }

    @Override
    public void HideMyScene() {

    }
    public void InitFireWork(Vector2 pos,int whatFW){
        Particle particle = new Particle("Fireworks"+whatFW, this);
        particle.setPOS(pos.x, pos.y, 0);
        particle.Loop(false);
        particle.Start(true);
    }
}
