package ScreenGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Controller.Game.AudioManager;
import Controller.GameController;
import Controller.GameData;
import GameGDX.ClickEvent;
import GameGDX.GAction;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.Particle;
import GameGDX.ScreenView;
import GameGDX.UI;
import Model.Level;
import Model.MapLevel;

public class LevelMap extends ScreenView {


    public static LevelMap instance;
    ScrollPane scrollLevelMap;
    Table tableLevelMap;
    ArrayList<Level> listLevelMap;
    ArrayList<MapLevel> listMap;
    int indexLevelUnlock=0;

    int indexChoseLevel=0;
    int indexScroll=0;
    float AmountscrollX=0;
    Image imBtScrollLeft,imBtScrollRight;
    int CountMap=0;
    Group gLocalLevel;
    Image imageLocalLevel;
    boolean legalUnlockLevel;
    boolean startMoveToLevel;
    boolean startAnmLocal=false;
    int directionLocal=1;
    float timetoLocal=0.6f,timetoWait=0.1f;
    Vector2 posLevel;


    private boolean readyNextMap;
    int caseNextMap=0;
    private  boolean isUnlock=false;

    public boolean isDrag() {
        return isDrag;
    }

    public void setDrag(boolean drag) {
        isDrag = drag;
    }

    private boolean isDrag=false;
    public LevelMap(Stage stage) {
        super(stage);
        instance=this;
        //setTextureOverlya(Loader.GetTexture("BackGround_1"));
        setOverlay(1f);
        indexLevelUnlock= GDX.GetPrefInteger("LevelIndex",0);
        if(indexLevelUnlock==0){
            indexLevelUnlock=1;
        }
        creatScrollView();
        LoadData();
        checkEnableScroll();
        gLocalLevel=new Group();
        listMap.get(getIndexMap()).addChild(gLocalLevel);
        gLocalLevel.setSize(5,5);
        gLocalLevel.setPosition(this.getWidth()/2,this.getHeight()/2);
        Image imLocal= UI.NewImage(Loader.GetTexture("Local_Charac"),gLocalLevel.getWidth()/2,0, Align.bottom,gLocalLevel);
        imLocal.setTouchable(Touchable.disabled);
        imageLocalLevel= UI.NewImage(Loader.GetTexture("Av_Matino"),gLocalLevel.getWidth()/2,imLocal.getHeight()*0.08f, Align.bottom,gLocalLevel);
        imageLocalLevel.setTouchable(Touchable.disabled);
        imageLocalLevel.setOrigin(Align.center);
        imageLocalLevel.setScale(0.45f);
        listMap.get(listLevelMap.get(indexLevelUnlock-1).getIndexMap()).addActor(gLocalLevel);
        gLocalLevel.setPosition(listLevelMap.get(indexLevelUnlock-1).getPos().x,listLevelMap.get(indexLevelUnlock-1).getPos().y);

        final Group gbtBack=new Group();
        this.addActor(gbtBack);
        Image btBackHome= UI.NewImage(Loader.GetTexture("btBackHome"),0,0, Align.bottom,gbtBack);
        gbtBack.setSize(btBackHome.getWidth(),btBackHome.getHeight());
        gbtBack.setPosition(btBackHome.getWidth()+15,this.getHeight()-15, Align.top);
        gbtBack.setOrigin(Align.center);
        gbtBack.addListener( new ClickEvent(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if(readyNextMap==false&&startMoveToLevel==false){
                    ClickEvent.ScaleSmooth(gbtBack,1,1,0.05f);
                }
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                BackMenu();
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    @Override
    public void act(float delta) {
        if(isStop())return;
        setDrag(scrollLevelMap.isPanning());
        if(startMoveToLevel){
            if(gLocalLevel.getX(Align.center)==posLevel.x){
                startMoveToLevel=false;
                isUnlock=false;
            }
        }
        if(startAnmLocal&&startMoveToLevel==false){
            if(timetoLocal>0){
                timetoLocal-=delta;
                gLocalLevel.setY(gLocalLevel.getY()+(directionLocal*(35*delta)));

            }else{
                directionLocal=-directionLocal;
                timetoLocal=0.6f;
            }
        }
        if(readyNextMap){
            if(caseNextMap==0){
                int nextMap=listLevelMap.get(indexLevelUnlock).getIndexMap();
                boolean haveDoor=listMap.get(nextMap).isHaveDoor();
                float scrollX= (float) (((nextMap)*(0.24))-0.11f);
                scrollLevelMap.setScrollPercentX(scrollX);
                if(haveDoor){
                    listMap.get(nextMap).OpenDoor(true);
                    caseNextMap=1;
                    listMap.get(nextMap).addActor(gLocalLevel);
                    gLocalLevel.setX(-150);
                }
            }
            if(caseNextMap==2){
                if(gLocalLevel.getX()<0){
                    gLocalLevel.setX(gLocalLevel.getX()+(50*delta));
                }else{
                    readyNextMap=false;
                    legalUnlockLevel=true;
                    caseNextMap=0;
                    EndSetStar();
                }
            }
        }

        super.act(delta);
    }
    void creatScrollView(){
        listLevelMap=new ArrayList<>();
        listMap=new ArrayList<>();
        tableLevelMap=new Table();
        tableLevelMap.setSize(this.getWidth(),this.getHeight());
        tableLevelMap.setPosition(this.getWidth()/2,this.getHeight()/2, Align.center);
        tableLevelMap.align(Align.topLeft);

        scrollLevelMap = UI.newScroll(tableLevelMap, this.getWidth() / 2, this.getHeight() / 2, Align.center, this.getWidth(), this.getHeight(), this);
        scrollLevelMap.setScrollingDisabled(false, true);
        scrollLevelMap.setOrigin(Align.center);
        scrollLevelMap.setOverscroll(false,false);
        indexLevelUnlock= GDX.GetPrefInteger("LevelIndex",0);
        scrollLevelMap.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                setDrag(true);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                setDrag(false);
                super.touchUp(event, x, y, pointer, button);
            }
        });

    }

    void LoadData(){
            String path = "LevelMap.txt";
            FileHandle file = Gdx.files.internal(path);
            FileReader fr = null;
            BufferedReader br = null;
            int countMap= 0;
            String line = null;
            try {
                // fr = new FileReader(file);
                br = new BufferedReader(file.reader());
                line = br.readLine();
                CountMap = Integer.parseInt(line);

                String[] listSplit=null;
                String strGet="";
                for (int i = 0; i < CountMap; i++) {
                    line = br.readLine();
                    line=br.readLine();
                    if(line==null) return;
                    if(line.equals("-MapLevel:")) {
                        line = br.readLine();
                        listSplit = line.split("HaveDoor:");
                        Boolean haveDoor=Boolean.parseBoolean(listSplit[1]);
                        line = br.readLine();
                        listSplit = line.split("Effect:");
                        String nameEffect=null;
                        if(listSplit.length>1){
                            nameEffect=listSplit[1];
                        }
                        line = br.readLine();
                        listSplit = line.split("NameTextureMap:");
                        strGet = listSplit[1];
                        String nameTextureMap = strGet;
                        MapLevel mapLevel = new MapLevel(nameTextureMap,haveDoor,nameEffect,this.getWidth(),this.getHeight());
                        listMap.add(mapLevel);
//                        tableLevelMap.add(mapLevel).width(this.getWidth()).height(this.getHeight());
                        tableLevelMap.add(mapLevel);
                        line = br.readLine();
                        if (line.equals("LevelChild:")) {
                            line = br.readLine();
                            listSplit = line.split("@CountChild:");
                            int countChild = Integer.parseInt(listSplit[1]);
                            if(countChild>0) {
                                for (int y = 0; y < countChild; y++) {
                                    line = br.readLine();
                                    if (line.equals("@Child:")) {
                                        line = br.readLine();
                                        listSplit=line.split("NameLoad:");
                                        String nameLoad=listSplit[1];
                                        line = br.readLine();
                                        listSplit=line.split("NameShow:");
                                        String nameShow=listSplit[1];
                                        line = br.readLine();
                                        listSplit=line.split("Position:");
                                        listSplit=listSplit[1].split(",");
                                        Vector2 Positon=new Vector2(Float.parseFloat(listSplit[0]),Float.parseFloat(listSplit[1]));
                                        line = br.readLine();
                                        listSplit=line.split("StarPoint:");
                                        int StarPoint=Integer.parseInt(listSplit[1]);
                                        line = br.readLine();
                                        listSplit=line.split("ChildSpecial:");
                                        boolean childSpecial=Boolean.parseBoolean(listSplit[1]);
                                        line = br.readLine();
                                        listSplit=line.split("SpecialType:");
                                        int SpecialType=Integer.parseInt(listSplit[1]);
                                        line = br.readLine();
                                        listSplit=line.split("NameAvatar:");
                                        String nameAvatar="";
                                        if(listSplit.length>1&&listSplit[1]!=null)nameAvatar=listSplit[1];
                                        Level level=new Level(Positon,nameLoad,nameShow,mapLevel,childSpecial,SpecialType,nameAvatar,mapLevel,i,y);
                                        listLevelMap.add(level);

                                    }
                                }
                            }

                        }

                    }
                    AmountscrollX=1f/(CountMap-1);
                }
                for(int j=0;j<indexLevelUnlock;j++){
                    UnlockLevel(j,false);
                }


            } catch (FileNotFoundException e) {
                GDX.Show("FILENot"+e);
                e.printStackTrace();
            } catch (IOException e) {
                GDX.Show("Not");
                e.printStackTrace();
            }
    }
    public void FinishLevel(){
        if(indexLevelUnlock<listLevelMap.size()) {
            if(getIndexChoseLevel()==indexLevelUnlock) {
                isUnlock=true;
                listLevelMap.get(indexLevelUnlock).setPrefUnlock();
                if(indexLevelUnlock%10==0){
                    caseNextMap=0;
                    readyNextMap=true;
                }else{
                    legalUnlockLevel=true;
                }
            }
          //  listLevelMap.get(getIndexChoseLevel()-1).setNewStar(GameController.instance.getCountStar());
        }else{
           // listLevelMap.get(indexLevelUnlock-1).setNewStar(GameController.instance.getCountStar());
        }
    }
  protected void UnlockNextLevel(){
        if(indexLevelUnlock<listLevelMap.size()) {
           // listLevelMap.get(getIndexChoseLevel()-1).setStar(GameController.instance.getCountStar(),true);
        }else{
            if(getIndexChoseLevel()==indexLevelUnlock&& GameData.Instance.GetCompleteAll()==0){
                UIScene.instance.ShowPanelAsk(4);
                GameData.Instance.SetCompleteAll();
            }
           // listLevelMap.get(indexLevelUnlock-1).setStar(GameController.instance.getCountStar(),true);
        }
    }
    private void UnlockNextLevelSilent(){
        if(indexLevelUnlock<listLevelMap.size()) {
          //  listLevelMap.get(getIndexChoseLevel()-1).setStar(GameController.instance.getCountStar(),false);
        }else{
            if(getIndexChoseLevel()==indexLevelUnlock&& GameData.Instance.GetCompleteAll()==0){
                UIScene.instance.ShowPanelAsk(4);
                GameData.Instance.SetCompleteAll();
            }
            //listLevelMap.get(indexLevelUnlock-1).setStar(GameController.instance.getCountStar(),false);
        }
    }
    public void EndSetStar() {
        if (legalUnlockLevel) {
            legalUnlockLevel = false;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    UnlockLevel(indexLevelUnlock ,true);
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            startMoveToLevel=true;
                            posLevel = listLevelMap.get(indexLevelUnlock).getPos();
                            GAction.MoveTo(gLocalLevel, posLevel.x, posLevel.y, Align.center, 0.5f, Interpolation.linear, null);
                        }

                    }, 0.8f);
                }

            }, 1.5f);
        }
    }
    void UnlockLevel(int index,boolean sound){
        listLevelMap.get(index).UnlockLevel(true,sound);
    }

    void ScrollButton(int Direction){
        float cal=(scrollLevelMap.getScrollPercentX()+(Direction*AmountscrollX));
        cal=(int)(((cal*scrollLevelMap.getPrefWidth())/1280));
        cal=(cal)/(CountMap-1);
        scrollLevelMap.setScrollPercentX(cal) ;
        checkEnableScroll();
    }
    void checkEnableScroll(){
    }
    int getIndexMap(){
        int indexLvUnlock=indexLevelUnlock-1;
        if(indexLvUnlock<0)indexLvUnlock=0;
        return listLevelMap.get(indexLvUnlock).getIndexMap();
    }
     public void HideScene(){
                 Hide();
     }
    @Override
    public void Show() {
//        MyGame.sdk.TrackCustomEvent("LevelMapScreen");
        UIScene.instance.setLevelSceneIsShow(true);
        if(legalUnlockLevel)UnlockNextLevelSilent();
//        WinLose.instance.setUseCoinRevive(false);
        for (int i=0;i<listMap.size();i++){
            listMap.get(i).setStop(false);
        }
       // GameController.instance.PlayMusicGamePlay(false);
        super.Show();
        startAnmLocal=true;
        int nextMap=listLevelMap.get(indexLevelUnlock-1).getIndexMap();
        final float scrollX= (float) (((nextMap)*(0.24f)));
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
               if(readyNextMap==false)scrollLevelMap.setScrollPercentX(scrollX);
            }

        }, .1f);

    }

    @Override
    public void Hide() {
        super.Hide();
        UIScene.instance.setLevelSceneIsShow(false);
        for (int i=0;i<listMap.size();i++){
            listMap.get(i).setStop(true);
        }
        startAnmLocal=false;
    }
    public int getIndexChoseLevel() {
        return indexChoseLevel;
    }

    public void setIndexChoseLevel(int indexChoseLevel) {
        indexLevelUnlock= GDX.GetPrefInteger("LevelIndex", 0);
        this.indexChoseLevel = indexChoseLevel;
    }
    public void InitStarPoin(Vector2 pos){
        AudioManager.instance.PlayAudioRand(1,3,"Star_");
        int nextMap=listLevelMap.get(legalUnlockLevel?indexLevelUnlock-1:getIndexChoseLevel()-1).getIndexMap();
        Particle particle = new Particle("StarPoin", listMap.get(nextMap));
        particle.setPOS(pos.x, pos.y, 0);
        particle.Loop(false);
        particle.Start(true);
    }
    public void InitUnlockLevel(Vector2 pos){
        AudioManager.instance.PlayAudio("Unlock");
        int nextMap=listLevelMap.get(indexLevelUnlock).getIndexMap();
        Particle particle = new Particle("UnlockLevel", listMap.get(nextMap));
        particle.setPOS(pos.x, pos.y, 0);
        particle.Loop(false);
        particle.Start(true);
    }
    public void NextCaseMap(){
        caseNextMap+=1;
    }
    public boolean isUnlock() {
        return isUnlock;
    }

    public void setUnlock(boolean unlock) {
        isUnlock = unlock;
    }
    public boolean isReadyNextMap() {
        return readyNextMap;
    }

    public void setReadyNextMap(boolean readyNextMap) {
        this.readyNextMap = readyNextMap;
    }
    @Override
    public void ShowMyScene() {

    }

    @Override
    public void HideMyScene() {

    }
    public void BackMenu(){
        if(readyNextMap==false&&startMoveToLevel==false) {
            StartMenu.instance.ShowScene();
            HideScene();
        }
    }
}
