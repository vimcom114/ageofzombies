package ScreenGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.geda.game.MyGame;

import java.util.ArrayList;

import Controller.Game.InputManager;
import Controller.GameController;
import GameGDX.GSpine.spine.utils.TwoColorPolygonBatch;
import GameGDX.Loader;
import Controller.MyWorld;
import Tools.ParallaxBackground;
import Tools.TextureRegionParallaxLayer;
import Tools.Utils;


public class WorldScreen implements Screen {
    public SpriteBatch batch;
    private boolean isExtraScreen;
    private MyGame myGame;


    //basic playscreen variables
    private OrthographicCamera gamecam;

    private Viewport gamePort;

    Stage stage, stageBehind;
    private Group background;
    private Group layer_2, layer_0, layer_1;
    private Group ui;
    ParallaxBackground parallaxBackground;


    MyWorld myWorld;

    public boolean isStop() {
        return Stop;
    }

    boolean Stop = true;

    public boolean isPause() {
        return Pause;
    }

    public void setPause(boolean pause) {
        Pause = pause;
    }

    boolean Pause = false;


    Vector2 minPosCamera = new Vector2(0, 0);

    public InputManager getInputManager() {
        return inputManager;
    }

    public void setInputManager(InputManager inputManager) {
        this.inputManager = inputManager;
    }

    InputManager inputManager;

   // PlayScreenExtra playScreenExtra;

    boolean stopCamera = false;

    private boolean growStartMap=false;
    public Vector2 getSpawnPos() {
        if (indexSpawnPos >= spawnPos.size()) indexSpawnPos = spawnPos.size() - 1;
        return spawnPos.get(indexSpawnPos);
    }

    public void setSpawnPos(Vector2 spawnPos) {
        this.spawnPos.add(spawnPos);
    }

    ArrayList<Vector2> spawnPos;
    public int getIndexSpawnPos() {
        return indexSpawnPos;
    }

    public void setIndexSpawnPos(int indexSpawnPos) {
        if (indexSpawnPos < 0) indexSpawnPos = 0;
        this.indexSpawnPos = indexSpawnPos;

    }

    int indexSpawnPos = 1;


    boolean isWaitBigMario = false;
    boolean resetCamera = false;

    public WorldScreen(MyGame mygame, String name, float w, float h) {
        batch = new SpriteBatch();
//        MyGame.V_WIDTH=Gdx.graphics.getWidth();
//        MyGame.V_HEIGHT=Gdx.graphics.getHeight();
        this.myGame = mygame;
        spawnPos = new ArrayList<>();
        spawnPos.add(new Vector2(1f, 10f));
        gamecam = new OrthographicCamera();

        gamePort = new ExtendViewport(MyGame.V_WIDTH / MyGame.PPM, MyGame.V_HEIGHT / MyGame.PPM, gamecam);
        stage = new Stage(gamePort, new TwoColorPolygonBatch());
        stageBehind = new Stage(gamePort, new TwoColorPolygonBatch());

        background = new Group();
        layer_2 = new Group();
        layer_0 = new Group();
        layer_1 = new Group();
        ui = new Group();
        stageBehind.addActor(layer_2);
        stage.addActor(background);
        stage.addActor(layer_0);
        stage.addActor(layer_1);
        stage.addActor(ui);

        gamecam.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldHeight() / 2, 0);

        parallaxBackground = new ParallaxBackground();
        myWorld = new MyWorld(this, gamecam, stage, batch, name);
        inputManager = new InputManager();
        GameController.instance.setInputManager(getInputManager());
        minPosCamera.x = gamecam.viewportWidth * .5f;

    }

    public void createLayers(String Data) {
        String[] splitData = Data.split("#");

        for (int i = 0; i < splitData.length; i++) {
            String[] splitParallax = splitData[i].split(":");
            String nameTexture = splitParallax[0];

            int type = Integer.parseInt(splitParallax[1]);

            float paddingLeft = Float.parseFloat(splitParallax[2]);
            float paddingBot = Float.parseFloat(splitParallax[3]);
            float speed = Float.parseFloat(splitParallax[4]);
            float ratioX = Float.parseFloat(splitParallax[5]);
            float ratioY = Float.parseFloat(splitParallax[6]);
            float plusW = 0f;
            float plusH = 0f;
            boolean drawTop = false;

            if (splitParallax.length > 7) {
                plusW = Float.parseFloat(splitParallax[7]);
                plusH = Float.parseFloat(splitParallax[8]);
            }
            if (splitParallax.length == 10) {
                drawTop = Boolean.parseBoolean(splitParallax[9]);
            }
            TextureRegion RegionCLayer = Loader.GetTexture(nameTexture);
            TextureRegionParallaxLayer ParallaxLayer = new TextureRegionParallaxLayer(getMyWorld(), RegionCLayer, stage, new Vector2(ratioX, ratioY), Utils.WH.width, speed, type, paddingLeft, paddingBot, plusW, plusH);
            if (type != 4) {
                if (drawTop == false) {
                    parallaxBackground.addLayers(ParallaxLayer);
                } else {
                    parallaxBackground.addLayersTop(ParallaxLayer);

                }
            }
        }
//
//        TextureRegion skyRegionA = Loader.GetTexture("sky");
//        TextureRegionParallaxLayer skyLayerA = new TextureRegionParallaxLayer(skyRegionA, stage.getWidth(), new Vector2(.3f,.3f), Utils.WH.width);
//        TextureRegion cloudRegionB = Loader.GetTexture("cloud");
//        TextureRegionParallaxLayer cloudLayerB = new TextureRegionParallaxLayer(cloudRegionB, stage.getWidth(), new Vector2(.6f,.6f), Utils.WH.width);
//        cloudLayerB.setPadBottom(stage.getHeight()*.7f);


    }

    @Override
    public void show() {

    }

    public void handleInput() {
//        //control our player using immediate impulses
        if (inputManager.isKeyDown(Input.Keys.UP)) {
            myWorld.HandleInPut("UP", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.UP)) {
            myWorld.HandleInPut("UP", false);
        }
        if (inputManager.isKeyDown(Input.Keys.LEFT)) {
            myWorld.HandleInPut("LEFT", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.LEFT)) {
            myWorld.HandleInPut("LEFT", false);
        }
        if (inputManager.isKeyDown(Input.Keys.RIGHT)) {
            myWorld.HandleInPut("RIGHT", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.RIGHT)) {
            myWorld.HandleInPut("RIGHT", false);
        }
        if (inputManager.isKeyDown(Input.Keys.DOWN)) {
            myWorld.HandleInPut("DOWN", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.DOWN)) {
            myWorld.HandleInPut("DOWN", false);
        }
        if (inputManager.isKeyDown(Input.Keys.SPACE)) {
            myWorld.HandleInPut("SPACE", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.SPACE)) {
            myWorld.HandleInPut("SPACE", false);
        }
         if (inputManager.isKeyDown(Input.Keys.ENTER)) {
            myWorld.HandleInPut("ENTER", true);
        }
        if (inputManager.isKeyReleased(Input.Keys.ENTER)) {
            myWorld.HandleInPut("ENTER", false);
        }
    }

    public void update(float dt) {
        if (Pause == false) {
            handleInput();
            myWorld.Update(dt);
            stage.act(dt);
        }
        // inputManager.update();
    }

    @Override
    public void render(float delta) {
        //separate our update logic from render
        if (resetCamera) {
            if (stopCamera == false) {
                gamecam.position.x = gamecam.viewportWidth * .5f;
                setMinPosCamera(new Vector2(gamecam.viewportWidth * .5f, gamecam.viewportHeight * .5f));
                gamecam.update();
            }
            resetCamera = false;
        }

        if (Stop) return;
        update(delta);
        myWorld.Render(delta);
        stage.draw();
    }
    @Override
    public void resize(int width, int height) {
        //updated our game viewport
        //gamePort.update((int)(width/MyGame.PPM), (int)(height/MyGame.PPM));
        stage.getCamera().viewportWidth = (int) ((width + 300) / MyGame.PPM);
        stage.getCamera().viewportHeight = (int) ((height + 0) / MyGame.PPM);
        gamecam.update();
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        //dispose of all our opened resources
        myWorld.dispose();

    }

    public Stage getStage() {
        return stage;

    }

    public void setCameraPos(float x, float y) {
       // if (myWorld.stopMove) return;
        if (stopCamera == false) {
            moveCamera(x,y);
        }

    }
    public boolean moveCamera(float x, float y){
        boolean EndMap=false;
        if(gamecam==null||myWorld.getCreator()==null)return false;
//        if (gamecam.position.x > minPosCamera.x + gamecam.viewportWidth)
//            minPosCamera.x = gamecam.position.x;
//        if (gamecam.position.y > minPosCamera.y+ gamecam.viewportHeight)
//            minPosCamera.x = gamecam.position.x;
        gamecam.position.x = MathUtils.clamp(x, minPosCamera.x, myWorld.getCreator().getMapWidth());
        gamecam.position.y=MathUtils.clamp(y,  gamecam.viewportHeight*0.5f, myWorld.getCreator().getMapHeight());
        // gamecam.position.y = MathUtils.clamp(gamecam.position.y, gamePort.getWorldHeight() / 2f, 100 - gamePort.getWorldHeight() / 2f);
        gamecam.position.z = 0;
        return EndMap;
    }


    public void setStop(final boolean stop) {
        Stop = stop;

    }

    void Change() {
       // myWorld = playScreenExtra.myWorld;
    }

    public OrthographicCamera getGamecam() {
        return gamecam;
    }

    public void setGamecam(OrthographicCamera gamecam) {
        this.gamecam = gamecam;
    }

    public Viewport getGamePort() {
        return gamePort;
    }

    public void setGamePort(Viewport gamePort) {
        this.gamePort = gamePort;
    }

    public void StopAndChangeMap(String NameMap, boolean change, boolean stop, boolean isReturnPoin) {
        if (stop == false && change == true) {
            Stop = false;
            // spawnControll.clearList()
          //  Pause(true,true);
            myWorld.setNameMap(NameMap);
        }

    }

    public void Changetarget() {
        if (GameController.instance.getMyWorld() != null&& GameController.instance.getMyWorld().equals(myWorld)==false) {
//            boolean isBig = GameController.instance.getMyWorld().getPlayer().getIsBig();
//            if (isBig) {
//                if(myWorld.getPlayer().isRunGrowDownAnimation()&&myWorld.getPlayer().isTimeToRedefineMario()){
//                    myWorld.getPlayer().setWaitToBig(true);
//                }else {
//                    myWorld.getPlayer().GrowUp();
//                }
//            }else if (isBig==false&&myWorld.getPlayer().isMarioIsBig()){
//                myWorld.getPlayer().GrowDown();
//            }
        }


        //GameController.instance.setScreenExtra(isExtraScreen);
        GameController.instance.setMyWorld(myWorld);
        GameController.instance.setMyCamera(getGamecam());
        GameController.instance.setMyWorldScreen(this);
        Pause(false,false);
    }


    public void ReUseAll() {
        setIndexSpawnPos(0);
        myWorld.ReUseAll();
        ResetCamera();
    }

    public void ResetCamera() {
        resetCamera = true;
    }

    public boolean isStopCamera() {
        return stopCamera;
    }

    public void setStopCamera(boolean stopCamera) {
        this.stopCamera = stopCamera;
    }

    public void setSizeViewPort(float w, float h) {
        stage.getViewport().update((int) (w / MyGame.PPM), (int) (h / MyGame.PPM));
    }

    public float clampMove(float value, float min, float max) {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }

    public MyWorld getMyWorld() {
        return myWorld;
    }

    public void setMyWorld(MyWorld myWorld) {
        this.myWorld = myWorld;
    }

    public void ReviceMario() {
        setStop(false);
        myWorld.RevicePlayer();
    }

    public void addStage(Group actor) {
        stage.addActor(actor);
    }

    public void checkPoin(int index) {
        indexSpawnPos=index;
    }

    public boolean isWaitBigMario() {
        return isWaitBigMario;
    }

    public void setWaitBigMario(boolean waitBigMario) {
        isWaitBigMario = waitBigMario;
    }

    public Vector2 getMinPosCamera() {

        return minPosCamera;
    }

    public void setMinPosCamera(Vector2 minPosCamera) {

        this.minPosCamera = minPosCamera;
    }
    public float getLeftCameraX(){
        return gamecam.position.x-(stage.getViewport().getWorldWidth()*.5f);
    }
    public float getRightCameraX(){
        return gamecam.position.x+(stage.getViewport().getWorldWidth()*.5f);
    }
    public float getRightCameraPercentX(float percent){
        return gamecam.position.x+(stage.getViewport().getWorldWidth()*percent);
    }
    public float getTopCameraY(){
        return gamecam.position.y+(stage.getViewport().getWorldHeight()*.5f);
    }
    public float getBotCameraY(){
        return gamecam.position.y-(stage.getViewport().getWorldHeight()*.5f);
    }
    public void addToLayer(int layer, Group group){
        switch (layer){
            case 0:
                layer_0.addActor(group);
                break;
            case 1:
                layer_1.addActor(group);
                break;
            case 2:
                layer_2.addActor(group);
                break;
        }
    }
    public int setzIndexTopLayer(Group group){
        int myzindex=group.getZIndex();
        int index=layer_0.getChildren().size-1;
        group.setZIndex(index);
        return myzindex;
    }
    public Group getLayerGroup(int indexlayer){
        Group parent=null;
        switch (indexlayer){
            case 0:
                parent= layer_0;
            break;
            case 1:
                parent=layer_1;
            break;
            case 2:
                parent=layer_2;
            break;
        }
        return parent;
    }
    public void Pause(final boolean pause, final boolean notplaymusic) {
        Pause = pause;
       // if (notplaymusic == false) GamePlayUI.instance.StartGame(!pause);

    }
    public void PlayMusicGamePlay(boolean play){
//        if(play&&myWorld.caseVideo<0) AudioManager.instance.PlayAudio(myWorld.getNameMusicMap());
//        else AudioManager.instance.StopAudio(myWorld.getNameMusicMap());
    }
    public boolean isExtraScreen() {
        return isExtraScreen;
    }

    public void setExtraScreen(boolean extraScreen) {
        isExtraScreen = extraScreen;
    }
    public boolean isGrowStartMap() {
        return growStartMap;
    }

    public void setGrowStartMap(boolean growStartMap) {
        this.growStartMap = growStartMap;
    }
    public boolean checkOutCameraPoin(Vector2 objectPoin){
          boolean checkOut=false;
          if(objectPoin.x<getLeftCameraX()||objectPoin.x>getRightCameraX()||objectPoin.y<getBotCameraY()||objectPoin.y>getTopCameraY())checkOut=true;
          return checkOut;

    }

}
