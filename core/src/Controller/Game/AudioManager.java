package Controller.Game;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

import Controller.GameData;
import GameGDX.GDX;
import Model.AudioScource;

public class AudioManager extends Group {
    public static AudioManager instance;


    public List<String> listNameMusic;
    public List<String> listNameSound;
    public List<AudioScource> listaudio;



    private int valueMusic=0;
    private int valueSound=0;
    public AudioManager(){

         listNameMusic=new ArrayList<String>();
         listNameSound=new ArrayList<String>();
         listaudio=new ArrayList<AudioScource>();
         instance=this;
     }
     public void creatGAudio(){
        for(int i=0;i<listNameMusic.size();i++){
            String name=listNameMusic.get(i);
            AudioScource audio=new AudioScource(name,1);
            listaudio.add(audio);
        }
         for(int i=0;i<listNameSound.size();i++){
             String name=listNameSound.get(i);
             AudioScource audio=new AudioScource(name,2);
             listaudio.add(audio);
         }
         startGame();

     }
     public void addNameMusic(String name){
        listNameMusic.add(name);
     }
    public void addNameSound(String name){

        listNameSound.add(name);
    }

    public  void PlayAudio(String name){
        for (int i=0;i<listaudio.size();i++){
            if(listaudio.get(i).Name.equals(name)){
                listaudio.get(i).Play();
            }
        }
    }
    public  void StopAudio(String name){
        for (int i=0;i<listaudio.size();i++){
            if(listaudio.get(i).Name.equals(name)){
                listaudio.get(i).Stop();
            }
        }
    }
    public  void setLoop(String name,boolean loop) {
        for (int i = 0; i < listaudio.size(); i++) {
            if (listaudio.get(i).Name.equals(name)) {
                listaudio.get(i).setLoop(loop);
            }
        }

    }
    public  void PlayAudioRand(int from,int to,String name) {

        int rand = GDX.RandomInt(from, to);
        String Name = name + rand;
        for (int i = 0; i < listaudio.size(); i++) {
            if (listaudio.get(i).Name.equals(Name)) {
                listaudio.get(i).Play();
            }
        }

    }

    public void slideMusic(int volume){
        GameData.Instance.SetValueMUSICBG((int)volume);
        for (int i=0;i<4;i++){
           // listaudio.get(i).setVolume(volume);
        }
    }
    public void slideSound(int volume){
        GameData.Instance.SetValueSOUNDEF((int)volume);
        for (int i=4;i<listaudio.size();i++){
           // listaudio.get(i).setVolume(volume);
        }
    }
    public AudioScource getAuidoSource(String name,int what){
        AudioScource audioScource=null;
        for (int i = 0; i < listaudio.size(); i++) {
            if (listaudio.get(i).Name.equals(name)) {
                audioScource=listaudio.get(i);
            }
        }
        return audioScource;
    }

    public void startGame(){
        setValueMusic(GameData.Instance.GetValueMUSICBG());
       setValueSound(GameData.Instance.GetValueSOUNDEF());
        slideMusic(getValueMusic());
        slideSound(getValueSound());
    }

    public int getValueMusic() {
        return valueMusic;
    }

    public void setValueMusic(int valueMusic) {
        this.valueMusic = valueMusic;
    }

    public int getValueSound() {
        return valueSound;
    }

    public void setValueSound(int valueSound) {
        this.valueSound = valueSound;
    }
}
