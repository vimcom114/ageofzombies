package Controller.Game;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class InputManager implements InputProcessor {
    public class InputState {
        public boolean pressed = false;
        public boolean down = false;
        public boolean released = false;
    }

    public class KeyState extends InputState{
        //the keyboard key of this object represented as an integer.
        public int key;

        public KeyState(int key){
            this.key = key;
        }
    }

    public class TouchState extends InputState{
        //keep track of which finger this object belongs to
        public int pointer;
        //coordinates of this finger/mouse
        public Vector2 coordinates;
        //mouse button
        public int button;
        //track the displacement of this finger/mouse
        private Vector2 lastPosition;
        public Vector2 displacement;

        public TouchState(int coord_x, int coord_y, int pointer, int button){
            this.pointer = pointer;
            coordinates = new Vector2(coord_x, coord_y);
            this.button = button;

            lastPosition = new Vector2(0, 0);
            displacement = new Vector2(lastPosition.x,lastPosition.y);
        }
    }
    public Array<KeyState> keyStates = new Array<KeyState>();
    public Array<TouchState> touchStates = new Array<TouchState>();

    public InputManager(){
        for (int i = 0; i < 256; i++) {
            keyStates.add(new KeyState(i));
        }

        touchStates.add(new TouchState(0, 0, 0, 0));

    }
    public void update(){
        //for every keystate, set pressed and released to false.
        for (int i = 0; i < 256; i++) {
            KeyState k = keyStates.get(i);
            if(k.pressed==true) return;
            k.pressed = false;
            k.released = false;
        }
    }
    @Override
    public boolean keyDown(int keycode) {
        keyStates.get(keycode).released = false;
        keyStates.get(keycode).pressed = false;
        keyStates.get(keycode).down = true;
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        keyStates.get(keycode).down = false;
        keyStates.get(keycode).released = true;
        keyStates.get(keycode).pressed = false;
        return false;
    }
    public boolean isKeyPressed(int key){
        return keyStates.get(key).pressed;
    }
    public boolean isKeyDown(int key){
        boolean isDown=keyStates.get(key).down;
        boolean isPress=keyStates.get(key).pressed;
        if(isDown==true&&isPress==false) {
            keyStates.get(key).pressed = true;
        } else  if(isDown==true&&isPress==true) {
            isDown = false;
        }
        return isDown;
    }
    public boolean isKeyReleased(int key){
        boolean reLeases=keyStates.get(key).released;
        if(reLeases){
            keyStates.get(key).released=false;
        }
        return reLeases;
    }
    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }


}
