package Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;

import java.util.ArrayList;

import GameGDX.GDX;

public class LevelDataControll {
    public static LevelDataControll instance;
    GameController gameController;
    ArrayList<SpawnWave> listSpawnWave;
    WaveEnemy WaveEnemyCoppy;
    public int indexLevel=0,indexInit=0,indexInitWave=0;

    public LevelDataControll(GameController gameCt){
        instance=this;
        gameController=gameCt;
        listSpawnWave=new ArrayList<>();
        ReadData();
    }
    private void ReadData()
    {
        Json json = new Json();
        try {
            listSpawnWave = json.fromJson(ArrayList.class, Gdx.files.internal("data/ListLevelSpawnWave.json"));
            if (listSpawnWave != null && listSpawnWave.size() > 0) {
                for (int i = listSpawnWave.size() - 1; i >= 0; i--) {
                    if (listSpawnWave.get(i).getListWaveEnemy().size() <= 0) {
                        listSpawnWave.remove(i);
                    }
                }
            }
        }catch (Exception e){
            listSpawnWave=new ArrayList<>();
            GDX.Show("NullSpawn :"+e);
        }

    }
    public void WriteData(){
        Json json = new Json();
        json.toJson("[{}]", Gdx.files.local("data/ListLevelSpawnWave.json"));
        json.toJson(listSpawnWave, Gdx.files.local("data/ListLevelSpawnWave.json"));
        indexInit=0;
        nextWave();
    }
    public void nextWave(){

//        if(listSpawnWave!=null&&listSpawnWave.size()>0) {
//            GDX.Show("Wave :"+indexInit+"---"+indexLevel+"----"+indexInitWave);
//            if (indexInit >= listSpawnWave.get(indexLevel).getListWaveEnemy().size()) {
//                GDX.Show("Stop");
//                FruitSpawnCT.instance.startGame = false;
//            } else {
//                if (indexInitWave >= listLevel.get(indexLevel).ListlevelInfos.get(indexInit).getSizeWave())
//                {
//                    indexInit += 1;
//                    indexInitWave=0;
//                    nextWave();
//                }else{
//                    FruitSpawnCT.instance.checkWave(getName(), getWhatFruit(), getTimeInit(), getForceX(), getForceY(), getZ(), getindexPos(), getcountInit(), true);
//                    indexInitWave+=1;
//                    FruitSpawnCT.instance.startGame = true;
//                }
//            }
//
//        }

    }
    public int checkSizeSpawnWave(){
        int size=0;
        if(listSpawnWave!=null)size=listSpawnWave.size();
        else size=0;
        return size;
    }
    public void setIndexLevel(int indexLV){
        if(indexLV<=listSpawnWave.size())indexLevel=indexLV-1;
    }
    public SpawnWave getSpawnWave(String nameLevelSpawn, boolean haveBoss, int countWave,int indexlevel){
        SpawnWave spawnWave=null;
        if(listSpawnWave==null)listSpawnWave=new ArrayList<>();
        if(indexlevel>listSpawnWave.size()){
            spawnWave=new SpawnWave( nameLevelSpawn,haveBoss,countWave,indexLevel);
            listSpawnWave.add(spawnWave);
        }else{
            spawnWave=listSpawnWave.get(indexlevel-1);
            spawnWave.ChangeDataSpawnWave(nameLevelSpawn,haveBoss,countWave,indexlevel);
        }
        return spawnWave;
    }
    public String getDataSpawnWave(int indexLevel){
        String DataSpawnWave="";
        String spawnwavename=getSpawnWaveName(indexLevel);
        int countwave=getSpawnWaveCount(indexLevel);
        boolean ishaveboss=getSpawnWaveHaveBoss(indexLevel);
        float totalTimeAllWave=getSpawnWaveTotalTime(indexLevel);
        int countenemyAllWave=getCountEnemyAllWave(indexLevel);
        DataSpawnWave=spawnwavename+"/"+countwave+"/"+ishaveboss+"/"+totalTimeAllWave+"/"+countenemyAllWave;
        return DataSpawnWave;
    }
    public void Coppy(int indexLevel,int indexRow){
        WaveEnemyCoppy=listSpawnWave.get(indexLevel).getListWaveEnemy().get(indexRow);
    }
    public void Paste(){
        if(WaveEnemyCoppy!=null)listSpawnWave.get(indexLevel).addCoppyWave(WaveEnemyCoppy);
    }

    public String getDataWave(int indexLevel,int indexChild) {
        String DataSpawnWave = "";
        //
        String wavedatatxt = getWaveNameData(indexLevel, indexChild);
        Vector2 waveposspawn = getWavePositionSpawn(indexLevel, indexChild);
        int wavecountenemy = getWaveCountEnemy(indexLevel, indexChild);
        float wavetimetospawn = getWaveTimeToSpawn(indexLevel, indexChild);
        float wavespeedmove = getWaveSpeedMove(indexLevel, indexChild);
        float wavetimetonext = getWaveTimeToNext(indexLevel, indexChild);
        //
        DataSpawnWave = waveposspawn.x + "/" + waveposspawn.y + "/" + wavedatatxt + "/" + wavecountenemy + "/" + wavetimetospawn + "/" + wavespeedmove + "/" + wavetimetonext;
        return DataSpawnWave;
    }
    public void checkAddNewSpawnWave(int indexLevel){
        if(indexLevel>listSpawnWave.size()){

        }
    }
    public int checkLenghSpawnWave(int indexLevel){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().size();
    }

    public String getSpawnWaveName(int indexLevel){
        return listSpawnWave.get(indexLevel-1).getNameLevelSpawn();
    }
    public boolean getSpawnWaveHaveBoss(int indexLevel){
        return listSpawnWave.get(indexLevel-1).isHaveBoss();
    }
    public float getSpawnWaveTotalTime(int indexLevel){
        return listSpawnWave.get(indexLevel-1).getTotalTimeAllWave();
    }
    public int getSpawnWaveCount(int indexLevel){
        return listSpawnWave.get(indexLevel-1).getCountWave();
    }
    public int getCountEnemyAllWave(int indexLevel){
        return listSpawnWave.get(indexLevel-1).getCountEnemyAllWave();
    }
    public String getWaveNameData(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getDataTexture();
    }
    public Vector2 getWavePositionSpawn(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getPositionSpaw();
    }
    public int getWaveCountEnemy(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getCountEnemy();
    }
    public float getWaveTimeToSpawn(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getTimeToSpawn();
    }

    public float getWaveSpeedMove(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getSpeedMove();
    }
    public float getWaveTimeToNext(int indexLevel,int indexChild){
        return listSpawnWave.get(indexLevel-1).getListWaveEnemy().get(indexChild).getTimeToNextWave();
    }
}
