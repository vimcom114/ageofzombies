package Controller;

import java.util.Calendar;
import java.util.Locale;

import GameGDX.GDX;

public class GameData {
    public static GameData Instance;
    private String MY_SCORE = "MyScore";
    private String MY_MONEY = "MyMoney";
    private String MY_POWER= "MyPower";
    private String MY_BOMB= "MyBomb";
    private String MY_WEAPON= "MyWeapon";
    private String MY_CHARACT= "MyCharact";
    private String MY_PET= "MyPet";
    private String NO_ADS= "NoAds";
    private String LEVEL_INDEX = "LevelIndex";
    private String OPTION_MOVE = "OPTION_MOVE";
    //MUSIC
    private String BGMUSIC = "BGMusic";

    //New
    private String VALUE_MUSICBG = "ValueMusicBG";
    private String VALUE_SOUNDEF = "ValueSoundEF";
    //Tutorial
    private String LANGUAGE= "Language";
    private String RATE = "Rate";
    //Time
    private String TIME_SPIN="Time_Spin";
    private String TIME_GIFT="Time_Gift";
    private String DAY_GIFT="Day_Gift";
    private String DAY = "Day";
    private String MONTH = "Month";
    private String YEAR = "Year";
    private String HOURS = "Hours";
    private String MINUTE = "Minute";
    private String SECONDS = "Seconds";

    private String DAY_DAILYGIFT = "Day_DailyGift";
    private String MONTH_DAILYGIFT = "Month_DailyGift";
    private String YEAR_DAILYGIFT = "Year_DailyGift";
    private String HOURS_DAILYGIFT = "Hours_DailyGift";
    private String MINUTE_DAILYGIFT = "Minute_DailyGift";
    private String SECONDS_DAILYGIFT = "Seconds_DailyGift";
    private String COMPLETE_ALLGAME = "Complete_AllGame";
    public GameData()
    {
        //PlayerPrefs.DeleteAll();
        MakeSingleInstance();
        IsGameStartedFirstTime();
    }

    void IsGameStartedFirstTime()
    {
        if (GDX.GetPrefInteger("IsGameStartedFirstTime",0)==0)
        {
           GDX.SetPrefInteger("IsGameStartedFirstTime", 1);


            GDX.SetPrefInteger(OPTION_MOVE, 2);
            GDX.SetPrefInteger(MY_MONEY, 0);
            GDX.SetPrefInteger(MY_SCORE, 0);
            GDX.SetPrefInteger(MY_POWER,0);
            GDX.SetPrefInteger(MY_BOMB, 0);
            GDX.SetPrefInteger(LEVEL_INDEX,1);
            GDX.SetPrefInteger(NO_ADS, 0);
            GDX.SetPrefInteger(MY_CHARACT, 0);
            GDX.SetPrefInteger("Matino", 1);
            GDX.SetPrefInteger(MY_WEAPON, 0);
            GDX.SetPrefInteger(MY_PET, 0);
            GDX.SetPrefInteger("FireBall", 1);
//            GDX.SetPrefInteger("Shuriken", 1);
//            GDX.SetPrefInteger("Boomerang", 1);
//            GDX.SetPrefInteger("RainBow", 1);
            String locale = Locale.getDefault().getLanguage();
            // locale = "vi";//en - vi
            SetLanguage( locale);

            GDX.SetPrefInteger(RATE, 0);
            GDX.SetPrefInteger(BGMUSIC, 0);
            GDX.SetPrefInteger(VALUE_MUSICBG,1);
            GDX.SetPrefInteger(VALUE_SOUNDEF, 1);
            GDX.SetPrefFloat(TIME_SPIN, 0);
            GDX.SetPrefFloat(TIME_GIFT, 0);
            GDX.SetPrefInteger(DAY_GIFT, 1);
            GDX.SetPrefInteger(DAY,0 );
            GDX.SetPrefInteger(MONTH,0);
            GDX.SetPrefInteger(YEAR,0);
            GDX.SetPrefInteger(HOURS,0);
            GDX.SetPrefInteger(MINUTE,0);
            GDX.SetPrefInteger(SECONDS,0);
            GDX.SetPrefInteger(DAY_DAILYGIFT ,0 );
            GDX.SetPrefInteger(MONTH_DAILYGIFT , 0);
            GDX.SetPrefInteger(YEAR_DAILYGIFT , 0);
            GDX.SetPrefInteger(HOURS_DAILYGIFT , 0);
            GDX.SetPrefInteger(MINUTE_DAILYGIFT , 0);
            GDX.SetPrefInteger(SECONDS_DAILYGIFT ,  0);
            GDX.SetPrefInteger(COMPLETE_ALLGAME ,  0);
        }
    }

    void MakeSingleInstance()
    {

        Instance = this;

    }


    public void SetRATE(int value)
    {
        GDX.SetPrefInteger(RATE, value);
    }

    public float GetRATE()
    {
        return GDX.GetPrefInteger(RATE,0);
    }
    public void SetLanguage(String value)
    {
        GDX.SetPrefString(LANGUAGE, value);
    }

    public String GetLanguage()
    {
        return GDX.GetPrefString(LANGUAGE,"");
    }

    public void SetValueMUSICBG(int value)
    {
        GDX.SetPrefInteger(VALUE_MUSICBG, value);
    }

    public int GetValueMUSICBG()
    {
        int value=(int)GDX.GetPrefInteger(VALUE_MUSICBG,0);
        return value;
    }
    public void SetValueSOUNDEF(int value)
    {
        GDX.SetPrefInteger(VALUE_SOUNDEF, value);
    }

    public int GetValueSOUNDEF()
    {
        return GDX.GetPrefInteger(VALUE_SOUNDEF,0);
    }




    //SCORE
    public void SaveScore(int score)
    {
        if(score>GetScore()) {
            GDX.SetPrefInteger(MY_SCORE, score);
        }
    }

    public int GetScore()
    {
        return  GDX.GetPrefInteger(MY_SCORE,0);
    }

    public void SetMyMoney(int money){
        GDX.SetPrefInteger(MY_MONEY,money);
    }

    public int GetMyMoney()
    {
        return   GDX.GetPrefInteger(MY_MONEY,0);
    }

    public void SetOptionMove(int option){
        GDX.SetPrefInteger(OPTION_MOVE,option);
    }

    public int GetOptionMove()
    {
        return   GDX.GetPrefInteger(OPTION_MOVE,0);
    }

    public void SetMyPower(int power){
        GDX.SetPrefInteger(MY_POWER,GDX.GetPrefInteger(MY_POWER,0)+ power);
    }

    public int GetMyPower()
    {
        return   GDX.GetPrefInteger(MY_POWER,0);
    }
    public void SetMyBomb(int bomb){
        GDX.SetPrefInteger(MY_BOMB,GDX.GetPrefInteger(MY_BOMB,0)+ bomb);
    }

    public int GetMyBomb()
    {
        return   GDX.GetPrefInteger(MY_BOMB,0);
    }
    public void SetNoAds(int noADs){
        GDX.SetPrefInteger(NO_ADS,noADs);
    }

    public int GetNoADs()
    {
        return   GDX.GetPrefInteger(NO_ADS,0);
    }
    public void SetMyCharact(int indexCharact){
        GDX.SetPrefInteger(MY_CHARACT,indexCharact);
    }

    public int GetMyCharact()
    {
        return   GDX.GetPrefInteger(MY_CHARACT,0);
    }
    public void SetMyWeapon(int indexCharact){
        GDX.SetPrefInteger(MY_WEAPON,indexCharact);
    }

    public int GetMyWeapon()
    {
        return   GDX.GetPrefInteger(MY_WEAPON,0);
    }
    public void SetMyPet(int indexCharact){
        GDX.SetPrefInteger(MY_PET,indexCharact);
    }

    public int GetMyPet()
    {
        return   GDX.GetPrefInteger(MY_PET,0);
    }
    ///Time
    public void SetTimeGift(float time)
    {
        GDX.SetPrefFloat(TIME_GIFT,time);
    }

    public float GetTimeGift()
    {
        return   GDX.GetPrefFloat(TIME_GIFT,0);
    }

    public void SetDayGift()
    {
        if(GDX.GetPrefInteger(DAY_GIFT,0)+1<=7) {
            GDX.SetPrefInteger(DAY_GIFT, GDX.GetPrefInteger(DAY_GIFT, 0) + 1);
        }else{
            GDX.SetPrefInteger(DAY_GIFT,1);
        }
    }

    public int GetDayGift()
    {
        return   GDX.GetPrefInteger(DAY_GIFT,0);
    }
    public void SetTimeSpin(float money)
    {
        GDX.SetPrefFloat(TIME_SPIN,money);
    }

    public float GetTimeSpin()
    {
        return   GDX.GetPrefFloat(TIME_SPIN,0);
    }
    public void SetDay()
    {
        GDX.SetPrefInteger(DAY,+ Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }

    public int GetDay()
    {
        return   GDX.GetPrefInteger(DAY,0);
    }

    public void SetMonth()
    {
        GDX.SetPrefInteger(MONTH,Calendar.getInstance().get(Calendar.MONTH));
    }

    public int GetMonth()
    {
        return   GDX.GetPrefInteger(MONTH,0);
    }

    public void SetYear()
    {
        GDX.SetPrefInteger(YEAR,Calendar.getInstance().get(Calendar.YEAR));
    }

    public int GetYear()
    {
        return   GDX.GetPrefInteger(YEAR,0);
    }
    public void SetHours()
    {
        GDX.SetPrefInteger(HOURS,Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
    }

    public int GetHours()
    {
        return   GDX.GetPrefInteger(HOURS,0);
    }
    public void SetMinute()
    {
        GDX.SetPrefInteger(MINUTE,Calendar.getInstance().get(Calendar.MINUTE));
    }

    public int GetMinute()
    {
        return   GDX.GetPrefInteger(MINUTE,0);
    }
    public void SetSeconds(){
        GDX.SetPrefInteger(SECONDS,Calendar.getInstance().get(Calendar.SECOND));
    }
    public int GetSeconds()
    {
        return   GDX.GetPrefInteger(SECONDS,0);
    }

    public void SetDayDAILYGIFT()
    {
        GDX.SetPrefInteger(DAY_DAILYGIFT ,+ Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }

    public int GetDayDAILYGIFT()
    {
        return   GDX.GetPrefInteger(DAY_DAILYGIFT ,0);
    }

    public void SetMonthDAILYGIFT()
    {
        GDX.SetPrefInteger(MONTH_DAILYGIFT ,Calendar.getInstance().get(Calendar.MONTH));
    }

    public int GetMonthDAILYGIFT()
    {
        return   GDX.GetPrefInteger(MONTH_DAILYGIFT ,0);
    }

    public void SetYearDAILYGIFT()
    {
        GDX.SetPrefInteger(YEAR_DAILYGIFT ,Calendar.getInstance().get(Calendar.YEAR));
    }

    public int GetYearDAILYGIFT()
    {
        return   GDX.GetPrefInteger(YEAR_DAILYGIFT ,0);
    }
    public void SetHoursDAILYGIFT()
    {
        GDX.SetPrefInteger(HOURS_DAILYGIFT ,Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
    }

    public int GetHoursDAILYGIFT()
    {
        return   GDX.GetPrefInteger(HOURS_DAILYGIFT ,0);
    }
    public void SetMinuteDAILYGIFT()
    {
        GDX.SetPrefInteger(MINUTE_DAILYGIFT ,Calendar.getInstance().get(Calendar.MINUTE));
    }

    public int GetMinuteDAILYGIFT()
    {
        return   GDX.GetPrefInteger(MINUTE_DAILYGIFT ,0);
    }
    public void SetSecondsDAILYGIFT(){
        GDX.SetPrefInteger(SECONDS_DAILYGIFT ,Calendar.getInstance().get(Calendar.SECOND));
    }
    public int GetSecondsDAILYGIFT()
    {
        return   GDX.GetPrefInteger(SECONDS_DAILYGIFT ,0);
    }
    public void SetCompleteAll()
    {
        GDX.SetPrefInteger(COMPLETE_ALLGAME ,1);
    }

    public int GetCompleteAll()
    {
        return   GDX.GetPrefInteger(COMPLETE_ALLGAME ,0);
    }
    public int CalculaSecons() {
        int TotalSeconds = 0;
        int countHours=0;
        int countMinute=0;
        int countSeconds=0;
        int countDay=0;
        int DayNow = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        int MonthNow = Calendar.getInstance().get(Calendar.MONTH);
        int YearNow = Calendar.getInstance().get(Calendar.YEAR);
        int HoursNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        // int HoursNow =11;
        int MinuteNow = Calendar.getInstance().get(Calendar.MINUTE);
        int SecondsNow =Calendar.getInstance().get(Calendar.SECOND);

        if (YearNow == GetYear()) {
            if (MonthNow == GetMonth()) {
                if (DayNow == GetDay()) {
                    if (HoursNow == GetHours()) {
                        if(MinuteNow!=GetMinute()) {
                            countMinute = MinuteNow - GetMinute();
                            countSeconds = SecondsNow +(60-GetSeconds());
                            if(countMinute>1){
                                countMinute-=1;
                            }else{
                                countMinute=0;
                            }
                        }else{
                            countSeconds = SecondsNow -GetSeconds();
                        }
                    }else{
                        countHours=HoursNow-GetHours();
                        if(countHours>1){
                            countHours-=1;
                        }else{
                            countHours=0;
                        }
                        countMinute = MinuteNow +(60-GetMinute());
                        countSeconds = SecondsNow +(60-GetSeconds());
                    }
                }else{
                    countDay=DayNow-GetDay();
                    if(countDay>1){
                        countDay-=1;
                    }else{
                        countDay=0;
                    }
                    countHours=HoursNow+(24-GetHours());
                    countMinute = MinuteNow +(60-GetMinute());
                    countSeconds = SecondsNow +(60-GetSeconds());
                }
            }
        }
        TotalSeconds =(((countDay*24)*60)*60)+((countHours*60)*60)+ (countMinute * 60) + countSeconds;
        return TotalSeconds;
    }

    public int CalculaSeconsGift() {
        int TotalSeconds = 0;
        int countHours=0;
        int countMinute=0;
        int countSeconds=0;
        int countDay=0;
        int DayNow = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        int MonthNow = Calendar.getInstance().get(Calendar.MONTH);
        int YearNow = Calendar.getInstance().get(Calendar.YEAR);
        int HoursNow = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        // int HoursNow =11;
        int MinuteNow = Calendar.getInstance().get(Calendar.MINUTE);
        int SecondsNow =Calendar.getInstance().get(Calendar.SECOND);


        if (YearNow == GetYearDAILYGIFT()) {
            if (MonthNow == GetMonthDAILYGIFT()) {
                if (DayNow == GetDayDAILYGIFT()) {
                    if (HoursNow == GetHoursDAILYGIFT()) {
                        if(MinuteNow!=GetMinuteDAILYGIFT()) {
                            countMinute = MinuteNow - GetMinuteDAILYGIFT();
                            countSeconds = SecondsNow +(60-GetSecondsDAILYGIFT());
                            if(countMinute>1){
                                countMinute-=1;
                            }else{
                                countMinute=0;
                            }
                        }else{
                            countSeconds = SecondsNow -GetSecondsDAILYGIFT();
                        }
                    }else{
                        countHours=HoursNow-GetHoursDAILYGIFT();
                        if(countHours>1){
                            countHours-=1;
                        }else{
                            countHours=0;
                        }
                        countMinute = MinuteNow +(60-GetMinuteDAILYGIFT());
                        countSeconds = SecondsNow +(60-GetSecondsDAILYGIFT());
                    }
                }else{
                    countDay=DayNow-GetDayDAILYGIFT();
                    if(countDay>1){
                        countDay-=1;
                    }else{
                        countDay=0;
                    }
                    countHours=HoursNow+(24-GetHoursDAILYGIFT());
                    countMinute = MinuteNow +(60-GetMinuteDAILYGIFT());
                    countSeconds = SecondsNow +(60-GetSecondsDAILYGIFT());
                }
            }
        }
        TotalSeconds =(((countDay*24)*60)*60)+((countHours*60)*60)+ (countMinute * 60) + countSeconds;

        return TotalSeconds;
    }
    public void SaveDateSpin(){
        SetMonth();
        SetYear();
        SetDay();
        SetHours();
        SetMinute();
        SetSeconds();
    }
    public void SaveDateDailyGift(){
        SetMonthDAILYGIFT();
        SetYearDAILYGIFT();
        SetDayDAILYGIFT();
        SetHoursDAILYGIFT();
        SetMinuteDAILYGIFT();
        SetSecondsDAILYGIFT();
    }

}
