package Controller;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.geda.game.MyGame;


import java.util.concurrent.LinkedBlockingQueue;

import Controller.Game.AudioManager;
import Controller.GameController;

import Controller.LevelSpawnControll;
import Controller.SpawnControll;
import GameGDX.GDX;
import GameGDX.Particle;

import Model.BehindObject;
import Model.Enemy;
import Model.GRIDMAP.GridMap;
import Model.InteractiveTileObject;
import Model.Item;
import Model.ItemType;
import Model.OTiledMapRenderWSprites;
import Model.Player;
import Model.Weapon;
import Model.WeaponWareHouse;
import Model.Zombies;
import ScreenGame.GamePlayUI;
import ScreenGame.WorldScreen;
import Tools.B2WorldCreator;
import Tools.CollisionSystem;
import Tools.WorldContactListener;

public class MyWorld {

    WorldScreen screenGame;
    //basic playscreen variables
    WeaponWareHouse weaponWareHouse;
     SpawnControll spawnControll;
    private OrthographicCamera gamecam;
    private TmxMapLoader maploader;


    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    OTiledMapRenderWSprites tileMapSprite;

    //Box2d variables
    Engine engineComponent;
    private World world;
    private Box2DDebugRenderer b2dr;
    Stage stage;


    private GridMap gridMap;

    public B2WorldCreator getCreator() {
        return creator;
    }

    //Box2d variables
    private B2WorldCreator creator;

    //sprites
    private Player player;
    boolean stopMove = false;

    Array<Particle> listparticle;
//    Array<Coin> listCoin;
   Array<InteractiveTileObject> listTileObject;
    Array<Enemy> listEnemy;
//    Array<Enemy> listEnemyDesRestart=new Array<>();
//
//
    private Array<Item> listItems;
    private LinkedBlockingQueue<ItemType> itemsToSpawn;
//
//    ReturnPointWorld returnPoint;

    SpriteBatch batch;
    boolean Stop = false;

    String nameMap = "";

    String nameTileMapAsset = "";
    boolean handleItemCheck = false;

    boolean isGameOver = false;


    public MyWorld(WorldScreen myscreen, OrthographicCamera cam, Stage stage1, SpriteBatch Batch, String namemap) {
        batch = Batch;
        stage = stage1;
        screenGame = myscreen;
        gamecam = cam;
        weaponWareHouse=new WeaponWareHouse(this,player);
        listparticle = new Array<>();
       listTileObject=new Array<>();
//        listCoin = new Array<>();
        listEnemy = new Array<>();
//
//        listItems = new Array<Item>();
//        listGroundSpecial=new Array<>();
//        itemsToSpawn = new LinkedBlockingQueue<ItemType>();
        gridMap=new GridMap(getScreenGame().getLayerGroup(1),this);
        nameMap = namemap;
        engineComponent=new PooledEngine();
        engineComponent.addSystem(new CollisionSystem());
        world = new World(new Vector2(0, -10), true);
        b2dr = new Box2DDebugRenderer();
        world.setContactListener(new WorldContactListener(screenGame));
        creatPlayer(getScreenGame().getLayerGroup(1));


    }

    public void Update(float dt) {
        if (handleItemCheck) handleSpawningItems();
        world.step(1 / 60f, 6, 2);
        if (Stop) {
            clearAndChangeMap();
        }
        if(Stop)return;
        engineComponent.update(dt);
        //update our gamecam with correct coordinates after changes
        player.Update(dt);
        for (InteractiveTileObject tileObject : listTileObject) {
            tileObject.Update(dt);
            if (tileObject.getB2body() == null) {
               // tileObject.removeObject();
                removeTileObject(tileObject);
            }
        }

        for (Enemy enemy : listEnemy) {
            enemy.Update(dt);
            if (enemy.getB2body() == null) {
                listEnemy.removeValue(enemy, true);
            }

        }
//        for (Item item : listItems) {
//            if (item.getBody() == null || item == null) {
//                listItems.removeValue(item, true);
//            }
//        }
        screenGame.setCameraPos(player.getB2body().getPosition().x, player.getB2body().getPosition().y);
        gamecam.update();
        // renderer.setView(gamecam);
        if (tileMapSprite != null) {
            tileMapSprite.setView(gamecam);

        }



    }

    public void Render(float delta) {
        //renderer.render();
        batch.setProjectionMatrix(gamecam.combined);
        if (tileMapSprite != null) {
            tileMapSprite.setView(gamecam);
            tileMapSprite.render();
        }
        //b2dr.render(world, gamecam.combined);

    }
    public void HandleInPut(String whatmove, boolean startmove) {
//        if (runVideo || stopMove || caseVideo >= 4 || player.isStartChangeExtra() || GamePlayUI.instance.isStartGame() == false)
//            return;
        switch (whatmove) {
            case "LEFT":
                player.setMoveLeft(startmove);
                break;
            case "RIGHT":
                player.setMoveRight(startmove);
                break;
            case "DOWN":
                player.setMoveDown(startmove);
                break;
            case "UP":
                player.setMoveUp(startmove);
                break;
            case "SPACE":
                player.setFireMainWP(startmove);
                break;
            case "ENTER":
                player.setFireExtraWP(startmove);
                break;
        }


    }
    public void addToEngineComponent(Entity entity){
        engineComponent.addEntity(entity);
    }
    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public TiledMap getMap() {
        return map;
    }

    public void setMap(TiledMap map) {
        this.map = map;
    }

    public void dispose() {
        //dispose of all our opened resources
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();

    }

    public Stage getStage() {
        return stage;

    }


    public Player getPlayer() {
        return player;
    }

    public String getNameMap() {
        return nameMap;
    }

    public void setNameMap(String namemap) {
        if (nameMap.equals(namemap)) {
            screenGame.Changetarget();
            return;
        }
        Stop = true;
        screenGame.setIndexSpawnPos(0);
        screenGame.setMinPosCamera(new Vector2(gamecam.viewportWidth * .5f, 0));
        nameMap = namemap;
        clearAndChangeMap();
    }

    public void clearAndChangeMap() {
        if (map != null) {
            map.dispose();
        }
        if (tileMapSprite != null) {
            tileMapSprite.dispose();
        }
        clearTileObject();
    }

    void clearTileObject() {

        for (int i = 0; i < listTileObject.size; i++) {
            listTileObject.get(i).Destroy();
        }
        for (Enemy enemy : listEnemy) {
            enemy.Destroy();
        }
        maploader = new TmxMapLoader();
        map = maploader.load("Map/" + nameMap + ".tmx");
        tileMapSprite = new OTiledMapRenderWSprites(map, 1 / MyGame.PPM, batch);
        tileMapSprite.setGameCam(gamecam);
        tileMapSprite.setView(gamecam);
        spawnControll = new SpawnControll(screenGame);
        creator = new B2WorldCreator(this);
        gridMap.creatGridMap();
        creator.creatMapObject();
        spawnControll.startSpawn();
        Stop = false;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                LevelSpawnControll.instance.setStartSpawn(true);
            }
        }, 2f);
    }

    public void ReUseAll() {
        getScreenGame().setGrowStartMap(false);
        GameController.instance.setFinishGame(false);

        stopMove = false;
        screenGame.setStopCamera(false);

    }


    public void spawnItem(ItemType idef) {
        itemsToSpawn.add(idef);
        handleItemCheck = true;
    }
    public void handleSpawningItems() {
        if (!itemsToSpawn.isEmpty()) {
            for (int i = 0; i < itemsToSpawn.size(); i++) {
                ItemType idef = itemsToSpawn.poll();
            }
        }
    }

    public void SpawnEnemy(String DataShow,String DataTexture,float speedMove, float x, float y) {

        switch (DataShow) {
            case "Zombies":
                    Enemy enemy=new Zombies(this,x,y,DataTexture,speedMove);
                    addEnemy(enemy);
                break;
        }
    }

    public boolean isGameOver() {

        return isGameOver;
    }

    public void setGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }



    public WorldScreen getScreenGame() {
        return screenGame;
    }

    public void setScreenGame(WorldScreen screenGame) {
        this.screenGame = screenGame;
    }

//    public ReturnPointWorld getReturnPoint() {
//        return returnPoint;
//    }

    public void setReturnPoint(Vector2 returnPoint, int Type) {

       // this.returnPoint = new ReturnPointWorld(returnPoint, Type);
    }

    public void ReturnPoin() {
       // player.ReTurnWorldPoint(returnPoint);
       // screenGame.setCameraPos(player.b2body.getPosition().x, player.b2body.getPosition().y);
    }

    public void RevicePlayer() {

        setGameOver(false);
//        player.ReviceMario();
        screenGame.ResetCamera();

    }

    public BehindObject addTextureMap(TextureRegion region, float W, float H) {
        return creator.addTxtBehindObjectLayer(region, W, H);
    }


    public void removeBehindObject(BehindObject behindObject) {
        creator.removeBehindObject(behindObject);
    }





    double distance(Vector2 vectorOne, Vector2 vectorTwo) {
        return Math.sqrt(Math.pow((vectorOne.x - vectorTwo.x), 2) + Math.pow((vectorOne.y - vectorTwo.y), 2));
    }

    public String getNameTileMapAsset() {
        return nameTileMapAsset;
    }

    public void setNameTileMapAsset(String nameTileMapAsset) {
        this.nameTileMapAsset = nameTileMapAsset;
    }

    public OTiledMapRenderWSprites getTileMapSprite() {
        return tileMapSprite;
    }

    public void setTileMapSprite(OTiledMapRenderWSprites tileMapSprite) {
        this.tileMapSprite = tileMapSprite;
    }

    public SpawnControll getSpawnControll() {
        return spawnControll;
    }

    public void setSpawnControll(SpawnControll spawnControll) {
        this.spawnControll = spawnControll;
    }
    public void creatPlayer(Group gameplay) {
        player = new Player(this, (5*80), (5*80), gameplay);
    }

    public void GivePlayerWeaponCode(String code){
        Weapon weapon=weaponWareHouse.takeWeaponByCode(code);
        weapon.checkIsUse(getPlayer());
        int numberCT=weapon.getMyWeaponType().getNumberCT();
        SettingWeapon(numberCT,weapon);

    }
    public void SettingWeapon(int numberCT,Weapon weapon){
        switch (numberCT){
            case 1:
                getPlayer().TakeMainWeapon(weapon);
                break;
            case 2:
                getPlayer().TakeExtraWeapon(weapon);
                break;
        }
    }
    public void addTileObject(InteractiveTileObject tileObject){
        if(listTileObject.contains(tileObject,true)==false) {
            getScreenGame().addToLayer(0,tileObject);
            listTileObject.add(tileObject);
        }
    }
    public void removeTileObject(InteractiveTileObject tileObject){
        if(listTileObject.contains(tileObject,true)) {

            listTileObject.removeValue(tileObject, true);

        }
    }
    public void addEnemy(Enemy enemy){
        if(listEnemy.contains(enemy,true)==false)listEnemy.add(enemy);
    }
    public Engine getEngineComponent() {
        return engineComponent;
    }

    public void setEngineComponent(Engine engineComponent) {
        this.engineComponent = engineComponent;
    }
    public boolean checkPosColl(Vector2 posCheck){
        boolean check=false;
        for(int i=0;i<listTileObject.size;i++){
            check=listTileObject.get(i).checkPosCollision(posCheck,80/MyGame.PPM,80/MyGame.PPM);
            if(check)return check;
        }
        return check;
    }
    public void setIconWPDetail(String count,String txtIcon){
        GameController.instance.setIconWPDetail(count,txtIcon);
    }
    public void setCountIconWP(String count){
        GameController.instance.setCountIconWP(count);
    }
    public void ShowJoyDirection(boolean show,String Count,String strTxtIcon){
        GameController.instance.ShowJoyDirection(show,Count,strTxtIcon);
    }
    public void setJoyDirectionCount(String Count){
        GameController.instance.setJoyDirectionCount(Count);
    }
    public GridMap getGridMap() {
        return gridMap;
    }

    public void setGridMap(GridMap gridMap) {
        this.gridMap = gridMap;
    }
}


