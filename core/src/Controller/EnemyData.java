package Controller;

public class EnemyData {
    private String NameGet="";
    private String Data="";
    public EnemyData(String nameGet, String data){
        NameGet=nameGet;
        Data=data;
    }
    public String getNameGet() {
        return NameGet;
    }

    public void setNameGet(String nameGet) {
        NameGet = nameGet;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
