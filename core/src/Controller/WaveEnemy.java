package Controller;

import com.badlogic.gdx.math.Vector2;

import GameGDX.GDX;

public class WaveEnemy {
    Vector2 PositionSpaw;
    String DataTexture = "";
    int CountEnemy;
    float TimeToSpawn = 0f;
    float SpeedMove = 0f;
    float TimeToNextWave = 0f;

    public WaveEnemy() {

    }
    public WaveEnemy(int ID,WaveEnemy waveEnemyCoppy) {
        PositionSpaw = waveEnemyCoppy.getPositionSpaw();
        DataTexture = waveEnemyCoppy.getDataTexture();
        CountEnemy = waveEnemyCoppy.getCountEnemy();
        TimeToSpawn = waveEnemyCoppy.getTimeToSpawn();
        SpeedMove = waveEnemyCoppy.getSpeedMove();
        TimeToNextWave = waveEnemyCoppy.getTimeToNextWave();
    }
    public WaveEnemy(Vector2 positionSpaw, String dataTexture, int countEnemy, float timeToSpawn, float speedMove, float timeToNextWave) {
        PositionSpaw = positionSpaw;
        DataTexture = dataTexture;
        CountEnemy = countEnemy;
        TimeToSpawn = timeToSpawn;
        SpeedMove = speedMove;
        TimeToNextWave = timeToNextWave;
    }

    public Vector2 getPositionSpaw() {
        return PositionSpaw;
    }

    public void setPositionSpaw(Vector2 positionSpaw) {
        PositionSpaw = positionSpaw;
    }

    public String getDataTexture() {
        return DataTexture;
    }

    public void setDataTexture(String dataTexture) {
        DataTexture = dataTexture;
    }

    public int getCountEnemy() {
        return CountEnemy;
    }

    public void setCountEnemy(int countEnemy) {
        CountEnemy = countEnemy;
    }

    public float getTimeToSpawn() {
        return TimeToSpawn;
    }

    public void setTimeToSpawn(float timeToSpawn) {
        TimeToSpawn = timeToSpawn;
    }

    public float getSpeedMove() {
        return SpeedMove;
    }

    public void setSpeedMove(float speedMove) {
        SpeedMove = speedMove;
    }

    public float getTimeToNextWave() {
        return TimeToNextWave;
    }

    public void setTimeToNextWave(float timeToNextWave) {
        TimeToNextWave = timeToNextWave;
    }

    public void ChangeData(Vector2 positionSpaw, String dataTexture, int countEnemy, float timeToSpawn, float speedMove, float timeToNextWave) {
        PositionSpaw = positionSpaw;
        DataTexture = dataTexture;
        CountEnemy = countEnemy;
        TimeToSpawn = timeToSpawn;
        SpeedMove = speedMove;
        TimeToNextWave = timeToNextWave;
    }

    public float getTotalTimeWave() {
        return getTimeToSpawn() * getCountEnemy();
    }
}
