package Controller;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.geda.game.MyGame;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Controller.Game.AudioManager;
import Controller.Game.InputManager;
import GameGDX.GDX;
import GameGDX.Language;

import ScreenGame.GamePlayUI;
import ScreenGame.PlayScreen;
import ScreenGame.UIScene;
import ScreenGame.WorldScreen;

public class GameController {
    public static GameController instance;
    ArrayList<EnemyData> listEnemyData=new ArrayList<>();
    String[] DataNameEnemy;
    LevelDataControll levelDataControll;
    LevelSpawnControll levelSpawnControll;
    ItemSpawnControll itemSpawnControll;
    MyWorld myWorld;
    WorldScreen myWorldScreen;
    PlayScreen playScreen;
    UIScene uiScene;

   // PlayScreenExtra playScreenExtra;
    //SpawnControll spawnControll;


    private boolean isScreenExtra;
    MyGame myGame;

    InputManager inputManager;

    OrthographicCamera myCamera;

    InputMultiplexer multiplexer;

    int CountStar=0;

    private String namMapChose;
    private String NameMap;
    private int indexMapChose;


    private String NameMapShow;
    boolean isChange;
    boolean StopScreen;
    boolean isReturnPoin;

    boolean FinishGame=false;

    boolean CheckSameMap=false;

    private boolean resetBigMario=false;

    String NameCharacter="";

    private int coinTakeInOneGame=0;
    private int targetCoinWatch=0;
    public GameController(MyGame mygame){
        instance = this;
        multiplexer = new InputMultiplexer();
        myGame=mygame;
        LoadScreen();
        AudioManager.instance.creatGAudio();
        LoadEnemyData();
        levelDataControll=new LevelDataControll(this);
        playScreen=new PlayScreen(mygame,"",MyGame.V_WIDTH/MyGame.PPM,MyGame.V_HEIGHT/MyGame.PPM);
        setMyWorld(playScreen.getMyWorld());
        uiScene=new UIScene(this);
        levelSpawnControll=new LevelSpawnControll(this);
        itemSpawnControll=new ItemSpawnControll(this);
       // playScreenExtra=new PlayScreenExtra(mygame,"",2.5f,(2.5f));
        GivePlayerWeaponByCode("1_1");

    }
    private void LoadScreen()
    {
        new GameData();
        String locale = GameData.Instance.GetLanguage();
        Language.SetLang(locale);
        //new MenuScreen();
    }
    public void Update(float dt)
    {
        uiScene.Update(dt);
        levelSpawnControll.Update(dt);
    }
    public void Render(float delta){

        playScreen.render(delta);
      //  playScreenExtra.render(delta);
        uiScene.render(delta);
    }
    public void ReSize(int w,int  h){
        playScreen.resize(w,h);
    }
    public MyWorld getMyWorld() {
        return myWorld;
    }

    public void setMyWorld(MyWorld myWorld) {
        this.myWorld = myWorld;
    }

    public InputManager getInputManager() {
        return inputManager;
    }

    public void setInputManager(InputManager inputmanager) {
        multiplexer.addProcessor(inputmanager);
        Gdx.input.setInputProcessor(multiplexer);
//        if(inputManager!=null&&inputManager.equals(inputmanager)) return;
//        Gdx.input.setInputProcessor(inputmanager);
//        this.inputManager = inputmanager;
    }
    public void addInputManager(Stage inputmanager) {
        multiplexer.addProcessor(inputmanager);
        Gdx.input.setInputProcessor(multiplexer);
//        if(inputManager!=null&&inputManager.equals(inputmanager)) return;
//        Gdx.input.setInputProcessor(inputmanager);
//        this.inputManager = inputmanager;
    }
    public OrthographicCamera getMyCamera() {
        return myCamera;
    }

    public void setMyCamera(OrthographicCamera myCamera) {
        this.myCamera = myCamera;
    }
    public WorldScreen getMyWorldScreen() {
        return myWorldScreen;
    }

    public void setMyWorldScreen(WorldScreen myWorldScreen) {
        this.myWorldScreen = myWorldScreen;
    }

    public void ChangeMap(String nameMap,int indexMap,boolean change,boolean stop,boolean isreturnPoin){
        NameMap=nameMap;
        indexMapChose=indexMap;
        isChange=change;
        StopScreen=stop;
        isReturnPoin=isreturnPoin;
        UIScene.instance.FinishOpenScreen(2);
    }
    public void StartChangeMap(){
        LevelSpawnControll.instance.GetDataAndSetData(NameMap,indexMapChose);
        playScreen.StopAndChangeMap(NameMap, isChange, StopScreen, isReturnPoin);
        setFinishGame(false);
    }
    public static float distance(Vector2 object1, Vector2 object2){
        return (float) Math.sqrt(Math.pow((object1.x - object2.x), 2) + Math.pow((object1.y - object2.y), 2));
    }


    public void GivePlayerWeaponByCode(String code){
        getMyWorld().GivePlayerWeaponCode(code);
    }
    public String getNameCharacter() {
        return NameCharacter;
    }

    public void setNameCharacter(String nameCharacter) {
        if(nameCharacter.equals(NameCharacter)==false){
            //  playScreen.getPlayer().ChangeCharacter(nameCharacter);
            //playScreenExtra.getMyWorld().getPlayer().ChangeCharacter(nameCharacter);
        }
        NameCharacter = nameCharacter;
    }

    private void LoadEnemyData(){
        String path = "EnemyData.txt";
        FileHandle file = Gdx.files.internal(path);
        FileReader fr = null;
        BufferedReader br = null;
        String line = null;
        try {
            // fr = new FileReader(file);
            br = new BufferedReader(file.reader());
            line = br.readLine();
            int Lengh = Integer.parseInt(line);

            String[] listSplit=null;
            String strGet="";
            DataNameEnemy=new String[Lengh];
            for (int i=0;i<Lengh;i++){
                line = br.readLine();
                if(line.equals("Enemy:")){
                    line = br.readLine();
                    listSplit=line.split("NameGet:");
                    String NameShow=listSplit[1];
                    DataNameEnemy[i]=NameShow;
                    line=br.readLine();
                    listSplit=line.split("Data:");
                    String Data=listSplit[1];
                    EnemyData enemyData=new EnemyData(NameShow,Data);
                    listEnemyData.add(enemyData);
                }
            }



        } catch (FileNotFoundException e) {
            GDX.Show("FILENot"+e);
            e.printStackTrace();
        } catch (IOException e) {
            GDX.Show("Not");
            e.printStackTrace();
        }
    }

    public void setFinishGame(boolean finishGame) {
        FinishGame = finishGame;
    }

    public String[] getDataNameEnemy() {
    return DataNameEnemy;
}

    public void setDataNameEnemy(String[] dataNameEnemy) {
        DataNameEnemy = dataNameEnemy;
    }
    public String getDataEnemy(String DataNameEnemy){
        String DataEnemy="";
        for(int i=0;i<listEnemyData.size();i++){
            if(listEnemyData.get(i).getNameGet().equals(DataNameEnemy))DataEnemy=listEnemyData.get(i).getData();
        }
        return DataEnemy;
    }

    public void SpawnItem(String Code,Vector2 position){
        ItemSpawnControll.instance.SpawnItemByCode(Code,position);
    }


    public boolean CheckPosLegal(Vector2 posCheck){
        boolean check=myWorld.checkPosColl(posCheck);
        return check;
    }
    public Vector2 GetScreenToWorld(Vector2 pos) {
        Vector3 position = myWorld.getScreenGame().getGamecam().unproject(new Vector3(pos.x, UIScene.instance.stage.getViewport().getScreenHeight() - pos.y, 0));
        return new Vector2(position.x,position.y);
    }

    public void setIconWPDetail(String count,String txtIcon){
        GamePlayUI.instance.setIconWPDetail(count,txtIcon);
    }
    public void setCountIconWP(String count){
        GamePlayUI.instance.setCountIconWP(count);
    }
    public void ShowJoyDirection(boolean show,String Count,String strTxtIcon){
        GamePlayUI.instance.ShowJoyDirection(show,Count,strTxtIcon);
    }
    public void setJoyDirectionCount(String Count){
        GamePlayUI.instance.setJoyDirectionCount(Count);
    }

}
