package Controller;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;

import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import Model.Bullet;
import Model.BulletPlayer;
import Model.GernadeBullet;
import Model.Player;
import Model.Weapon;
import Model.WeaponType;

public class GernadeWP extends Weapon {
    public GernadeWP(MyWorld myworld, WeaponType weaponType) {
        super(myworld, weaponType);
    }

    @Override
    public void Update(float dt) {
        if (StartShoot) {

            firePoin = calculateOrbit(myPlayer.getMyRotate(), new Vector2(this.getWidth(), this.getWidth()), new Vector2(myPlayer.getCenterPoin().x, myPlayer.getCenterPoin().y));
            GernadeBullet gernadeBullet = new GernadeBullet(getMyWorld(), getMyWeaponType().getDataTextureBullet(), getMyWeaponType().getSpeedBullet(), getMyWeaponType().getDameBullet(), firePoin, myPlayer.getMyRotate());
            timetoShoot = getMyWeaponType().getStartTimeToShoot();
            boolean checkFinish = minusBullet(1);
            getMyWorld().setJoyDirectionCount(getCountBullet());
            if (checkFinish) getMyWorld().ShowJoyDirection(false, "", "");
            StartShoot = false;
        }

    }


    @Override
    public void setStartShoot(boolean shoot) {
        super.setStartShoot(shoot);
        if(StartShoot==false&&shoot)timetoShoot=0;
        StartShoot=shoot;

    }

    @Override
    public void checkIsUse(Player player) {
        super.checkIsUse(player);
        if (isUse() == false) {
            myPlayer = player;
            myPlayer.addActor(this);
//            setMyFrame(UI.NewImage(Loader.GetTexture(getMyWeaponType().getDataTextureWeapon()), 0, 0, this));
//            getMyFrame().setSize(80 / MyGame.PPM, 80 / MyGame.PPM);
//            this.setSize(getMyFrame().getWidth(), getMyFrame().getHeight());
//            getMyFrame().setPosition(0, 0);
//            setPosition(myPlayer.getWidth(), myPlayer.getHeight() / 2, Align.center);
//            //Vector2 firePoinToWorld=new Vector2(player.getPoinToWorld(firePoin));
////          firePoin=firePoinToWorld;
            setUse(true);
        } else {
            setCountBullet(getMyWeaponType().getMagCount());
            setVisible(true);
        }
        getMyWorld().ShowJoyDirection(true,getCountBullet(),getMyWeaponType().getDataTextureIcon());
    }
}
