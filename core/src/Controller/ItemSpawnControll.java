package Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import GameGDX.GDX;
import Model.ItemGernade;
import Model.ItemGun;
import Model.ItemType;

public class ItemSpawnControll {
    public  static ItemSpawnControll instance;
    GameController gameCT;
    MyWorld myWorld;
    ArrayList<ItemType> listItemType;
    int Count=0;
    public ItemSpawnControll(GameController gameController) {
        instance = this;
        gameCT = gameController;
        myWorld = gameController.getMyWorld();
        listItemType = new ArrayList<>();
        LoadData();
        SpawnItemByCode("1_4",new Vector2(5f,5f));
        SpawnItemByCode("1_3",new Vector2(5f,7f));
    }
    void LoadData(){
        String path = "Item.txt";
        FileHandle file = Gdx.files.internal(path);
        FileReader fr = null;
        BufferedReader br = null;
        int countItem= 0;
        String line = null;
        try {
            // fr = new FileReader(file);
            br = new BufferedReader(file.reader());
            line = br.readLine();
            countItem = Integer.parseInt(line);

            String[] listSplit=null;
            String strGet="";
            for (int i = 0; i < countItem; i++) {
                line=br.readLine();
                if(line==null) return;
                if(line.equals("Item:")) {
                    line=br.readLine();
                    listSplit=line.split("-Type:");
                    int Type=Integer.parseInt(listSplit[1]);
                    line=br.readLine();
                    listSplit=line.split("-Code:");
                    String Code=listSplit[1];
                    line=br.readLine();
                    listSplit=line.split("-DataTextureItem:");
                    String DataTextureItem=listSplit[1];
                    line=br.readLine();
                    listSplit=line.split("-DataPanel:");
                    String Datapanel=listSplit[1];
                    line=br.readLine();
                    listSplit=line.split("-DataParticle:");
                    String DataParticle=listSplit[1];
                    //
                    ItemType itemType=new ItemType(Type,Code,DataTextureItem,Datapanel,DataParticle);
                    listItemType.add(itemType);
                }

            }
        } catch (FileNotFoundException e) {
            GDX.Show("FILENot"+e);
            e.printStackTrace();
        } catch (IOException e) {
            GDX.Show("Not");
            e.printStackTrace();
        }
    }
    public ItemType getItemTypeCode(String Code){
        ItemType itemTypeGet=null;
        for(ItemType itemType:listItemType){
            if(itemType.getCode().equals(Code)){
                itemTypeGet=itemType;
                return itemTypeGet;
            }
        }
        return itemTypeGet;
    }

    public void SpawnItemByCode(String Code,Vector2 pos){
        ItemType itemType=getItemTypeCode(Code);
        int type=itemType.getType();
        if(itemType!=null){
            switch (type){
                case 1:
                    if(Count<2) {
                        new ItemGun(myWorld,pos.x,pos.y,itemType);
                        Count=1;
                    }
                    break;
                case 2:
                    if(Count<2) {
                        new ItemGernade(myWorld,pos.x,pos.y,itemType);
                        Count=1;
                    }
                    break;
            }

        }
    }
}
