package Controller;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class SpawnWave {
    int indexLevel=0;
    String NameLevelSpawn = "";
    boolean HaveBoss = false;
    int CountWave = 0;
    int CountEnemyAllWave=0;
    ArrayList<WaveEnemy> listWaveEnemy;
    public SpawnWave(){

    }
    public SpawnWave(String nameLevelSpawn, boolean haveBoss, int countWave,int indexlv) {
        indexLevel=indexlv-1;
        listWaveEnemy=new ArrayList<>();
        NameLevelSpawn = nameLevelSpawn;
        HaveBoss = haveBoss;
        CountWave = countWave;
    }
    public String getNameLevelSpawn() {
        return NameLevelSpawn;
    }

    public void setNameLevelSpawn(String nameLevelSpawn) {
        NameLevelSpawn = nameLevelSpawn;
    }

    public boolean isHaveBoss() {
        return HaveBoss;
    }

    public void setHaveBoss(boolean haveBoss) {
        HaveBoss = haveBoss;
    }

    public int getCountWave() {
        return CountWave;
    }

    public void setCountWave(int countWave) {
        CountWave = countWave;
    }

    public ArrayList<WaveEnemy> getListWaveEnemy(){
        return listWaveEnemy;
    }
    public void addWaveEnemy(WaveEnemy waveEnemy){
        if(listWaveEnemy.contains(waveEnemy)==false)listWaveEnemy.add(waveEnemy);
    }
    public void ChangeDataSpawnWave(String nameLevelSpawn, boolean haveBoss, int countWave,int indexlv) {
        indexLevel=indexlv-1;
        NameLevelSpawn = nameLevelSpawn;
        HaveBoss = haveBoss;
        CountWave = countWave;
    }
    public void changeDataWave(int index, Vector2 positionSpaw, String dataTexture, int countEnemy, float timeToSpawn, float speedMove, float timeToNextWave){

        listWaveEnemy.get(index).ChangeData( positionSpaw, dataTexture,countEnemy,timeToSpawn, speedMove,timeToNextWave);
    }
    public float getTotalTimeAllWave(){
        float totalTime=0;
        for(int i=0;i<listWaveEnemy.size();i++){
            totalTime+=listWaveEnemy.get(i).getTotalTimeWave();
        }
        return totalTime;
    }
    public void addNewWave(){
        WaveEnemy waveEnemy=new WaveEnemy();
        listWaveEnemy.add(waveEnemy);
    }
    public void removeWave(int indexChild){
        if(listWaveEnemy.size()>0&&indexChild<listWaveEnemy.size()){
            listWaveEnemy.remove(indexChild);
        }
    }
    public int getCountEnemyAllWave(){
        CountEnemyAllWave=0;
        for(int i=0;i<listWaveEnemy.size();i++){
            CountEnemyAllWave+=listWaveEnemy.get(i).getCountEnemy();
        }
        return CountEnemyAllWave;
    }
    public void addCoppyWave(WaveEnemy waveEnemyCoppy){
        WaveEnemy waveEnemy1=new WaveEnemy(listWaveEnemy.size()-1,waveEnemyCoppy);
        listWaveEnemy.add(waveEnemy1);

    }
}
