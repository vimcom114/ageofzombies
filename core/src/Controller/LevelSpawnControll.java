package Controller;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import GameGDX.GDX;
import ScreenGame.UIScene;

public class LevelSpawnControll {
    public static LevelSpawnControll instance;
    private GameController gameController;
    private MyWorld myWorld;
    private boolean startSpawn;
    private String DataNameMapChose="";



    private String DataNameMap="";
    private int indexLevelMapChose=0;
    private boolean spawnwavehaveBoss=false;
    private int indexWave,countWave=0;
    private String DataNameEnemy="";
    private String DataEnemy="";
    private int countEnemy=0;

    private int countEnemySpawn=0;
    private float starttimetoSpawn,timetoSpawn;
    private float speedMoveEnemy=0;
    private float timetoNextWave;
    private Vector2 postionWave;
    public LevelSpawnControll(GameController gameCt){
        instance=this;
        gameController=gameCt;
    }
    public void Update(float dt){
        if(startSpawn){
              if(indexWave<countWave){
                  if(getCountEnemy()<getCountEnemySpawn()) {
                      if (timetoSpawn > 0) {
                          timetoSpawn -= dt;

                      } else {

                          myWorld.SpawnEnemy(DataNameEnemy, getDataEnemy(), getSpeedMoveEnemy(), getPostionWave().x, getPostionWave().y);
                          countEnemy += 1;
                          setTimetoSpawn(getStarttimetoSpawn());
                      }
                  }else{
                      if(timetoNextWave>0){
                          timetoNextWave-=dt;
                      }else{
                          //NextWave();
                      }
                  }
              }else{
                 RestartSpawnWave();
              }
        }

    }

    public void GetDataAndSetData(String dataNameMapChose,int indexlevelMapChose){
        setDataNameMapChose(dataNameMapChose);
        setIndexLevelMapChose(indexlevelMapChose);
        LoadDataSpawnWave();
        LoadDataWave();
        resetSetting();
    }
    public void SpauseSpawn(){
        setStartSpawn(false);
    }
    private void resetSetting(){
       setIndexWave(0);
        setTimetoSpawn(getStarttimetoSpawn());
        setCountEnemy(0);
    }
    public void ReLoad(){
        LoadDataSpawnWave();
        LoadDataWave();
    }
    private void LoadDataSpawnWave(){
        String DataSpawn= LevelDataControll.instance.getDataSpawnWave(getIndexLevelMapChose());
        String[] splitData=DataSpawn.split("/");
        setDataNameMapChose(splitData[0]);
        setCountWave(Integer.parseInt(splitData[1]));
        setSpawnwavehaveBoss(Boolean.parseBoolean(splitData[2]));
        float totalTimeAllWave=Float.parseFloat(splitData[3]);
        float countEnemyAllWave=Integer.parseInt(splitData[4]);
    }
    private void LoadDataWave(){
        String DataWave= LevelDataControll.instance.getDataWave(getIndexLevelMapChose(),getIndexWave());
        String[] splitData=DataWave.split("/");
        setPostionWave(new Vector2(Float.parseFloat(splitData[0]),Float.parseFloat(splitData[1])));
        setDataNameEnemy(splitData[2]);
        setDataEnemy(gameController.getDataEnemy(getDataNameEnemy()));
        setCountEnemySpawn(Integer.parseInt(splitData[3]));
        setStarttimetoSpawn(Float.parseFloat(splitData[4]));
        setSpeedMoveEnemy(Float.parseFloat(splitData[5]));
        setTimetoNextWave(Float.parseFloat(splitData[6]));
    }

    private void NextWave(){
        indexWave+=1;
        if(indexWave<getCountWave()) {
            LoadDataWave();
            resetSetting();
        }else{
            indexWave=0;
            LoadDataWave();
            resetSetting();

        }
    }
    public void RestartSpawnWave(){
        resetSetting();
    }
    public boolean isStartSpawn() {
        return startSpawn;
    }

    public void setStartSpawn(boolean startSpawn) {

        if(myWorld==null)myWorld=gameController.getMyWorld();
        this.startSpawn = startSpawn;
    }

    public String getDataNameMapChose() {
        return DataNameMapChose;
    }

    public void setDataNameMapChose(String dataNameMapChose) {
        DataNameMapChose = dataNameMapChose;
    }

    public int getIndexLevelMapChose() {
        return indexLevelMapChose;
    }

    public void setIndexLevelMapChose(int indexLevelMapChose) {
        this.indexLevelMapChose = indexLevelMapChose;
    }

    public int getCountWave() {
        return countWave;
    }

    public void setCountWave(int countWave) {
        this.countWave = countWave;
    }

    public float getTimetoSpawn() {
        return timetoSpawn;
    }

    public void setTimetoSpawn(float timetoSpawn) {
        this.timetoSpawn = timetoSpawn;
    }

    public float getTimetoNextWave() {
        return timetoNextWave;
    }

    public void setTimetoNextWave(float timetoNextWave) {
        this.timetoNextWave = timetoNextWave;
    }
    public String getDataNameMap() {
        return DataNameMap;
    }

    public void setDataNameMap(String dataNameMap) {
        DataNameMap = dataNameMap;
    }

    public boolean isSpawnwavehaveBoss() {
        return spawnwavehaveBoss;
    }

    public void setSpawnwavehaveBoss(boolean spawnwavehaveBoss) {
        this.spawnwavehaveBoss = spawnwavehaveBoss;
    }

    public int getIndexWave() {
        return indexWave;
    }

    public void setIndexWave(int indexWave) {
        this.indexWave = indexWave;
    }

    public String getDataNameEnemy() {
        return DataNameEnemy;
    }

    public void setDataNameEnemy(String dataNameEnemy) {
        DataNameEnemy = dataNameEnemy;
    }

    public int getCountEnemy() {
        return countEnemy;
    }

    public void setCountEnemy(int countEnemy) {
        this.countEnemy = countEnemy;
    }

    public float getSpeedMoveEnemy() {
        return speedMoveEnemy;
    }

    public void setSpeedMoveEnemy(float speedMoveEnemy) {
        this.speedMoveEnemy = speedMoveEnemy;
    }

    public Vector2 getPostionWave() {
        return postionWave;
    }

    public void setPostionWave(Vector2 postionWave) {
        this.postionWave = postionWave;
    }
    public String getDataEnemy() {
        return DataEnemy;
    }

    public void setDataEnemy(String dataEnemy) {
        DataEnemy = dataEnemy;
    }
    public float getStarttimetoSpawn() {
        return starttimetoSpawn;
    }

    public void setStarttimetoSpawn(float starttimetoSpawn) {
        this.starttimetoSpawn = starttimetoSpawn;
    }
    public int getCountEnemySpawn() {
        return countEnemySpawn;
    }

    public void setCountEnemySpawn(int countEnemySpawn) {
        this.countEnemySpawn = countEnemySpawn;
    }
    public void SpawnTest(Vector2 pos){
        Vector3 position=myWorld.getScreenGame().getGamecam().unproject(new Vector3(pos.x,UIScene.instance.stage.getViewport().getScreenHeight()-pos.y,0));
        myWorld.SpawnEnemy(DataNameEnemy, getDataEnemy(), getSpeedMoveEnemy(), position.x,position.y);
    }

}
