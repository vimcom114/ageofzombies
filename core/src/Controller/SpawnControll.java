package Controller;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import Model.ItemType;
import ScreenGame.WorldScreen;


public class SpawnControll {
    GameController gameController;
    ArrayList<ItemType> listItemWaitSpawn;
    WorldScreen worldScreen;
    public SpawnControll(WorldScreen worldscreen){

       worldScreen=worldscreen;
       listItemWaitSpawn=new ArrayList<>();
    }
    public void SpawnWhat(int Type,String Name,float x, float y) {
        ItemType itemType = null;
        if(Name.equals("Return"))worldScreen.getMyWorld().setReturnPoint(new Vector2(x,y),Type);
        switch (Type) {

        }
    }

    public void startSpawn(){
        for(int i=0;i<listItemWaitSpawn.size();i++){
            worldScreen.getMyWorld().spawnItem(listItemWaitSpawn.get(i));
        }
        listItemWaitSpawn.clear();
    }
    public void clearList(){

        listItemWaitSpawn.clear();
    }
}
