package Tools;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;

import GameGDX.GDX;
import Model.Enemy;
import Model.Player;
import Model.Zombies;

public class CollisionSystem extends IteratingSystem {
    ComponentMapper<CollisionComponent> cm;

    @SuppressWarnings("unchecked")
    public CollisionSystem() {
        // only need to worry about player collisions
        super(Family.all(CollisionComponent.class).get());
        cm = ComponentMapper.getFor(CollisionComponent.class);

    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        // get player collision component
        CollisionComponent cc = cm.get(entity);
        Entity collidedEntity = cc.collisionEntity;
        if(collidedEntity != null){
            TypeComponent type = collidedEntity.getComponent(TypeComponent.class);
            CollisionComponent collisionComponent=collidedEntity.getComponent(CollisionComponent.class);
            cc.Collision2DEnter(type,collisionComponent.getMyParent());
//            if(type != null){
//                switch(type.type){
//                    case TypeComponent.ENEMY:
//                        //do player hit enemy thing
//                        System.out.println("player hit enemy");
//                        break;
//                    case TypeComponent.SCENERY:
//                        //do player hit scenery thing
//                        System.out.println("player hit scenery");
//                        break;
//                    case TypeComponent.OTHER:
//                        //do player hit other thing
//                        System.out.println("player hit other");
//                        break; //technically this isn't needed
//                    case TypeComponent.PLAYER:
//                        //do player hit other thing
//                        System.out.println(" hit Player");
//                        break;
//                }
//                cc.collisionEntity = null; // collision handled reset component
//            }
            cc.collisionEntity = null; // collision handled reset component
        }

    }


}
