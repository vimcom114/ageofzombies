package Tools;

import com.badlogic.ashley.core.Component;

public class TypeComponent implements Component {
    public static final int PLAYER = 0;
    public static final int BULLET_PLAYER = 5;
    public static final int GERNADE_PLAYER = 6;
    public static final int ENEMY = 1;
    public static final int ITEM = 3;
    public static final int OTHER = 4;

    public int type = OTHER;

}
