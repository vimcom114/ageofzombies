package Tools;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;


/**
 * A ParallaxBacground encapsulates the logic to Render ParallaxLayers.
 * It owns a collection of ParallaxLayers. 
 * These Layers a rendered on screen showing parallax effect based on scrolling characteristics of individual layers. 
 * This rendering module does not owns any Batch ,you need to provide one in the draw method. 
 * <p>
 * Also see {@link ParallaxLayer}. 
 * @author Rahul Verma
 *
 */
public class ParallaxBackground {

	public Array<ParallaxLayer> layers;
	public Array<ParallaxLayer> layersTop;
	private Matrix4 cachedProjectionView;
	private Vector3 cachedPos;
	private float cachedZoom;

	public ParallaxBackground(){
		initialize();
	}
	ArrayList<Vector2> listPosDraw=new ArrayList<>();
	

	public ParallaxBackground(ParallaxLayer... layers){
		initialize();
		this.layers.addAll(layers);
	}
	
    private void initialize() {
    	layers = new Array<ParallaxLayer>();
		layersTop = new Array<ParallaxLayer>();
		cachedPos = new Vector3();
		cachedProjectionView = new Matrix4();
	}
	

	public void addLayers(ParallaxLayer layers){
		this.layers.add(layers);
	}
	public void addLayersTop(ParallaxLayer layers){
		this.layersTop.add(layers);
	}


	public void draw(OrthographicCamera worldCamera, Batch batch, float dt){
		cachedProjectionView.set(worldCamera.combined);
		cachedPos.set(worldCamera.position);
		cachedZoom = worldCamera.zoom;
		for(int i=0; i<layers.size; i++) {

			ParallaxLayer layer = layers.get(i);
			Vector2 origCameraPos = new Vector2(cachedPos.x, cachedPos.y);
			worldCamera.position.set(origCameraPos.scl(layer.getParallaxRatio()), cachedPos.z);
			worldCamera.update();
			batch.setProjectionMatrix(worldCamera.combined);
			float currentX = 0;
			if (layer.getTileModeX().equals(ParallaxLayer.TileMode.auto) == false) {
				currentX = (layer.getTileModeX().equals(ParallaxLayer.TileMode.single) ? worldCamera.position.x - worldCamera.viewportWidth * .15f :
						((int) ((worldCamera.position.x - worldCamera.viewportWidth * .5f * worldCamera.zoom) / layer.getWidth())) *
								layer.getWidth()) - (Math.abs((1 - layer.getParallaxRatio().x) % 1) * worldCamera.viewportWidth * .5f);
			} else {
				layer.setPosX(layer.getPosX() - (layer.getSpeed() * dt));
				currentX = layer.getPosX();
			}
			int flipX = 1;
			do {
				float currentY = (layer.getTileModeY().equals(ParallaxLayer.TileMode.single) ? 0 : ((int) ((worldCamera.position.y - worldCamera.viewportHeight * .5f * worldCamera.zoom) / layer.getHeight())) * layer.getHeight()) - (((1 - layer.getParallaxRatio().y) % 1) * worldCamera.viewportHeight * .5f);
				do {
					if (!((worldCamera.position.x - worldCamera.viewportWidth * worldCamera.zoom * .5f > currentX + layer.getWidth()) || (worldCamera.position.x + worldCamera.viewportWidth * worldCamera.zoom * .5f < currentX) || (worldCamera.position.y - worldCamera.viewportHeight * worldCamera.zoom * .5f > currentY + layer.getHeight()) || (worldCamera.position.y + worldCamera.viewportHeight * worldCamera.zoom * .5f < currentY))) {
						layer.DrawRepeat(batch, new Vector2(currentX, currentY), flipX);
					}
					currentY += layer.getHeight();
					if (layer.getTileModeY().equals(ParallaxLayer.TileMode.single))
						break;
				} while (currentY < worldCamera.position.y + worldCamera.viewportHeight * worldCamera.zoom * .5f);
				currentX += layer.getWidth();

				if (layer.getTileModeX().equals(ParallaxLayer.TileMode.single))
					break;

			} while (currentX < worldCamera.position.x + worldCamera.viewportWidth * worldCamera.zoom * .5f);

		}
		worldCamera.combined.set(cachedProjectionView);
		worldCamera.position.set(cachedPos);
		worldCamera.zoom = cachedZoom;
		worldCamera.update();
		//batch.setProjectionMatrix(worldCamera.combined);
	}

	public void drawTop(OrthographicCamera worldCamera, Batch batch, float dt){
		if(layersTop.size==0)return;
		cachedProjectionView.set(worldCamera.combined);
		cachedPos.set(worldCamera.position);
		cachedZoom = worldCamera.zoom;
		for(int i=0; i<layersTop.size; i++) {

			ParallaxLayer layer = layersTop.get(i);
			Vector2 origCameraPos = new Vector2(cachedPos.x, cachedPos.y);
			worldCamera.position.set(origCameraPos.scl(layer.getParallaxRatio()), cachedPos.z);
			worldCamera.update();
			batch.setProjectionMatrix(worldCamera.combined);
			float currentX = 0;
			if (layer.getTileModeX().equals(ParallaxLayer.TileMode.auto) == false) {
				currentX = (layer.getTileModeX().equals(ParallaxLayer.TileMode.single) ? worldCamera.position.x - worldCamera.viewportWidth * .15f :
					((int) ((worldCamera.position.x - worldCamera.viewportWidth * .5f * worldCamera.zoom) / layer.getWidth())) *
							layer.getWidth()) - (Math.abs((1 - layer.getParallaxRatio().x) % 1) * worldCamera.viewportWidth * .5f);
			} else {
				layer.setPosX(layer.getPosX() - (layer.getSpeed() * dt));
				currentX = layer.getPosX();
			}
			int flipX = 1;
			do {
				float currentY = (layer.getTileModeY().equals(ParallaxLayer.TileMode.single) ? 0 : ((int) ((worldCamera.position.y - worldCamera.viewportHeight * .5f * worldCamera.zoom) / layer.getHeight())) * layer.getHeight()) - (((1 - layer.getParallaxRatio().y) % 1) * worldCamera.viewportHeight * .5f);
				do {
					if (!((worldCamera.position.x - worldCamera.viewportWidth * worldCamera.zoom * .5f > currentX + layer.getWidth()) || (worldCamera.position.x + worldCamera.viewportWidth * worldCamera.zoom * .5f < currentX) || (worldCamera.position.y - worldCamera.viewportHeight * worldCamera.zoom * .5f > currentY + layer.getHeight()) || (worldCamera.position.y + worldCamera.viewportHeight * worldCamera.zoom * .5f < currentY))) {
						layer.DrawRepeat(batch, new Vector2(currentX, currentY), flipX);

					}
					currentY += layer.getHeight();
					if (layer.getTileModeY().equals(ParallaxLayer.TileMode.single))
						break;
				} while (currentY < worldCamera.position.y + worldCamera.viewportHeight * worldCamera.zoom * .5f);
				currentX += layer.getWidth();

				if (layer.getTileModeX().equals(ParallaxLayer.TileMode.single))
					break;

			} while (currentX < worldCamera.position.x + worldCamera.viewportWidth * worldCamera.zoom * .5f);
		}
		worldCamera.combined.set(cachedProjectionView);
		worldCamera.position.set(cachedPos);
		worldCamera.zoom = cachedZoom;
		worldCamera.update();
		//batch.setProjectionMatrix(worldCamera.combined);
	}

	public void ClearLayer(){
		layers.clear();
		layersTop.clear();
	}
}
