package Tools.hud.progressbar;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import GameGDX.Loader;

/**
 * The progress bar which reassembles the behaviour of the health bar.
 * 
 * @author serhiy
 */
public class HealthBar extends ProgressBar {

	/**
	 * @param width of the health bar
	 * @param height of the health bar
	 */
	public HealthBar(int width, int height,String namBg,String nameFill, Group parent) {
		super(0f, 1f, 0.01f, false, new ProgressBarStyle());
		parent.addActor(this);
		TextureRegion bgBar= Loader.GetTexture(namBg);
		TextureRegion bgKnob= Loader.GetTextureNoAtlas(nameFill);
		bgKnob.setRegionWidth(0);
		getStyle().knob = new TextureRegionDrawable(bgKnob);
		TextureRegion bgKnobb= Loader.GetTextureNoAtlas(nameFill);
		getStyle().knobBefore = new TextureRegionDrawable(bgKnobb);

		setWidth(bgKnobb.getRegionWidth());
		setHeight(bgKnobb.getRegionHeight());
		
		setAnimateDuration(0.0f);
		setValue(1f);
		setAnimateDuration(0.25f);
	}

}
