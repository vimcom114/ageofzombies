package Tools;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.geda.game.MyGame;


import Model.BehindObject;
import Controller.MyWorld;


/**
 * 
 * TextureRegionParallaxLayer is the direct extension of {@link ParallaxLayer}. It represents a layer in the parallax scrolling background with just one texture region per layer . It still is very nifty and useful as it encapsulates the logic of padding in all the directions.
 * The padding can help you avoid using single full screen texture regions of background.
 * <p>
 * As an example let us suppose you wish to render repeatedly a single cloud region with 1/10  the width and height of the screen . You wish to render it at half way above the screen baseline. An plus no more than one could should be there within one screen width. You can easily achieve this using proper bottom, left and right padding in this layer  
 * @author Rahul
 *
 */
public class TextureRegionParallaxLayer extends ParallaxLayer  {



	/**
	 * Creates a TextureRegionParallaxLayer with regionWidth and regionHeight equal that of the texRegion. Paddings are set to 0.
	 * @param texRegion the texture region
	 * @param parallaxScrollRatio the parallax ratio in x and y direction
	 */
	TextureMapObject textureMapObject;

	BehindObject behindObject ;
	public TextureRegionParallaxLayer(TextureRegion texRegion, Vector2 parallaxScrollRatio){
		this.setTexRegion(texRegion);
		setRegionWidth(texRegion.getRegionWidth());
		setRegionHeight(texRegion.getRegionHeight());
		setParallaxRatio(parallaxScrollRatio);
	}
	

	public TextureRegionParallaxLayer(TextureRegion texRegion, float regionWidth, float regionHeight, Vector2 parallaxScrollRatio, float plusW, float plusH){
		this.setTexRegion(texRegion);
		setRegionWidth(regionWidth);
		setRegionHeight(regionHeight);
		setParallaxRatio(parallaxScrollRatio);
	}

	public TextureRegionParallaxLayer(MyWorld myWorld1, TextureRegion texRegion, Stage stage1, Vector2 parallaxScrollRatio, Utils.WH wh, float speed, int type, float padleft, float padbot, float plusW, float plusH){
		stage=stage1;
		myWorld= myWorld1;
		setPadLeft((stage.getWidth())*padleft);
		setPadBottom((stage.getHeight())*padbot);
		plusWidth=plusW;
		plusHeight=plusH;
		this.setTexRegion(texRegion);
		setSpeed(speed);
		Type=type;

		float calSize=((float)texRegion.getRegionWidth()/(float) MyGame.V_WIDTH);
		multiScreenScaleX=(stage.getWidth()*MyGame.PPM)*calSize;
		calSize=((float)texRegion.getRegionHeight()/(float)MyGame.V_HEIGHT);
		multiScreenScaleY= (stage.getHeight()*MyGame.PPM)*calSize;

		plusoutSize=getPadLeft()+(multiScreenScaleX/MyGame.PPM);
		if(plusoutSize>stage.getWidth()){
			plusoutSize=plusoutSize-stage.getWidth();
		}else{
			plusoutSize=0;
		}
        setPosX(getPadLeft());
		switch (type){
			case 1:
				tileModeX=TileMode.repeat;
				break;
			case 11:
				tileModeX=TileMode.repeat;
				break;
			case 2:
				tileModeX=TileMode.single;
				break;
			case 3:
				tileModeX=TileMode.auto;
				break;

		}
		switch(wh){

		    case width:
		    	if(Type!=11&&Type!=3) {
					setRegionWidth(stage.getWidth()+(plusWidth*stage.getWidth()));
				}else{
		    		if(Type==11){
						setRegionWidth(stage.getWidth());
		    		}
		    		if(Type==3) {
						setRegionWidth(((multiScreenScaleX / MyGame.PPM) + ((stage.getWidth() - getPadLeft()) - ((multiScreenScaleX / MyGame.PPM)))));
					}

				}
		    	setRegionHeight(Utils.calculateOtherDimension(Utils.WH.width, stage.getWidth(), this.getTexRegion()));

				break;
		    case height:
		    	setRegionHeight(stage.getWidth());
		    	setRegionWidth(Utils.calculateOtherDimension(Utils.WH.height, stage.getWidth(), this.getTexRegion()));
		    	break;
		}

		setParallaxRatio(parallaxScrollRatio);

	}

	@Override
	public void DrawRepeat(Batch batch, Vector2 pos, float scaleX) {
         if(Type!=11&&Type!=3) {
			 batch.draw(getTexRegion(), pos.x +getPadLeft() , pos.y + getPadBottom(), getRegionWidth() / 2, getRegionHeight() / 2, getRegionWidth(), getRegionHeight()+(plusHeight*stage.getHeight()), scaleX, 1, 0);
		 }else {
			 if (Type == 11) {
				 batch.draw(getTexRegion(), pos.x + getPadLeft(), pos.y + getPadBottom(), (multiScreenScaleX / MyGame.PPM) / 2, (multiScreenScaleY / MyGame.PPM) / 2,(multiScreenScaleX / MyGame.PPM)+(plusWidth*stage.getWidth()), (multiScreenScaleY / MyGame.PPM)+(plusHeight*stage.getHeight()),1,1,0);
			 }
			 if (Type == 3) {
				 batch.draw(getTexRegion(), pos.x, pos.y + getPadBottom(), (multiScreenScaleX / MyGame.PPM) / 2, (multiScreenScaleY / MyGame.PPM) / 2, multiScreenScaleX / MyGame.PPM, multiScreenScaleY / MyGame.PPM, scaleX, 1, 0);
			 }
		 }
	}
	@Override
	public void DrawAuto(Batch batch, OrthographicCamera camera, float dt){

	}

	@Override
	public void draw(Batch batch, float x, float y, float scaleX) {

	}
	@Override
	public float getWidth() {
		float Width=0;
		if(Type!=11&&Type!=3&&Type!=4) {
			Width = getRegionWidth() + getPadRight() - ((plusWidth * stage.getWidth()));
		}
		else{
			if(getPadLeft()<0.f) {
				Width = (getPadLeft())+getRegionWidth() + getPadRight();
			}else{
				Width = getRegionWidth() + getPadRight();
			}
		}
		return Width;
	}


	@Override
	public float getHeight() {
		return getPadTop()+getRegionHeight()+getPadBottom();
	}
	

	public void setAllPad(float pad){
		setPadLeft(pad);
		setPadRight(pad);
		setPadTop(pad);
		setPadBottom(pad);
	}





}
