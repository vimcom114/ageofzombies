package Tools;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class B2dBodyComponent implements Component {

    private Body b2body;
    private FixtureDef fdef;
    private BodyDef bdef;


    private Fixture fixture;

    public Body getB2body() {
        return b2body;
    }

    public void setB2body(Body b2body) {
        this.b2body = b2body;
    }

    public FixtureDef getFdef() {
        return fdef;
    }

    public void setFdef(FixtureDef fdef) {
        this.fdef = fdef;
    }

    public BodyDef getBdef() {
        return bdef;
    }

    public void setBdef(BodyDef bdef) {
        this.bdef = bdef;
    }
    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }
}