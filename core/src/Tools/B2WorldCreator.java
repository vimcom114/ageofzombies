package Tools;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.geda.game.MyGame;


import GameGDX.GDX;
import Model.BehindObject;

import Model.Ground;
import Controller.MyWorld;



/**
 * Created by brentaureli on 8/28/15.
 */
public class B2WorldCreator {


    private int mapPxWidth=0;

    private int mapPxHeight=0;
    private float mapWidth=0;
    private float mapHeight=0;
    private float realmapWidth=0;
    private float mapPixelWidth=0;
    private float mapPixelHeight=0;
    MyWorld myWorld;
    TiledMap map;
    World world;
    MapLayer behindObjectLayer;

    public B2WorldCreator(MyWorld myworld) {
        myWorld=myworld;
        world = myWorld.getWorld();
        map = myWorld.getMap();
        //create body and fixture variables
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        MapProperties prop = map.getProperties();

        mapPxWidth = prop.get("width", Integer.class);
        mapPxHeight= prop.get("height", Integer.class);
        int tilePixelWidth = prop.get("tilewidth", Integer.class);
        int tilePixelHeight = prop.get("tileheight", Integer.class);
         mapPixelWidth = mapPxWidth * tilePixelWidth;
         mapPixelHeight = mapPxHeight * tilePixelHeight;
        realmapWidth=((mapWidth)* MyGame.PLAYER_WIDTH/MyGame.PPM);
         mapWidth=((mapPxWidth)*MyGame.PLAYER_WIDTH/MyGame.PPM)-myworld.getScreenGame().getGamecam().viewportWidth*.5f;
        mapHeight=((mapPxHeight)*MyGame.PLAYER_WIDTH/MyGame.PPM)-myworld.getScreenGame().getGamecam().viewportHeight*.5f;
        behindObjectLayer = map.getLayers().get("BehindObject");

        //create ground bodies/fixtures


    }
    public  void creatMapObject(){
//        for(int i=0;i<map.getLayers().size();i++){
//            String nameLayer=map.getLayers().get(i).getName();
//            GDX.Show(""+nameLayer);
//        }
        for (MapObject object : map.getLayers().get(2).getObjects()) {
            String Data=object.getName();
            String strtype=object.getProperties().get("type",String.class);
            int type=0;
            if(strtype!=null){
                type=Integer.parseInt(strtype);
            }

            new Ground(myWorld,object,type,Data);
        }
//
//        for (MapObject object : map.getLayers().get(4).getObjects()) {
//            int id=map.getLayers().get(4).getObjects().getIndex(object);
//            String Data=object.getName();
//        }
//
//        //create coin bodies/fixtures
//        for (MapObject object : map.getLayers().get(5).getObjects()) {
//            int id=map.getLayers().get(5).getObjects().getIndex(object);
//            int type=0;
//            String Data=object.getName();
//            String strtype=object.getProperties().get("type",String.class);
//            if(strtype!=null) {
//                type=Integer.parseInt(strtype);
//            }
//
//        }
//        for (MapObject object : map.getLayers().get(6).getObjects()) {
//            int id=map.getLayers().get(6).getObjects().getIndex(object);
//            String name=object.getName();
//            int type=0;
//            String strtype=object.getProperties().get("type",String.class);
//            if(strtype!=null) {
//                type = Integer.parseInt(strtype);
//            }
//
//        }
//
//        for (MapObject object : map.getLayers().get(7).getObjects().getByType(RectangleMapObject.class)) {
//            int id=map.getLayers().get(7).getObjects().getIndex(object);
//            new Finish(myWorld,object,id);
//        }
//        for (MapObject object : map.getLayers().get(8).getObjects().getByType(RectangleMapObject.class)) {
//            int id=map.getLayers().get(8).getObjects().getIndex(object);
//            myWorld.setFlag(new Flag(myWorld,object,id));
//        }
//        for (MapObject object : map.getLayers().get(9).getObjects().getByType(RectangleMapObject.class)) {
//            int id=map.getLayers().get(9).getObjects().getIndex(object);
//            String strtype=object.getProperties().get("type",String.class);
//            String name=object.getName();
//            int type=0;
//            int index=0;
//            int returnPoin=0;
//            int TypeMove = 0;
//            if(strtype!=null) {
//                String[] splitType = strtype.split("_");
//                type = Integer.parseInt(splitType[0]);
//                TypeMove= Integer.parseInt(splitType[1]);
//                index= Integer.parseInt(splitType[2]);
//                if(splitType.length>3) {
//                    returnPoin = Integer.parseInt(splitType[3]);
//                }
//            }
//            new Gate(myWorld,object,id,type,index,TypeMove,returnPoin,name);
//        }
//        for (MapObject object : map.getLayers().get(10).getObjects().getByType(RectangleMapObject.class)) {
//            String strrun = object.getProperties().get("type", String.class);
//            String name = object.getName();
//
//            int type = Integer.parseInt(strrun);
//            Rectangle bounds = ((RectangleMapObject) object).getRectangle();
//            Vector2 posXY;
//            posXY = new Vector2((bounds.getX()) / MyGame.PPM, (bounds.getY()) / MyGame.PPM);
//            myWorld.getSpawnControll().SpawnWhat(type, name, posXY.x, posXY.y);
//        }
//
//        for (MapObject object : map.getLayers().get(11).getObjects().getByType(RectangleMapObject.class)) {
//            String strType = object.getProperties().get("type", String.class);
//            int type = 0;
//            if(strType!=null){
//                try{
//                    type=Integer.parseInt(strType);
//                }catch (Exception e){
//                    type=0;
//                }
//            }
//            String name=object.getName();
//            new Trigger(myWorld,object,name,type);
//        }
    }

    public float getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(float mapWidth) {
        this.mapWidth = mapWidth;
    }

    public float getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(float mapHeight) {
        this.mapHeight = mapHeight;
    }

    public float getMapPixelWidth() {

        return mapPixelWidth;
    }

    public void setMapPixelWidth(float mapPixelWidth) {
        this.mapPixelWidth = mapPixelWidth;
    }

    public float getMapPixelHeight() {
        return mapPixelHeight;
    }

    public void setMapPixelHeight(float mapPixelHeight) {
        this.mapPixelHeight = mapPixelHeight;
    }
    public MapLayer getBehindObjectLayer() {
        return behindObjectLayer;
    }

    public void setBehindObjectLayer(MapLayer behindObjectLayer) {
        this.behindObjectLayer = behindObjectLayer;
    }
    public BehindObject addTxtBehindObjectLayer(TextureRegion textureRegion, float w, float h) {
        TextureMapObject tmo = new TextureMapObject(textureRegion);
         behindObjectLayer.getObjects().add(tmo);
         MapObject mapObject=behindObjectLayer.getObjects().get(behindObjectLayer.getObjects().getCount()-1);
         mapObject.setName(""+textureRegion.getTexture().getTextureData());
         String name=mapObject.getName();
        BehindObject behindObject=new BehindObject(tmo,name,w,h,new Vector2(0.3f,0.3f));
        myWorld.getTileMapSprite().addListBehindMap(behindObject);
        return behindObject;
    }
    public void removeBehindObject(BehindObject behindObject){
        if(getBehindObjectLayer().getObjects().getCount()>0){
            String name=behindObject.getName();
            getBehindObjectLayer().getObjects().remove(behindObjectLayer.getObjects().get(name));
            myWorld.getTileMapSprite().remListBehindMap(behindObject);
        }
    }
    public TextureMapObject getTextureMap(BehindObject behindObject){
        String name=behindObject.getName();
        int index=behindObjectLayer.getObjects().getIndex(name);
        TextureMapObject character = (TextureMapObject)behindObjectLayer.getObjects().get(index);
        return character;
    }
    public float getRealmapWidth() {
        return realmapWidth;
    }

    public void setRealmapWidth(float realmapWidth) {
        this.realmapWidth = realmapWidth;
    }
    public Vector2 convertTileMapXY(Vector2 posXY){
        Vector2 XY=new Vector2((posXY.x>0?posXY.x/MyGame.PPM:0),(posXY.y>0?(mapPixelHeight-posXY.y)/MyGame.PPM:0));
        XY.x= MathUtils.clamp(XY.x, 0,mapWidth);
        XY.y= MathUtils.clamp(XY.y, 0,mapHeight);
        return XY;
    }
    public int getMapPxWidth() {
        return mapPxWidth;
    }

    public int getMapPxHeight() {
        return mapPxHeight;
    }
}
