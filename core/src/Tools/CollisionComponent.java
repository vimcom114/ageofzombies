package Tools;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

import GameGDX.GDX;

public class CollisionComponent  implements Component {
    public Entity collisionEntity;
    private GameObject myParent;
    public void Collision2DEnter(TypeComponent typeComponent,GameObject gameObjectColl) {
        if(getMyParent()!=null)getMyParent().Collision2DEnter(typeComponent,gameObjectColl);
    }
    public GameObject getMyParent() {
        return myParent;
    }
    public void setMyParent(GameObject myParent) {
        this.myParent = myParent;
    }

}
