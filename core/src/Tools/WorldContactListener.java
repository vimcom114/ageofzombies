package Tools;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import GameGDX.GDX;
import ScreenGame.WorldScreen;


/**
 * Created by brentaureli on 9/4/15.
 */
public class WorldContactListener implements ContactListener {

    boolean movingOut=false;
    WorldScreen worldScreen;
    public WorldContactListener(WorldScreen worldScreen1) {
        worldScreen = worldScreen1;
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture fa = contact.getFixtureA();
        Fixture fb = contact.getFixtureB();
        //if(fa.getFilterData().categoryBits ==fb.getFilterData().categoryBits)return;
        if(fa.getBody().getUserData() instanceof Entity){
            Entity ent = (Entity) fa.getBody().getUserData();
            entityCollision(ent,fb);

        }
        if(fb.getBody().getUserData() instanceof Entity){
            Entity ent = (Entity) fb.getBody().getUserData();
            entityCollision(ent,fa);

        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    private void entityCollision(Entity ent, Fixture fb) {
        if(fb.getBody().getUserData() instanceof Entity){
            Entity colEnt = (Entity) fb.getBody().getUserData();
            CollisionComponent col = ent.getComponent(CollisionComponent.class);
            CollisionComponent colb = colEnt.getComponent(CollisionComponent.class);
            if(col != null){
                col.collisionEntity = colEnt;
            }else if(colb != null){
                colb.collisionEntity = ent;
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        if(movingOut){
            Fixture fixA = contact.getFixtureA();
            Fixture fixB = contact.getFixtureB();

            int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;
            switch (cDef) {

            }
        }
    }
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}
