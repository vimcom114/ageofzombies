package Tools.PathFinding;

import Model.GRIDMAP.NodeMap;

/**
 * Heuristic functional interface.
 * 
 * @author Leonardo Ono (ono.leo80@gmail.com)
 */
public interface Heuristic {

    public double calculate(NodeMap start, NodeMap target, NodeMap current);
    
}
