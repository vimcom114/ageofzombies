package Tools.PathFinding;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import GameGDX.GDX;
import Model.GRIDMAP.Edge;
import Model.GRIDMAP.NodeMap;


public class Graph {

    private final List<NodeMap> nodes = new ArrayList<>();
    private Heuristic heuristic;
    
    public Graph() {
        this.heuristic = new Heuristic() {
            @Override
            public double calculate(NodeMap start, NodeMap target, NodeMap current) {
                int dx = (int) (target.getObj().x - current.getObj().x);
                int dy = (int) (target.getObj().y - current.getObj().y);
                return Math.sqrt(dx * dx + dy * dy);
            }
        };
    }

    public void addNode(NodeMap n) {
        nodes.add(n);
    }

    public List<NodeMap> getNodes() {
        return nodes;
    }

    public Heuristic getHeuristic() {
        return heuristic;
    }
    
    public void link(NodeMap a, NodeMap b, double cost) {
        Edge edge = new Edge(cost, a, b);
        a.addEdge(edge);
        b.addEdge(edge);
    }
    public void findPath(NodeMap start, NodeMap target, List<NodeMap> path) {
        for (NodeMap node : nodes) {
            node.setState(NodeMap.State.UNVISITED);
            node.setBackPathNode(null);
            node.setG(Double.MAX_VALUE);
        };
        
        start.setG(0);
        start.setH(heuristic.calculate(start, target, start));
        PriorityQueue<NodeMap> activeNodes = new PriorityQueue<>();
        activeNodes.add(start);
        
        while (!activeNodes.isEmpty()) {
            NodeMap currentNode = activeNodes.poll();
            currentNode.setState(NodeMap.State.CLOSED);
            // target node found !
            if (currentNode == target) {
                path.clear();
                target.retrievePath(path);
                return;
            }
            for (Edge edge : currentNode.getEdges()) {
                NodeMap neighborNode = edge.getOppositeNode(currentNode);
                double neighborG = currentNode.getG() + edge.getG();
                if (!neighborNode.isBlocked() && neighborG < neighborNode.getG()) {

                    neighborNode.setBackPathNode(currentNode);
                    neighborNode.setG(neighborG);
                    double h = heuristic.calculate(start, target, neighborNode);
                    neighborNode.setH(h);
                    if (!activeNodes.contains(neighborNode)) {
                        activeNodes.add(neighborNode);
                        neighborNode.setState(NodeMap.State.OPEN);
                    }
                }
            }

        }
    }
    
}
