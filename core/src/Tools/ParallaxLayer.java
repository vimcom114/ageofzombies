package Tools;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.geda.game.MyGame;

import Controller.MyWorld;



public abstract class ParallaxLayer  {
	public void setTexRegion(TextureRegion texRegion) {
		this.texRegion = texRegion;
	}

	private TextureRegion texRegion;
	public TextureRegion getTexRegion() {
		return texRegion;
	}

	public enum TileMode{
		repeat,single,auto
	}
	

	protected Vector2 parallaxRatio;
	protected TileMode tileModeX = TileMode.repeat;
	protected TileMode tileModeY = TileMode.single;


	private float PosY=0;
	private float PosX=0;

	private float Speed=0;
	public int Type=0;

	Stage stage;
	private float padLeft=0,padRight=0,padBottom=0,padTop=0;
	private float regionWidth,regionHeight;
	float multiScreenScaleX=0,multiScreenScaleY=0;
	float plusoutSize=0,plusWidth=0,plusHeight=0;
	public abstract float getWidth();

	public abstract float getHeight();
	public MyWorld myWorld;


	public Vector2 getParallaxRatio() {
		return parallaxRatio;
	}

	public void setParallaxRatio(Vector2 parallaxRatio) {
		if(this.parallaxRatio == null)
			this.parallaxRatio = new Vector2();
		this.parallaxRatio.set(parallaxRatio);
	}
	

	public void setParallaxRatio(float ratioX, float ratioY) {
		if(this.parallaxRatio == null)
			this.parallaxRatio = new Vector2();
		this.parallaxRatio.set(ratioX,ratioY);
	}

	public abstract void DrawRepeat(Batch batch, Vector2 pos, float scaleX);

	public abstract void DrawAuto(Batch batch, OrthographicCamera camera, float dt);


	public abstract void draw(Batch batch, float x, float y, float scaleX);


	public TileMode getTileModeX() {
		return tileModeX;
	}

	public void setTileModeX(TileMode tileModeX) {
		this.tileModeX = tileModeX;
	}


	public TileMode getTileModeY() {
		return tileModeY;
	}

	/**
	 * set the {@link TileMode} in y direction
	 * @param tileModeY TileMode in y direction
	 */
	public void setTileModeY(TileMode tileModeY) {
		this.tileModeY = tileModeY;
	}
	public float getPosX() {
		return PosX;
	}

	public void setPosX(float posX) {
		PosX = posX;
	}


	public float getPosY() {
		return PosY;
	}

	public void setPosY(float posY) {
		PosY = posY;
	}
	public float getSpeed() {
		return Speed;
	}

	public void setSpeed(float speed) {
		Speed = speed;
	}
	public float parToStageW(){

		return getPadLeft()+(multiScreenScaleX/ MyGame.PPM);
	}

	public float getPadLeft() {

		return padLeft;
	}

	public void setPadLeft(float padleft) {
		this.padLeft = padleft;
		setPosX(padLeft);
	}


	public float getPadRight() {
		return padRight;
	}


	public void setPadRight(float padRight) {
		this.padRight = padRight;
	}


	public float getPadBottom() {
		return padBottom;
	}


	public void setPadBottom(float padBottom) {
		this.padBottom = padBottom;
	}


	public float getPadTop() {
		return padTop;
	}


	public void setPadTop(float padTop) {
		this.padTop = padTop;
	}


	public float getRegionWidth() {

		return regionWidth;
	}


	public float getRegionHeight() {
		return regionHeight;
	}


	public void setRegionWidth(float width){
		this.regionWidth = width;
	}
	public void setRegionHeight(float height){
		if(Type==2)height=MyGame.V_HEIGHT/MyGame.PPM;
		this.regionHeight = height;
	}
}
