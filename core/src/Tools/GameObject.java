package Tools;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.scenes.scene2d.Group;

import Model.Bullet;
import Model.Enemy;

public class GameObject extends Group implements Component{
    public Entity entity;

    private Enemy myEnemy;

    private Bullet myBullet;

    public void Collision2DEnter(TypeComponent typeComponent,GameObject gameObjectColl){
    }
    public void addComponent(Component component){
       entity.add(component);
    }
    public Enemy getMyEnemy() {
        return myEnemy;
    }

    public void setMyEnemy(Enemy myEnemy) {
        this.myEnemy = myEnemy;
    }
    public Bullet getMyBullet() {
        return myBullet;
    }

    public void setMyBullet(Bullet myBullet) {
        this.myBullet = myBullet;
    }
}
