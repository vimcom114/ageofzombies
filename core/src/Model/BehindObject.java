package Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.Vector2;
import com.geda.game.MyGame;


public class BehindObject {
    TextureMapObject textureMapObject;
    float w,h;
    Vector2 Pos;
    String Name;
    float firstW=0;
    float firstH=0;

    public float getScaleX() {
        return scaleX;
    }

    public void setScaleX(float scaleX) {

        this.scaleX = scaleX;
    }

    float scaleX=1;

    public float getZ() {
        return Z;
    }

    public void setZ(float z) {
        Z = z;
    }

    float Z=0;
    public BehindObject(TextureMapObject textureMap, String name, float width, float height, Vector2 pos){
        textureMapObject=textureMap;
        Name=name;
        w=width;
        h=height;
        firstW=w;
        firstH=h;
        Pos=pos;
    }
    public void drawBehind(Batch batch){
        batch.draw(textureMapObject.getTextureRegion(), Pos.x, Pos.y,(w)/ MyGame.PPM,(h)/MyGame.PPM);
    }
    public TextureMapObject getTextureMapObject() {
        return textureMapObject;
    }

    public void setTextureMapObject(TextureRegion region) {
        this.textureMapObject.setTextureRegion(region);
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;

    }

    public float getH() {
        return h;
    }

    public void setH(float h) {
        this.h = h;

    }

    public Vector2 getPos() {
        return Pos;
    }

    public void setPos(Vector2 pos) {
        Pos = pos;
        textureMapObject.setX(pos.x);
        textureMapObject.setY(pos.y);
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
    public boolean checkContant(TextureMapObject textureMapObject1){
        boolean check=false;
        if(textureMapObject.equals(textureMapObject1)) check= true;
        return check;
    }
    public void Enable(boolean enable){
        if(enable){
            setW(0);
            setH(0);
        }else{
            setW(firstW);
            setH(firstH);
        }
    }
}
