package Model;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;

import Controller.MyWorld;
import GameGDX.Loader;
import GameGDX.UI;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;

public abstract class Bullet extends GameObject {
    public enum State {WAITING,PREPARE,SHOOT,COLL,DIE}
    State currentState;
    State previousState;
    private MyWorld myWorld;
    private World world;
    Image myFrame;

    CollisionComponent collisionComponent;
    TypeComponent typeComponent;
    B2dBodyComponent b2dBodyComponent;

    private int Dame=50;
    public Vector2 velocity;
    private float SpeedMove;


    private boolean waitDie;
    private boolean Destroy;
    public Bullet(Controller.MyWorld myworld, String DataTxt, Vector2 Speed, int dame, Vector2 firePoin, float Rotate){
        myWorld=myworld;
        world=myworld.getWorld();
        myWorld.getScreenGame().addToLayer(1,this);
        creatComponent();
        SpeedMove=Speed.x;
        Dame=dame;
        myFrame= UI.NewImage(Loader.GetTexture(DataTxt),0,0,this);
        myFrame.setSize(20/ MyGame.PPM,20/MyGame.PPM);
        this.setSize(myFrame.getWidth(),myFrame.getHeight());
        setOrigin(Align.center);
        setPosition(firePoin.x,firePoin.y+0.1f, Align.center);
        setRotation(Rotate);
        defineBody();
        getB2body().setTransform(getB2body().getPosition().x,getB2body().getPosition().y,MathUtils.degreesToRadians*this.getRotation());
        velocity=velocityDir();
        currentState=State.WAITING;
        setZIndex(myWorld.getPlayer().getIndexPlayer()-1);
    }
    public void creatComponent(){
        Engine engine=myWorld.getEngineComponent();
        entity= engine.createEntity();

        collisionComponent = engine.createComponent(CollisionComponent.class);
        collisionComponent.setMyParent(this);
        typeComponent = engine.createComponent(TypeComponent.class);
        typeComponent.type=TypeComponent.BULLET_PLAYER;
        b2dBodyComponent = engine.createComponent(B2dBodyComponent.class);
        addComponent(collisionComponent);
        addComponent(typeComponent);
        addComponent(b2dBodyComponent);
        // add the entity to the engine
        engine.addEntity(entity);
        setMyBullet(this);

    }
    @Override
    public void act(float delta) {
        super.act(delta);
        getFrame(delta);
        if(Destroy){
            world.destroyBody(getB2body());
            setB2body(null);
            this.remove();
        }
        if(getB2body()==null)return;
        Update(delta);
        this.setPosition(getB2body().getPosition().x-this.getWidth()/2,getB2body().getPosition().y-this.getHeight()/2);
        CheckOutCamera(new Vector2(this.getX()+this.getWidth()/2,this.getY()+this.getHeight()/2));

    }
    public abstract void Update(float dt);
    public void getFrame(float dt) {
        switch (currentState) {
            case PREPARE:
                break;
            case SHOOT:
                break;
            case COLL:
                Coll();
                break;
            case DIE:
                Die();
                break;
            case WAITING:
            default:
                break;
        }

        previousState = currentState;
        //return our final adjusted frame
    }
    public abstract void defineBody();
    public abstract void CollDo(TypeComponent typeComponent, GameObject gameObjectColl);
    public abstract void Coll();
    public abstract void Die();
    private Vector2 velocityDir(){
        float velocity =SpeedMove ;// Your desired velocity of the car.
        float angle = MathUtils.degreesToRadians*this.getRotation(); // Body angle in radians.

        float velX = MathUtils.cos(angle) * velocity; // X-component.
        float velY = MathUtils.sin(angle) * velocity; // Y-component.
        return new Vector2(velX,velY);
    }
    private void CheckOutCamera(Vector2 myPoin){
        boolean check=myWorld.getScreenGame().checkOutCameraPoin(myPoin);
        if(check)Destroy=true;
    }
    public Body getB2body() {
        return b2dBodyComponent.getB2body();
    }

    public void setB2body(Body b2body) {
        b2dBodyComponent.setB2body( b2body);
    }

    public Fixture getFixture(){
        return b2dBodyComponent.getFixture();
    }
    public void setFixture(Fixture fixture){
        b2dBodyComponent.setFixture(fixture);
    }
    @Override
    public void Collision2DEnter(TypeComponent typeComponent, GameObject gameObjectColl) {
        super.Collision2DEnter(typeComponent, gameObjectColl);
        CollDo(typeComponent, gameObjectColl);
    }
    public Controller.MyWorld getMyWorld() {
        return myWorld;
    }

    public void setMyWorld(MyWorld myWorld) {
        this.myWorld = myWorld;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public boolean isDestroy() {
        return Destroy;
    }

    public void setDestroy(boolean destroy) {
        Destroy = destroy;
    }
    public int getDame() {
        return Dame;
    }

    public void setDame(int dame) {
        Dame = dame;
    }
    public boolean isWaitDie() {
        return waitDie;
    }

    public void setWaitDie(boolean waitDie) {
        this.waitDie = waitDie;
    }
}
