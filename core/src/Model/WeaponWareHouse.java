package Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import Controller.GernadeWP;
import Controller.MyWorld;
import GameGDX.GDX;


public class WeaponWareHouse {
    Controller.MyWorld myWorld;
    Player Player;
    ArrayList<WeaponType> listWeaponType=new ArrayList<>();
    HashMap<String,Weapon> listWeaponSpawn=new HashMap<>();

    public WeaponWareHouse(MyWorld myworld, Player player){

        myWorld=myworld;
        Player=player;
        LoadData();

    }
    private void LoadData(){
        String path = "WeaponWareHouse.txt";
        FileHandle file = Gdx.files.internal(path);
        FileReader fr = null;
        BufferedReader br = null;
        int countMap= 0;
        String line = null;
        try {
            // fr = new FileReader(file);
            br = new BufferedReader(file.reader());
            line = br.readLine();
            int CountWeapon = Integer.parseInt(line);
            String[] listSplit=null;
            String strGet="";
            for (int i = 0; i < CountWeapon; i++) {
                line=br.readLine();
                if(line==null) return;
                if(line.equals("Weapon:")) {
                    line = br.readLine();
                    listSplit = line.split("-NumberControll:");
                    int numberCT=Integer.parseInt(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-Type:");
                    int Type=Integer.parseInt(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-Code:");
                    String Code=listSplit[1];
                    line = br.readLine();
                    listSplit = line.split("-DataTextureWeapon:");
                    String DataTxtWeapon=listSplit[1];
                    line = br.readLine();
                    listSplit = line.split("-Mag:");
                    int MagCount=Integer.parseInt(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-TimeShoot:");
                    float timetoShoot=Float.parseFloat(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-TimeReload:");
                    float timetoReload=Float.parseFloat(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-DataTextureBullet:");
                    String DataTxtBullet=listSplit[1];
                    line = br.readLine();
                    listSplit = line.split("-SpeedBullet:");
                    String[] strSpeed=listSplit[1].split("-");
                    Vector2 speedBullet=new Vector2(Integer.parseInt(strSpeed[0]),Integer.parseInt(strSpeed[1]));
                    line = br.readLine();
                    listSplit = line.split("-DameBullet:");
                    int DameBullet=Integer.parseInt(listSplit[1]);
                    line = br.readLine();
                    listSplit = line.split("-DataTextureIcon:");
                    String dataTxtIcon=listSplit[1];

                    WeaponType weaponType=new WeaponType(numberCT,Type,Code,DataTxtWeapon,DataTxtBullet,MagCount,timetoShoot,timetoReload,speedBullet,DameBullet,dataTxtIcon);
                    listWeaponType.add(weaponType);

                }
            }

        } catch (FileNotFoundException e) {
            GDX.Show("FILENot"+e);
            e.printStackTrace();
        } catch (IOException e) {
            GDX.Show("Not");
            e.printStackTrace();
        }
    }

    public Weapon takeWeaponByCode(String Code){
        WeaponType weaponType=null;
        for (int i=0;i<listWeaponType.size();i++){
            if(listWeaponType.get(i).getCode().equals(Code)){
                weaponType=listWeaponType.get(i);
                return MakeWeapon(weaponType);
            }
        }
        return MakeWeapon(weaponType);
    }

    private Weapon MakeWeapon(WeaponType weaponType) {
        Weapon weapon = null;
        String code = weaponType.getCode();
        int Type = weaponType.getType();
        if (listWeaponSpawn.size() > 0 && listWeaponSpawn.containsKey(code)) {
            weapon = listWeaponSpawn.get(code);
        } else {
            weapon = MakeWeaponType(Type, weaponType);
        }

        return weapon;
    }
    private Weapon MakeWeaponType(int Type,WeaponType weaponType){
        Weapon weapon=null;
        switch (Type){
            case 1:
                weapon=new GunWP(myWorld,weaponType);
                break;
            case 2:
                weapon=new GernadeWP(myWorld,weaponType);
                break;
        }
        return weapon;
    }
}
