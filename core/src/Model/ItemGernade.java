package Model;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.geda.game.MyGame;

import Controller.MyWorld;

public class ItemGernade extends Item {
    public ItemGernade(MyWorld myworld, float x, float y, ItemType itemType) {
        super(myworld, x, y, itemType);
    }

    @Override
    public void defineItem() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX() + this.getWidth() / 2, getY() + this.getWidth() / 2);
        bdef.type = BodyDef.BodyType.KinematicBody;
        setB2body(world.createBody(bdef));

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(this.getWidth() * 0.5f);
        fdef.filter.categoryBits = MyGame.FRIEND_BIT;
        fdef.filter.maskBits = MyGame.PLAYER_BIT;
        fdef.shape = shape;
        setFixture(getB2body().createFixture(fdef));
        getB2body().setUserData(entity);

    }

    @Override
    public void CollTake() {
        myWorld.GivePlayerWeaponCode(myItemType.getCode());
        setToDestroy(true);
    }

    @Override
    public void ReUse() {
    }

    @Override
    public void Destroy() {
    }
}
