package Model;

import com.badlogic.gdx.math.Vector2;

public class WeaponType {
    private int NumberCT;
    private int Type=1;
    private String Code="";
    private String DataTextureWeapon="";
    private String DataTextureBullet="";
    private String DataTextureIcon="";
    private int MagCount=0;
    private float startTimeToShoot=0;
    private float startTimeToReload=0;
    private Vector2 speedBullet;
    private int dameBullet=50;

    public WeaponType(int numberCT,int type, String code, String dataTextureWeapon, String dataTextureBullet, int magCount, float startTimeToShoot, float startTimeToReload, Vector2 speedBullet,int Dame, String dataTextureIcon) {
        NumberCT=numberCT;
        Type = type;
        Code = code;
        DataTextureWeapon = dataTextureWeapon;
        DataTextureBullet = dataTextureBullet;
        DataTextureIcon = dataTextureIcon;
        MagCount = magCount;
        this.startTimeToShoot = startTimeToShoot;
        this.startTimeToReload = startTimeToReload;
        this.speedBullet = speedBullet;
        this.dameBullet=Dame;
    }
    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getDataTextureWeapon() {
        return DataTextureWeapon;
    }

    public void setDataTextureWeapon(String dataTextureWeapon) {
        DataTextureWeapon = dataTextureWeapon;
    }

    public String getDataTextureBullet() {
        return DataTextureBullet;
    }

    public void setDataTextureBullet(String dataTextureBullet) {
        DataTextureBullet = dataTextureBullet;
    }

    public int getMagCount() {
        return MagCount;
    }

    public void setMagCount(int magCount) {
        MagCount = magCount;
    }

    public float getStartTimeToShoot() {
        return startTimeToShoot;
    }

    public void setStartTimeToShoot(float startTimeToShoot) {
        this.startTimeToShoot = startTimeToShoot;
    }


    public float getStartTimeToReload() {
        return startTimeToReload;
    }

    public void setStartTimeToReload(float startTimeToReload) {
        this.startTimeToReload = startTimeToReload;
    }


    public Vector2 getSpeedBullet() {
        return speedBullet;
    }

    public void setSpeedBullet(Vector2 speedBullet) {
        this.speedBullet = speedBullet;
    }
    public String getDataTextureIcon() {
        return DataTextureIcon;
    }

    public void setDataTextureIcon(String dataTextureIcon) {
        DataTextureIcon = dataTextureIcon;
    }

    public int getDameBullet() {
        return dameBullet;
    }

    public void setDameBullet(int dameBullet) {
        this.dameBullet = dameBullet;
    }
    public int getNumberCT() {
        return NumberCT;
    }

    public void setNumberCT(int numberCT) {
        NumberCT = numberCT;
    }

}
