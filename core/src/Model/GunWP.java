package Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.geda.game.MyGame;

import Controller.MyWorld;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import Model.GRIDMAP.GridMap;

public class GunWP extends Weapon{
    private Animation flareGUnAnim;
    private Group gFlareGun;
    private Image flareGunFrame;
    float stateTimer=0;
    public GunWP(MyWorld myworld, WeaponType weaponType) {
        super(myworld, weaponType);
    }

    @Override
    public void Update(float dt) {
        if (StartShoot) {
            if(flareGunFrame!=null) {
                stateTimer += dt;
                Vector2 flarePlus=myPlayer.getFlareGunPoin();
                gFlareGun.setPosition(myPlayer.getX()+flarePlus.x,(myPlayer.getY()+(myPlayer.getHeight()*0.2f)+flarePlus.y));
                gFlareGun.setRotation(myPlayer.getMyRotate());
                flareGunFrame.setDrawable(new TextureRegionDrawable((TextureRegion) flareGUnAnim.getKeyFrame(stateTimer, true)));

            }
            if (timetoShoot > 0) {
                timetoShoot -= dt;
            } else {
                Vector2 fp=myPlayer.getFirepoin();
                //  GDX.Show("XP :"+fp);
                firePoin = calculateOrbit(myPlayer.getMyRotate(), new Vector2(myPlayer.getWidth()/2, myPlayer.getWidth()/2), new Vector2(myPlayer.getCenterPoin().x+fp.x, myPlayer.getCenterPoin().y+fp.y));
                Bullet bullet = new BulletPlayer(getMyWorld(), getMyWeaponType().getDataTextureBullet(), getMyWeaponType().getSpeedBullet(), getMyWeaponType().getDameBullet(), firePoin, myPlayer.getMyRotate());
                timetoShoot = getMyWeaponType().getStartTimeToShoot();
                if (checkIsDefaultWP() == false) {
                    boolean checkFinish = minusBullet(1);
                    getMyWorld().setCountIconWP(getCountBullet());
                    if (checkFinish) checkFinish();
                }
            }
        }
    }
    private void creatAnimation(){
        gFlareGun=new Group();
        gFlareGun.setSize(myPlayer.getWidth(),myPlayer.getHeight()*0.15f);
        getMyWorld().getScreenGame().getLayerGroup(1).addActor(gFlareGun);
        gFlareGun.setPosition(myPlayer.getX(),myPlayer.getY()+(myPlayer.getHeight()*0.2f));

        gFlareGun.setOrigin(Align.center);
        Array<TextureRegion> frames_Flare = new Array<TextureRegion>();
        int degree=0;
        for (int i=0;i<9;i++) {
            TextureRegion textureRegion_Flare = Loader.GetTexture("FlareGun_"+i);
            frames_Flare.add(textureRegion_Flare);
        }
        flareGUnAnim=  new Animation(0.05f, frames_Flare);
        TextureRegion textureRegion = (TextureRegion) flareGUnAnim.getKeyFrame(0, true);
        flareGunFrame = UI.NewImage(textureRegion, 0, 0, Align.bottomLeft, this);
        gFlareGun.addActor(flareGunFrame);
        flareGunFrame.setSize(flareGunFrame.getWidth()/MyGame.PPM,flareGunFrame.getHeight()/MyGame.PPM);
        flareGunFrame.setPosition(gFlareGun.getWidth()*0.85f, gFlareGun.getHeight()*0.4f, Align.left);
        flareGunFrame.setOrigin(Align.left);
        flareGunFrame.setVisible(false);

    }
    @Override
    public void setStartShoot(boolean shoot) {
        super.setStartShoot(shoot);
        if(StartShoot==false&&shoot)timetoShoot=0;
        flareGunFrame.setVisible(shoot);
        if(shoot&&StartShoot==false){
            stateTimer=0;
            flareGunFrame.setZIndex(myPlayer.getIndexPlayer()-1);
        }
        StartShoot=shoot;
    }

    @Override
    public void checkIsUse(Player player) {
        super.checkIsUse(player);
        if (isUse() == false) {
            myPlayer = player;
            myPlayer.addActor(this);
            creatAnimation();
            setMyFrame(UI.NewImage(Loader.GetTexture(getMyWeaponType().getDataTextureWeapon()), 0, 0, this));
            getMyFrame().setSize(80 / MyGame.PPM, 80 / MyGame.PPM);
            this.setSize(getMyFrame().getWidth(), getMyFrame().getHeight());
            getMyFrame().setPosition(0, 0);
            setPosition(myPlayer.getWidth(),myPlayer.getHeight()/2, Align.center);
            getMyFrame().setVisible(false);
            //Vector2 firePoinToWorld=new Vector2(player.getPoinToWorld(firePoin));
//          firePoin=firePoinToWorld;

        } else {
            setCountBullet(getMyWeaponType().getMagCount());
            setVisible(true);
        }
        getMyWorld().setIconWPDetail(""+getCountBullet(),getMyWeaponType().getDataTextureIcon());
    }

}
