package Model;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.geda.game.MyGame;


import java.text.DecimalFormat;

import Controller.MyWorld;
import GameGDX.GDX;
import Model.GRIDMAP.NodeMap;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;


/**
 * Created by brentaureli on 8/28/15.
 */
public abstract class InteractiveTileObject extends GameObject {
    int ID=0;
    protected World world;
    protected TiledMap map;
    protected Rectangle bounds;
    protected Polygon polybounds;
    protected Controller.MyWorld myWorld;
    protected MapObject object;
    CollisionComponent collisionComponent;
    TypeComponent typeComponent;
    B2dBodyComponent b2dBodyComponent;

    boolean destroyBody=false;
    Vector2 respawnPos;
    float Width=0;
    float Height=0;
    float xPolygon,yPolygon;
    private boolean isOutCamera=false;
    boolean isPolygon=false;
    public InteractiveTileObject(MyWorld myworld, MapObject object, int id) {
        ID = id;
        this.object = object;
        this.myWorld = myworld;
        this.world = myworld.getWorld();
        this.map = myworld.getMap();
        creatComponent();
        myWorld.addTileObject(this);
        defineObject();

        // player = myWorld.getPlayer();

    }
    public void creatComponent(){
        Engine engine=myWorld.getEngineComponent();
        entity= engine.createEntity();
        collisionComponent = engine.createComponent(CollisionComponent.class);
        typeComponent = engine.createComponent(TypeComponent.class);
        typeComponent.type=TypeComponent.OTHER;
        b2dBodyComponent = engine.createComponent(B2dBodyComponent.class);
        addComponent(collisionComponent);
        addComponent(typeComponent);
        addComponent(b2dBodyComponent);
        // add the entity to the engine
        engine.addEntity(entity);

    }
    public void defineObject(){
        BodyDef bdef=new BodyDef();
        FixtureDef fdef=new FixtureDef();
        PolygonShape shape = new PolygonShape();
        bdef.type = BodyDef.BodyType.StaticBody;
        if (object instanceof RectangleMapObject) {
            shape = getRectangle((RectangleMapObject)object);
            DecimalFormat df = new DecimalFormat("0.");
            bdef.position.set((bounds.getX() + bounds.getWidth()/ 2) / MyGame.PPM, (bounds.getY() + bounds.getHeight()/2 ) / MyGame.PPM);
            setWidthText(((int)(bounds.getWidth())) / MyGame.PPM);
            setHeightText(((int)(bounds.getHeight()))/ MyGame.PPM);
        }
        else if (object instanceof PolygonMapObject) {
            isPolygon=true;
            shape = getPolygon((PolygonMapObject)object);
            setWidthText((polybounds.getBoundingRectangle().getWidth()) / MyGame.PPM);
            setHeightText((polybounds.getBoundingRectangle().getHeight()) / MyGame.PPM);
        }
        b2dBodyComponent.setB2body(world.createBody(bdef));
        fdef.shape = shape;
        b2dBodyComponent.setFixture(b2dBodyComponent.getB2body().createFixture(fdef));
        b2dBodyComponent.getB2body().setUserData(entity);

    }
    public abstract void defineBody();
    public abstract void Destroy();
    public abstract void ReUse();
    public abstract void Update(float dt);

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //if(isPart==false&&isUseSpine==false&&mySprite==null||ReUse||stopDraw) return;
        super.draw(batch, parentAlpha);
    }

    public float getWidthText() {
        return Width;
    }

    public void setWidthText(float width) {
        Width = width;
    }


    public float getHeightText() {
        return Height;
    }

    public void setHeightText(float height) {
        Height = height;
    }

    public PolygonShape getRectangle(RectangleMapObject rectangleObject) {
        bounds= rectangleObject.getRectangle();
        PolygonShape polygon = new PolygonShape();
        polygon.setAsBox(bounds.getWidth() / 2 / MyGame.PPM, bounds.getHeight() / 2 / MyGame.PPM);
        return polygon;
    }

    public CircleShape getCircle(CircleMapObject circleObject) {
        Circle circle = circleObject.getCircle();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(circle.radius / MyGame.PPM);
        circleShape.setPosition(new Vector2(circle.x / MyGame.PPM, circle.y / MyGame.PPM));
        return circleShape;
    }

   public PolygonShape getPolygon(PolygonMapObject polygonObject) {
        polybounds=polygonObject.getPolygon();
        PolygonShape polygon = new PolygonShape();
        xPolygon=polygonObject.getPolygon().getX()/ MyGame.PPM;
        yPolygon= polygonObject.getPolygon().getY()/ MyGame.PPM;
        float[] vertices = polygonObject.getPolygon().getTransformedVertices();
        float[] worldVertices = new float[vertices.length];
        for (int i = 0; i < vertices.length; ++i) {
            worldVertices[i] = vertices[i] / MyGame.PPM;
        }

        polygon.set(worldVertices);
        return polygon;
    }
    public void setCategoryFilter(short filterBit){
        Filter filter = new Filter();
        filter.categoryBits = filterBit;
        filter.maskBits=MyGame.ENEMY_BIT|MyGame.PLAYER_BIT;
        filter.groupIndex=-2;
        b2dBodyComponent.getFixture().setFilterData(filter);
    }
    public Body getB2body() {
        return b2dBodyComponent.getB2body();
    }

    public void setB2body(Body b2body) {
        b2dBodyComponent.setB2body( b2body);
    }
    public Fixture getFixture(){
        return b2dBodyComponent.getFixture();
    }
    public void setFixture(Fixture fixture){
        b2dBodyComponent.setFixture(fixture);
    }

    public boolean checkPosCollision(Vector2 posC,float w,float h){
        Vector2 posCheck=new Vector2(posC.x-w/2,posC.y-h/2);
        boolean checkColl=false;
         boolean checkCollX=false;
         boolean checkCollY=false;
         if(posCheck.x<this.getX()&&posCheck.x+w>this.getX()||posCheck.x>this.getX()&&posCheck.x<this.getX()+getWidthText())checkCollX=true;
         if(posCheck.y<this.getY()&&posCheck.y+h>this.getY()||posCheck.y>this.getY()&&posCheck.y<this.getY()+getHeightText())checkCollY=true;
         if(checkCollX&&checkCollY)checkColl=true;
         return checkColl;
    }

    public void setNodeBlock(){

        int countRows= (int) (getHeightText()*MyGame.PPM)/MyGame.PLAYER_WIDTH;
        int countCols= (int) (getWidthText()*MyGame.PPM)/MyGame.PLAYER_WIDTH;;
        for(int i=0;i<countRows;i++){
            for(int j=0;j<countCols;j++){
                float x=this.getX()+((MyGame.PLAYER_WIDTH/MyGame.PPM)*j);
                float y=this.getY()+((MyGame.PLAYER_HEIGHT/MyGame.PPM)*i);
                NodeMap nodeMap=myWorld.getGridMap().convertRC(x,y);
                nodeMap.setBlocked(true);
            }
        }
    }
}
