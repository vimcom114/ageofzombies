package Model;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Queue;
import com.geda.game.MyGame;


import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Controller.GameController;
import Controller.MyWorld;
import GameGDX.GDX;
import GameGDX.GSpine.GSpine;
import GameGDX.GSpine.spine.AnimationState;
import GameGDX.GSpine.spine.IkConstraint;
import GameGDX.Loader;
import GameGDX.UI;
import Model.GRIDMAP.NodeMap;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;

public abstract class Enemy extends GameObject {
    public enum State {WALKING, FLY, MOVING_SHELL, STANDING_SHELL, JUMPING, STANDING, ATTACK_1, ATTACK_2, RUNNING, HURT, NEARDIE, DEAD}
    private HashMap<String, Animation> listAnimation=new HashMap<>();

    boolean flipAnm=false;
    int degreeAnm;
    private float myRotate=0;
    private Image myFrame;
    private Animation currentAnim;
    State currentState;
    State previousState;
    CollisionComponent collisionComponent;



    TypeComponent typeComponent;
    B2dBodyComponent b2dBodyComponent;
    private boolean runningRight,runningUp;

    boolean isBullet = false;
    boolean isFly = false;
    boolean isDead = false, nearDie;
    String nameTexture = "";
    AnimationState.TrackEntry tracks;
    IkConstraint ikSpine;
    int ID = 0;

    public float getyPlayPos() {
        return yPlayPos;
    }

    public void setyPlayPos(float xPlayPos) {
        this.yPlayPos = xPlayPos;
    }

    private float yPlayPos = 0;

    public class EnemyStats {
        public int maxHealth = 0;

        private int _curHealth;
        public int curHealth;

        public void setMaxHealth(int max) {
            maxHealth = max;
        }
        public int getCurHealth() {
            return _curHealth;
        }
        public void setCurHealth(int value) {
            curHealth += value;
            if (curHealth <= 0) curHealth = 0;
            _curHealth = MathUtils.clamp(curHealth, 0, maxHealth);
        }
        public void Init() {
            curHealth = maxHealth;
            setCurHealth(0);
        }
    }

    private EnemyStats stats = new EnemyStats();

    boolean isUseSpine = false;
    boolean isUsePart = false;
    boolean isUseAnmFrame = false;
    boolean isUseOneFrame = false;
    private float stateTimer;
    protected World world;
    protected MyWorld myWorld;

    public Vector2 velocity=new Vector2(0,0), deltaXY=new Vector2(0,0);
    private float distanceToPlayer=0,distanceToEndPath=0;
    TextureMapObject textureMapObject;
    int sizeFrame;
    BehindObject behindObject;
    public float stateTime;
    boolean isDie = false, isHit = false;
    Player player;
    private boolean isDraw;
    boolean setToDestroy;
    boolean destroyed;
    boolean Start = false;
    boolean AutoMove = false;
    boolean startGame = false;
    boolean Enable = false;
    boolean ReUse = false;
    Vector2 respawnPos;
    float targetRotate = 0;
    boolean resetAnm = false;
    boolean startFire_1 = false;
    boolean startFire_2 = false;
    int startcountFire_1 = 1;
    int countFire_1 = 1;
    int startcountFire_2 = 1;
    int countFire_2 = 1;
    float startTimeToFire_1 = 1;
    float timetoFire_1 = 0;
    float startTimeToFire_2 = 1;
    float timetoFire_2 = 0;
    boolean startJump = false;
    boolean startFadeColor = false, nolimitFadeColor = false, isColorIn = false, isColorOut = false;
    int countFadeColor = 0;
    Color colorIn, colorOut;
    Vector2 targetPath;
    Vector2 nowPath;
    Vector2 testPath;
    NodeMap previousNode,targetCity;
    NodeMap myNode,targetNode;
    private List<NodeMap> myPath;
    ShapeRenderer shapeRenderer;
    Queue<NodeMap> pathQueue = new Queue<>();
    int prevPathSize=0;
    float speed=0.5f;
    boolean startTest=false;
    boolean startCheckMoveAround,startMoveAroundTarget=false;
    Vector2 pointMoveAround;
    int directionMoveAround=1;
    float degreeMoveAround=0;
    int randChange=0;
    public Enemy(MyWorld myworld, float x, float y) {
        myworld.getScreenGame().addToLayer(1, this);
        shapeRenderer=new ShapeRenderer();
        myPath = new ArrayList<>();
        isDraw = true;
        this.world = myworld.getWorld();
        this.myWorld = myworld;
        player = myWorld.getPlayer();
        setPosition(x, y, Align.center);
        velocity = new Vector2(getCenterPoin());
        player = myWorld.getPlayer();
        currentState = State.STANDING;
        setOrigin(Align.center);
        creatComponent();

    }

    private void creatComponent() {
        Engine engine = myWorld.getEngineComponent();
        entity = engine.createEntity();

        collisionComponent = engine.createComponent(CollisionComponent.class);
        collisionComponent.setMyParent(this);
        typeComponent = engine.createComponent(TypeComponent.class);
        typeComponent.type = TypeComponent.ENEMY;
        setMyEnemy(this);
        b2dBodyComponent = engine.createComponent(B2dBodyComponent.class);
        addComponent(Enemy.this);
        addComponent(collisionComponent);
        addComponent(typeComponent);
        addComponent(b2dBodyComponent);
        // add the entity to the engine
        engine.addEntity(entity);
    }
    public void creatAnimation(String nameCharacter){
        //   Array<TextureRegion> frames_Idle = new Array<TextureRegion>();
        Array<TextureRegion>frames_Run = new Array<TextureRegion>();
        int degree=0;
        for (int i=0;i<9;i++) {
            degree=i*22;
            if(degree==88||i>=4)degree+=2;
            if(degree==178)degree+=2;
            for (int j = 0; j < 10; j++) {
//                TextureRegion textureRegion_Idle = Loader.GetTexture(nameCharacter +"_"+degree+"_Idle"+"_0"+j);
//                frames_Idle.add(textureRegion_Idle);
                TextureRegion textureRegion_Run = Loader.GetTexture(nameCharacter +"_"+degree+"_Run"+"_0"+j);
                frames_Run.add(textureRegion_Run);
            }
            //   Animation animation_Idle=  new Animation(0.05f, frames_Idle);
            Animation animation_Run=  new Animation(0.05f, frames_Run);
            //  listAnimation.put("Idle"+degree,animation_Idle);
            listAnimation.put("Run"+degree,animation_Run);
            // frames_Idle.clear();
            frames_Run.clear();
        }
        currentAnim=listAnimation.get(("Run0"));
        TextureRegion textureRegion = (TextureRegion) currentAnim.getKeyFrame(0, true);
        myFrame = UI.NewImage(textureRegion, 0, 0, Align.bottomLeft, this);
        myFrame.setSize(myFrame.getWidth()/MyGame.PPM,myFrame.getHeight()/MyGame.PPM);
        myFrame.setPosition(this.getWidth() / 2, this.getHeight() / 2, Align.center);
        myFrame.setOrigin(Align.center);
        setSize(myFrame.getWidth(),myFrame.getHeight());
        myFrame.setPosition(0,0);

    }
    public void setBehindObject(TextureRegion frames, float w, float h) {
        behindObject = myWorld.addTextureMap(frames, w, h);
        textureMapObject = myWorld.getCreator().getTextureMap(behindObject);
        behindObject.setPos(new Vector2(this.getX() - getWidth() / 2, this.getY()));
        respawnPos = new Vector2(getB2body().getPosition().x, getB2body().getPosition().y);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        //if (Start == false || isDraw == false) return;
        shapeRenderer.setProjectionMatrix(myWorld.getScreenGame().getGamecam().combined);
        if(myNode!=null&&targetNode!=null)draw(shapeRenderer,batch);
        super.draw(batch, parentAlpha);
    }

    public void Update(float dt) {

        if (setToDestroy && !destroyed) {
            world.destroyBody(getB2body());
            setB2body(null);
            destroyed = true;
            // setRegion(new TextureRegion(Loader.GetTexture("goomba"), 32,0,16, 16));
            //frames.setDrawable(new TextureRegionDrawable(new TextureRegion(Loader.GetTexture("goomba"), 32, 0, 16, 16)));
            stateTime = 0;
        }
        if (getB2body() == null) {
            remove();
            return;
        }
        CalculateDistance();
        CalculateNode();
        findPlayerPath();
        MyUpdate(dt);
        myFrame.setDrawable(new TextureRegionDrawable((getFrame(dt))));


    }

    public abstract void MyUpdate(float dt);
    public void MoveByType(){
        if(startMoveAroundTarget){

        }
    }

    public TextureRegion getFrame(float dt) {
        currentState = getState();
        TextureRegion region = null;
        degreeAnm = getdegreeAnim();
        degreeAnm = degreeAnm < 0 ? (int) (-1f * degreeAnm) : degreeAnm;
        if (degreeAnm < 5 || (degreeAnm >= 90 && degreeAnm < 95)) degreeAnm = 0;
        if (degreeAnm > 5 && degreeAnm <= 22.5f) degreeAnm = 22;
        if (degreeAnm > 22.5f && degreeAnm <= 45) degreeAnm = 44;
        if (degreeAnm > 45 && degreeAnm <= 80f) degreeAnm = 66;
        if (degreeAnm > 80f && degreeAnm <= 90) degreeAnm = 90;
        if (degreeAnm > 95 && degreeAnm <= 112.5f) degreeAnm = 112;
        if (degreeAnm > 112.5f && degreeAnm <= 135) degreeAnm = 134;
        if (degreeAnm > 135 && degreeAnm <= 170f) degreeAnm = 156;
        if (degreeAnm > 170f && degreeAnm <= 180) degreeAnm = 180;

//
        if(degreeAnm >110&&this.getZIndex()<player.getIndexPlayer())this.setZIndex(player.getIndexPlayer());
        else if(degreeAnm <110&&this.getZIndex()>player.getIndexPlayer())this.setZIndex(player.getIndexPlayer()-1);

        switch (currentState) {
            case DEAD:
                Die();
                break;
            case NEARDIE:
                nearDie = false;
                break;
            case HURT:
                if (startFadeColor) startFadeColor = false;
                break;
            case ATTACK_1:
                Attack_1(dt);
                break;
            case ATTACK_2:
                Attack_2(dt);
                break;
            case JUMPING:
                Jump();
                break;
            case RUNNING:
                Running();
                if (listAnimation.containsKey(("Run" + degreeAnm))) {
                    currentAnim = listAnimation.get(("Run" + degreeAnm));
                }
                region = (TextureRegion) currentAnim.getKeyFrame(stateTimer, true);
                break;
            case STANDING:
            default:
                if (listAnimation.containsKey(("Run" + degreeAnm))) {
                    currentAnim = listAnimation.get(("Run" + degreeAnm));
                }
                region = (TextureRegion) currentAnim.getKeyFrame(stateTimer, true);
                break;
        }

        if (region!=null&&flipAnm && !region.isFlipX()) {
            region.flip(true, false);
            runningRight = false;
        } else if (region!=null&&flipAnm == false && region.isFlipX()) {
            region.flip(true, false);
            runningRight = true;
        }
        //if the current state is the same as the previous state increase the state timer.
        //otherwise the state has changed and we need to reset timer.
        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        //update previous state
        previousState = currentState;
        //return our final adjusted frame

        return region;
    }


    public State getState() {
        //Test to Box2D for velocity on the X and Y-Axis
        //if mario is going positive in Y-Axis he is jumping... or if he just jumped and is falling remain in jump state
        if (isDie())
            return State.DEAD;
        else if (nearDie)
            return State.NEARDIE;
        else if (isHit)
            return State.HURT;
        else if (startFire_1)
            return State.ATTACK_1;
        else if (startFire_2)
            return State.ATTACK_2;
        else if ((startJump))
            return State.JUMPING;
        else if (getB2body().getLinearVelocity().x != 0)
            return State.RUNNING;
        else if (isFly)
            return State.FLY;
        else
            return State.STANDING;
    }

    protected abstract void defineEnemy();
    private void CalculateDistance(){
        Vector2 target=player.getCenterPoin();
        if(myPath!=null&&myPath.size()>0)target=myPath.get(myPath.size()-1).getCenterPoin();
        distanceToEndPath = Vector2.dst(getCenterPoin().x, getCenterPoin().y, target.x, target.y);
        distanceToPlayer = Vector2.dst(getCenterPoin().x, getCenterPoin().y, player.getCenterPoin().x,player.getCenterPoin().y);
    }
    private void CalculateNode() {
        if (myWorld.getGridMap() != null) {
            nowPath = getCenterPoin();
            NodeMap Nownode = myWorld.getGridMap().convertRC(nowPath.x, nowPath.y);
            if (myNode != null && (myNode.getRows() != Nownode.getRows() || myNode.getCols() != Nownode.getCols())) {
                myNode.setColorNode(false, Color.WHITE);
                myNode.setBlocked(false);
                myNode = Nownode;
              //  if(myNode!=targetNode)myNode.setBlocked(true);
                //   myNode.setColorNode(true, Color.RED);
            } else {
                if (myNode == null) {
                    myNode = Nownode;
                   // myNode.setBlocked(true);
                    //myNode.setColorNode(true, Color.RED);
                }
            }
        }
    }
    private void findPlayerPath() {
        Vector2 RC=player.getRandomNodeFCenter(getCenterPoin());
        targetNode =  myWorld.getGridMap().convertRC(RC.x,RC.y);
        myPath.clear();
        targetCity=null;
        if (targetNode != null && myNode != null){
            myWorld.getGridMap().getpath(myNode, targetNode, myPath);
            calculatePathQueue();
        }
    }



    private void calculatePathQueue() {

        if (myPath.size() > 1 && (prevPathSize != myPath.size())&&distanceToPlayer>1.2f) {
            if (previousNode != null) {
                previousNode.setBlocked(false);
                previousNode = null;
            }
            targetCity = myPath.get(1);
            targetPath = targetCity.getCenterPoin();
            startCheckMoveAround = false;
            startMoveAroundTarget = false;
             GDX.Show("Goo ");
        }else {
            if (startCheckMoveAround == false) {
                if (startMoveAroundTarget == false) {
                    degreeMoveAround = rotateToPoint(player.getCenterPoin(), getCenterPoin());
                    startCheckMoveAround = true;
                    startTest = true;
                }
            } else {
                pointMoveAround = getDirectionPoin(degreeMoveAround, new Vector2(player.getWidth()/2, player.getWidth()/2), player.getCenterPoin());
                NodeMap nodeMap = myWorld.getGridMap().convertRC(pointMoveAround.x, pointMoveAround.y);
                targetPath = pointMoveAround;
                if (nodeMap.isBlocked() == false) {
                    //   if (previousNode != null) previousNode.setBlocked(false);
                    // nodeMap.setBlocked(true);
                    previousNode = nodeMap;
                    startMoveAroundTarget = true;
                    targetPath = pointMoveAround;
                    startCheckMoveAround = false;
                    randChange=GDX.RandomInt(1,5);

                    // GDX.Show("Move");
                } else {
                    directionMoveAround = -directionMoveAround;
                    degreeMoveAround += (directionMoveAround * 25);
                    if (degreeMoveAround < 0 || degreeMoveAround > 360) degreeMoveAround = 0;
                }
            }
            if (startMoveAroundTarget) {
                // testPath=player.getRandomNodeFCenter(getCenterPoin());
//                float distance = Vector2.dst(testPath.x, testPath.y, targetPath.x, targetPath.y);
//                GDX.Show("D :"+distance);
//                if (distance < 0.2f) {
//                    targetPath = getCenterPoin();
                int rand=GDX.RandomInt(1,5);
                if(rand==randChange) {
                    startMoveAroundTarget = false;
                    startCheckMoveAround = true;
//                    directionMoveAround = GDX.RandomInt(-1, 1);
                    degreeMoveAround += (directionMoveAround * 25);
                }

            }
        }


    }



    private int getdegreeAnim(){
        float myDegree=myRotate;
        //GDX.Show("My :"+myRotate);
        float calculate=0;
        flipAnm=false;
//        if(myDegree>270&&myDegree<315){
//            firepoin=new Vector2(-0.0f,0);
//        }else if (myDegree > 225 && myDegree < 270) {
//            firepoin = new Vector2(0.0f, 0);
//        }else{
//            firepoin = new Vector2(0, -0.25f);
//        }
        if(myDegree>270&&myDegree<=360){
            calculate=-((myDegree-270)-90);
        }
        if(myDegree>180&&myDegree<=270){
            flipAnm=true;
            calculate=-((myDegree-180));
        }
        if(myDegree>0&&myDegree<=90){
            calculate=90+((myDegree/90f)*90);
        }
        if(myDegree>90&&myDegree<=180){
            flipAnm=true;
            calculate=-(270-myDegree);
        }
        int degreeAnm=(int)calculate;

        return degreeAnm;
    }

    public Vector2 calculateOrbit(float currentOrbitDegrees, Vector2 distanceFromCenterPoint, Vector2 centerPoint) {
        float radians = MathUtils.degreesToRadians*currentOrbitDegrees;
        float x = (MathUtils.cos(radians) * distanceFromCenterPoint.x) + centerPoint.x;
        float y = (MathUtils.sin(radians) * distanceFromCenterPoint.y) + centerPoint.y;
        return new Vector2(x, y);
    }
    public float  rotateToPoint(Vector2 startpoin,Vector2 targetpoin){
        float degrees=0;
        Vector2 targetPoint=targetpoin;
        Vector2 startPoint=startpoin;
        float angle = MathUtils.atan2(targetPoint.y - startPoint.y, targetPoint.x -startPoint.x);
        degrees= MathUtils.radiansToDegrees*angle;
        if (degrees< 0) degrees += 360;
        else if (degrees > 360) degrees -= 360;

        return degrees;
    }
    public void MoveAroundTarget(){

    }
    public Vector2 getDirectionPoin(float degree,Vector2 distanceFcenter,Vector2 centerPoin){
        Vector2 center;
        center=calculateOrbit(degree, new Vector2(distanceFcenter.x, distanceFcenter.y), new Vector2(centerPoin.x, centerPoin.y));
        return center;
    }
    @Override
    public void Collision2DEnter(TypeComponent typeComponent, GameObject gameObjectColl) {
        super.Collision2DEnter(typeComponent, gameObjectColl);
    }


    public Vector2 getCenterPoin() {
        Vector2 center = new Vector2();
        center.x = this.getX() + this.getWidth() / 2;
        center.y = this.getY() + this.getHeight() / 2;
        return center;
    }


    private void draw(ShapeRenderer shapeRenderer, Batch batch) {
//        if(myPath!=null) {
//            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//            shapeRenderer.setColor(com.badlogic.gdx.graphics.Color.BLUE);
//            float x1 = getCenterPoin().x;
//            float y1 = getCenterPoin().y;
//            float x2 = myPath.get(myPath.size()-1).getCenterPoin().x;
//            float y2 = myPath.get(myPath.size()-1).getCenterPoin().y;
//
//            shapeRenderer.rectLine(x1, y1, x2, y2, 1 / MyGame.PPM);
//            shapeRenderer.end();
//        }
        // drawPath(shapeRenderer, batch);

    }
    private void drawPath(ShapeRenderer shapeRenderer, Batch batch) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(com.badlogic.gdx.graphics.Color.BLUE);
        for (int i = 0; i <(myPath.size()) - 1; i++) {
            if(i==0) shapeRenderer.setColor(Color.YELLOW);
            else shapeRenderer.setColor(Color.BLUE);
            NodeMap a = myPath.get(i);
            NodeMap b = myPath.get(i + 1);
            float x1 =  (a.getX() + ((MyGame.PLAYER_WIDTH / 2)/ MyGame.PPM));
            float y1 = (a.getY()+( (MyGame.PLAYER_WIDTH / 2)/MyGame.PPM));
            float x2 =  (b.getX() + ((MyGame.PLAYER_WIDTH / 2)/MyGame.PPM));
            float y2 =  (b.getY() + ((MyGame.PLAYER_WIDTH / 2)/MyGame.PPM));

            shapeRenderer.rectLine(x1, y1, x2, y2, 1/MyGame.PPM);
        }
        shapeRenderer.end();
    }
    public float getMyRotate() {
        return myRotate;
    }

    public void setMyRotate(float myRotate) {
        this.myRotate = myRotate;
    }

    public Image getMyFrame() {
        return myFrame;
    }

    public void setMyFrame(Image myFrame) {
        this.myFrame = myFrame;
    }
    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }
    public abstract void hitByPlayer(Player mario);


    public abstract void hitByEnemy(Enemy enemy);

    public abstract void hitByBullet(int dame);

    public abstract void hitByObject(InteractiveTileObject tileObject);

    public abstract void ChangeDir();

    public abstract void ReUse();

    public abstract void Stand(InteractiveTileObject standObject);


    public void Attack_1(float dt) {

    }

    public void Attack_2(float dt) {

    }

    public void Running() {
    }

    public void Jump() {
    }

    public void Die() {
    }

    public void OutDistance() {
    }

    public abstract void Destroy();

    public boolean isDie() {
        return isDie;
    }

    public void setDie(boolean die) {
        isDie = die;
    }


    public boolean isStartGame() {
        return startGame;
    }

    public void setStartGame(boolean startGame) {
        this.startGame = startGame;
    }


    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public EnemyStats getStats() {
        return stats;
    }

    public void setStats(EnemyStats stats) {
        this.stats = stats;
    }

    public void setFadeInOutColor(boolean startFade, boolean nolimitFade, Color clIn, Color clOut, int countFade) {
        colorIn = clIn;
        colorOut = clOut;
        countFadeColor = countFade;
        startFadeColor = startFade;
        nolimitFadeColor = nolimitFade;
        isColorIn = true;
    }
    public boolean isDraw() {
        return isDraw;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }

    public Body getB2body() {
        return b2dBodyComponent.getB2body();
    }

    public void setB2body(Body b2body) {
        b2dBodyComponent.setB2body(b2body);
    }

    public Fixture getFixture() {
        return b2dBodyComponent.getFixture();
    }

    public void setFixture(Fixture fixture) {
        b2dBodyComponent.setFixture(fixture);
    }
    public TypeComponent getTypeComponent() {
        return typeComponent;
    }

    public void setTypeComponent(TypeComponent typeComponent) {
        this.typeComponent = typeComponent;
    }
}


