package Model.GRIDMAP;

import javax.xml.soap.Node;

/**
 * Edge class.
 * 
 * @author Leonardo Ono (ono.leo80@gmail.com)
 */
public class Edge {

    private double g;
    private final NodeMap a;
    private final NodeMap b;

    public Edge(double g, NodeMap a, NodeMap b) {
        this.g = g;
        this.a = a;
        this.b = b;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public NodeMap getA() {
        return a;
    }

    public NodeMap getB() {
        return b;
    }
    
    public NodeMap getOppositeNode(NodeMap thisNode) {
        if (thisNode == a) {
            return b;
        }
        else if (thisNode == b) {
            return a;
        }
        return null;
    }

    @Override
    public String toString() {
        return "Edge{" + "g=" + g + ", a=" + a + ", b=" + b + "}";
    }
    
}
