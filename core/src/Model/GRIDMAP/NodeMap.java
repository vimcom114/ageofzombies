package Model.GRIDMAP;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;

import java.util.ArrayList;
import java.util.List;

import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;

public class NodeMap extends Group implements Comparable<NodeMap> {


    public static enum State { UNVISITED, OPEN, CLOSED };

    private final Vector2 Poin;
    private State state = State.UNVISITED;
    private boolean blocked = false;
    private int rows;



    private int cols;
    private double g; // cost
    private double h; // heuristic
    // f = g + h

    private NodeMap backPathNode;
    private final List<Edge> edges = new ArrayList<>();
    Image imNode;
    public NodeMap(Vector2 Poin,float r,float c,float size,Group parent) {
        rows= (int) r;
        cols= (int) c;
        this.Poin = Poin;
        parent.addActor(this);
        this.setSize(size,size);
        this.setPosition(c*size,r*size);

        imNode = UI.NewImage(Loader.GetTexture("Node"),0,0, Align.bottomLeft, (int)this.getWidth() ,(int) this.getHeight(),this);
         imNode.setSize(size,size);
         imNode.setVisible(false);
        imNode.setPosition(0,0);

        //Label lb = UI.NewLabel(""+rows+"-"+cols, Color.WHITE,0.1f,this.getWidth()/2,this.getHeight()/2, Align.center,this);


    }

    public void setColorNode(boolean visible,Color color){
        imNode.setColor(color.r,color.g,color.b,0.4f);
        imNode.setVisible(visible);
    }
    public Vector2 getObj() {
        return Poin;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public double getG() {
        return g;
    }

    public void setG(double g) {
        this.g = g;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public NodeMap getBackPathNode() {
        return backPathNode;
    }

    public void setBackPathNode(NodeMap  backPathNode) {
        this.backPathNode = backPathNode;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    // f(n) = g(n) + h(n) -> cost + heuristic
    public double getF() {
        return g + h;
    }

    public void retrievePath(List<NodeMap> path) {
        if (backPathNode != null) {
            backPathNode.retrievePath(path);
        }
        path.add(this);
    }
    public String getRC(){
        return "Rows :"+rows+"--"+"Cols :"+cols;
    }


    @Override
    public int compareTo(NodeMap o) {
        double dif = getF() - o.getF();
        return dif == 0 ? 0 : dif > 0 ? 1 : -1;
    }


    public String toString() {
        return "Node{" + "id=" + Poin + ", state=" + state + ", g=" + g + ", h="
                + h + ", parentNode=" + backPathNode + ", edges=" + edges + "}";
    }
    public Vector2 getCenterPoin() {
        Vector2 center = new Vector2();
        center.x = this.getX() + this.getWidth() / 2;
        center.y = this.getY() + this.getHeight() / 2;
        return center;
    }
    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }
}
