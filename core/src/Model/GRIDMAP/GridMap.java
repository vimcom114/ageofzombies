package Model.GRIDMAP;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.geda.game.MyGame;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import Controller.MyWorld;
import GameGDX.GDX;
import Tools.B2WorldCreator;
import Tools.PathFinding.Graph;

public class GridMap extends Group {
    MyWorld myWorld;
    B2WorldCreator b2WorldCreator;
    private static int TILE_SIZE = 40;
    private static int GRID_COLS = 15;
    private static int GRID_ROWS = 15;
    private NodeMap[][] grid;

    private List<NodeMap> mypath;
    private Graph graph;


    private NodeMap startNode,targetNode;
    ShapeRenderer shapeRenderer;

    public GridMap(Group parent, MyWorld myworld){
        parent.addActor(this);
        myWorld=myworld;
        shapeRenderer=new ShapeRenderer();
        mypath = new ArrayList<>();
        graph=new Graph();
        TILE_SIZE= MyGame.PLAYER_WIDTH;

    }
    public void creatGridMap(){
        b2WorldCreator=myWorld.getCreator();
        GRID_ROWS=b2WorldCreator.getMapPxHeight();
        GRID_COLS=b2WorldCreator.getMapPxWidth();
        grid = new NodeMap[GRID_ROWS][GRID_COLS];
        createGrid();
    }
    private void createGrid() {

        for (int y = 0; y < GRID_ROWS; y++) {
            for (int x = 0; x < GRID_COLS; x++) {
                float nx = x * (TILE_SIZE/MyGame.PPM);
                float ny = y * (TILE_SIZE/MyGame.PPM);
                NodeMap node = new NodeMap(new Vector2(nx, ny),y,x,TILE_SIZE/MyGame.PPM,this);
                graph.addNode(node);
                grid[y][x] = node;
            }
        }

        // link all nodes

        double diagonalG = Math.sqrt(TILE_SIZE * TILE_SIZE + TILE_SIZE * TILE_SIZE);
        for (int y = 0; y < GRID_ROWS - 1; y++) {
            for (int x = 0; x < GRID_COLS; x++) {
                // vertical '|'
                NodeMap top = grid[y][x];
                NodeMap bottom = grid[y + 1][x];
                graph.link(top, bottom, TILE_SIZE);

                // diagonals 'X'
                if (x < GRID_COLS - 1) {

                    // diagonal '\'
//					top = grid[y][x];
//					bottom = grid[y + 1][x + 1];
//					graph.link(top, bottom, diagonalG);
//
//					// diagonal '/'
//					top = grid[y][x + 1];
//					bottom = grid[y + 1][x];
//					graph.link(top, bottom, diagonalG);
                }
            }
        }

        for (int x = 0; x < GRID_COLS - 1; x++) {
            for (int y = 0; y < GRID_ROWS; y++) {
                // horizontal '-'
                NodeMap left = grid[y][x];
                NodeMap right = grid[y][x + 1];
                graph.link(left, right, TILE_SIZE);
            }
        }
    }
    public NodeMap convertRC(float x,float y) {
        int row = (int)((x*MyGame.PPM)/TILE_SIZE);
        int col = (int)((y*MyGame.PPM)/TILE_SIZE);
        if(row>GRID_COLS-1)row=GRID_COLS-1;
        else if(row<0)row=0;
        if(col>GRID_ROWS-1)col=GRID_ROWS-1;
        else if(col<0)col=0;

        return grid[col][row];
//        if (col < 0 || row < 0
//                || col > GRID_COLS - 1 || row > GRID_COLS - 1) {
//
//            return;
//        }

//        if (button== Input.Buttons.LEFT) {
//            //Gdx.app.log("LEftKeys",""+button);
//            startNode = grid[col][row];
//        } else if (button== Input.Buttons.RIGHT) {
//            //Gdx.app.log("RightKeys",""+button);
//            targetNode = grid[col][row];
//        } else if (button== Input.Buttons.MIDDLE) {
//            if (!grid[col][row].isBlocked()) {
//                grid[col][row].setBlocked(true);
//            } else {
//                grid[col][row].setBlocked(false);
//            }
//        }


    }
    public void getpath(NodeMap start, NodeMap target, List<NodeMap> path){
        startNode=start;
        targetNode=target;
        graph.findPath(start, target, path);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        for (NodeMap  node : graph.getNodes()) {
            if(node.isBlocked())node.setColorNode(true,Color.BLACK);
            else node.setColorNode(false,Color.WHITE);
        }

    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
//        shapeRenderer.setProjectionMatrix(myWorld.getScreenGame().getGamecam().combined);
//        if(startNode!=null&&targetNode!=null)draw(shapeRenderer,batch);
        super.draw(batch, parentAlpha);
    }

    private void draw(ShapeRenderer shapeRenderer, Batch batch) {
        mypath.clear();
        graph.findPath(startNode, targetNode, mypath);
        for (NodeMap  node : graph.getNodes()) {
            drawNode(shapeRenderer, batch, node);
        }
        drawPath(shapeRenderer, batch);

    }

    private void drawNode(ShapeRenderer shapeRenderer, Batch batch, NodeMap node) {
        shapeRenderer.end();
        if(node == startNode||node == targetNode) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            com.badlogic.gdx.graphics.Color color = com.badlogic.gdx.graphics.Color.BLACK;

//        switch (node.getState()) {
//            case OPEN:
//                color = com.badlogic.gdx.graphics.Color.CYAN;
//                break;
//            case CLOSED:
//                color = com.badlogic.gdx.graphics.Color.ORANGE;
//                break;
//            case UNVISITED:
//                color = com.badlogic.gdx.graphics.Color.WHITE;
//                break;
//        }
            if (node == startNode) {
                color = com.badlogic.gdx.graphics.Color.BLUE;
            } else if (node == targetNode) {
                color = com.badlogic.gdx.graphics.Color.RED;
            }
            float rx = node.getX();
            float ry = node.getY();
            shapeRenderer.setColor(color);
            shapeRenderer.rect(rx, ry, TILE_SIZE / MyGame.PPM, TILE_SIZE / MyGame.PPM);
            shapeRenderer.end();
        }
//        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//        shapeRenderer.setColor(com.badlogic.gdx.graphics.Color.BLACK);
//        shapeRenderer.rect(rx, ry, TILE_SIZE/MyGame.PPM, TILE_SIZE/MyGame.PPM);
//        shapeRenderer.end();
    }

    private void drawPath(ShapeRenderer shapeRenderer, Batch batch) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(com.badlogic.gdx.graphics.Color.BLUE);
        for (int i = 0; i < 2; i++) {
            NodeMap a = mypath.get(i);
            NodeMap b = mypath.get(i + 1);
            float x1 =  (a.getX() + ((TILE_SIZE / 2)/MyGame.PPM));
            float y1 = (a.getY()+( (TILE_SIZE / 2)/MyGame.PPM));
            float x2 =  (b.getX() + ((TILE_SIZE / 2)/MyGame.PPM));
            float y2 =  (b.getY() + ((TILE_SIZE / 2)/MyGame.PPM));
            shapeRenderer.rectLine(x1, y1, x2, y2, 1/MyGame.PPM);
        }
        shapeRenderer.end();
    }
    public void setStartNode(NodeMap startNode) {
        this.startNode = startNode;
    }

    public void setTargetNode(NodeMap targetNode) {
        this.targetNode = targetNode;
    }
}
