package Model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.geda.game.MyGame;

import Controller.MyWorld;
import Tools.GameObject;
import Tools.TypeComponent;

public class BulletPlayer extends Bullet {
    public BulletPlayer(MyWorld myworld, String DataTxt, Vector2 Speed, int dame, Vector2 firePoin, float Rotate) {
        super(myworld, DataTxt, Speed,dame, firePoin, Rotate);
    }

    @Override
    public void Update(float dt) {
        getB2body().setLinearVelocity(velocity.x,velocity.y);
    }

    @Override
    public void defineBody() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(this.getX()+this.getWidth()/2, this.getY()+this.getHeight()/2);
        bdef.type = BodyDef.BodyType.DynamicBody;
        setB2body( getWorld().createBody(bdef));
        getB2body().setGravityScale(0);

        FixtureDef fde = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox( ((this.getWidth()/2)), (this.getHeight()/2));
        fde.filter.categoryBits = MyGame.PLAYER_BIT;
        fde.filter.maskBits = MyGame.ENEMY_BIT|MyGame.GROUND_BIT;
        fde.shape = shape;
        fde.isSensor=true;

//        if(isSprings) {
//            fde.restitution =Restitution;
//            fde.friction = Friction;
//        }
        setFixture(getB2body().createFixture(fde));
        getB2body().setUserData(entity);
    }

    @Override
    public void CollDo(TypeComponent typeComponent, GameObject gameObjectColl) {
        if(typeComponent.type==TypeComponent.ENEMY){
            if(gameObjectColl.getMyEnemy()!=null)gameObjectColl.getMyEnemy().hitByBullet(-getDame());
        }
        currentState=State.DIE;
    }

    @Override
    public void Coll() {

    }
    @Override
    public void Die() {
        setDestroy(true);
    }

}
