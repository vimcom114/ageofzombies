package Model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;


import java.util.ArrayList;

import Controller.Game.AudioManager;
import Controller.GameController;
import GameGDX.ClickEvent;
import GameGDX.GAction;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import ScreenGame.LevelMap;
import ScreenGame.UIScene;


public class Level extends Group {
    MapLevel myMapLevel;
    Image imageStar_Empty1,imageStar_Empty2,imageStar_Empty3;
    Image imageStar_1,imageStar_2,imageStar_3;
    Image imageBlank;
    Label lbwhatLevel;
    Image imageBlank_Block;
    String nameMap="",namShow;
    boolean Unlock=false;
    int countStar=0;
    boolean isSpecial;
    int SpecialType=0;
    String nameAvatar;
    ArrayList<Image> listStar;
    int IndexMap;
    int IndexLevel;


    public Level(Vector2 position, final String namemap, final String NameShow, MapLevel mymap, boolean isspecial, int specialtype, String nameav, Group parent, final int indexMap, final int indexLevel){
        parent.addActor(this);
        listStar=new ArrayList<>();
        nameMap=namemap;
        namShow=NameShow;
        myMapLevel=mymap;
        Unlock= GDX.GetPrefBoolean(nameMap,false);
        countStar= GDX.GetPrefInteger(nameMap+"_Star",0);
        isSpecial=isspecial;
        SpecialType=specialtype;
        nameAvatar=nameav;
        IndexMap=indexMap;
        IndexLevel=(indexMap*10)+(indexLevel+1);
        Group groupBlank=new Group();
        this.addActor(groupBlank);
        groupBlank.setSize(this.getWidth(),this.getHeight()/1.3f);

        imageBlank= UI.NewImage(Loader.GetTexture("bottonlv_xanh"),0,0, Align.bottomLeft,groupBlank);
        groupBlank.setSize(imageBlank.getWidth(),imageBlank.getHeight());
        setSize(imageBlank.getWidth(),imageBlank.getHeight());
        position.y=parent.getHeight()-position.y;
        position.x=(position.x/1280)*parent.getWidth();
        setPosition(position.x,position.y, Align.center);
        setOrigin(Align.center);
        imageBlank.setOrigin(Align.center);
        imageBlank.setScale(0.6f);

        Group groupStar=new Group();
        this.addActor(groupStar);
        groupStar.setSize(this.getWidth(),this.getHeight()*0.2f);
        groupStar.setPosition(this.getWidth()/2,15f, Align.center);
        imageStar_Empty1= UI.NewImage(Loader.GetTexture("Star_Empty"),groupStar.getWidth()*0.2f,8, Align.bottom,groupStar);
        imageStar_Empty2= UI.NewImage(Loader.GetTexture("Star_Empty"),groupStar.getWidth()*0.5f,0, Align.bottom,groupStar);
        imageStar_Empty3= UI.NewImage(Loader.GetTexture("Star_Empty"),groupStar.getWidth()*0.8f,8, Align.bottom,groupStar);

        imageStar_1= UI.NewImage(Loader.GetTexture("Star"),imageStar_Empty1.getX(),8, Align.bottomLeft,groupStar);
        imageStar_1.setOrigin(Align.center);
        imageStar_2= UI.NewImage(Loader.GetTexture("Star"),imageStar_Empty2.getX(),0, Align.bottomLeft,groupStar);
        imageStar_2.setOrigin(Align.center);
        imageStar_3= UI.NewImage(Loader.GetTexture("Star"),imageStar_Empty3.getX(),8, Align.bottomLeft,groupStar);
        imageStar_3.setOrigin(Align.center);
        listStar.add(imageStar_1);
        listStar.add(imageStar_2);
        listStar.add(imageStar_3);
        lbwhatLevel= UI.NewLabel(""+NameShow, Color.WHITE,0.5f,groupBlank.getWidth()/2,groupBlank.getHeight()*0.55f, Align.center,this);
        imageBlank_Block= UI.NewImage(Loader.GetTexture("bottonlv_khongmau"),0,0, Align.bottomLeft,groupBlank);
        imageBlank_Block.setColor(1,1,1,1f);
        imageBlank_Block.setOrigin(Align.center);
        imageBlank_Block.setScale(0.6f);
        UnlockLevel(Unlock,false);
        setStar(countStar,false);
        if(isSpecial&&SpecialType==1){
            myMapLevel.creatLocalBoss(nameAvatar,this.getPos());
            imageBlank.setDrawable(new TextureRegionDrawable(Loader.GetTexture("bottonlv_do")));
        }
        this.addListener( new ClickEvent(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ClickEvent.ScaleSmooth(Level.this,1,1,0.05f);
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if(Unlock&& LevelMap.instance.isUnlock()==false&&LevelMap.instance.isReadyNextMap()==false) {
                    UIScene.instance.setLevelSceneIsShow(false);
                    UIScene.instance.setStopBackHandle(true);
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            if(LevelMap.instance.isDrag()==false) {
                                UIScene.instance.setStartCountToPlay(true);
                                UIScene.instance.setLevelSceneIsShow(false);
                                AudioManager.instance.StopAudio("Menu");
                               // MyGame.sdk.TrackLevelStart(IndexLevel);
                                LevelMap.instance.setIndexChoseLevel(IndexLevel);
//                                GameController.instance.setNameMapShow(namShow);
//                                GameController.instance.setCheckSameMap(true);
//                                GameController.instance.setNamMapChose(nameMap);
                                GameController.instance.ChangeMap(nameMap,indexLevel+1, true, false, false);
                            }else{
                                UIScene.instance.setLevelSceneIsShow(true);
                                UIScene.instance.setStopBackHandle(false);
                            }
                        }

                    }, 0.2f);
                }
                super.touchUp(event, x, y, pointer, button);
            }
        });
    }

    public void setPrefUnlock(){
        GDX.SetPrefBoolean(nameMap,true);
    }
    public void UnlockLevel(boolean unlock,boolean sound){
        Unlock=unlock;
        if(Unlock==true){
            if(sound)LevelMap.instance.InitUnlockLevel(new Vector2(this.getX(Align.center),this.getY()));
            imageBlank_Block.setScale(0);
            imageBlank_Block.setVisible(false);
        }else{
            imageBlank_Block.setScale(0.6f);
        }
    }
    public void setNewStar(int start){
        if(GDX.GetPrefInteger(nameMap+"_Star",0)<start) GDX.SetPrefInteger(nameMap+"_Star", start);
    }
    public void setStar(int count, final boolean action ) {
        if(count>countStar)countStar = count;
        boolean notLegal=false;
        if(countStar<=0||count<=countStar){
            notLegal=true;
            LevelMap.instance.EndSetStar();
        }
        for (int i = 0; i < 3; i++) {
            if (i < countStar) {
                final int finalI = i;
                if(action) {
                    final int finalI1 = i;
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            listStar.get(finalI).setVisible(true);
                            GAction.ScaleTo(listStar.get(finalI), 1, 1, 0.5f, Interpolation.linear, new Action() {
                                @Override
                                public boolean act(float delta) {
                                    actor.clearActions();
                                    LevelMap.instance.InitStarPoin(new Vector2(Level.this.getX(Align.center), Level.this.getY(Align.center)));
                                    AudioManager.instance.PlayAudioRand(1,3,"Star");
                                    if(finalI1 ==countStar-1)LevelMap.instance.EndSetStar();
                                    return false;
                                }
                            });
                        }

                    }, 0.7f * (i+1));
                }else{
                    listStar.get(finalI).setVisible(true);
                    listStar.get(finalI).setScale(1);
                }
                //listStar.get(i).setDrawable(new TextureRegionDrawable(Loader.GetTexture("Star")));
            } else {
                listStar.get(i).setScale(5);
                listStar.get(i).setVisible(false);
                // listStar.get(i).setDrawable(new TextureRegionDrawable(Loader.GetTexture("Star_empty")));
            }
        }
    }
    public int getIndexMap() {
        return IndexMap;
    }

    public void setIndexMap(int indexMap) {
        IndexMap = indexMap;
    }
    public Vector2 getPos(){
        return new Vector2(this.getX(Align.center),this.getY(Align.center));
    }


}
