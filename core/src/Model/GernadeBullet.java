package Model;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;

import Controller.MyWorld;
import GameGDX.GDX;
import GameGDX.Particle;
import Tools.GameObject;
import Tools.TypeComponent;

public class GernadeBullet  extends Bullet{
    float timeToDie=0f;
    public GernadeBullet(MyWorld myworld, String DataTxt, Vector2 Speed, int dame, Vector2 firePoin, float Rotate) {
        super(myworld, DataTxt, Speed, dame, firePoin, Rotate);
        getB2body().applyForce(velocity.x,velocity.y,0,0,true);
        typeComponent.type=TypeComponent.GERNADE_PLAYER;
    }
    @Override
    public void Update(float dt) {
        if(isWaitDie()&&currentState!=State.DIE) {
            if (timeToDie > 0) timeToDie -= dt;
            else currentState = State.DIE;
        }
    }

    @Override
    public void defineBody() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(this.getX()+this.getWidth()/2, this.getY()+this.getHeight()/2);
        bdef.type = BodyDef.BodyType.DynamicBody;
        setB2body( getWorld().createBody(bdef));
        getB2body().setGravityScale(0);

        FixtureDef fde = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox( ((this.getWidth()/2)), (this.getHeight()/2));
        fde.filter.categoryBits = MyGame.PLAYER_BIT;
        fde.filter.maskBits = MyGame.ENEMY_BIT|MyGame.GROUND_BIT;
        fde.shape = shape;

//        if(isSprings) {
//            fde.restitution =Restitution;
//            fde.friction = Friction;
//        }
        setFixture(getB2body().createFixture(fde));
        getB2body().setUserData(entity);
    }

    public void defineDie() {
        getWorld().destroyBody(getB2body());
        setB2body(null);
        BodyDef bdef = new BodyDef();
        bdef.position.set(this.getX()+this.getWidth()/2, this.getY()+this.getHeight()/2);
        bdef.type = BodyDef.BodyType.DynamicBody;
        setB2body( getWorld().createBody(bdef));
        getB2body().setGravityScale(0);

        FixtureDef fde = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(this.getWidth()*3);
        fde.filter.categoryBits = MyGame.PLAYER_BIT;
        fde.filter.maskBits = MyGame.ENEMY_BIT|MyGame.GROUND_BIT;
        fde.shape = shape;
        fde.isSensor=true;

//        if(isSprings) {
//            fde.restitution =Restitution;
//            fde.friction = Friction;
//        }
        setFixture(getB2body().createFixture(fde));
        getB2body().setUserData(entity);
    }
    @Override
    public void CollDo(TypeComponent typeComponent, GameObject gameObjectColl) {
        if(currentState!=State.COLL) {
            myFrame.setVisible(false);
            getFixture().setSensor(true);
            Particle particle = new Particle("Explose", getMyWorld().getScreenGame().getLayerGroup(1));
            particle.setPOS(this.getX(Align.center), this.getY(Align.center), 0);
            particle.Loop(false);
            particle.setScaleE(1);
            particle.Start(true);
            currentState=State.COLL;
        }
       // if(isDestroy()==false)setDestroy(true);
    }
    @Override
    public void Coll() {
       if(isWaitDie()==false) {
           defineDie();
           timeToDie = 0.5f;
           setWaitDie(true);
       }
    }

    @Override
    public void Die() {
         if(isDestroy()==false)setDestroy(true);
    }
}
