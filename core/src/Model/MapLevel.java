package Model;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;

import Controller.Game.AudioManager;
import GameGDX.GAction;
import GameGDX.GDX;
import GameGDX.GSpine.GSpine;
import GameGDX.Loader;
import GameGDX.Particle;
import GameGDX.UI;
import ScreenGame.LevelMap;
import ScreenGame.UIScene;

public class MapLevel extends Group {
    Image imageBGMap;
    ArrayList<Level> listLevel;
    Particle effect;

    public boolean isHaveDoor() {
        return haveDoor;
    }

    public void setHaveDoor(boolean haveDoor) {
        this.haveDoor = haveDoor;
    }
   String nameMap;
    private boolean haveDoor;
    Group gGateMap;
    Image imBdDoor,imDoor;
    Group gLocalBoss;
    Image imBdLocal,imLocal;
    GSpine mySpine;
    boolean isOpenGate;
    public MapLevel(String nameTexture,boolean havedoor,String nameEffect,float w,float h){
        this.setSize(w,h);
        haveDoor=havedoor;
        nameMap=nameTexture;
        imageBGMap= UI.NewImage(Loader.GetTexture(nameTexture),this);
        imageBGMap.setPosition(0,0);
        imageBGMap.setScaleX(UIScene.instance.getScaleWidth());
        if(haveDoor)creatDoor();
        if(nameEffect!=null) {
            effect = new Particle(nameEffect, this);
            effect.setPOS(this.getWidth()*0.6f, 0, 0);
            effect.Loop(true);
            effect.Start(true);
        }


    }
    public void addChild(Group child){
        this.addActor(child);
        if(effect!=null)effect.setZIndex(this.getChildren().size-1);
    }
    void creatDoor(){
        isOpenGate= GDX.GetPrefBoolean(nameMap,false);
        gGateMap=new Group();
        this.addActor(gGateMap);
        imBdDoor= UI.NewImage(Loader.GetTexture("borderDoor"),gGateMap);
        gGateMap.setSize(imBdDoor.getWidth(),imBdDoor.getHeight());
        imBdDoor.setPosition(0,0);
        imDoor= UI.NewImage(Loader.GetTexture("Door"),gGateMap);
        imDoor.setPosition(gGateMap.getWidth()/2,20, Align.bottom);
        imDoor.setOrigin(Align.left);
        gGateMap.setPosition(0,this.getHeight()/2, Align.center);
        if(isOpenGate)OpenDoor(true);
    }
    public void setStop(boolean Stop){
        if(mySpine!=null)mySpine.setStop(Stop);
        if(effect!=null) {
            effect.Start(!Stop);
            effect.setVisible(!Stop);
        }
    }
    public void OpenDoor(boolean Open) {
        if (Open) {
            if(isOpenGate==false) GDX.SetPrefBoolean(nameMap,true);
            isOpenGate=true;
            AudioManager.instance.PlayAudio("OpenDoor");
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    GAction.ScaleTo(imDoor, -1, 1, 0.8f, Interpolation.linear, new Action() {
                        @Override
                        public boolean act(float delta) {
                            actor.clearActions();
                            LevelMap.instance.NextCaseMap();
                            return false;
                        }
                    });
                }
            }, 0.5f);
        }
    }
    public void creatLocalBoss(String nameAV, Vector2 pos) {
        gLocalBoss = new Group();
        this.addActor(gLocalBoss);
        imBdLocal = UI.NewImage(Loader.GetTexture("Local_Boss"), gLocalBoss.getWidth() / 2, 0, Align.bottom, gLocalBoss);
        imBdLocal.setTouchable(Touchable.disabled);
        gLocalBoss.setSize(imBdLocal.getWidth(), imBdLocal.getHeight());
        imBdLocal.setPosition(0, 0);

        Image imLocalBg = UI.NewImage(Loader.GetTexture("Local_BossBG"), (gLocalBoss.getWidth() / 2) - 0.7f, gLocalBoss.getHeight() * 0.56f, Align.center, gLocalBoss);
        mySpine = new GSpine(nameAV, gLocalBoss.getWidth() / 2, gLocalBoss.getHeight() * 0.5f, Align.bottomLeft, gLocalBoss, "animation");
        mySpine.setScale(1f);
        mySpine.setStop(false);
        mySpine.setOrigin(Align.center);
        if(nameAV.contains("3")){
            mySpine.setY(gLocalBoss.getHeight() * 0.37f);
        }else if(nameAV.contains("4")){
            mySpine.setScale(0.75f);
            mySpine.setPosition(mySpine.getX()-5,gLocalBoss.getHeight() * 0.43f);
        } else if(nameAV.contains("5")){
            mySpine.setScale(0.8f);
            mySpine.setPosition(mySpine.getX()-15,gLocalBoss.getHeight() * 0.45f);
        }

        imBdLocal.setZIndex(gLocalBoss.getChildren().size - 1);
        gLocalBoss.setPosition(pos.x, pos.y, Align.bottom);

    }

}