package Model;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;


import Controller.MyWorld;
import GameGDX.Loader;
import GameGDX.UI;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;

public abstract class Item extends GameObject {
    ItemType myItemType;
    CollisionComponent collisionComponent;
    TypeComponent typeComponent;
    B2dBodyComponent b2dBodyComponent;
    protected Controller.MyWorld myWorld;
    protected World world;

    protected boolean toDestroy;
    protected boolean destroyed;
    boolean ReUse=false;
    Vector2 respawnPos;

    public Item(MyWorld myworld, float x, float y, ItemType itemType){
        myworld.getStage().addActor(this);
        this.myWorld= myworld;
        this.world = myWorld.getWorld();
        myItemType=itemType;
        creatComponent();
        respawnPos=new Vector2(x,y);
        setSize((MyGame.PLAYER_WIDTH)/MyGame.PPM,(MyGame.PLAYER_HEIGHT)/MyGame.PPM);
        setPosition(x, y,Align.center);
        Image imItem=UI.NewImage(Loader.GetTexture(myItemType.getDataTextureItem()),0,0, Align.bottomLeft,this);
        imItem.setSize(imItem.getWidth()/MyGame.PPM,imItem.getHeight()/MyGame.PPM);
        imItem.setPosition(0,0);

        defineItem();
       // myWorld.addListItems(this);
    }
    public void creatComponent(){
        Engine engine=myWorld.getEngineComponent();
        entity= engine.createEntity();
        collisionComponent = engine.createComponent(CollisionComponent.class);
        collisionComponent.setMyParent(this);
        typeComponent = engine.createComponent(TypeComponent.class);
        typeComponent.type=TypeComponent.ITEM;
        b2dBodyComponent = engine.createComponent(B2dBodyComponent.class);
        addComponent(collisionComponent);
        addComponent(typeComponent);
        addComponent(b2dBodyComponent);
        // add the entity to the engine
        engine.addEntity(entity);

    }
    public abstract void defineItem();
    public abstract void CollTake();
    public abstract void ReUse();
    public abstract void Destroy();

    @Override
    public void act(float delta) {
        if(getB2body()==null)return;
        Update(delta);
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        if(ReUse||stopDraw||Start==false) return;
        super.draw(batch, parentAlpha);
    }
    public void Update(float dt){
        if(toDestroy && !destroyed){
            world.destroyBody(getB2body());
            setB2body(null);
            destroyed = true;
            remove();
        }
        if(getB2body()==null) return;
        myUpdate(dt);
    }
    public void myUpdate(float dt){

    }
    public Body getB2body() {
        return b2dBodyComponent.getB2body();
    }
    public void setB2body(Body b2body) {
        b2dBodyComponent.setB2body( b2body);
    }
    public Fixture getFixture(){
        return b2dBodyComponent.getFixture();
    }
    public void setFixture(Fixture fixture){
        b2dBodyComponent.setFixture(fixture);
    }
    @Override
    public void Collision2DEnter(TypeComponent typeComponent, GameObject gameObjectColl) {
        super.Collision2DEnter(typeComponent, gameObjectColl);
        if(typeComponent.type==TypeComponent.PLAYER)CollTake();

    }

    public boolean isToDestroy() {
        return toDestroy;
    }

    public void setToDestroy(boolean toDestroy) {
        this.toDestroy = toDestroy;
    }

}
