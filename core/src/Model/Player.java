package Model;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.geda.game.MyGame;

import java.util.HashMap;

import javax.xml.soap.Node;

import Controller.MyWorld;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import Model.GRIDMAP.NodeMap;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;

public class Player extends GameObject {
    public enum State {FALLING, FIRE, JUMPING, CHOKE, SWIMING, STANDING, SIT, STOPRUN, RUNNING, GROWING, GROWDOWN, DEAD}
    private HashMap<String,Animation> listAnimation=new HashMap<>();
    boolean flipAnm=false;
    int degreeAnm;
    private float myRotate=0;


    private Vector2 firepoin=new Vector2(0,0);

    private Vector2 flareGunPoin=new Vector2(0,0);
    public State currentState;
    public State previousState;
    private boolean moveLeft;
    private boolean moveRight;
    private boolean moveUp;
    private boolean moveDown;
    private boolean FireMainWP;
    private boolean FireExtraWP;
    private Controller.MyWorld myWorld;
    private World world;

    private Weapon myMainWeapon,myExtraWeapon;
    private float speedMoveX = 0.1f;
    private float speedMoveY = 0.1f;
    private float limitspeedMoveX = 2.5f;
    private float limitspeedMoveY = 2.5f;
    private Image playerFrame;
    private Animation currentAnim;
    private boolean playerIsDead;
    private boolean runGrowAnimation;

    private boolean runningRight, runningUp;
    private float stateTimer;
    CollisionComponent collisionComponent;
    TypeComponent typeComponent;
    B2dBodyComponent b2dBodyComponent;
    NodeMap myNode;
    public Player(MyWorld myworld, float x, float y, Group parent) {
        parent.addActor(this);
        setPosition(x, y);
        this.myWorld = myworld;
        this.world = myWorld.getWorld();
        creatComponent();
        currentState = State.STANDING;
        previousState = State.STANDING;
        stateTimer = 0;
        runningRight = true;

        //get run animation frames and add them to marioRun Animation
        creatAnimation("Boy");
        defineMario(x, y);
        setOrigin(Align.center);
        //SetSwiming();
    }
    private void creatAnimation(String nameCharacter){
        Array<TextureRegion>frames_Idle = new Array<TextureRegion>();
        Array<TextureRegion>frames_Run = new Array<TextureRegion>();
        int degree=0;
        for (int i=0;i<9;i++) {
            degree=i*22;
            if(degree==88||i>=4)degree+=2;
            if(degree==178)degree+=2;
            for (int j = 0; j < 10; j++) {
                TextureRegion textureRegion_Idle = Loader.GetTexture(nameCharacter +"_"+degree+"_Idle"+"_0"+j);
                frames_Idle.add(textureRegion_Idle);
                TextureRegion textureRegion_Run = Loader.GetTexture(nameCharacter +"_"+degree+"_Run"+"_0"+j);
                frames_Run.add(textureRegion_Run);
            }
            Animation animation_Idle=  new Animation(0.05f, frames_Idle);
            Animation animation_Run=  new Animation(0.05f, frames_Run);
            listAnimation.put("Idle"+degree,animation_Idle);
            listAnimation.put("Run"+degree,animation_Run);
           frames_Idle.clear();
           frames_Run.clear();
        }
        currentAnim=listAnimation.get(("Idle0"));
        TextureRegion textureRegion = (TextureRegion) currentAnim.getKeyFrame(0, true);
        playerFrame = UI.NewImage(textureRegion, 0, 0, Align.bottomLeft, this);
        playerFrame.setSize(playerFrame.getWidth()/MyGame.PPM,playerFrame.getHeight()/MyGame.PPM);
        playerFrame.setOrigin(Align.center);
        setSize(playerFrame.getWidth(),playerFrame.getHeight());
        playerFrame.setPosition(this.getWidth() / 2, this.getHeight() / 2, Align.center);

    }
   public void creatComponent(){
        Engine engine=myWorld.getEngineComponent();
       entity= engine.createEntity();

       collisionComponent = engine.createComponent(CollisionComponent.class);
       collisionComponent.setMyParent(this);
       typeComponent = engine.createComponent(TypeComponent.class);
       typeComponent.type=TypeComponent.PLAYER;
       b2dBodyComponent = engine.createComponent(B2dBodyComponent.class);
       addComponent(collisionComponent);
       addComponent(typeComponent);
       addComponent(b2dBodyComponent);
       // add the entity to the engine
       engine.addEntity(entity);

   }
    public void Update(float dt) {
        handleInputMovement(dt);
        setPosition(b2dBodyComponent.getB2body().getPosition().x - (this.getWidth() / 2),b2dBodyComponent.getB2body().getPosition().y - (this.getHeight() / 2));
        MoveMentControll(dt);
        playerFrame.setDrawable(new TextureRegionDrawable((getFrame(dt))));
        handlePhysicMovement(dt);

    }

    private void defineMario(float x, float y) {
        float[] vertices = new float[]{
                10 / MyGame.PPM, 0,
                4 / MyGame.PPM, 0,
                2 / MyGame.PPM, 2 / MyGame.PPM,
                12 / MyGame.PPM, 2 / MyGame.PPM,
                2 / MyGame.PPM, 15 / MyGame.PPM,
                12 / MyGame.PPM, 15 / MyGame.PPM
        };
        BodyDef bdef = new BodyDef();
        bdef.position.set(x / MyGame.PPM, y / MyGame.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        setB2body(world.createBody(bdef));
        getB2body().setGravityScale(0);
        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius((MyGame.PLAYER_WIDTH / MyGame.PPM) / 2/2);
        fdef.filter.categoryBits = MyGame.PLAYER_BIT;
        fdef.filter.maskBits = MyGame.ENEMY_BIT|MyGame.GROUND_BIT|MyGame.FRIEND_BIT;
        fdef.shape = shape;
        fdef.friction = 1f;
        fdef.restitution = 1f;

        setFixture(getB2body().createFixture(fdef));
        getB2body().setUserData(entity);


    }
    private void MoveMentControll(float dt){

        if (getB2body().getPosition().x < myWorld.getScreenGame().getLeftCameraX()) {// b2body.setTransform(0.2f, b2body.getPosition().y, 0);
            getB2body().setLinearVelocity(1, getB2body().getLinearVelocity().y);
        }
        if(getB2body().getPosition().x >= myWorld.getScreenGame().getRightCameraX()-(MyGame.PLAYER_WIDTH/MyGame.PPM)){
            getB2body().setLinearVelocity(-1, getB2body().getLinearVelocity().y);
        }
        if (getB2body().getPosition().y < MyGame.PLAYER_HEIGHT/MyGame.PPM||getB2body().getPosition().y>myWorld.getScreenGame().getTopCameraY()) {// b2body.setTransform(0.2f, b2body.getPosition().y, 0);
            getB2body().setLinearVelocity(getB2body().getLinearVelocity().x, 1);
        }
        if (getB2body().getPosition().y>myWorld.getScreenGame().getTopCameraY()) {// b2body.setTransform(0.2f, b2body.getPosition().y, 0);
            getB2body().setLinearVelocity(getB2body().getLinearVelocity().x, -1);
        }
        if(getB2body().getLinearVelocity().x!=0||getB2body().getLinearVelocity().y!=0)currentState=State.RUNNING;
        else currentState=State.STANDING;

    }

    private void handleInputMovement(float dt) {
        //control our player using immediate impulses
        if (currentState != State.DEAD) {
            if(moveDown||moveUp||moveLeft||moveRight&&getB2body().getType()== BodyDef.BodyType.StaticBody)getB2body().setType(BodyDef.BodyType.DynamicBody);
            if (moveUp &&getB2body().getLinearVelocity().y <= limitspeedMoveY) {
               getB2body().applyLinearImpulse(new Vector2(0, speedMoveX), getB2body().getWorldCenter(), true);
            }
            if (moveRight && getB2body().getLinearVelocity().x <= limitspeedMoveX) {
                getB2body().applyLinearImpulse(new Vector2(speedMoveX, 0), getB2body().getWorldCenter(), true);
            }
            if (moveLeft &&getB2body().getLinearVelocity().x >= -limitspeedMoveX) {
                getB2body().applyLinearImpulse(new Vector2(-speedMoveX, 0), getB2body().getWorldCenter(), true);
            }
            if (moveDown && getB2body().getLinearVelocity().y >= -limitspeedMoveY) {
               getB2body().applyLinearImpulse(new Vector2(0, -speedMoveY),getB2body().getWorldCenter(), true);
            }
            if (FireMainWP) StartFireMainWP();
            if (FireExtraWP){
                StartFireExtraWP();
                setFireExtraWP(false);
            }
        }

    }

    public TextureRegion getFrame(float dt) {

        //get marios current state. ie. jumping, running, standing...

        degreeAnm=getdegreeAnim();
        degreeAnm= degreeAnm<0? (int) (-1f * degreeAnm) :degreeAnm;

        if(degreeAnm<5||(degreeAnm>=90&&degreeAnm<95))degreeAnm=0;
        if(degreeAnm>5&&degreeAnm<=22.5f)degreeAnm=22;
        if(degreeAnm>22.5f&&degreeAnm<=45)degreeAnm=44;
        if(degreeAnm>45&&degreeAnm<=80f)degreeAnm=66;
        if(degreeAnm>80f&&degreeAnm<=90)degreeAnm=90;
        if(degreeAnm>95&&degreeAnm<=112.5f)degreeAnm=112;
        if(degreeAnm>112.5f&&degreeAnm<=135)degreeAnm=134;
        if(degreeAnm>135&&degreeAnm<=170f)degreeAnm=156;
        if(degreeAnm>170f&&degreeAnm<=180)degreeAnm=180;

        if(degreeAnm>=66&&degreeAnm<=90){
            playerFrame.setPosition(0,-this.getHeight()*0.05f);
        }
        else{
            playerFrame.setPosition(0,0);
        }

        if(degreeAnm>45&&degreeAnm<=90){
            flareGunPoin=new Vector2(flipAnm?0.05f:-0.05f,0.1f);
        }
        else{
            flareGunPoin=new Vector2(0,0);
        }
        currentState = getState();
        TextureRegion region = null;
        //depending on the state, get corresponding animation keyFrame.
        //GDX.Show("State :"+currentState);
        switch (currentState) {
            case DEAD:

                break;
            case GROWING:

                break;
            case JUMPING:
                break;
            case RUNNING:
                if(listAnimation.containsKey(("Run"+degreeAnm))) {
                    currentAnim = listAnimation.get(("Run"+degreeAnm));
                }
                region = (TextureRegion) currentAnim.getKeyFrame(stateTimer, true);
                break;
            case FALLING:
                break;
            case STANDING:
            default:
                if(listAnimation.containsKey(("Idle"+degreeAnm))) {
                    currentAnim = listAnimation.get(("Idle" + degreeAnm));
                }
                region = (TextureRegion) currentAnim.getKeyFrame(stateTimer, true);
                break;
        }

        if(flipAnm && !region.isFlipX()){
            region.flip(true, false);
            runningRight = false;
        } else if(flipAnm==false&& region.isFlipX()){
            region.flip(true, false);
            runningRight = true;
        }
        if (moveLeft && (b2dBodyComponent.getB2body().getLinearVelocity().x < 0 || !runningRight)) {
            runningRight = false;
        } else if ((b2dBodyComponent.getB2body().getLinearVelocity().x > 0 || runningRight)) {
            runningRight = true;
        }
        if (b2dBodyComponent.getB2body().getLinearVelocity().y < 0 && runningUp) {
            runningUp = false;
        } else if (b2dBodyComponent.getB2body().getLinearVelocity().y > 0 && !runningUp) {
            runningUp = true;
        }

        stateTimer = currentState == previousState ? stateTimer + dt : 0;
        //update previous state
        previousState = currentState;
        //return our final adjusted frame
        return region;

    }

    private State getState() {
        //Test to Box2D for velocity on the X and Y-Axis
        //if mario is going positive in Y-Axis he is jumping... or if he just jumped and is falling remain in jump state
        if (playerIsDead)
            return State.DEAD;
        else if (runGrowAnimation)
            return State.GROWING;
        else if ((b2dBodyComponent.getB2body().getLinearVelocity().y > 0 && currentState == State.JUMPING) || (b2dBodyComponent.getB2body().getLinearVelocity().y < 0 && previousState == State.JUMPING))
            return State.JUMPING;
            //if negative in Y-Axis mario is falling
//        else if(b2body.getLinearVelocity().y < 0)
//            return State.FALLING;
            //if mario is positive or negative in the X axis he is running
        else if (moveLeft||moveUp||moveRight||moveDown)
            return State.RUNNING;
            //if none of these return then he must be standing
        else
            return State.STANDING;
    }

    private void handlePhysicMovement(float dt) {
        if(getCurrentState()==State.STANDING&&getB2body().getType()== BodyDef.BodyType.DynamicBody)getB2body().setType(BodyDef.BodyType.StaticBody);
        if (runningRight == false && moveLeft == false && b2dBodyComponent.getB2body().getLinearVelocity().x < 0 || runningRight && moveRight == false && b2dBodyComponent.getB2body().getLinearVelocity().x > 0) {
            b2dBodyComponent.getB2body().setLinearVelocity(b2dBodyComponent.getB2body().getLinearVelocity().x + (runningRight ? -speedMoveX : speedMoveX), b2dBodyComponent.getB2body().getLinearVelocity().y);
            if (runningRight && b2dBodyComponent.getB2body().getLinearVelocity().x < 0 || !runningRight &&b2dBodyComponent.getB2body().getLinearVelocity().x > 0)
                b2dBodyComponent.getB2body().setLinearVelocity(0, b2dBodyComponent.getB2body().getLinearVelocity().y);
        }
        if (runningUp == false && moveDown == false && b2dBodyComponent.getB2body().getLinearVelocity().y < 0 || runningUp && moveUp == false && b2dBodyComponent.getB2body().getLinearVelocity().y > 0) {
            b2dBodyComponent.getB2body().setLinearVelocity(b2dBodyComponent.getB2body().getLinearVelocity().x, b2dBodyComponent.getB2body().getLinearVelocity().y + (runningUp ? -speedMoveY : speedMoveY));
            if (runningUp && b2dBodyComponent.getB2body().getLinearVelocity().y < 0 || !runningUp && b2dBodyComponent.getB2body().getLinearVelocity().y > 0)
                b2dBodyComponent.getB2body().setLinearVelocity(b2dBodyComponent.getB2body().getLinearVelocity().x, 0);

        }
    }

    public void setRotBody(float degrees) {
        myRotate=degrees;
        //this.setRotation(degrees);
    }

    public void TakeMainWeapon(Weapon weapon) {
        if(getMyMainWeapon()!=null)getMyMainWeapon().HidingBag();
        setMyMainWeapon(weapon);
    }
    public void TakeExtraWeapon(Weapon weapon) {
        if(getMyExtraWeapon()!=null)getMyExtraWeapon().HidingBag();
        setMyExtraWeapon(weapon);
    }
    private void StartFireMainWP() {
        if (getMyMainWeapon() != null)getMyMainWeapon().setStartShoot(isFireMainWP());
    }
    private void StartFireExtraWP() {
        if (getMyExtraWeapon() != null) getMyExtraWeapon().setStartShoot(isFireExtraWP());
    }
    public Vector2 getPoinToWorld(Vector2 localPoin) {
        Vector2 poinToWorld = new Vector2(localToParentCoordinates(localPoin));
        poinToWorld.x=poinToWorld.x/MyGame.PPM;
        poinToWorld.y=poinToWorld.y/MyGame.PPM;

        return poinToWorld;
    }

    public boolean isMoveLeft() {
        return moveLeft;
    }

    public void setMoveLeft(boolean moveLeft) {
        this.moveLeft = moveLeft;
    }

    public boolean isMoveRight() {
        return moveRight;
    }

    public void setMoveRight(boolean moveRight) {
        this.moveRight = moveRight;
    }

    public boolean isMoveDown() {
        return moveDown;
    }

    public void setMoveDown(boolean moveDown) {

        this.moveDown = moveDown;
    }

    public boolean isMoveUp() {
        return moveUp;
    }

    public void setMoveUp(boolean moveup) {
        this.moveUp = moveup;
    }



    public Body getB2body() {
        return b2dBodyComponent.getB2body();
    }

    public void setB2body(Body b2body) {
        b2dBodyComponent.setB2body( b2body);
    }

    public Fixture getFixture(){
        return b2dBodyComponent.getFixture();
    }
    public void setFixture(Fixture fixture){
        b2dBodyComponent.setFixture(fixture);
    }
    public float getSpeedMoveX() {
        return speedMoveX;
    }

    public void setSpeedMoveX(float speedMoveX) {
        this.speedMoveX = speedMoveX;
    }

    public float getSpeedMoveY() {
        return speedMoveY;
    }

    public void setSpeedMoveY(float speedMoveY) {
        this.speedMoveY = speedMoveY;
    }

    public float getLimitspeedMoveX() {
        return limitspeedMoveX;
    }

    public void setLimitspeedMoveX(float limitspeedMoveX) {
        this.limitspeedMoveX = limitspeedMoveX;
    }
    public Vector2 getCenterPoin(){
        Vector2 center=new Vector2();
        center.x=this.getX()+this.getWidth()/2;
        center.y=this.getY()+this.getHeight()/2;
        return center;
    }
    public Vector2 getRandomNodeFCenter(Vector2 target){
        float targetDegree=rotateToPoint(getCenterPoin(),target);
        Vector2 center;
        center=calculateOrbit(targetDegree, new Vector2(this.getWidth(), this.getWidth()), new Vector2(getCenterPoin().x, getCenterPoin().y));
        return center;
    }
    public Vector2 getDirectionPoin(){
        Vector2 center;
        center=calculateOrbit(myRotate, new Vector2(this.getWidth(), this.getWidth()), new Vector2(getCenterPoin().x, getCenterPoin().y));
        return center;
    }
    public float  rotateToPoint(Vector2 startpoin,Vector2 targetpoin){
        float degrees=0;
        Vector2 targetPoint=targetpoin;
        Vector2 startPoint=startpoin;
        float angle = MathUtils.atan2(targetPoint.y - startPoint.y, targetPoint.x -startPoint.x);
        degrees= MathUtils.radiansToDegrees*angle;
        if (degrees< 0) degrees += 360;
        else if (degrees > 360) degrees -= 360;

        return degrees;
    }
    @Override
    public void Collision2DEnter(TypeComponent typeComponent, GameObject gameObjectColl) {
        super.Collision2DEnter(typeComponent, gameObjectColl);
    }
    public Weapon getMyMainWeapon() {
        return myMainWeapon;
    }

    public void setMyMainWeapon(Weapon myMainWeapon) {
        this.myMainWeapon = myMainWeapon;
    }

    public Weapon getMyExtraWeapon() {
        return myExtraWeapon;
    }

    public void setMyExtraWeapon(Weapon myExtraWeapon) {
        this.myExtraWeapon = myExtraWeapon;
    }
    public boolean isFireMainWP() {
        return FireMainWP;
    }

    public void setFireMainWP(boolean fireMainWP) {
        FireMainWP = fireMainWP;
        getMyMainWeapon().setStartShoot(isFireMainWP());
    }

    public boolean isFireExtraWP() {
        return FireExtraWP;
    }

    public void setFireExtraWP(boolean fireExtraMainWP) {
        FireExtraWP= fireExtraMainWP;

    }
    public Vector2 calculateOrbit(float currentOrbitDegrees, Vector2 distanceFromCenterPoint, Vector2 centerPoint) {
        float radians = MathUtils.degreesToRadians*currentOrbitDegrees;
        float x = (MathUtils.cos(radians) * distanceFromCenterPoint.x) + centerPoint.x;
        float y = (MathUtils.sin(radians) * distanceFromCenterPoint.y) + centerPoint.y;
        return new Vector2(x, y);
    }
    private int getdegreeAnim(){
//        if(myDegree>270&&myDegree<290){
//            firepoin=new Vector2(-0.0f,0);
//        }else
        float myDegree=myRotate;
        float calculate=0;
        flipAnm=false;
        flareGunPoin=new Vector2(-0.1f,0);
        firepoin=new Vector2(0.0f,0);
        if (myDegree > 250 && myDegree < 270) {
            firepoin = new Vector2(0.0f, 0);
        }else{
            firepoin = new Vector2(0, -0.4f);
        }

//        if(myDegree>315&&myDegree<=360){
//            flareGunPoin=new Vector2(-0.1f,0);
//        }else if(myDegree>270&&myDegree<315) {
//            flareGunPoin=new Vector2(0,0.25f);
//        } else if(myDegree>225&&myDegree<270) {
//            flareGunPoin=new Vector2(0,0.0f);
//        }
        if(myDegree>270&&myDegree<=360){
            calculate=-((myDegree-270)-90);
        }
        if(myDegree>180&&myDegree<=270){
            flipAnm=true;
            calculate=-((myDegree-180));
        }
        if(myDegree>0&&myDegree<=90){
            calculate=90+((myDegree/90f)*90);
        }
        if(myDegree>90&&myDegree<=180){
            flipAnm=true;
            calculate=-(270-myDegree);
        }
        int degreeAnm=(int)calculate;

        return degreeAnm;
    }
    public float getMyRotate() {
        return myRotate;
    }

    public void setMyRotate(float myRotate) {
        this.myRotate = myRotate;
    }
    public boolean isRunningRight() {
        return runningRight;
    }

    public void setRunningRight(boolean runningRight) {
        this.runningRight = runningRight;
    }
    public Vector2 getFirepoin() {
        return firepoin;
    }

    public void setFirepoin(Vector2 firepoin) {
        this.firepoin = firepoin;
    }
    public NodeMap getPlayerNode(){
        NodeMap nodeMap=null;
        if(myWorld.getGridMap()!=null){
           nodeMap= myWorld.getGridMap().convertRC(getCenterPoin().x,getCenterPoin().y);
        }
        return nodeMap;
    }

    public int getIndexPlayer(){
        return this.getZIndex();
    }
    public Vector2 getFlareGunPoin() {
        return flareGunPoin;
    }

    public void setFlareGunPoin(Vector2 flareGunPoin) {
        this.flareGunPoin = flareGunPoin;
    }
    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }
}
