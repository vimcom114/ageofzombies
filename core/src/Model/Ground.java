package Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.geda.game.MyGame;

import Controller.MyWorld;

public class Ground extends InteractiveTileObject {

    boolean Done=false;
    int Type=0;
    public void setStartSpecial(boolean startSpecial) {
        this.startSpecial = startSpecial;
    }
    boolean startSpecial=false;
    public Ground(MyWorld myWorld, MapObject object, int type, String data) {
        super(myWorld, object, 0);
        if(data!=null){
            boolean through=Boolean.parseBoolean(data);
            //setThrough(through);
        }
        if (type > 0) {
            Type = type;
        }
        defineBody();
        setSize(getWidthText(),getHeightText());
        setPosition(getB2body().getPosition().x - getWidth()/2 , getB2body().getPosition().y - getHeight() / 2);
        respawnPos=new Vector2(getB2body().getPosition().x,getB2body().getPosition().y);
        setNodeBlock();

    }



    @Override
    public void Update(float dt) {
        if(destroyBody&&getB2body()!=null){

            myWorld.getWorld().destroyBody(getB2body());
           setB2body(null);
            destroyBody=false;
        }
        if(getB2body()==null) return;

        if(startSpecial){
            setScale(0);
            getB2body().setActive(false);
            startSpecial=false;
        }
        getB2body().setTransform(respawnPos.x,respawnPos.y,0);


    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(startSpecial)return;
        super.draw(batch, parentAlpha);
    }

    @Override
    public void defineBody() {
        System.out.println("Decimal 10:"+Short.decode(String.valueOf(MyGame.PLAYER_BIT))+" "+Short.decode(String.valueOf(MyGame.GROUND_BIT)));
        System.out.println("Octal 10:"+Short.decode(String.valueOf(MyGame.PLAYER_BIT))+" "+Short.decode(String.valueOf(MyGame.GROUND_BIT)));
        System.out.println("Hex F:"+Short.decode(String.valueOf(MyGame.PLAYER_BIT))+" "+Short.decode(String.valueOf(MyGame.GROUND_BIT)));
        System.out.println("Negative Hex F:"+Short.decode(String.valueOf(MyGame.PLAYER_BIT))+" "+Short.decode(String.valueOf(MyGame.GROUND_BIT)));
        setCategoryFilter(MyGame.GROUND_BIT);
        getFixture().setUserData(this);

    }

    @Override
    public void Destroy() {
        destroyBody=true;
    }
    public void Special(){
        startSpecial=true;
        setScale(0);
       getB2body().setActive(false);
    }

    @Override
    public void ReUse() {
        setScale(1);
        getB2body().setActive(true);
        Done=false;
    }
}

