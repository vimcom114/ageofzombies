package Model;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Align;
import com.geda.game.MyGame;

import Controller.GameController;
import Controller.MyWorld;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import Tools.B2dBodyComponent;
import Tools.CollisionComponent;
import Tools.GameObject;
import Tools.TypeComponent;

public class Zombies extends Enemy {
    int countWTexture=0;
    int countHTexture=0;
    float SpeedMove=1f;
    String myData="";
    public Zombies(MyWorld myWorld, float x, float y, String Data, float speedMove) {
        super(myWorld, x, y);
        myData=Data;
        String[] splitstr=myData.split(":");
        nameTexture=splitstr[0];
        getStats().setMaxHealth(Integer.parseInt(splitstr[1]));
        countWTexture=Integer.parseInt(splitstr[2]);
        countHTexture=Integer.parseInt(splitstr[3]);
        SpeedMove=speedMove;
        isUseOneFrame=true;
        creatAnimation("Zom1");
        defineEnemy();
        getStats().Init();
        this.setOrigin(Align.center);

        setZIndex(player.getIndexPlayer()-1);


    }
    @Override
    public void MyUpdate(float dt) {
        Vector2 positionDelta =player.getCenterPoin().sub(getCenterPoin());
       if(targetPath!=null)positionDelta = (new Vector2(targetPath).sub(getCenterPoin()));
       positionDelta=positionDelta.nor();
        velocity.x = positionDelta.x*2;
        velocity.y= positionDelta.y*2;
        getB2body().setLinearVelocity(velocity);
        setPosition(getB2body().getPosition().x-this.getWidth()/2,getB2body().getPosition().y-this.getHeight()/2);
        setMyRotate(rotateToPoint(nowPath,player.getCenterPoin()));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        ShapeRenderer sr = new ShapeRenderer();
//        sr.setColor(Color.BLACK);
//        sr.setProjectionMatrix(myWorld.getScreenGame().getGamecam().combined);
//        sr.begin(ShapeRenderer.ShapeType.Filled);
//        sr.rectLine(nowPath.x, nowPath.y, targetPath.x, targetPath.y,5/MyGame.PPM);
//        sr.end();
    }

    @Override
    protected void defineEnemy() {
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(), getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        setB2body(world.createBody(bdef));
        getB2body().setGravityScale(0);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius((this.getWidth()*0.2f ));

        fdef.filter.categoryBits = MyGame.ENEMY_BIT;
        fdef.filter.maskBits = MyGame.PLAYER_BIT|MyGame.GROUND_BIT|MyGame.ENEMY_BIT ;
        fdef.shape = shape;
        fdef.friction=1f;
        setFixture(getB2body().createFixture(fdef));
        getB2body() .setUserData(entity);
        //Create the Head here:

    }

    @Override
    public void hitByPlayer(Player mario) {

    }

    @Override
    public void hitByEnemy(Enemy enemy) {

    }

    @Override
    public void hitByBullet(int dame) {
       getStats().setCurHealth(dame);

       if(getStats().getCurHealth()<=0)Destroy();

    }

    @Override
    public void hitByObject(InteractiveTileObject tileObject) {

    }

    @Override
    public void ChangeDir() {

    }

    @Override
    public void ReUse() {

    }

    @Override
    public void Stand(InteractiveTileObject standObject) {

    }

    @Override
    public void Destroy() {
        setToDestroy=true;
    }
//    public void defineTexture(){
//        if(isUseOneFrame){
//            mySprite= Loader.GetTexture(nameTexture);
//            frames = UI.NewImage(Loader.GetTexture(nameTexture),this);
//            frames.setSize(80/ MyGame.PPM,80/MyGame.PPM);
//            setSize(frames.getWidth(),frames.getHeight());
//            frames.setPosition(0,0);
//            frames.setOrigin(Align.center);
//        }
//    }
    @Override
    public void Collision2DEnter(TypeComponent typeComponent, GameObject gameObjectColl) {
        super.Collision2DEnter(typeComponent, gameObjectColl);
        if(typeComponent.type==1)GDX.Show("CollEnemy :"+getTypeComponent().type);
        if(typeComponent.type==TypeComponent.GERNADE_PLAYER)hitByBullet(-gameObjectColl.getMyBullet().getDame());

    }


}
