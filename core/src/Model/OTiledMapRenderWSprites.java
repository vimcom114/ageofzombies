package Model;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.geda.game.MyGame;


import java.util.ArrayList;

public class OTiledMapRenderWSprites extends OrthogonalTiledMapRenderer {

    public OTiledMapRenderWSprites(TiledMap map) {
        super(map);
    }

    public void setGameCam(OrthographicCamera gameCam) {
        this.gameCam = gameCam;
    }

    OrthographicCamera gameCam;



     ArrayList<BehindObject>listBehindMap=new ArrayList<>();

    @Override
    public void renderObject(MapObject object) {
//        for(int i=0;i<listBehindMap.size();i++){
//            // batch.draw(listBehindMap.get(i).getTextureMapObject().getTextureRegion(), listBehindMap.get(i).getPos().x,listBehindMap.get(i).getPos().y,(listBehindMap.get(i).w)/MyGame.PPM,(listBehindMap.get(i).h)/MyGame.PPM);
//            listBehindMap.get(i).drawBehind(batch);
//        }
        if(object instanceof TextureMapObject) {
            TextureMapObject textureObj = (TextureMapObject) object;
            Vector2 wh=getWH(textureObj);
            float Z=getZ(textureObj);
            float scaleX=getScaleX(textureObj);

            batch.draw(textureObj.getTextureRegion(), textureObj.getX(), textureObj.getY(),((wh.x)/ MyGame.PPM)/2,((wh.y)/MyGame.PPM)/2,(wh.x)/MyGame.PPM,(wh.y)/MyGame.PPM,scaleX,1,Z);
        }
        super.renderObject(object);
    }

    public OTiledMapRenderWSprites(TiledMap map, Batch batch) {
        super(map, batch);
    }

    public OTiledMapRenderWSprites(TiledMap map, float unitScale) {
        super(map, unitScale);
    }

    public OTiledMapRenderWSprites(TiledMap map, float unitScale, Batch batch) {
        super(map, unitScale, batch);
    }
    public void addListBehindMap(BehindObject behindObject){
        if(listBehindMap.contains(behindObject)==false)listBehindMap.add(behindObject);
    }
    public void remListBehindMap(BehindObject behindObject){
        if(listBehindMap.contains(behindObject))listBehindMap.remove(behindObject);
    }

    Vector2 getWH(TextureMapObject textureMapObject){
        Vector2 Wh=new Vector2(80,80);
        for (int i=0;i<listBehindMap.size();i++){
            boolean check=listBehindMap.get(i).checkContant(textureMapObject);
            if(check){
                Wh=new Vector2(listBehindMap.get(i).getW(),listBehindMap.get(i).getH());
            }
        }
        return Wh;
    }

    float getZ(TextureMapObject textureMapObject){
        float Z=0;
        for (int i=0;i<listBehindMap.size();i++){
            boolean check=listBehindMap.get(i).checkContant(textureMapObject);
            if(check){
                Z=listBehindMap.get(i).getZ();
            }
        }
        return Z;
    }
    float getScaleX(TextureMapObject textureMapObject){
        float scaleX=0;
        for (int i=0;i<listBehindMap.size();i++){
            boolean check=listBehindMap.get(i).checkContant(textureMapObject);
            if(check){
                scaleX=listBehindMap.get(i).getScaleX();
            }
        }
        return scaleX;
    }
}
