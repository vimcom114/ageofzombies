package Model;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import Controller.MyWorld;

public abstract class Weapon extends Group {
    private boolean isUse=false,Hiding=false;
    private WeaponType myWeaponType;
    private Controller.MyWorld myWorld;
    public Player myPlayer;

    private Image myFrame;
    public Vector2 firePoin;
    public boolean StartShoot;
    public float timetoShoot,tiemtoReload;

    private int countBullet;

    public Weapon(Controller.MyWorld myworld, WeaponType weaponType){
         myWorld=myworld;
         myWeaponType=weaponType;
         countBullet=myWeaponType.getMagCount();
         firePoin=new Vector2(this.getX(),this.getY());
    }
    public abstract void Update(float dt);
    @Override
    public void act(float delta) {
        super.act(delta);
        Update(delta);
    }
    public void checkIsUse(Player player){

    }
    public void setStartShoot(boolean shoot){

    }
    public void HidingBag(){
        setStartShoot(false);
        setVisible(false);
    }
    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean use) {
        isUse = use;
    }


    public Image getMyFrame() {
        return myFrame;
    }

    public void setMyFrame(Image myFrame) {
        this.myFrame = myFrame;
    }
    public WeaponType getMyWeaponType() {
        return myWeaponType;
    }

    public void setMyWeaponType(WeaponType myWeaponType) {
        this.myWeaponType = myWeaponType;
    }

    public MyWorld getMyWorld() {
        return myWorld;
    }

    public void setMyWorld(MyWorld myWorld) {
        this.myWorld = myWorld;
    }
    public boolean checkIsDefaultWP(){
        boolean isDefaultWP=false;
        isDefaultWP=getMyWeaponType().getCode().equals("1_1");
        return isDefaultWP;
    }
    public String getCountBullet() {
        String strCountBullet="";
        if(checkIsDefaultWP()==false)strCountBullet=""+countBullet;
        return strCountBullet;
    }

    public void setCountBullet(int countBullet) {
        this.countBullet = countBullet;
    }
    public boolean minusBullet(int minusCount){
        boolean checkFinish=false;
        countBullet-=minusCount;
        if(countBullet<=0){
            countBullet=0;
            checkFinish=true;
        }
        return checkFinish;
    }

    public boolean checkFinish(){
        boolean finish=false;
        if(checkIsDefaultWP()==false){
            HidingBag();
            getMyWorld().GivePlayerWeaponCode("1_1");
        }
        return finish;
    }
    public Vector2 calculateOrbit(float currentOrbitDegrees, Vector2 distanceFromCenterPoint, Vector2 centerPoint) {
        float radians = MathUtils.degreesToRadians*currentOrbitDegrees;
        float x = (MathUtils.cos(radians) * distanceFromCenterPoint.x) + centerPoint.x;
        float y = (MathUtils.sin(radians) * distanceFromCenterPoint.y) + centerPoint.y;
        return new Vector2(x, y);
    }
}
