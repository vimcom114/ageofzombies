package Model;

import com.badlogic.gdx.math.Vector2;

import GameGDX.GDX;

public class ItemType {

    private int Type=0;
    private String Code="";
    private String DataTextureItem="";
    private String DataTexturePanel="";
    private String DataParticle ="";
    public ItemType(int type, String code, String dataTextureItem, String dataTexturePanel, String dataParticle) {
        Type = type;
        Code = code;
        DataTextureItem = dataTextureItem;
        DataTexturePanel = dataTexturePanel;
        DataParticle = dataParticle;
    }
    public int getType() {
        return Type;
    }

    public String getCode() {
        return Code;
    }

    public String getDataTextureItem() {
        return DataTextureItem;
    }

    public String getDataTexturePanel() {
        return DataTexturePanel;
    }

    public String getDataParticle() {
        return DataParticle;
    }

}
