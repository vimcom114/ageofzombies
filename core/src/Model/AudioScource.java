package Model;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import Controller.Game.AudioManager;
import GameGDX.GDX;
import GameGDX.Loader;
import ScreenGame.StartMenu;

public class AudioScource {

    public String Name;
    private Music music;
    private Sound sound;
    int What = 0;
    private float Volume = 0.7f;

    private float pitch = 1f;

    private float randomVolume = 0.1f;

    private float randomPitch = 1f;

    private boolean loop = false;
    private boolean mute = false;
    private boolean isCheckDistance;
    private int caseCheckDistance;
    private float maxDistance = 0;
    private long idSound=0;

    public AudioScource(String name, int what) {
        String Url = "";
        What = what;
        Name = name;
        try {
            if (what == 1) {
                Url = Loader.instance.geturlMusic(name);
                music = Loader.getMusic(Url);
                music.setLooping(true);
            } else {
                Url = Loader.instance.geturlSound(name);
                sound = Loader.getSound(Url);
            }
        } catch (Exception e) {
            GDX.Show("loiAudio :" + e);
        }
    }


    public void Play() {
        if (music != null) {
            if (!music.isPlaying()) {
                int valueMusic = AudioManager.instance.getValueMusic();
                int valueSound = AudioManager.instance.getValueSound();
                if (What == 1 && valueMusic != 1) return;
                if (What != 1 && valueSound != 1) return;
                music.play();
            }
        }
        if (sound != null) {
            int valueMusic = AudioManager.instance.getValueMusic();
            int valueSound = AudioManager.instance.getValueSound();
            if (What == 1 && valueMusic != 1) return;
            if (What != 1 && valueSound != 1) return;
            idSound = sound.play();

        }
    }


    public void Stop() {
        if (music != null) music.stop();
        if (sound != null) sound.stop();

    }
    public void Loop(boolean loop){
        int valueMusic = AudioManager.instance.getValueMusic();
        int valueSound = AudioManager.instance.getValueSound();
        if (What == 1 && valueMusic != 1) return;
        if (What != 1 && valueSound != 1) return;
        if (music != null) music.setLooping(loop);
        if(sound!=null) sound.loop();
    }

    public void setVolume(float volume) {
        Volume = volume;
        if (music != null) music.setVolume(volume);
        if (sound != null) sound.setVolume(idSound, volume);
        if (Name.equals("Menu") && music.getVolume() == 1 && StartMenu.instance != null && StartMenu.instance.isVisible()) Play();
    }

    public void setLoop(boolean loop) {
        int valueMusic = AudioManager.instance.getValueMusic();
        int valueSound = AudioManager.instance.getValueSound();
        if (What == 1 && valueMusic != 1) return;
        if (What != 1 && valueSound != 1) return;
        if (music != null) music.setLooping(loop);
        if(sound!=null) sound.setLooping(idSound,loop);
    }

    public void setDistance(boolean checkDistance, float maxdistance, int caseCheck) {
        maxDistance = maxdistance;
        caseCheckDistance = caseCheck;
        if (checkDistance == false && isCheckDistance) {
            if (music != null) {
                music.stop();
                music.setVolume(0);
            }
            if (sound != null) {
                sound.stop();
                sound.setVolume(idSound, 0);
            }
        }
        isCheckDistance = checkDistance;
    }

    public void Update(float distance) {
        if (isCheckDistance) {
            if (caseCheckDistance == 1) {
                if (distance <= maxDistance) {
                    float volume = (float) (1f - (distance / maxDistance));
                    setVolume(volume);
                } else {
                    setVolume(0);
                }
            } else if (caseCheckDistance == 2) {
                if (distance <= maxDistance) {
                    setVolume(1);
                    if (music != null) music.play();
                    if (sound != null) sound.play();
                    isCheckDistance = false;
                }
            }
        }
    }



}
