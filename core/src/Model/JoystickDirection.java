package Model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import Controller.GameController;
import GameGDX.AnalogStick;
import GameGDX.ClickEvent;
import GameGDX.GDX;
import GameGDX.Loader;
import GameGDX.UI;
import ScreenGame.GamePlayUI;

public class JoystickDirection extends Group {
    Image imageICon;
    AnalogStick analogStick;
    Label lbCount;
    String strTexureIcon="";
    public JoystickDirection(Group parent, Vector2 pos, String txtIcon, String txtTouchBG, String txtTouchKnob){
        parent.addActor(this);
        imageICon = UI.NewImage(Loader.GetTexture(txtIcon),0,0, Align.bottomLeft,this);
        this.setOrigin(Align.center);
        analogStick=new AnalogStick(0,0,parent.getWidth()*0.15f,parent.getWidth()*0.15f,txtTouchBG,txtTouchKnob);
        this.addActor(analogStick);
        this.setSize(parent.getWidth()*0.15f,parent.getWidth()*0.15f);
        this.setPosition(pos.x,pos.y,Align.center);
        lbCount=UI.NewLabel("0",Color.WHITE,0.6f,this.getWidth()/2,this.getHeight()/2,Align.center,this);
        analogStick.setPosition(this.getWidth()/2,this.getHeight()/2,Align.center);
        analogStick.setVisible(false);
        imageICon.setPosition(this.getWidth()/2,this.getHeight()/2,Align.center);
        imageICon.addListener(new ClickEvent() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                analogStick.setVisible(true);
                analogStick.touchDownJoy(MovementJoyTickOutSide(x,y).x,MovementJoyTickOutSide(x,y).y,true);
                return super.touchDown(event, x, y, pointer, button);
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                analogStick.setVisible(false);
                analogStick.touchUpJoy(MovementJoyTickOutSide(x,y).x,MovementJoyTickOutSide(x,y).y,true);
                GameController.instance.getMyWorld().HandleInPut("ENTER", true);
            }
            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                analogStick.touchDragJoy(MovementJoyTickOutSide(x,y).x,MovementJoyTickOutSide(x,y).y,true);
            }
        });
    }
    public void UpdateAttackJoyTick(float delta){
        boolean Touch=analogStick.isTouched();
        if(Touch){
            analogStick.setTouch(true);
            GameController.instance.getMyWorld().getPlayer().setRotBody(analogStick.getAngleDegree());
        }else{
            boolean isTouch=analogStick.isTouch();
            if(isTouch) {
                analogStick.setTouched(false);
                analogStick.setTouch(false);
                analogStick.setPrevPercenX(0.0f);
                analogStick.setPrevPercenY(0.0f);

            }
        }
    }
    public Vector2 MovementJoyTickOutSide(float x, float y){
        x=x+imageICon.getX();
        y=y+imageICon.getY();
        Vector2 percentXY=calculateJoyTickPercent(x,y);
        return percentXY;
    }
    private Vector2 calculateJoyTickPercent(float x, float y){
        Vector2 percentXY;
        percentXY=new Vector2(x,y);
        // float pX=(x-this.getWidth()*0.25f)/(this.getWidth()*0.25f);
//        float pX=x/(this.getWidth());
//       // pX= MathUtils.clamp(pX, 0, 1);
//        // float pY=(y-this.getHeight()*0.4f)/(this.getHeight()*0.4f);
//        float pY=y/(this.getHeight());
//        //pY= MathUtils.clamp(pY, 0, 1);
//        percentXY=new Vector2(pX,pY);
        return percentXY;
    }
    public void Show(String Count,String strtxt){

        if(strTexureIcon.equals(""))strTexureIcon=strtxt;
        if(strTexureIcon.equals("")==false&&strTexureIcon.equals(strtxt)==false){
            strTexureIcon=strtxt;
            setIcon(strTexureIcon);
        }
        setCount(Count);
        this.setVisible(true);
    }
    public void setCount(String Count){
        lbCount.setText(""+Count);
    }
    public void Hide(){
        this.setVisible(false);
    }
    public void setIcon(String strTexture){
        imageICon.setDrawable(new TextureRegionDrawable(Loader.GetTexture(strTexture)));
    }

}
