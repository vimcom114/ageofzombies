package com.geda.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap;
import com.geda.game.MyGame;

import zen.IZen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width=1280;
		config.height=720;
		config.x=100;
		config.y=50;
		new LwjglApplication(new MyGame(new IZen() {
			@Override
			public void log(String str) {

			}

			@Override
			public void LoadUrlTexture(String url, UrlTextureCallback callback) {

			}

			@Override
			public Pixmap getFrameBufferPixmap(int x, int y, int w, int h) {
				return null;
			}

			@Override
			public String getBase64URL(Pixmap pixmap) {
				return null;
			}

			@Override
			public String GetNumberFormat(long number) {
				return null;
			}

			@Override
			public String GetNumberDotFormat(long number) {
				return null;
			}

			@Override
			public void FBInstant_GetJsonStats(FBInstant_GetJsonStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetJsonStats(String value, FBInstant_SetJsonStatsCallback callback) {

			}

			@Override
			public void FBInstant_GetStringData(String statname, FBInstant_GetStringStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetStringData(String statname, String value, FBInstant_SetStringStatsCallback callback) {

			}

			@Override
			public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {

			}

			@Override
			public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {

			}

			@Override
			public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_LoadMyLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_LoadLeaderboardFriend(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {

			}

			@Override
			public String FBInstant_GetPlayerName() {
				return null;
			}

			@Override
			public String FBInstant_GetPlayerPhoto() {
				return null;
			}

			@Override
			public String FBInstant_GetPlayerID() {
				return null;
			}

			@Override
			public boolean FBInstant_IsPreloadLoadingFinished(int num) {
				return false;
			}

			@Override
			public void FBInstant_StartLoadingPreload(int num) {

			}

			@Override
			public void FBInstant_ReportScore(double score) {

			}

			@Override
			public void OnShow() {

			}

			@Override
			public void FBInstant_CanCreateShortCut(FBInstant_CanCreateCallback callback) {

			}

			@Override
			public void FBInstant_CreateShortCut() {

			}

			@Override
			public void FBInstant_CanSubscribeBot() {

			}

			@Override
			public String FBInstant_GetDataContext() {
				return null;
			}

			@Override
			public void FBInstant_ShareAsync(Pixmap pixmap, FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_InviteAsync(FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_UpdateAsync(String data, FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_ChooseAsync(FBInstant_ContextCallback callback) {

			}

			@Override
			public void FBInstant_SwitchGame() {

			}

			@Override
			public String FBInstant_GetLinkSwitchGame() {
				return null;
			}

			@Override
			public void FBInstant_Quit() {

			}

			@Override
			public String FBInstant_Getlocale() {
				return null;
			}

			@Override
			public String FBInstant_GetPlatform() {
				return null;
			}

			@Override
			public long getCurrentTimeMillis(int hour, int minute, int second) {
				return 0;
			}

			@Override
			public Pixmap getPixmapFromByte(byte[] bytes) {
				return null;
			}

			@Override
			public void LinkOtherGame(String packageName) {

			}

			@Override
			public void Rate() {

			}

			@Override
			public void Like() {

			}

			@Override
			public void ReportScore(String leaderboardID, long score) {

			}

			@Override
			public void ShowFullscreen() {

			}

			@Override
			public void ShowBanner(boolean visible) {

			}

			@Override
			public void TrackLevelStart(int level) {

			}

			@Override
			public void TrackLevelFailed(int level) {

			}

			@Override
			public void TrackLevelCompleted(int level) {

			}

			@Override
			public void TrackLevelRevive(int level) {

			}

			@Override
			public void TrackCustomEvent(String event) {

			}

			@Override
			public double GetConfigValue(String name, double defaultValue) {
				return 0;
			}

			@Override
			public String GetConfigString(String name, String defaultValue) {
				return null;
			}

			@Override
			public int GetConfigIntValue(String name, int defaultValue) {
				return 0;
			}

			@Override
			public void ShowLeaderBoard() {

			}

			@Override
			public boolean IsVideoRewardReady() {
				return true;
			}

			@Override
			public void ShowVideoReward(OnVideoRewardClosed callback) {
				callback.OnEvent(true);
			}

			@Override
			public void NonFatalCrash(Throwable throwable) {

			}

			@Override
			public void CrashKey(String key, String value) {

			}
		}), config);
	}
}
